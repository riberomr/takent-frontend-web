export const paths = {
    public: {
      login: '/login',
      singIn: {
        user: '/singIn/user',
        company: '/singIn/company'
      },
      logout: '/logout',
      recovery: '/recovery',
      takent: '/takent',
      support: '/support',
      opinions: '/opinions',
      activateUser: '/userActivated',
      recoverUser: '/userRecover',
      termsAndConditions: '/termsAndConditions'
    },
    protected: {
      home: '/app',
      profile: '/my-profile',
      requiredMail: '/required-mail',
      company: {
        profile: '/company/profile',
        data: '/company/data',
        members: 'company/members'
      }
  }
}