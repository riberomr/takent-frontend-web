import React from 'react'
import PersonIcon from '@material-ui/icons/Person'
import DescriptionIcon from '@material-ui/icons/Description'
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter'
import EqualizerIcon from '@material-ui/icons/Equalizer'

import PAGE_HOME from '../../components/pages/Home'
import PAGE_ENTERPRISE_USER_PROFILE from '../../components/pages/EnterpriseUser/Profile'
import PAGE_COMPANY_NEW_OFFER from '../../components/pages/Company/NewOffer'
import PAGE_COMPANY_OFFER_LIST from '../../components/pages/Company/OfferList'
import PAGE_COMPANY_DRAFT_OFFER_LIST from '../../components/pages/Company/DraftOfferList'
import PAGE_COMPANY_BUY_OFFER from '../../components/pages/Company/BuyOffer'
import PAGE_COMPANY_PROFILE from '../../components/pages/Company/Profile'

import PAGE_COMMON_USER_NEW_SERVICE from '../../components/pages/CommonUser/Services/NewService'
import PAGE_COMMON_USER_SERVICE_LIST from '../../components/pages/CommonUser/Services/ServiceList'
import PAGE_COMMON_USER_MY_SERVICES from '../../components/pages/CommonUser/Services/MyServices'
import PAGE_COMMON_USER_BUY_SERVICE_PREFERENCE from '../../components/pages/CommonUser/Services/BuyServicePreference'
import PAGE_COMMON_USER_PROFILE from '../../components/pages/CommonUser/Profile'
import PAGE_COMMON_USER_EDUCATION from '../../components/pages/CommonUser/Education'
import PAGE_COMMON_USER_EXPERIENCE from '../../components/pages/CommonUser/Experience'
import PAGE_COMMON_USER_OTHER_DATA from '../../components/pages/CommonUser/OtherData'
import PAGE_COMMON_USER_ACCOUNT from '../../components/pages/CommonUser/Account'
import PAGE_COMMON_USER_OFFER_LIST from '../../components/pages/CommonUser/Offers/OfferList'
import PAGE_ENTERPRISE_USER_ACCOUNT from '../../components/pages/Company/Account'
import PAGE_COMPANY_STATISTICS from '../../components/pages/Company/Statistics'
import PAGE_ALLOWED_USER_LIST from '../../components/pages/Company/AllowedUsersList'
import PAGE_COMMON_USER_MY_POSTULATIONS from '../../components/pages/CommonUser/Offers/MyPostulations'
import PAGE_COMMON_USER_NOTIFICATIONS_CONFIGURATION from '../../components/pages/CommonUser/NotificationsConfiguration'
import PAGE_ENTERPRISE_USER_NOTIFICATIONS_CONFIGURATION from '../../components/pages/Company/NotificationsConfiguration'
import PAGE_COMMON_USER_SERVICE_REQUESTS from '../../components/pages/CommonUser/Services/ServiceRequests'

export const navigationMenu = [{
//   icon: ,
  id: 'Inicio',
  dropdown: false,
  active: false,
  link: '/app',
  component: PAGE_HOME
},
{
  icon: <DescriptionIcon style={{color: '#F29222'}}/>,
  id: 'Ofertas laborales',
  dropdown: true,
  active: false,
  opened: false,
  user_type: 'common',
  children: [
  {
    id: 'Buscar ofertas',
    active: false,
    link: '/app/user/offer/list',
    component: PAGE_COMMON_USER_OFFER_LIST,
    user_type: 'common'
  },
  {
    id: 'Mis postulaciones',
    active: false,
    link: '/app/user/offer/applied',
    component: PAGE_COMMON_USER_MY_POSTULATIONS,
    user_type: 'common'
  }
]
},
{
  icon: <i className="fas fa-tools" style={{color: '#F29222'}}></i>,
  id: 'Servicios',
  dropdown: true,
  active: false,
  opened: false,
  user_type: 'common',
  children: [
    {
      id: 'Buscar servicios',
      active: false,
      link: '/app/user/service/list',
      component: PAGE_COMMON_USER_SERVICE_LIST,
      user_type: 'common'
    },
    {
      id: 'Publicar servicio',
      active: false,
      link: '/app/user/service/new',
      component: PAGE_COMMON_USER_NEW_SERVICE,
      user_type: 'common'
    },
    {
      id: 'Mis servicios',
      active: false,
      link: '/app/user/service/mine',
      component: PAGE_COMMON_USER_MY_SERVICES,
      user_type: 'common'
    },
    {
      id: "Servicios solicitados",
      active: false,
      link: '/app/user/service/requests',
      component: PAGE_COMMON_USER_SERVICE_REQUESTS,
      user_type: 'common'
    },
    {
      id: 'Pagar servicio preferencial',
      active: false,
      link: '/app/user/service/buy',
      component: PAGE_COMMON_USER_BUY_SERVICE_PREFERENCE,
      user_type: 'common'
    }
  ]
},
{
icon: <PersonIcon style={{color: '#F29222'}}/>,
id: 'Mi perfil',
dropdown: true,
active: false,
opened: false,
user_type: 'common',
children: [
  {
    id: 'Mis datos personales',
    active: false,
    link: '/app/user/data',
    component: PAGE_COMMON_USER_PROFILE,
    user_type: 'common'
  },
  {
    id: 'Mi experiencia laboral',
    active: false,
    link: '/app/user/experience',
    component: PAGE_COMMON_USER_EXPERIENCE,
    user_type: 'common',
  },
  {
    id: 'Mis datos académicos',
    active: false,
    link: '/app/user/education',
    component: PAGE_COMMON_USER_EDUCATION,
    user_type: 'common',
  },
  {
    id: 'Aptitudes e idiomas',
    active: false,
    link: '/app/user/moredata',
    component: PAGE_COMMON_USER_OTHER_DATA,
    user_type: 'common',
  },
  {
    id: 'Mi cuenta',
    active: false,
    link: '/app/user/account',
    component: PAGE_COMMON_USER_ACCOUNT,
    user_type: 'common',
  },
  {
    id: 'Notificaciones',
    active: false,
    link: '/app/user/notifications',
    component: PAGE_COMMON_USER_NOTIFICATIONS_CONFIGURATION,
    user_type: 'common'
  }
]
},
{
  icon: <BusinessCenterIcon style={{color: '#F29222'}}/>,
  id: 'Gestión de cuenta',
  dropdown: true,
  active: false,
  opened: false,
  user_type: 'enterprise',
  children: [{
    id: 'Empresa',
    active: false,
    link: '/app/company/profile',
    component: PAGE_COMPANY_PROFILE,
    user_type: 'enterprise',
  }, {
    id: 'Mis datos personales',
    active: false,
    link: '/app/company/data',
    component: PAGE_ENTERPRISE_USER_PROFILE,
    user_type: 'enterprise',
  },
  {
    id: 'Colaboradores',
    active: false,
    link: '/app/company/members/list',
    component: PAGE_ALLOWED_USER_LIST,
    user_type: 'enterprise',
  },
  {
    id: 'Mi cuenta',
    active: false,
    link: '/app/company/account',
    component: PAGE_ENTERPRISE_USER_ACCOUNT,
    user_type: 'enterprise',
  },
  {
    id: 'Notificaciones',
    active: false,
    link: '/app/company/notifications',
    component: PAGE_ENTERPRISE_USER_NOTIFICATIONS_CONFIGURATION,
    user_type: 'enterprise'
  }]
  },
  {
    icon: <DescriptionIcon style={{color: '#F29222'}}/>,
    id: 'Ofertas',
    dropdown: true,
    active: false,
    opened: false,
    user_type: 'enterprise',
    children: [
    {
      id: 'Publicar oferta',
      active: false,
      link: '/app/company/offer/new',
      component: PAGE_COMPANY_NEW_OFFER,
      user_type: 'enterprise'
    },
    {
      id: 'Consultar ofertas',
      active: false,
      link: '/app/company/offer/list',
      component: PAGE_COMPANY_OFFER_LIST,
      user_type: 'enterprise'
    },
    {
      id: 'Comprar publicaciones',
      active: false,
      link: '/app/company/offer/buy',
      component: PAGE_COMPANY_BUY_OFFER,
      user_type: 'enterprise'
    },
    {
      id: 'Borradores',
      active: false,
      link: '/app/company/offer/draft-list',
      component: PAGE_COMPANY_DRAFT_OFFER_LIST,
      user_type: 'enterprise'
    },
  ]
  },
  {
    icon: <EqualizerIcon />,
    id: 'Estadísticas',
    dropdown: false,
    active: false,
    opened: false,
    user_type: 'enterprise',
    link: '/app/company/statistics',
    component: PAGE_COMPANY_STATISTICS
  }
]