import {
    createMuiTheme,
  } from '@material-ui/core/styles'
  
  
  let muiTheme = createMuiTheme({
    palette: {
      primary: {
        light: '#F8AF3C',
        main: '#F29222',
        dark: '#EA8017',
        contrastText: '#fff'
      },
    },
    typography: {
      h5: {
        fontWeight: 500,
        fontSize: 26,
        letterSpacing: 0.5
      }
    },
    shape: {
      borderRadius: 4
    },
    props: {
      MuiTab: {
        disableRipple: true
      }
    },
    mixins: {
      toolbar: {
        minHeight: 48
      }
    }
  })
    
  muiTheme = {
    ...muiTheme,
    overrides: {
      MuiDrawer: {
        paper: {
          backgroundColor: '#18202c'
        }
      },
      MuiButton: {
        label: {
          textTransform: 'none'
        },
        contained: {
          boxShadow: 'none',
          '&:active': {
            boxShadow: 'none'
          }
        },
        containedSecondary: {
          backgroundColor: '#1C4391',
          '&:hover': {
            backgroundColor: '#1C4391',
          }
        },
        outlined: {
          color: muiTheme.palette.primary.main,
          background: '#fff',
        }
      },
      MuiTabs: {
        root: {
          marginLeft: muiTheme.spacing(1)
        },
        indicator: {
          height: 3,
          borderTopLeftRadius: 3,
          borderTopRightRadius: 3,
          backgroundColor: muiTheme.palette.common.white
        }
      },
      MuiTab: {
        root: {
          textTransform: 'none',
          margin: '0 16px',
          minWidth: 0,
          padding: 0,
          [muiTheme.breakpoints.up('md')]: {
            padding: 0,
            minWidth: 0
          }
        }
      },
      MuiIconButton: {
        root: {
          padding: muiTheme.spacing(1)
        }
      },
      MuiTooltip: {
        tooltip: {
          borderRadius: 4
        }
      },
      MuiDivider: {
        root: {
          backgroundColor: '#404854'
        }
      },
      MuiListItem: { // For ListItem, change this to MuiListItem
        root: {
          "&$selected": {       // this is to refer to the prop provided by M-UI
            backgroundColor: muiTheme.palette.primary.main, // updated backgroundColor
          },
        },
      },
      MuiListItemText: {
        primary: {
          fontWeight: muiTheme.typography.fontWeightMedium
        }
      },
      MuiListItemIcon: {
        root: {
          color: 'inherit',
          marginRight: 0,
          '& svg': {
            fontSize: 20
          }
        }
      },
      MuiAvatar: {
        root: {
          width: 32,
          height: 32
        }
      }
    }
  }
  
  export const theme = muiTheme