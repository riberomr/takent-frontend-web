import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'
import { Store, StoreProvider } from './Store'
import PrivateRoute from './components/routing/PrivateRoute'
import PublicRoute from './components/routing/PublicRoute'
import './App.css';
import {
  PAGE_LOGIN,
  PAGE_MAIN,
  PAGE_RECOVERY_PASSWORD,
  PAGE_TAKENT,
  PAGE_SIGNIN,
  PAGE_TERMS_AND_CONDITIONS,
  PAGE_SUPPORT,
  PAGE_OPINIONS
} from './components'
import { paths } from './constants/generalConstants/paths'
import PAGE_ACTIVATE_USER from './components/pages/Auth/ActivateUser'
import PAGE_RECOVER_USER from './components/pages/Auth/RecoverUser'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import esLocale from 'date-fns/locale/es'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hasError: false,
      error: null
    }
  }

  // static getDerivedStateFromError(error) {
  //   return { hasError: true, error }
  // }

  /*componentDidCatch(error, errorInfo) {
    // Logging service
    console.error(error, errorInfo)
  }*/

  render() {
    // if (this.state.hasError) return <PAGE_ERROR />

    return (
      <StoreProvider>
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={esLocale}>
          <SnackbarProvider
            maxSnack={6}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
          >
            <Store.Consumer>
              {({ user, handleSetUser }) => {
                // also needs isOnline
                const isLoggedIn = !!user
                return (
                  <Router>
                    <Switch>
                      <PublicRoute
                        exact
                        path={paths.public.login}
                        component={PAGE_LOGIN}
                        loggedIn={isLoggedIn} // if user is already logged in, then redirects to home
                      />
                      <PublicRoute
                        exact
                        path={paths.public.recovery}
                        component={PAGE_RECOVERY_PASSWORD}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.takent}
                        component={PAGE_TAKENT}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.support}
                        component={PAGE_SUPPORT}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.opinions}
                        component={PAGE_OPINIONS}
                      />
                      <PublicRoute
                        exact
                        allScreenSize
                        path={paths.public.activateUser}
                        component={PAGE_ACTIVATE_USER}
                      />
                      <PublicRoute
                        exact
                        allScreenSize
                        path={paths.public.recoverUser}
                        component={PAGE_RECOVER_USER}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.singIn.user}
                        component={PAGE_SIGNIN}
                        type={'USER'}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.singIn.company}
                        component={PAGE_SIGNIN}
                        type={'COMPANY'}
                      />
                      <PublicRoute
                        exact
                        path={paths.public.termsAndConditions}
                        component={PAGE_TERMS_AND_CONDITIONS}
                      />
                      <PrivateRoute
                        path={paths.protected.home}
                        component={PAGE_MAIN}
                        loggedIn={isLoggedIn}
                      />
                      <PublicRoute exact path='/' />
                      {/* <PublicRoute component={PAGE_NOT_FOUND} /> */}

                    </Switch>
                  </Router>
                )
              }}
            </Store.Consumer>
          </SnackbarProvider>
        </MuiPickersUtilsProvider>
      </StoreProvider>
    )
  }
}

export default App