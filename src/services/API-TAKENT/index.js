import { axiosService } from '../axios.service'

const getCountries = () => {
    return axiosService.get('/countries')
}

const getProvinces = () => {
    return axiosService.get('/provinces')
}

const getCities = (idProvince) => {
    return axiosService.get('/cities/'+idProvince)
}

const postCommonUser = (body) => {
    return axiosService.post('/user/common', body)
}

const postCommonUserEducation = (body) => {
    return axiosService.postAuthorized('/user/common/education', body)
}

const postCommonUserExperience = (body) => {
    return axiosService.postAuthorized('/user/common/experience', body)
}

const postCommonUserSkills = (body) => {
    return axiosService.postAuthorized('/user/common/skill', body)
}

const postCommonUserLanguages = (body) => {
    return axiosService.postAuthorized('/user/common/language', body)
}

const postProfilePhotoCommonUser = (body) => {
    return axiosService.postProfilePhotoUser('/user/common/profile_photo', body)
}

const getProfilePhotoCommonUser = (_id) => {
    if(_id) return axiosService.getProfilePhotoUser('/user/common/profile_photo?user_id='+_id)
    return axiosService.getProfilePhotoUser('/user/common/profile_photo')
}

const deleteProfilePhotoCommonUser = () => {
    return axiosService.deleteProfilePhotoUser('/user/common/profile_photo')
}

const getCommonUserData = () => {
    return axiosService.getAuthorized('/user/common')
}

const getCommonUserEducation = (id='') => {
    let param = ''
    if (id !== '') param = '/'+id

    return axiosService.getAuthorized('/user/common/education'+param)
}

const getCommonUserExperience = (id='') => {
    let param = ''
    if (id !== '') param = '/'+id

    return axiosService.getAuthorized('/user/common/experience'+param)
}

const getCommonUserSkills = (id='') => {
    let param = ''
    if (id !== '') param = '/'+id

    return axiosService.getAuthorized('/user/common/skill'+param)
}

const getCommonUserLanguages= (id='') => {
    let param = ''
    if (id !== '') param = '/'+id
    return axiosService.getAuthorized('/user/common/language'+param)
}

const putCommonUserEducation = (id, body) => {
    return axiosService.putAuthorized('/user/common/education/'+id, body)
}

const putCommonUserExperience = (id, body) => {
    return axiosService.putAuthorized('/user/common/experience/'+id, body)
}

const putCommonUserSkills = (id, body) => {
    return axiosService.putAuthorized('/user/common/skill/'+id, body)
}

const putCommonUserLanguages= (id, body) => {
    return axiosService.putAuthorized('/user/common/language/'+id, body)
}

const deleteCommonUserEducation = (id) => {
    return axiosService.deleteAuthorized('/user/common/education/'+id)
}

const deleteCommonUserExperience = (id) => {
    return axiosService.deleteAuthorized('/user/common/experience/'+id)
}

const deleteCommonUserSkills = (id) => {
    return axiosService.deleteAuthorized('/user/common/skill/'+id)
}

const deleteCommonUserLanguages= (id) => {
    return axiosService.deleteAuthorized('/user/common/language/'+id)
}

const deleteCommonUser = () => {
    return axiosService.deleteAuthorized('/user/common')
}

const deleteEnterpriseUser = () => {
    return axiosService.deleteAuthorized('/user/enterprise')
}

const getEducationLevels = () => {
    return axiosService.get('/education_level')
}

const getLanguages = () => {
    return axiosService.get('/language')
}

const getLanguageLevels = () => {
    return axiosService.get('/language_level')
}

const getSectors = () => {
  return axiosService.get('/sector')
}

const putCommonUserData = (body) => {
    return axiosService.putAuthorized('/user/common', body)
}

const putEnterpriseUserData = (body) => {
    return axiosService.putAuthorized('/user/enterprise', body)
}

const putEnterpriseData = (body) => {
    return axiosService.putAuthorized('/enterprise', body)
}

const postCompanyUser = (body) => {
    return axiosService.post('/user_and_enterprise', body )
}

const postCompanyOffer = (body) => {
  return axiosService.postAuthorized('/offer', body)
}

const deleteCompanyOffer = (_id) => {
    return axiosService.deleteAuthorized('/offer/'+_id)
  }

const putCompanyOffer = (_id, body) => {
    return axiosService.putAuthorized('/offer/'+_id, body)
  }

const postCurriculum = (body) => {
    return axiosService.postAuthorized('/user/common/curriculum', body)
}

const getCurriculum = (params) => {
    return axiosService.getPDF('/user/common/curriculum', params)
}

const deleteCurriculum = () => {
    return axiosService.deleteAuthorized('/user/common/curriculum')
}

const postChangePassword = (body) => {
    return axiosService.postAuthorized('/user/common/change_password', body)
}

const postChangePasswordEnterprise = (body) => {
    return axiosService.postAuthorized('/user/enterprise/change_password', body)
}

const postRecoveryPassword = (body) => {
    return axiosService.post('/user/common/recover_password', body)
}

const getUserExists = (account) => {
    return axiosService.get(`/user/exists?account=${account}`)
}

const getOffers = (params) => {
    return axiosService.getAuthorized('/offer', params)
}

const getOfferImages = (offerId) => {
  return axiosService.getAuthorized(`/offer/${offerId}/images`)
}

const postOfferImages = (offerId, body) => {
    return axiosService.postAuthorized(`/offer/${offerId}/images`, body)
}

const deleteOfferImages = (offerId) => {
    return axiosService.deleteAuthorized(`/offer/${offerId}/images`)
}

const getEnterpriseImages = (enterpriseId) => {
    return axiosService.getAuthorized(`/enterprise/images?id=`+enterpriseId)
}

const getServices = (params) => {
    return axiosService.getAuthorized('/service', params)
}

const getServiceImages = (serviceId) => {
  return axiosService.getAuthorized(`/service/${serviceId}/images`)
}

const getEnterprise = () => {
  return axiosService.getAuthorized('/enterprise')
}

const getEnterpriseUserData = () => {
    return axiosService.getAuthorized('/user/enterprise')
}

const getEnterpriseProfilePhoto = (_id) => {
    return axiosService.getProfilePhotoUser('/enterprise/profile_photo?id='+_id)
}

const postEnterpriseProfilePhoto = (body) => {
    return axiosService.postAuthorized('/enterprise/profile_photo', body)
}

const deleteEnterpriseProfilePhoto = () => {
    return axiosService.deleteAuthorized('/enterprise/profile_photo')
}

const postEnterpriseImages = (body) => {
    return axiosService.postAuthorized('/enterprise/images', body)
}

const getEnterpriseProfilePhotoAuth = () => {
    return axiosService.getProfilePhotoUser('/enterprise/profile_photo')
}

const postGenerateMPPayment = (body) => {
  return axiosService.postAuthorized('/mercadopago/generate_payment', body)
}

const getOfferPacks = () => {
  return axiosService.get('/offer_pack')
}

const getPostulants = offerId => {
  return axiosService.getAuthorized(`/offer/${offerId}/postulant`)
}

const getServiceRequests = serviceId => {
  return axiosService.getAuthorized(`/service/${serviceId}/request`)
}

const postulationCommonUser = (_id) => {
    return axiosService.postAuthorized('/postulate/'+_id)
}

const requestService = (_id) => {
  return axiosService.postAuthorized('/request_service/'+_id)
}

const postContactPostulant = (offerId, postulationId, body) => {
    return axiosService.postAuthorized('/offer/'+offerId+'/postulant/'+postulationId+'/contact', body)
}

const postPreselectPostulant = (offerId, postulationId, body) => {
  return axiosService.postAuthorized('/offer/'+offerId+'/postulant/'+postulationId+'/pre_select', body)
}

const postDiscardPostulant = (offerId, postulationId, body) => {
  return axiosService.postAuthorized('/offer/'+offerId+'/postulant/'+postulationId+'/discard', body)
}

const putPostulant = (offerId, postulationId, body) => {
  return axiosService.putAuthorized('/offer/'+offerId+'/postulant/'+postulationId, body)
}

const putServiceRequest = (serviceId, requestId, body) => {
  return axiosService.putAuthorized('/service/'+serviceId+'/request/'+requestId, body)
}

const postService = (body) => {
  return axiosService.postAuthorized('/service', body)
}

const putService = (_id, body) => {
  return axiosService.putAuthorized('/service/'+_id, body)
}

const activateUser = (ticketId) => {
  return axiosService.get(`/user/activate?ticket=${ticketId}`)
}

const postRecoverAccount = body => {
  return axiosService.post(`/recover_account`, body)
}

const getRecoverAccount = ticketId => {
  return axiosService.get(`/recover_account?ticket=${ticketId}`)
}

const getEnterpriseStatistic = (statistic, params) => {
  return axiosService.getAuthorized(`/enterprise/statistic/${statistic}`, params)
}
const getEnterpriseUsers = () => {
    return axiosService.getAuthorized(`/enterprise/users`);
}

const postAllowedUsers = (body) => {
    return axiosService.postAuthorized('/member_user', body);
}

const deleteAllowedUser = (account) => {
    return axiosService.deleteAuthorized(`/member_user/${account}`);
}

const getMyPostulations = () => {
  return axiosService.getAuthorized('/user/common/postulations')
}

const getNotifications = () => {
  return axiosService.getAuthorized('/user/notifications')
}

const seeNotifications = () => {
  return axiosService.postAuthorized('/user/notifications')
}

const refreshToken = () => {
    return axiosService.postAuthorized('/authentication')
}

const doAdmin = account => {
  return axiosService.postAuthorized('/user/enterprise/do_admin', {account: account})
}

const getNotificationsConfiguration = () => {
  return axiosService.getAuthorized('/user/notifications_configuration')
}

const putNotificationsConfiguration = body => {
  return axiosService.putAuthorized('/user/notifications_configuration', body)
}

const getMyServiceRequests = () => {
  return axiosService.getAuthorized('/common/service_requests')
}

const getRatingStatistics = (user) => {
  return axiosService.getAuthorized(`/user/common/assessment_statistics/${user}`)
}

const postOpinion = body => {
  return axiosService.post('/feedback', body)
}

export const apiTakent = {
    getCountries,
    getProvinces,
    getCities,
    postCommonUser,
    postCompanyUser,
    postProfilePhotoCommonUser,
    getProfilePhotoCommonUser,
    getEnterpriseUserData,
    putEnterpriseUserData,
    putCommonUserData,
    getCommonUserData,
    getEnterpriseUsers,
    postAllowedUsers,
    deleteAllowedUser,
    deleteProfilePhotoCommonUser,
    getEducationLevels,
    getLanguages,
    getLanguageLevels,
    getSectors,
    deleteCommonUser,
    deleteCommonUserEducation,
    deleteCommonUserExperience,
    deleteCommonUserLanguages,
    deleteCommonUserSkills,
    putCommonUserEducation,
    putCommonUserExperience,
    putCommonUserSkills,
    putCommonUserLanguages,
    getCommonUserEducation,
    getCommonUserExperience,
    getCommonUserSkills,
    getCommonUserLanguages,
    postCommonUserEducation,
    postCommonUserExperience,
    postCommonUserSkills,
    postCommonUserLanguages,
    postCurriculum,
    postCompanyOffer,
    getCurriculum,
    deleteCurriculum,
    postChangePassword,
    postRecoveryPassword,
    getUserExists,
    getOffers,
    getEnterprise,
    postGenerateMPPayment,
    getOfferPacks,
    getEnterpriseProfilePhoto,
    getOfferImages,
    getPostulants,
    postulationCommonUser,
    getEnterpriseProfilePhotoAuth,
    postContactPostulant,
    putPostulant,
    putCompanyOffer,
    postPreselectPostulant,
    postDiscardPostulant,
    getServices,
    postService,
    getServiceImages,
    putService,
    getServiceRequests,
    putServiceRequest,
    getEnterpriseImages,
    requestService,
    activateUser,
    putEnterpriseData,
    postEnterpriseProfilePhoto,
    postEnterpriseImages,
    deleteCompanyOffer,
    postRecoverAccount,
    getRecoverAccount,
    deleteEnterpriseProfilePhoto,
    deleteEnterpriseUser,
    postChangePasswordEnterprise,
    getEnterpriseStatistic,
    postOfferImages,
    deleteOfferImages,
    getMyPostulations,
    getNotifications,
    seeNotifications,
    refreshToken,
    doAdmin,
    getNotificationsConfiguration,
    putNotificationsConfiguration,
    getMyServiceRequests,
    getRatingStatistics,
    postOpinion
}
