import axios from 'axios'
const API_URL = process.env.REACT_APP_API_URL

export const axiosService = {
    get,
    getAuthorized,
    postAuthorized,
    postProfilePhotoUser,
    getProfilePhotoUser,
    putAuthorized,
    deleteProfilePhotoUser,
    deleteAuthorized,
    getPDF,
    post,
    getProfilePhotoEnterprise
}
  
function get(endpoint) {
    let url = API_URL + endpoint
    return axios.get(url)
        .then(res =>{
            if(res.data && res.status===200)
                return Promise.resolve({data: res.data, statusCode: res.status})
            else return Promise.reject(res)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

function getAuthorized(endpoint, params = null) {
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint

    return axios.get(url, { headers: {    
            'Accept' : '*/*',
            'Content-Type': 'application/json',
            "Authorization" : `Bearer ${token}`
            },
            params: params
        }
        ).then(res =>{
            if(res.data && res.status===200)
                return Promise.resolve({data: res.data, statusCode: res.status})
            else return Promise.reject(res)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

function getPDF(endpoint, params = null) {
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint

    return axios.get(url, { headers: {    
            'Accept' : '*/*',
            'Content-Type': 'application/pdf',
            "Authorization" : `Bearer ${token}`
            },
            params: params
        }
        ).then(res =>{
            if(res.data && res.status===200)
                return Promise.resolve({data: res.data, statusCode: res.status})
            else return Promise.reject(res)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

function deleteAuthorized(endpoint) {
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint

    return axios.delete(url, { headers: {    
            'Accept' : '*/*',
            "Authorization" : `Bearer ${token}`
            }
        }
        ).then(res =>{
            if(res.status===204 || res.status === 200)
                return Promise.resolve()
            else return Promise.reject(res)
        })
        .catch(err => {
            return Promise.reject(err)
        })
}

function postAuthorized(endpoint, body){
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint

    return axios.post(url, body,
        { headers: {    
            'Accept' : 'application/json',
            'Content-Type': 'application/json',
            "Authorization" : `Bearer ${token}`},
        }
        ).then(res =>{
        if(res.data && (res.status===201 || res.status===200))
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function post(endpoint, body){

    let url = API_URL + endpoint

    return axios.post(url, body,
        { headers: {    
            'Accept' : 'application/json',
            'Content-Type': 'application/json'},
        }
        ).then(res =>{
        if(res.data && (res.status===201 || res.status===200))
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function putAuthorized(endpoint, body){
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint

    return axios.put(url, body,
        { headers: {    
            'Accept' : 'application/json',
            'Authorization' : `Bearer ${token}`,
            'Content-Type': 'application/json'}
        }
        ).then(res =>{
            if(res && res.status===204)
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function postProfilePhotoUser(endpoint, body){
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint
    // let data = new FormData();
    // data.append('profile_photo',body.profile_photo)
    return axios.post(url, body,
        { headers: {  
            'Accept' : 'application/json',  
            "Authorization" : `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        }
        ).then(res =>{
        if(res.data && res.status===201)
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function getProfilePhotoUser(endpoint){
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint
    return axios.get(url, 
        { headers: {    
            'Accept' : 'application/json',
            "Authorization" : `Bearer ${token}`,
            'Content-Type': 'aplication/json'
        },
        }
        ).then(res =>{
        if(res.data && res.status===200)
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function getProfilePhotoEnterprise(endpoint){
    let url = API_URL + endpoint
    return axios.get(url, 
        { headers: {    
            'Accept' : 'application/json',
            'Content-Type': 'aplication/json'
        },
        }
        ).then(res =>{
        if(res.data && res.data.profile_photo && res.status===200)
            return Promise.resolve({data: res.data, statusCode: res.status})
        else return Promise.reject(res)
    })
    .catch(err => {
        return Promise.reject(err)
    })
}

function deleteProfilePhotoUser(endpoint){
    let token = localStorage.getItem('takent_token')
    let url = API_URL + endpoint
    return axios.delete(url,
                { headers: {  
                    "Authorization" : `Bearer ${token}`,
                    "Accept": "*/*"
                },
            } 
        )
        .then(() => 
            {return Promise.resolve('joya3')
        })
        .catch(() => {
            return Promise.reject('err')
        })
}
