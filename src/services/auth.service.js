import axios from 'axios'
const API_URL = process.env.REACT_APP_API_URL

export const authService = {
  login,
  logout,
  getUser,
  checkPass
}

function checkPass(email, password) {
    let body = {
        account: email,
        password: password
        }
    return axios.post(API_URL+'/authentication', body)
        .then(response => {
        // login successful if there's a jwt token in the response
        if (response && response.data.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            return Promise.resolve(response)
        } else {
            return Promise.reject('error')
        }
        })
        .catch((error)=> {
            return Promise.reject(error)}
        )
}

function login(email, password) {
    let body = {
        account: email,
        password: password
    }
    return axios.post(API_URL + '/authentication', body)
    .then(response => {
        if (response && response.data.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('takent_token', response.data.token)
        } 
        else {
            return Promise.reject('Error: response result not found')
        }
    })
    .catch(error => {
            return Promise.reject(error)}
    )
}

function logout() {
  localStorage.removeItem('takent_token')
  localStorage.removeItem('takent_user')
}

function getUser() { 
    let token = localStorage.getItem('takent_token')
    const parseJwt = () => {
        try {
          return JSON.parse(atob(token.split('.')[1]))
        } catch (e) {
          return null
        }
      }
    let tokenData = parseJwt()
    if (tokenData.sub.user_type === 'common') {
        return axios.get(API_URL+'/user/common', { headers: {
            "Authorization" : `Bearer ${token}`,
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
                }
            }
        )
        .then( res => {
            if (res.data) {
                localStorage.setItem('takent_user', JSON.stringify(res.data))
                localStorage.setItem('type', 'common')
            }
            return res || {}
            }
        )
    }
    if (tokenData.sub.user_type === 'enterprise') {
        return axios.get(API_URL+'/user/enterprise', { headers: {
            "Authorization" : `Bearer ${token}`,
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
                }
            }
        )
        .then( res => {
            if (res.data) {
                localStorage.setItem('takent_user', JSON.stringify(res.data))
                localStorage.setItem('type', 'enterprise')
            }
            return res || {}
            }
        )
    }
}
