import React, { useState, useEffect } from 'react'

export const Store = React.createContext()

export const StoreProvider = (props) => {
  const storedUser = JSON.parse(localStorage.getItem('takent_user'))
  const [user, setUser] = useState(storedUser)

  const [leftDialog, setLeftDialog] = useState(null)
  const [drawerWidth, setDrawerWidth] = useState(256)
  const [isOnline, setIsOnline] = useState(true)
  const [isIdle, setIsIdle] = useState(!!localStorage.getItem('_metadata') || false)
  const [alertInfo, showAlert] = useState(null)
  const [renderComponents, setRenderComponents] = useState(false)
  const [mobileOpen, setMobileOpen] = useState(false)
  const [photoChange, setPhotoChange] = useState(null)

  const handleSetUser = user => setUser(user)

  const handleDrawerToggle = () => setMobileOpen(!mobileOpen)

  useEffect(() => {
    window.addEventListener('load', () => {
      setIsOnline(navigator.onLine)
      window.addEventListener('online', () => setIsOnline(true))
      window.addEventListener('offline', () => setIsOnline(false))
    })
  }, [])

  return (
    <Store.Provider
      value={{
        user,
        setRenderComponents,
        renderComponents,
        isOnline,
        handleSetUser,
        mobileOpen,
        handleDrawerToggle,
        isIdle,
        setIsIdle,
        alertInfo,
        showAlert,
        leftDialog,
        setLeftDialog,
        drawerWidth,
        setDrawerWidth,
        photoChange,
        setPhotoChange
      }}
    >
      {props.children}
    </Store.Provider>
  )
}
