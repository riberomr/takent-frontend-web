import PAGE_LOGIN from './pages/Auth/Login'
import PAGE_RECOVERY_PASSWORD from './pages/Auth/RecoveryPassword'
import PAGE_TAKENT from './pages/ExtraInfo/About'
import PAGE_SIGNIN from './pages/Auth/SignIn'
import PAGE_MAIN from './pages/Paperbase'
import PAGE_TERMS_AND_CONDITIONS from './pages/ExtraInfo/TermsAndConditions'
import PAGE_SUPPORT from './pages/ExtraInfo/Support'
import PAGE_OPINIONS from './pages/ExtraInfo/Opinions'
 
export {
  PAGE_LOGIN,
  PAGE_MAIN,
  PAGE_RECOVERY_PASSWORD,
  PAGE_TAKENT,
  PAGE_SIGNIN,
  PAGE_TERMS_AND_CONDITIONS,
  PAGE_SUPPORT,
  PAGE_OPINIONS
}
