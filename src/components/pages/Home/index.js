import React from 'react'
import PAGE_COMMON_USER_OFFER_LIST from '../CommonUser/Offers/OfferList'
import PAGE_COMPANY_OFFER_LIST from '../Company/OfferList'

const PAGE_HOME = (props) => {
    const { user_type } = props
    return (
        <>
        {user_type === 'common' && <PAGE_COMMON_USER_OFFER_LIST/>}
        {user_type === 'enterprise' && <PAGE_COMPANY_OFFER_LIST/>}
        </>
    )
}

export default PAGE_HOME