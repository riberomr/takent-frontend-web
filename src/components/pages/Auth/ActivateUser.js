import React from 'react'
import ActivateUserWrapper from '../../wrappers/Auth/ActivateUser'

const ActivateUser = props => {

  return (
    <ActivateUserWrapper {...props}/>
  )

}

export default ActivateUser