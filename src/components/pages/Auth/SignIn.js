import React from 'react'
import SignInWrapper from '../../wrappers/Auth/SignIn'
const SignIn = (props) => {

    const { type } = props

    return(
       <SignInWrapper type={type}/>
    )
}

export default SignIn

