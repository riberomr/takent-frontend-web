import React from 'react'
import RecoveryPasswordWrapper from '../../wrappers/Auth/RecoveryPassword'

const RecoveryPassword = props => {
  return (
      <RecoveryPasswordWrapper {...props} />
  )
}

export default RecoveryPassword