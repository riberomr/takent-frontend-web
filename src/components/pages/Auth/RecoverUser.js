import React from 'react'
import RecoverUserWrapper from '../../wrappers/Auth/RecoverUser'

const RecoverUser = props => {

  return (
    <RecoverUserWrapper {...props}/>
  )

}

export default RecoverUser
