import React from 'react'
import GridLayout from './GridLayout'
import LoginWrapper from '../../wrappers/Auth/Login'

const Login = props => {
  return (
    <GridLayout>
      <LoginWrapper {...props} />
    </GridLayout>
  )
}

export default Login