export const styles = {
    completeLogo:{
        justifyContent: 'center',
        display: 'flex',
        alignItems: 'center'
    },
    imgLogo:{
        width: '50%',
        minWidth: '300px'
    },
    firstTextContainer:{
        display: 'flex',
        justifyContent: 'center'
    },
    lastTextContainer:{
        marginTop: '15px',
        display: 'flex',
        justifyContent: 'center'
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: '35px'
    },
    lastText:{
        fontFamily: 'Roboto',
        fontSize: '35px',
        marginLeft:'15px',  
        textDecoration: 'underline'
    },
    container: {
        marginTop: '100px',
        marginBottom: '50px'
    }
}