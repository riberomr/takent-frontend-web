import React from 'react'
import Grid from '@material-ui/core/Grid'
import {
  ThemeProvider,
} from '@material-ui/core/styles'
import { theme } from '../../../../constants/generalConstants/theme'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import HeaderLayout from './HeaderLayout'
import OrangeCard from '../../../ui/generalUI/OrangeCard'
import Footer from '../../../ui/generalUI/Footer'
import { paths } from '../../../../constants/generalConstants/paths'
import { Container, Typography } from '@material-ui/core'

const useStyles = makeStyles(styles)

const GridLayout = props => {

    const isXSSize = useMediaQuery('(max-width:600px)')
    const classes = useStyles({isXSSize})
    const REGISTER_USER = 'Registrar usuario'
    const REGISTER_COMPANY = 'Registrar empresa'
    const CARD_USER_TITLE = 'Cuenta de usuario'
    const CARD_COMPANY_TITLE = 'Cuenta de empresa'
    const CARD_USER_DESCRIPTION = 'Vas a poder buscar ofertas de trabajo que sean de tu interés y postularte a ese trabajo que tanto querés! También vas a poder publicar y buscar servicios! No esperés más, registrate ya y empezá a vivir la experiencia Takent!'
    const CARD_COMPANY_DESCRIPTION = 'Vas a poder publicar y gestionar tus ofertas de trabajo! Takent te ayuda a encontrar al candidato ideal y contactarlo sin moverte de la aplicación! Sumate hoy a la nueva forma de contratación y averiguá los beneficios disponibles para las empresas!'

    return (
        <ThemeProvider theme={theme}>
            <HeaderLayout/>
            <Container maxWidth="lg" className={classes.container}>           
              <Grid container spacing={5}>
                <Grid item xs={12}>
                  <Typography variant="h4" align="center">¿Buscás trabajo? ¿Necesitás un servicio?</Typography>
                </Grid>
                <Grid item xs={12} sm={6} lg={6} className={classes.completeLogo}>
                    <img className={classes.imgLogo} src="/images/logo-completo-takent.png" alt="takent" />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                    <div>
                        {props.children}
                    </div>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="h4" align="center">¿No tenés cuenta? ¡Registrate ahora!</Typography>
                </Grid>
                <Grid item xs={12}>
                    <OrangeCard 
                        title={CARD_USER_TITLE}
                        description={CARD_USER_DESCRIPTION} 
                        buttonText={REGISTER_USER}
                        redirectTo={paths.public.singIn.user}
                        />
                </Grid>
                <Grid item xs={12}>
                    <OrangeCard 
                        title={CARD_COMPANY_TITLE}
                        description={CARD_COMPANY_DESCRIPTION}  
                        buttonText={REGISTER_COMPANY}
                        redirectTo={paths.public.singIn.company}
                        />
                </Grid>
            </Grid>
            </Container> 
            <Footer/>
        </ThemeProvider>
    )
}

export default GridLayout