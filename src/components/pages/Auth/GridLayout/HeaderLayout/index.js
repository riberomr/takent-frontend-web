import React from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { Link } from 'react-router-dom'
import { paths } from '../../../../../constants/generalConstants/paths'
import { AppBar, Toolbar, Typography } from '@material-ui/core'

const useStyles = makeStyles(styles)

const HeaderLayout = props => {

    const isXSSize = useMediaQuery('(max-width:600px)')
    const classes = useStyles({isXSSize})

    return (
      <div className={classes.grow}>
        <AppBar position="fixed" style={{background: "#DCDCDC"}} className={classes.appBar}>
          <Toolbar >
            <Link to={paths.public.login} style={{display: 'flex', alignItems: 'center'}}>
              <img
                alt="menu-logo"
                src="/images/logo-maletin-solo-takent.png"
                className={classes.menuLogo}
              />
            </Link>
            <Link to={paths.public.login} className={classes.navLink}>
              <Typography className={classes.title} variant="h6" noWrap color="textPrimary">
                TAKENT
              </Typography>
            </Link>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <Link 
                to={paths.public.takent}
                className={classes.navLink}
                children={<Typography variant="h6" noWrap color="textPrimary" component="div" className={classes.denseTyphography}>¿Qué es TAKENT?</Typography>}
              />
              <Link
                to={paths.public.support}
                className={classes.navLink}
                children={<Typography variant="h6" noWrap color="textPrimary" component="div" className={classes.denseTyphography}>Soporte</Typography>}
              />
              <Link
                to={paths.public.opinions}
                className={classes.navLink}
                children={<Typography variant="h6" noWrap color="textPrimary" component="div" className={classes.denseTyphography}>Dejanos tu opinión</Typography>}
              />
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
}

export default HeaderLayout