import { theme } from '../../../constants/generalConstants/theme'

export const styles = {
  root: {
    display: 'flex',
    minHeight: 'calc(100vh - 96px)',
    paddingTop: '48px',
    fontFamily: 'Roboto',
    fontSize: '14px'
  },
  actions: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  isError: {
    color: 'red',
  },
  button: {
    margin: '3px'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: ({drawerWidth, isXSSize}) => !isXSSize? drawerWidth : 256,
      flexShrink: 0
    },
    backgroundColor: '#FBFBFB',
    borderRight: '1px solid rgba(0, 0, 0, 0.12)'  
  },
  app: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  main: {
    flex: 1,
    padding: theme.spacing(6, 0),
    paddingBottom: 0,
    background: '#fff',
    width: '100%',
  },
  footer: {
    paddingBottom: theme.spacing(2),
    background: '#fff'
  },
  minimized: {
    position: 'fixed',
    bottom: 0,
    width: 'calc(100% - 256px)', // 256px is the drawer width
    display: 'flex',
    zIndex: 30,
    flexDirection: 'row-reverse'
  },
  footerContainer:{
    display: 'flex',
    justifyContent: 'flex-end',
    zIndex: 9999
  }
}