import React, { useState, useContext, useEffect } from 'react'
import { ThemeProvider} from '@material-ui/core/styles'
import { theme } from '../../../constants/generalConstants/theme'
import Navigator from '../Drawer2'
import { Route, Switch, Redirect } from 'react-router-dom'
import { Store } from '../../../Store'
import PrincipalHeaderWrapper from '../../wrappers/PrincipalHeader'
import { tokenTimeChecker } from '../../../helpers/generalHelpers/tokenTimeChecker'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import PrivateRoute from '../../routing/PrivateRoute'
import Footer from '../../ui/generalUI/Footer'
import { navigationMenu } from '../../../constants/generalConstants/navigationMenu'
import LeftDialog from '../../ui/generalUI/LeftDialog'
import { apiTakent } from '../../../services/API-TAKENT'
import { authService } from '../../../services/auth.service'
import jwtdecode from 'jwt-decode'

const useStyles = makeStyles(styles)

function Paperbase(props) {

  const isXSSize = useMediaQuery('(max-width:600px)')
  const {drawerWidth, user, handleSetUser, photoChange } = useContext(Store)
  const [userName, setUserName] = useState(null)
  const [profilePhoto, setProfilePhoto] = useState(null)
  const [ navigatorOpen, setNavigatorOpen ] = useState(false)

  const classes = useStyles({drawerWidth, isXSSize})

  useEffect(() => {

    const refreshToken = setInterval(async () => {
      const token = localStorage.getItem("takent_token")
      if(!token) return
      const expires = jwtdecode(token).exp * 1000
      const now = new Date().getTime()
      if(expires - now < 60000) {
        try {
          const response = await apiTakent.refreshToken()
          localStorage.setItem("takent_token", response.data.token)
        }
        catch(error) {
          authService.logout()
          handleSetUser(null)
        }
      }

    }, 5000)

    return () => clearInterval(refreshToken)

  }, [handleSetUser])

  useEffect(() => {
    if(tokenTimeChecker()){
      authService.logout()
      handleSetUser(null)
    }
  }, [handleSetUser])

  useEffect( () => {
    apiTakent.getCommonUserData().then( res => {
      setUserName(`${res.data.name} ${res.data.surname}`)
    })
  }, [user])

  useEffect( () => {
    if(user.permission){
      apiTakent.getEnterpriseProfilePhotoAuth()
      .then( res => {
        setProfilePhoto(res.data.profile_photo)
    })
    }
    else {
      apiTakent.getProfilePhotoCommonUser()
      .then( res => {
        setProfilePhoto(res.data.profile_photo)
    })
    }
  }, [photoChange, user])

  return (
    <ThemeProvider theme={theme}>
        <LeftDialog/>
        <PrincipalHeaderWrapper userName={userName} profilePhoto={profilePhoto} navigatorOpen={navigatorOpen} setNavigatorOpen={setNavigatorOpen}/>
      <div className={classes.root}>
          <Navigator
            open={navigatorOpen}
            setOpen={setNavigatorOpen}
            location={props.location}
          />
        <div className={classes.app}>
          <main className={classes.main}>
            <Switch>
              {
                navigationMenu.map((menuOption, index) => {
                  if(menuOption.children){
                    return menuOption.children.map((submenuOption, subindex) =>{
                      return(
                          <PrivateRoute key={subindex} 
                            exact path={submenuOption.link} 
                            component={submenuOption.component} 
                          />
                      )
                    })
                  }
                  return(
                    <PrivateRoute 
                      key={index} 
                      exact path={menuOption.link} 
                      component={menuOption.component}
                    />
                  )
                })
              }
              <Route render={() => <Redirect to="/404" />} />
            </Switch>
          </main>
        </div>
      </div >
      <Footer/> 
    </ThemeProvider >
  )
}

export default Paperbase
