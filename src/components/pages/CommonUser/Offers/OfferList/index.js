import React from 'react'
import CommonUserOfferListWrapper from '../../../../wrappers/CommonUser/Offers/OfferList'

const PAGE_COMMON_USER_OFFER_LIST = (props) => {
    return (
        <CommonUserOfferListWrapper {...props} />
    )
}

export default PAGE_COMMON_USER_OFFER_LIST 