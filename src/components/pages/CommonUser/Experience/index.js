import React from 'react'
import CommonUserExperienceWrapper from '../../../wrappers/CommonUser/Experience'

const PAGE_COMMON_USER_EXPERIENCE = (props) => {
    return (
        <CommonUserExperienceWrapper {...props} />
    )
}

export default PAGE_COMMON_USER_EXPERIENCE