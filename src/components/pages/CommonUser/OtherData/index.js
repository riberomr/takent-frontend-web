import React from 'react'
import CommonUserSkillsWrapper from '../../../wrappers/CommonUser/Skills'
import CommonUserLanguagesWrapper from '../../../wrappers/CommonUser/Languages'

const PAGE_COMMON_USER_OTHER_DATA = (props) => {
    return (
        <>
            <CommonUserSkillsWrapper {...props} />
            <CommonUserLanguagesWrapper {...props} />
        </>
    )
}

export default PAGE_COMMON_USER_OTHER_DATA