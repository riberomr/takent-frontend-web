import React from 'react'
import CommonUserNewServiceWrapper from '../../../../wrappers/CommonUser/Services/NewService'

const PAGE_COMMON_USER_NEW_SERVICE = props => {

  return (
    <CommonUserNewServiceWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_NEW_SERVICE
