import React from 'react'
import CommonUserNewServiceListWrapper from '../../../../wrappers/CommonUser/Services/ServiceList'

const PAGE_COMMON_USER_SERVICE_LIST = props => {

  return (
    <CommonUserNewServiceListWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_SERVICE_LIST
