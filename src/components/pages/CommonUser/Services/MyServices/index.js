import React from 'react'
import CommonUserMyServicesWrapper from '../../../../wrappers/CommonUser/Services/MyServices'

const PAGE_COMMON_USER_MY_SERVICES = props => {

  return (
    <CommonUserMyServicesWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_MY_SERVICES
