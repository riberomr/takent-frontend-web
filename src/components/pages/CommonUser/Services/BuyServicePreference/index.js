import React from 'react'
import CommonUserBuyServicePreferenceWrapper from '../../../../wrappers/CommonUser/Services/BuyServicePreference'

const PAGE_COMMON_USER_BUY_SERVICE_PREFERENCE = props => {

  return (
    <CommonUserBuyServicePreferenceWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_BUY_SERVICE_PREFERENCE
