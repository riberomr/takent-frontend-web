import React from 'react'
import CommonUserServiceRequestsWrapper from '../../../../wrappers/CommonUser/Services/ServiceRequests'

const PAGE_COMMON_USER_SERVICE_REQUESTS = props => {

  return (
    <CommonUserServiceRequestsWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_SERVICE_REQUESTS
