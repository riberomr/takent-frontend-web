import React from 'react'
import CommonUserEducationWrapper from '../../../wrappers/CommonUser/Education'

const PAGE_COMMON_USER_EDUCATION = (props) => {
    return (
        <CommonUserEducationWrapper {...props} />
    )
}

export default PAGE_COMMON_USER_EDUCATION