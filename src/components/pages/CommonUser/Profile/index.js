import React from 'react'
import CommonUserProfileWrapper from '../../../wrappers/CommonUser/Profile'

const PAGE_COMMON_USER_PROFILE = (props) => {
    return (
        <CommonUserProfileWrapper {...props} />
    )
}

export default PAGE_COMMON_USER_PROFILE