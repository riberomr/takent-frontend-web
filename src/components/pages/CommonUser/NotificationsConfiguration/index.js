import React from 'react'
import CommonUserNotificationsConfigurationWrapper from '../../../wrappers/CommonUser/NotificationsConfiguration'

const PAGE_COMMON_USER_NOTIFICATIONS_CONFIGURATION = props => {

  return (
    <CommonUserNotificationsConfigurationWrapper {...props}/>
  )

}

export default PAGE_COMMON_USER_NOTIFICATIONS_CONFIGURATION
