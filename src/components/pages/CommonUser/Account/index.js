import React from 'react'
import CommonUserAccountWrapper from '../../../wrappers/CommonUser/Account'

const PAGE_COMMON_USER_ACCOUNT = (props) => {
    return (
        <CommonUserAccountWrapper {...props} />
    )
}

export default PAGE_COMMON_USER_ACCOUNT