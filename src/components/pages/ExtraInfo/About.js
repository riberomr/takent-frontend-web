import React from 'react'
import GridLayout from './GridLayout'
import AboutWrapper from '../../wrappers/ExtraInfo/About'

const About = props => {
  return (
    <GridLayout>
      <AboutWrapper {...props} />
    </GridLayout>
  )
}

export default About