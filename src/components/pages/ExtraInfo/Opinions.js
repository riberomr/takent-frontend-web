import React from 'react'
import OpinionsWrapper from '../../wrappers/ExtraInfo/Opinions'
import GridLayout from './GridLayout'

const PAGE_OPINIONS = () => (
  <GridLayout>
    <OpinionsWrapper/>
  </GridLayout>
)

export default PAGE_OPINIONS
