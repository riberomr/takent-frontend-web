import React from 'react'
import GridLayout from './GridLayout'
import SupportWrapper from '../../wrappers/ExtraInfo/Support'

const Support = props => {
  return (
    <GridLayout>
      <SupportWrapper {...props} />
    </GridLayout>
  )
}

export default Support