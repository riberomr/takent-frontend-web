import React from 'react'
import {
  ThemeProvider,
} from '@material-ui/core/styles'
import { theme } from '../../../../constants/generalConstants/theme'
import HeaderLayout from '../../Auth/GridLayout/HeaderLayout'
import Footer from '../../../ui/generalUI/Footer'
import { Container } from '@material-ui/core'

const GridLayout = props => {
    
    return (
        <ThemeProvider theme={theme}>
            <HeaderLayout />
            <Container maxWidth="lg" style={{paddingTop: '100px', minHeight: 'calc(100vh - 48px)'}}>
              {props.children}
            </Container>
            <Footer />
        </ThemeProvider>
    )
}

export default GridLayout