import React from 'react'
import GridLayout from './GridLayout'
import TermsAndConditionsWrapper from '../../wrappers/ExtraInfo/TermsAndConditions'

const TermsAndConditions = (props) => {
  
  return (
    <GridLayout>
        <TermsAndConditionsWrapper {...props} />
    </GridLayout>
   )
}

export default TermsAndConditions