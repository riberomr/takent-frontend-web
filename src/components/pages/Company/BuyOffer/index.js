import React from 'react'
import CompanyBuyOfferWrapper from '../../../wrappers/Company/BuyOffer'

const PAGE_COMPANY_BUY_OFFER = (props) => {
  
  return (
    <CompanyBuyOfferWrapper {...props} />
  )

}

export default PAGE_COMPANY_BUY_OFFER