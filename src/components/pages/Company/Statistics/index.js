import React from 'react'
import CompanyStatisticsWrapper from '../../../wrappers/Company/Statistics'

const PAGE_COMPANY_STATISTICS = props => {

  return (
    <CompanyStatisticsWrapper {...props}/>
  )

}

export default PAGE_COMPANY_STATISTICS
