import React from 'react'
import EnterpriseUserNotificationsConfigurationWrapper from '../../../wrappers/Company/NotificationsConfiguration'

const PAGE_ENTERPRISE_USER_NOTIFICATIONS_CONFIGURATION = props => {

  return (
    <EnterpriseUserNotificationsConfigurationWrapper {...props}/>
  )

}

export default PAGE_ENTERPRISE_USER_NOTIFICATIONS_CONFIGURATION
