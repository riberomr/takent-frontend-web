import React from 'react'
import CompanyDraftOfferListWrapper from '../../../wrappers/Company/DraftOfferList'

const PAGE_COMPANY_DRAFT_OFFER_LIST = (props) => {
  
  return (
    <CompanyDraftOfferListWrapper {...props} />
  )

}

export default PAGE_COMPANY_DRAFT_OFFER_LIST
