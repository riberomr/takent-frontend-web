import React from 'react'
import CompanyProfileWrapper from '../../../wrappers/Company/Profile'

const PAGE_COMPANY_PROFILE = (props) => {
    return (
        <CompanyProfileWrapper {...props} />
    )
}

export default PAGE_COMPANY_PROFILE
