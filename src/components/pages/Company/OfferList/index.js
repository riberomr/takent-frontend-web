import React from 'react'
import CompanyOfferListWrapper from '../../../wrappers/Company/OfferList'

const PAGE_COMPANY_OFFER_LIST = (props) => {
  
  return (
    <CompanyOfferListWrapper {...props} />
  )

}

export default PAGE_COMPANY_OFFER_LIST
