import React from 'react'
import CompanyAccountWrapper from '../../../wrappers/Company/Account'

const PAGE_ENTERPRISE_USER_ACCOUNT = (props) => {
    return (
        <CompanyAccountWrapper {...props} />
    )
}

export default PAGE_ENTERPRISE_USER_ACCOUNT