import React from 'react'
import CompanyNewOfferWrapper from '../../../wrappers/Company/NewOffer'

const PAGE_COMPANY_NEW_OFFER = (props) => {
  
  return (
    <CompanyNewOfferWrapper {...props} />
  )

}

export default PAGE_COMPANY_NEW_OFFER
