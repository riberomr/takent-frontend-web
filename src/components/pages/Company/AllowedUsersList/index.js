import React from 'react'
import AllowedUserListWrapper from '../../../wrappers/Company/AllowedUsers/AllowedUsersList'

const PAGE_ALLOWED_USER_LIST = (props) => {
  
  return (
    <AllowedUserListWrapper {...props} />
  )

}

export default PAGE_ALLOWED_USER_LIST;