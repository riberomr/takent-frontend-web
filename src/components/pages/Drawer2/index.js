import React, { useContext, useEffect, useState } from 'react'
import clsx from 'clsx'
import { Drawer, List, ListItem, ListItemIcon, ListItemText, makeStyles, Toolbar, Tooltip } from '@material-ui/core'
import { styles } from './styles'
import { navigationMenu } from '../../../constants/generalConstants/navigationMenu'
import MakeNavItems from '../../ui/generalUI/Drawer/MakeNavItems'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { authService } from '../../../services/auth.service'
import { Store } from '../../../Store'
import AlertAction from '../../ui/generalUI/AlertAction'
import { theme } from '../../../constants/generalConstants/theme'

const useStyles = makeStyles(styles)

const ID_DASHBOARD = 'Inicio'

const setActiveLink = (location, menu) => {
  return menu.map(i => {
    if(i.dropdown){
      let opened = i.children.some( c => location.includes(c.link))
      let children = i.children.map(c => (
        {...c, active: location.includes(c.link)}
      ))
      return {...i, children, opened}
    }
    if(i.id === ID_DASHBOARD)
      return {...i, active: location === '/home'}
    return {...i, active: location.includes(i.link)}
  })
}

const Navigator = props => {
  
  const classes = useStyles()

  const { open, setOpen, location } = props

  const [ itemList, setItemList ] = useState(navigationMenu)
  const context = useContext(Store)

  useEffect(() => {
    setItemList(prevMenu => setActiveLink(location.pathname, prevMenu))
  }, [location.pathname])

  const handleSetOpen = id => { // open or close dropdown items
    let mappedList = itemList.map(item => (
      item.id === id ? {...item, opened: open? !item.opened : true} : {...item, opened: false}
    ))
    setItemList(mappedList)
    setOpen(true)
  }

  const [isAlertOpen, setIsAlertOpen] = useState(false)

  const handleLogout = (type) => {
    if(type === 'accept') {
      setIsAlertOpen(false)
      authService.logout()
      context.handleSetUser(null)  
    }
    if(type === 'cancel') {
      setIsAlertOpen(false)
    }
  }

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleLogout
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleLogout
    }  
  ]

  const openAlert = () => {  
    setIsAlertOpen(true)
  }

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: open,
        [classes.drawerClose]: !open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        }),
      }}
    >
      <Toolbar/>
      <List>
        <MakeNavItems items={itemList} handleSetOpen={handleSetOpen} isDrawerOpen={open}/>
        <ListItem button key={"logout"} onClick={openAlert} divider style={{paddingLeft: theme.spacing(3)}}>
          <Tooltip title="Cerrar sesión">
            <ListItemIcon><ExitToAppIcon style={{color: '#F29222'}}/></ListItemIcon>
          </Tooltip>
          <ListItemText primary={"Cerrar sesión"} primaryTypographyProps={{noWrap: true}}/>
        </ListItem>
      </List>
      {isAlertOpen && <AlertAction
          isOpen={isAlertOpen}
          setIsOpen={setIsAlertOpen}
          actions={actions}
          type='doubt'
          title={"¿Cerrar sesión?"}
          loading={false}
        />}
    </Drawer>
  )
}

export default Navigator
