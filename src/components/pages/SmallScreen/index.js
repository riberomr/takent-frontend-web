import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import { Typography } from '@material-ui/core'
import Button from '../../ui/generalUI/Buttons/PrimaryButton'
import { ThemeProvider} from '@material-ui/core/styles'
import { theme } from '../../../constants/generalConstants/theme'

const useStyles = makeStyles(styles)

const SmallScreen = () => {

    const classes = useStyles()
    return(
        <ThemeProvider theme={theme}>
            <div className={classes.container}>
                <Typography variant="body1" className={classes.title}>
                    Muchas gracias por elegirnos para encontrar ese trabajo que tanto buscás
                </Typography>
                <img className={classes.imgLogo} src="/images/logo-maletin-solo-takent.png" alt="takent" />
                <Typography variant="body1" className={classes.subtitle}>
                    Para continuar, bajate la app de Takent y empezá hoy mismo a ser parte de la familia!
                </Typography>
                <Button 
                    variant="contained"
                    color="primary"
                >
                        Descargar
                </Button>
            </div>
        </ThemeProvider>
    )
}

export default SmallScreen