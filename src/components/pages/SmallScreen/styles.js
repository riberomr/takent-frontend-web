export const styles = {
    imgLogo: {
        width: '250px',
        height: '215px',
        paddingLeft: '35px'
    },
    container:{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center'
    },
    title: {
        padding: '35px',
        fontWeight: 'bold'

    },
    subtitle:{
        padding: '35px'
    }
}