import React, { useEffect, useState } from 'react'
import {
  Drawer,
  List
} from '@material-ui/core'
import MakeNavItems from '../../ui/generalUI/Drawer/MakeNavItems'
import { navigationMenu } from '../../../constants/generalConstants/navigationMenu'
import { omit } from 'lodash'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'

import useMediaQuery from '@material-ui/core/useMediaQuery'

const useStyles = makeStyles(styles)

const ID_DASHBOARD = 'Inicio'

const setActiveLink = (location, menu) => {
  return menu.map(i => {
    if(i.dropdown){
      let opened = i.children.some( c => location.includes(c.link))
      let children = i.children.map(c => (
        {...c, active: location.includes(c.link)}
      ))
      return {...i, children, opened}
    }
    if(i.id === ID_DASHBOARD)
      return {...i, active: location === '/home'}
    return {...i, active: location.includes(i.link)}
  })
}

const Navigator = (props) => {
  let { location, setDrawerWidth, drawerWidth } = props

  const newProps = omit(props, 'setDrawerWidth', 'drawerWidth')
  const isXSSize = useMediaQuery('(max-width:600px)')
  const [itemList, setItemList] = useState(navigationMenu)
  const classes = useStyles({isXSSize})
  const [isOpenDrawer, setIsOpenDrawer] = useState(true)
  const handleSetOpen = id => { // open or close dropdown items
    let mappedList = itemList.map(item => (
      item.id === id ? {...item, opened: !item.opened} : item
    ))
    setItemList(mappedList)
  }

  useEffect(() => { // set a link "active" when url matches with link url redirection
    setItemList(prevMenu => setActiveLink(location.pathname, prevMenu))
  }, [location.pathname])

  const handleChangeWidth = () =>{
    setIsOpenDrawer(!isOpenDrawer)
    if(drawerWidth===256) setDrawerWidth(62)
    else setDrawerWidth(256)
  }

  return (
    <Drawer
      classes={{paper: classes.paper}}
      variant="permanent"
      {...newProps}
    >
      {!isXSSize && <div className={classes.drawerDiv} onClick={() => handleChangeWidth()}>
          {isOpenDrawer && <div className={classes.menuLabelContainer}>
            <span className={classes.spanMenu}>Menú</span>
          </div>}
          <div className={classes.drawerButtonContainer}>
            <Button>
              {!isOpenDrawer &&<ArrowForwardIosIcon />}
              {isOpenDrawer && <ArrowBackIosIcon />}
            </Button>
          </div>
        </div>
      }
      {isOpenDrawer &&<List>
        <MakeNavItems
          items={itemList} 
          handleSetOpen={handleSetOpen}
        />
      </List>}
    </Drawer>
  )
}

export default Navigator