export const styles = { 
  drawerHeader:{
    display: 'flex',
    padding: '20px'
  },
  imgDrawerHeader:{
    display: 'block',
    width: '94px',
    height: '46px'
  },
  paper: {
    background: '#FBFBFB',
    top: ({isXSSize}) => {
      return (isXSSize ? '48px' : 'unset')
    },
    width: ({isXSSize}) => {
      return (isXSSize ? '24px' : '256px')
    }
  },
  navMenuBackground: {
    background: '#E6E6E6'
  },
  drawerDiv: {
    display: 'flex',
    justifyContent: 'flex-end',
    paddingLeft: '10px'
  },
  menuLabelContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '70%',
    justifyContent: 'center'
  },
  drawerButtonContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: 'auto'
  },
  spanMenu: {
    color: '#6F6F6F',
    fontSize: 20,
    fontWeight: 'bold'
  }
}
