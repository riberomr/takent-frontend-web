import React from 'react'
import EnterpriseUserProfileWrapper from '../../../wrappers/EnterpriseUser/Profile'

const PAGE_ENTERPRISE_USER_PROFILE = (props) => {
    return (
        <EnterpriseUserProfileWrapper {...props} />
    )
}

export default PAGE_ENTERPRISE_USER_PROFILE
