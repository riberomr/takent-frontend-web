import * as Yup from 'yup'
import { differenceInYears, isAfter } from 'date-fns'

const today = new Date()

const commonUserProfile = Yup.object().shape({
    name:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo nombre'),
    surname:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo apellido'),
    birth_date: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo fecha')
        .test(
            "DOB",
            "Debés ser mayor de 18 años",
            value => {
            return differenceInYears(today, new Date(value)) >= 18
            })
        .test("DOB", "El año tiene que ser mayor a 1900", value => {
            return isAfter(new Date(value), new Date("01/01/1900"))
        }),
    phone:
        Yup
        .string().matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "No se cumple el formato de número telefónico de 10 dígitos")
        .nullable()
        .required("Debés completar el campo teléfono")
        
})

export default commonUserProfile