import * as Yup from 'yup'
import { differenceInYears, isAfter } from 'date-fns'

const today = new Date()
const decimal =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,15}$/

const newUserForm = Yup.object().shape({
    email:
        Yup
        .string()
        .nullable()
        .email('Formato incorrecto. Ejemplo: takent@mail.com')
        .required('Debés ingresar tu correo electrónico'),
    password:
        Yup
        .string()
        .nullable()
        .required('Debés ingresar una contraseña')
        .test(
            "PASS",
            "La contraseña debe contener al menos una mayúscula, una minúscula, un número, y entre 8 a 15 caracteres",
            value => {
              return value? value.match(decimal) : null
        }),
    password2: 
        Yup
        .string()
        .nullable()
        .oneOf([Yup.ref('password'), null], 'Las contraseñas no coinciden')
        .required('Es necesario confirmar la contraseña'),
    lastName: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo apellido'),
    name: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo nombre'),
    birthDay: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo fecha')
        .test(
            "DOB",
            "Debés ser mayor de 18 años",
            value => {
              return differenceInYears(today, new Date(value)) >= 18
            })
        .test("DOB", "El año tiene que ser mayor a 1900", value => {
            return isAfter(new Date(value), new Date("01/01/1900"))
        }),
    phone:
        Yup
        .string().matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "No se cumple el formato de número telefónico de 10 dígitos")
        .nullable()
        .required("Debés completar el campo teléfono")
        .max(10)
})

export default newUserForm