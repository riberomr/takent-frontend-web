import * as Yup from 'yup'

const CompanyNewUserForm = Yup.object().shape({
  title: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo título")
    .nullable(),
  position: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo puesto")
    .nullable(),
  experience:
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés seleccionar un nivel de experiencia")
    .nullable(),
  contract_type:
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés seleccionar un tipo de contrato")
    .nullable(),
  language:
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés seleccionar un idioma")
    .nullable(),
  description:
    Yup
    .string()
    .max(1500, "Como máximo 1500 caracteres para este campo")
    .nullable()
})

export default CompanyNewUserForm