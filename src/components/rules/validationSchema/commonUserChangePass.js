import * as Yup from 'yup'

const decimal =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,15}$/

const commonUserChangePass = Yup.object().shape({
    pass:
        Yup
        .string()
        .required('Debés ingresar tu contraseña actual')
        .nullable(),
    newPass:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo nueva contraseña')
        .test(
            "PASS",
            "La contraseña debe contener al menos una mayúscula, una minuscula, un número, y debe tener entre 8 a 15 caracteres",
            value => {
              return value? value.match(decimal) : null
        }),
    repeatNewPass: 
        Yup
        .string()
        .nullable()
        .oneOf([Yup.ref('newPass'), null], 'Las contraseñas no coinciden')
        .required('Es necesario confirmar la contraseña'),
})

export default commonUserChangePass