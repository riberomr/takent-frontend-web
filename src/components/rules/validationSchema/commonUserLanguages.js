import * as Yup from 'yup'

const commonUserLanguages = Yup.object().shape({
    language:
        Yup
        .string()
        .required('Debés seleccionar un idioma')
        .nullable(),
    communication_level:
        Yup
        .string()
        .required('Debés seleccionar el nivel de comunicación')
        .nullable(),
    writing_level:
        Yup
        .string()
        .required('Debés seleccionar el nivel de escritura')
        .nullable(),
    description:
        Yup
        .string()
        .max(300, 'Como máximo 300 caracteres para este campo')
        .nullable()
})

export default commonUserLanguages