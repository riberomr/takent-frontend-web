import * as Yup from 'yup'

const AddressForm = Yup.object().shape({
  province: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo provincia")
    .nullable(),
  city: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo localidad")
    .nullable(),
  street: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo calle")
    .nullable(),
  // number: 
  //   Yup
  //   .number()
  //   .required("Debés completar el campo número")
  //   .nullable(),
})

export default AddressForm