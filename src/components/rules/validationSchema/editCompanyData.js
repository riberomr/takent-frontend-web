import * as Yup from 'yup'

export const editCompanyData = Yup.object().shape({
    business_name:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo Razón social'),
    fantasy_name:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo'),
    sector:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo sector'),
    enterprise_type:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo tipo de empresa'),
    contact_email:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .email('Formato incorrecto. Ejemplo: takent@mail.com')
        .required('Debés ingresar un correo electrónico de contacto'),
    enterprise_phone:
        Yup
        .string().matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "No es un teléfono válido, ingrese un teléfono sin 0 en la caracteristica y sin 15 en el número")
        .nullable()
        .required("Debés ingresar un numero de teléfono para tu empresa"),
    description: 
        Yup
        .string()
        .nullable()
        .max(1500, 'Como máximo 1500 caracteres para este campo'),
    province:
        Yup
        .string()
        .nullable()
        .required('Debés seleccionar una provincia'),
    city:
        Yup
        .string()
        .nullable()
        .required('Debés seleccionar una localidad'),
    street:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés ingresar una calle'),
    web_page:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo'),
    district:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
})