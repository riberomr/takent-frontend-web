import * as Yup from 'yup'
import { differenceInYears, isAfter } from 'date-fns'

const today = new Date()

const commonUserProfile = Yup.object().shape({
    name:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo Nombre')
        .min(2, "Mínimo 2 caractéres")
        .max(100, "Como máximo 100 caracteres para este campo"),

    surname:
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo Apellido')
        .min(2, "Mínimo 2 caractéres")
        .max(100, "Como máximo 100 caracteres para este campo"),

    birth_date: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo Fecha')
        .test(
            "DOB",
            "Debés ser mayor de 18 años",
            value => {
            return differenceInYears(today, new Date(value)) >= 18
            })
        .test("DOB", "El año tiene que ser mayor a 1900", value => {
            return isAfter(new Date(value), new Date("01/01/1900"))
        }),

    phone:
        Yup
        .string().matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "Debés ingresar el teléfono sin 0 ni 15")
        .nullable()
        .required("Debés completar el campo Teléfono"),

    position:
        Yup
        .string()
        .nullable()
        .min(2, "Mínimo 2 caractéres")
        .max(100, "Como máximo 100 caracteres para este campo"),
        
})

export default commonUserProfile