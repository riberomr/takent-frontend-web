import * as Yup from 'yup'

const limitFistDate = new Date('1900-01-01')
const limitLastDate = new Date('9999-12-31')
const today = new Date()

const commonUserExperience = Yup.object().shape({
    company:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo empresa'),
    position:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo puesto'),
    start_date: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo fecha inicio')
        .test(
             "LimitDate",
             "La fecha inicio debe estar entre 1900 y la actualidad",
                value => {
                    if (limitFistDate >= new Date(value)) return limitFistDate <= new Date(value)
                    if (today <= new Date(value)) return  today >= new Date(value)
                    else return limitFistDate <= new Date(value)
             }),
    end_date:
        Yup
        .date()
        .test(
            "LimitDate",
            "Formato inválido de fecha",
            value => {
                if(value === '' || value == null) return true

                return limitLastDate >= new Date(value)
            })
        .nullable()
        .when(
            'start_date',
            (start_date, schema) => { return start_date && schema.min(start_date, 'La fecha de inicio debe ser menor a la de finalización') 
        })
        .when('checkPresentEndDate', {
          is: false,
          then: () => Yup.date().required("Debés completar el campo fecha de finalización")
        }),
    description:
        Yup
        .string()
        .max(300, 'Como máximo 300 caracteres para este campo')
        .nullable(),
    nameReference:
        Yup
        .string()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .nullable(),
    phoneReference:
        Yup
        .string().matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "No se cumple el formato de número telefónico de 10 dígitos")
        .nullable(),
    checkPresentEndDate:
        Yup.boolean()
})

export default commonUserExperience