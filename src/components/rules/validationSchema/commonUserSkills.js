import * as Yup from 'yup'

const commonUserSkills = Yup.object().shape({
    name:
        Yup
        .string()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo aptitud')
        .nullable(),
    description:
        Yup
        .string()
        .max(300, 'Como máximo 300 caracteres para este campo')
        .nullable(),
})

export default commonUserSkills