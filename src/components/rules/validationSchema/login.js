import * as Yup from 'yup'

const login = Yup.object().shape({
    email:
        Yup
        .string()
        .email('Formato incorrecto. Ejemplo: takent@mail.com')
        .required('Debés ingresar tu correo electrónico'),
    password:
        Yup
        .string()
        .required('Debés ingresar tu contraseña'),
})

export default login
