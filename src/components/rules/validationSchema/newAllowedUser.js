import * as Yup from 'yup'
import { differenceInYears, isAfter } from 'date-fns'

const today = new Date()
const decimal =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,15}$/

const newUserForm = Yup.object().shape({
    account:
        Yup
        .string()
        .nullable()
        .email('Formato incorrecto. Ejemplo: takent@gmail.com')
        .required('Debés ingresar un correo electrónico'),
    password:
        Yup
        .string()
        .nullable()
        .required('Debés ingresar una contraseña')
        .test(
            "PASS",
            "La contraseña debe contener al menos una mayúscula, una minúscula, un número, y entre 8 a 15 caracteres",
            value => {
              return value? value.match(decimal) : null
        }),
    surname: 
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo Apellido'),
    name: 
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo')
        .required('Debés completar el campo Nombre'),
    position:
        Yup
        .string()
        .nullable()
        .max(100, 'Como máximo 100 caracteres para este campo'),
    birth_date: 
        Yup
        .string()
        .nullable()
        .required('Debés completar el campo Fecha')
        .test(
            "DOB",
            "Debés ser mayor de 18 años",
            value => {
              return differenceInYears(today, new Date(value)) >= 18
            })
        .test("DOB", "El año tiene que ser mayor a 1900", value => {
            return isAfter(new Date(value), new Date("01/01/1900"))
        }),
    phone:
        Yup
        .string()
        .required("Debés completar el campo Teléfono")
        .matches(/^(?:(?:00)?549?)?(?:11|[2368]\d)(?:(?=\d{0,2}15)\d{2})??\d{8}$/, "No se cumple el formato de número telefónico de 10 dígitos")
})

export default newUserForm