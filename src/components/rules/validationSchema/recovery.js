import * as Yup from 'yup'

const recovery = Yup.object().shape({
    account:
        Yup
        .string()
        .email('El usuario debe ser de formato email')
        .required('Debés ingresar su email de usuario'),
})

export default recovery