import * as Yup from 'yup'

const CommonUserNewServiceForm = Yup.object().shape({
  title: 
    Yup
    .string()
    .max(100, "Como máximo 100 caracteres para este campo")
    .required("Debés completar el campo título")
    .nullable(),
  description:
    Yup
    .string()
    .max(1500, "Como máximo 1500 caracteres para este campo")
    .required("Debés completar el campo descripción")
    .nullable(),
  sector:
    Yup
    .string()
    .required("Debés completar el campo rubro")
    .nullable(),
  cost:
    Yup
    .number()
    .max(999999, "El costo no puede superar los $999.999")
    .required("Debés completar el campo costo")
    .nullable()
})

export default CommonUserNewServiceForm
