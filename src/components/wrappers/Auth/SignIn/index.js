import React, { useState, useEffect} from 'react'
import SingInForm from '../../../ui/businessUI/AuthForms/SignInForm'
import SingInCompany from '../../../ui/businessUI/AuthForms/SignInCompany'
import { apiTakent } from '../../../../services/API-TAKENT'

const SignInWrapper = (props) => {

    const { type } = props

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [provinces, setProvinces] = useState(null)
    const [cities, setCities] = useState([])
    const [idProvince, setIdProvince] = useState(null)
    const [sectors, setSectors] = useState(null)
    const enterpriseTypes = ['Reclutamiento','Empleador directo','Servicios temporales']

    const [ loadingPost, setLoadingPost ] = useState(false)

    useEffect(()=>{
        if(!provinces)
            apiTakent.getProvinces()
                .then(res => setProvinces(res.data))
                .catch(() => setProvinces([]))
        if(idProvince)
            apiTakent.getCities(idProvince)
                .then(res => setCities(res.data))
                .catch(() => setCities([]))
        if(!sectors && type === 'COMPANY')
                apiTakent.getSectors()
                .then((response) => {
                  setSectors(response.data.map(item => {
                    return item._id
                  }))})
            
    },[provinces, idProvince, sectors, type])

    const submitCommonUser = (commonUser) => {
      setLoadingPost(true)
      apiTakent.postCommonUser(commonUser)
      .then(() => {
          setIsSubmitted('OK')
      })
      .catch(() => {
          setIsSubmitted('ERROR')
      })
      .finally(() => setLoadingPost(false))
    }
    const submitCompanyUser = (companyUser) => {
      setLoadingPost(true)
      apiTakent.postCompanyUser(companyUser)
      .then(()=>{
          setIsSubmitted('OK')
      })
      .catch(()=>{
          setIsSubmitted('ERROR')
      })
      .finally(() => setLoadingPost(false))
    }
    const submitNewUser = type==='USER'? submitCommonUser : submitCompanyUser
    return(
        <>
        {type === 'USER' && <SingInForm 
            type={type} 
            submitNewUser={submitNewUser} 
            isSubmitted={isSubmitted} 
            provinces={provinces}
            cities={cities}
            setIdProvince={setIdProvince}
            loadingSubmit={loadingPost}
        />}
        {type === 'COMPANY' && <SingInCompany
            type={type} 
            submitNewUser={submitNewUser} 
            isSubmitted={isSubmitted} 
            provinces={provinces}
            cities={cities}
            setIdProvince={setIdProvince}
            sectors={sectors}
            enterpriseTypes={enterpriseTypes}
            loadingSubmit={loadingPost}
        />}
        </>
    )
}

export default SignInWrapper