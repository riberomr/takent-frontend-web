import React, { useEffect, useState } from 'react'
import queryString from 'query-string'
import { apiTakent } from '../../../../services/API-TAKENT'
import RecoverUserForm from '../../../ui/businessUI/AuthForms/RecoverUserForm'

const RecoverUserWrapper = props => {

  const { location } = props

  const [ user, setUser ] = useState(null)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const activate = () => {
      setLoading(true)
      if(!location.search)
        return
      const search = queryString.parse(location.search)
      if(!search.ticket)
        return
      apiTakent.getRecoverAccount(search.ticket)
      .then(response => {
        setUser(response.data.user)
      })
      .finally(()=>setLoading(false))
    }
    activate()
  }, [location])

  return (
    <RecoverUserForm user={user} loading={loading}/>
  )

}

export default RecoverUserWrapper
