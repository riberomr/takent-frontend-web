import React, { useState } from 'react'
import RecoveryPasswordForm from '../../../ui/businessUI/AuthForms/RecoveryPasswordForm'
import { apiTakent } from '../../../../services/API-TAKENT'

const RecoveryPasswordWrapper = () => {

  const [isSubmitted, setIsSubmitted] = useState(false)

  const handleSubmitEmail = (body) => {
    apiTakent.postRecoveryPassword(body)
      .then(() => setIsSubmitted('OK'))
      .catch(() => setIsSubmitted('ERROR'))
  }
  return (
      <RecoveryPasswordForm isSubmitted={isSubmitted}
        handleFormSubmit={handleSubmitEmail}
      />
   )
}

export default RecoveryPasswordWrapper