import React, { useEffect, useState } from 'react'
import ActivateUserForm from '../../../ui/businessUI/AuthForms/ActivateUserForm'
import queryString from 'query-string'
import { apiTakent } from '../../../../services/API-TAKENT'

const statuses = {
  PENDING: 0,
  SUCCESS: 1,
  ERROR: 2,
  INVALID_LINK: 3
}

const ActivateUserWrapper = props => {

  const { location } = props

  const [ user, setUser ] = useState(null)
  const [ status, setStatus ] = useState(statuses.PENDING)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const activate = () => {
      setLoading(true)
      if(!location.search) {
        setStatus(statuses.INVALID_LINK)
        setLoading(false)
        return
      }
      const search = queryString.parse(location.search)
      if(!search.ticket) {
        setStatus(statuses.INVALID_LINK)
        setLoading(false)
        return
      }
      apiTakent.activateUser(search.ticket)
      .then(response => {
        setUser(response.data.user)
        setStatus(statuses.SUCCESS)
      })
      .catch(error => {
        if(error.response?.status === 400) {
          setStatus(statuses.INVALID_LINK)
        }
        else {
          setStatus(statuses.ERROR)
        }
      })
      .finally(() => setLoading(false))
    }
    activate()
  }, [location])

  const getAlertPath = () => {
    let alert = null
    switch(status) {
      case statuses.SUCCESS:
        alert = "success"
        break
      case statuses.ERROR:
        alert = "error"
        break
      case statuses.INVALID_LINK:
        alert = "doubt"
        break
      default:
        alert = "error"
    }
    return `/images/AlertIcons/${alert}.png`
  }

  const getMessage = () => {
    let result = null
    switch(status) {
      case statuses.SUCCESS:
        result = `¡Usuario ${user} activado con éxito!`
        break
      case statuses.ERROR:
        result = "Error al activar usuario"
        break
      case statuses.INVALID_LINK:
        result = "Link de activación no válido o expirado"
        break
      default:
        result = "Error al activar usuario"
    }
    return result
  }

  return (
    <ActivateUserForm
      user={user}
      alert={getAlertPath()}
      message={getMessage()}
      loading={loading}
    />
  )

}

export default ActivateUserWrapper
