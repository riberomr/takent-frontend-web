import React, { useState, useContext } from 'react'
import LoginForm from '../../../ui/businessUI/AuthForms/LoginForm'
import { Store } from '../../../../Store'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { authService } from '../../../../services/auth.service'
import { apiTakent } from '../../../../services/API-TAKENT'
import RecoveryUserDialog from '../../../ui/businessUI/AuthForms/RecoveryUserDialog'

const useStyles = makeStyles(styles)

const LoginWrapper = (props) => {
  
  const context = useContext(Store)
  const [isError, setIsError] = useState(false)
  const [recoveryDialogOpen, setRecoveryDialogOpen] = useState(false)
  const [user, setUser] = useState(false)
  const isXSSize = useMediaQuery('(max-width:600px)')
  const classes = useStyles({isXSSize})

  const [ loading, setLoading ] = useState(false)

  const handleLoginFormSubmit = values => {
    setLoading(true)
    setUser(values.email)
    authService.login(values.email, values.password)
    .then(() => {
      authService.getUser()
      .then(res => {
        context.handleSetUser(res.data)
        return res
      })
      .then(prevRes => {
        if (!prevRes.permission && !prevRes.data.permission) {
          apiTakent.getProfilePhotoCommonUser()
            .then(res => {
              context.handleSetUser({ ...prevRes, profile_photo: res.data.profile_photo })
            })
        }
      })
    })
    .catch((error) => {
      if(error?.response?.data?.state === "deleted") {
        setRecoveryDialogOpen(true)
      }
      else {
        setIsError(true)
      }
    })
    .finally(() => setLoading(false))
  }

  return (
    <div className={classes.content}>
      <LoginForm 
        isError={isError}
        setIsError={setIsError}
        handleSubmit={handleLoginFormSubmit}
        loading={loading}
      />
      <RecoveryUserDialog
        open={recoveryDialogOpen}
        setOpen={setRecoveryDialogOpen}
        user={user}
      />
    </div>
   )
}

export default LoginWrapper