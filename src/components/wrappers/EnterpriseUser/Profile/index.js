import React, { useState, useEffect, useContext } from 'react'
import Profile from '../../../ui/businessUI/EnterpriseUser/Profile'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const EnterpriseUserProfileWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [userData, setUserData] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        apiTakent.getEnterpriseUserData()
            .then(res => { setUserData(res.data) });;
    }, [context, loading])


    const putFormUser = (values) => {
        apiTakent.putEnterpriseUserData(values)
            .then(() => enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' }))
            .catch(() => enqueueSnackbar('Error al intentar cambiar los datos', { variant: 'error' }))
    }

    return (
        <Profile {...props}
            handleFormSubmit={putFormUser}
            userData={userData}
            loading={loading}
            setLoading={setLoading}
        />
    )
}
export default EnterpriseUserProfileWrapper