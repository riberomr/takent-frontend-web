import React, { useState, useEffect, useContext } from 'react'
import Languages from '../../../ui/businessUI/CommonUser/Languages'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const CommonUserExperienceWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [userLanguages, setUserLanguages] = useState(null)
    const [flagReload, setFlagReload] = useState(false)
    const [languages, setLanguages] = useState([])
    const [languageLevels, setLanguageLevels] = useState([])
    // const [loading, setLoading] = useState(true)

    useEffect(()=>{
        Promise.all([apiTakent.getLanguages(), apiTakent.getLanguageLevels()])
        .then((res)=>{
            setLanguages(res[0].data)
            setLanguageLevels(res[1].data)
        })
    },[])

    useEffect(()=>{
        const loadUserLanguages = () => {
            // setLoading(true)
            apiTakent.getCommonUserLanguages()
                .then(res => { setUserLanguages(res.data)})
                // .finally(setLoading(false))
        }
        if(languages) loadUserLanguages()
    },[context, flagReload, languages])


    const putFormUser = (values) => {
        // setLoading(true)
        apiTakent.postCommonUserLanguages(values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Idioma cargado con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cargar idioma', { variant: 'error' }))
            // .finally(setLoading(false))
    }

    const deleteLanguage = (id) => {
        // setLoading(true)
        apiTakent.deleteCommonUserLanguages(id)
        .then(()=> {
            setFlagReload(!flagReload)
            enqueueSnackbar('Idioma eliminado', { variant: 'success' })
        })
        .catch(() => enqueueSnackbar('Error al intentar eliminar idioma', { variant: 'error' }))
        // .finally(setLoading(false))   
    }

    const editLanguage = (id, values) => {
        // setLoading(true)
        apiTakent.putCommonUserLanguages(id, values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar eliminar idioma', { variant: 'error' }))
            // .finally(setLoading(false))    
    }

    return(
        <Languages {...props}
            // loading={loading}
            languages={languages}
            languageLevels={languageLevels}
            handleFormSubmit={putFormUser}
            userLanguages={userLanguages}
            deleteLanguage={deleteLanguage}
            editLanguage={editLanguage}
        />
    )
 }
 export default CommonUserExperienceWrapper