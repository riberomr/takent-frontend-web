import React, { useEffect, useState } from 'react'
// import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../../services/API-TAKENT'
import { differenceInCalendarDays, format } from 'date-fns'
import { cleanString } from '../../../../../helpers/customHelpers'
import OfferList from '../../../../ui/businessUI/CommonUser/Offers/OfferList'
import queryString from 'query-string'

const OfferListWrapper = (props) => {
    const [offers, setOffers] = useState([])
    const [totalElements, setTotalElements] = useState(0)
    const [totalPages, setTotalPages] = useState(0)
    const [searcherInputValue, setSearcherInputValue] = useState('')
    const [filters, setFilters] = useState(null)
    const [lodaing, setLoading] = useState(false)
    const [postulationChanged, setPostulationChanged] = useState(null)

    // useEffect(() => {
    //   setOffers(prevState => {
    //     let newState = [...prevState]
    //     let offer = newState.find(offer => offer._id === postulationChanged)
    //     if(offer){
    //       offer.postulated = true
    //     }
    //     return newState
    //   })
    // }, [postulationChanged])

    useEffect(() => {
      let locationParams = {}
      if(props.location?.search) {
        locationParams = queryString.parse(props.location.search)
        const { latitude, longitude, radius, filter_by_preferred } = locationParams
        if(latitude && longitude && radius && filter_by_preferred) {
          locationParams = {
            location: `${latitude},${longitude},${radius}`,
            filter_by_preference: filter_by_preferred
          }
        }
      }
      let options = {sort: "random", ...locationParams}
      if(filters) {
        options = {...options, ...filters}
      }
      setLoading(true)
      apiTakent.getOffers(options)
        .then((res) => {
          let mappedData = res.data.map((element) => {
            return {
              ...element,
              publishedDays: differenceInCalendarDays(new Date(), new Date(element.publication_date)),
              publication_date_formatted: format(new Date(element.publication_date),'dd/MM/yyyy'),
              enterPriseNameForFilter: element.enterprise.fantasy_name? element.enterprise.fantasy_name : element.enterprise.bussines_name
          }})
          if(searcherInputValue && searcherInputValue !== ''){
            mappedData = mappedData.filter((element) => (cleanString(element.title).includes(cleanString(searcherInputValue))
            || cleanString(element.enterPriseNameForFilter).includes(cleanString(searcherInputValue)))
            )
          }
          setOffers(mappedData)
          setTotalElements(mappedData.length)
          setTotalPages(Math.ceil(mappedData.length/5))
        })
        .finally(() => setLoading(false))
        // eslint-disable-next-line
      }, [searcherInputValue, filters, postulationChanged])
    
    const handleChangeFilters = (filters) => {
      if(filters !== {} ) setFilters(filters)
      else setFilters(null)
    }
    return(
        <>
          <OfferList
            loading={lodaing} 
            offers={offers}
            totalPages={totalPages}
            totalElements={totalElements}
            searcherInputValue={searcherInputValue}
            setSearcherInputValue={setSearcherInputValue}
            handleChangeFilters={handleChangeFilters}
            setPostulationChanged={setPostulationChanged}
          />
        </>
    )
}

export default OfferListWrapper