//----- React Core Imports ------
import React, {useState, useEffect} from 'react'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import Dialog from '@material-ui/core/Dialog'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
//----- Custom Components -------
import { DialogActions } from './DialogActions'
import { DialogContent } from './DialogContent'
import { DialogTitle } from './DialogTitle'
import CustomCarousel from '../../../../../ui/generalUI/CustomCarousel'
// ----- Styles and MakeStyles ------
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
// ----- Helpers ------
import { publishedDaysOffer } from '../../../../../../helpers/customHelpers/publishedDays'
// ----- API Calls ------
import { apiTakent } from '../../../../../../services/API-TAKENT'
import GoogleMap from '../../../../../ui/generalUI/GoogleMaps/GoogleMap'
import { theme } from '../../../../../../constants/generalConstants/theme'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

const useStyles = makeStyles(styles)

const isRemote = (flag) => {
  return flag? 'Si' : 'No'
}

export const ShowOffer =(props)=> {

  const { data, open, setOpen, avatar, postulatedCommonUser } = props
  const [images, setImages]=useState([])
  const [enterpriseImages, setEnterpriseImages] = useState([])
  const { 
    title='',
    position='',
    enterprise={fantasy_name:'', business_name:''},
    description='-',
    address={city:{name:''}, province:{name:''}},
    publication_date,
    sector, 
    contract_type, 
    experience, 
    language={language: "", certificate: false},
    postulated,
    remote
  } = data //Desestructuración de props según respuesta del Endpoint

    const [showEnterprise, setShowEnterprise] = useState(false)
    const classes = useStyles()

    const handleClose = () => {
        setOpen(false)
        setShowEnterprise(false)
    };

    const handleConfirm = () => {
      postulatedCommonUser(data._id)
      setOpen(false)
    }

  useEffect(() => {
    if(open) {
    apiTakent.getOfferImages(data._id)
      .then(res => {
        setImages(res.data)
      })}
  },[data._id, open])

  useEffect(() => {
    if(open && showEnterprise) {
    apiTakent.getEnterpriseImages(data.enterprise._id)
      .then(res => {
        setEnterpriseImages(res.data)
      })}
  },[data.enterprise, open, showEnterprise])
  
    return (
      <>
        <div className={classes.container}>
         <Dialog classes={{ root: classes.container }} onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
            {showEnterprise &&
            <Tooltip title="Ir a la oferta">
              <IconButton onClick={()=>setShowEnterprise(false)} className={classes.goBack}>
                <ArrowBackIcon />
              </IconButton>
            </Tooltip>
            }
            <DialogTitle id="customized-dialog-title" onClose={handleClose}>
              {!showEnterprise && <div className={classes.containerTitle}>
                <div className={classes.containerAvatarTextUbication}>
                 <div className={classes.containerAvatarAndText}>
                   <div className={classes.containerAvatar} onClick={()=>setShowEnterprise(true)}>
                    <Tooltip title="Ver empresa">
                      <Avatar alt="Picture" src={avatar} className={classes.avatarPointer} variant="square"></Avatar>
                    </Tooltip>
                   </div>
                   <div className={classes.containerText}>
                     <div style={{fontSize: '24px'}}>{title}</div>
                     <Tooltip title="Ver empresa">
                      <div className={classes.titleCompanyPointer} style={{display: "flex", alignItems: "center", justifyContent: "flex-start"}} onClick={()=>setShowEnterprise(true)}>
                        {enterprise.verified && 
                          <CheckCircleIcon
                            fontSize="small"
                            style={{marginRight: theme.spacing(1), color: "#1DA1F2"}}
                          />}
                        {enterprise.fantasy_name !== ''? enterprise.fantasy_name : enterprise.business_name}
                      </div>
                     </Tooltip>
                   </div>
                 </div>
                 <div className={classes.containerUbiAndPublic}>
                    <div className={classes.textUbication}>
                      {address.city.name} - {address.province.name} <LocationOnIcon className={classes.locationIcon}/>
                    </div> 
                  <div className={classes.textPublication}> {publishedDaysOffer(publication_date)} </div>
                 </div>
                </div>
              </div>}
              {showEnterprise &&<div className={classes.containerTitle}>
                <div className={classes.containerAvatarTextUbication}>
                 <div className={classes.containerAvatarAndText}>
                   <div className={classes.containerAvatar} >
                      <Avatar alt="Picture" src={avatar} className={classes.avatar} variant="square"></Avatar>
                   </div>
                   <div style={{display: "flex", justifyContent: "flex-start", alignItems: "center", width: "70%"}}>
                    {enterprise.verified && 
                      <Tooltip title="Empresa verificada">
                        <CheckCircleIcon
                          fontSize="small"
                          style={{marginRight: theme.spacing(1), color: "#1DA1F2"}}
                        />
                      </Tooltip>}
                     <div>{enterprise.fantasy_name !== ''? enterprise.fantasy_name : enterprise.business_name}</div>
                   </div>
                 </div>
                </div>
              </div>}
            </DialogTitle>
              <div className={classes.horizontalLine}></div>
              <DialogContent className={classes.dialogContent} dividers>
                {!showEnterprise && <>
                  <Typography gutterBottom>
                    <div className={classes.firstBlockParagraph}>
                      <div className={classes.sourceForSubTitles}>Puesto
                        <div className={classes.contentForSubTitle}>{` ${position}`}</div>
                      </div>
                    </div>
                    <div className={classes.firstBlockParagraph}>
                      <div className={classes.sourceForSubTitles}>Trabajo remoto
                        <div className={classes.contentForSubTitle}>{` ${isRemote(remote)}`}</div>
                      </div>
                    </div>
                  <div className={classes.firstBlockParagraph}>
                    <div className={classes.sourceForSubTitles}>Rubro 
                      <div className={classes.contentForSubTitle}>{` ${sector}`}</div>
                    </div>

                   <div className={classes.sourceForSubTitles}>Tipo de contrato
                    <div className={classes.contentForSubTitle}>{` ${contract_type}`}</div>
                   </div>
                  </div>  
                </Typography>
                <Typography gutterBottom>
                    <div className={classes.firstBlockParagraph}>
                      <div className={classes.sourceForSubTitles}>Descripción
                        <p className={classes.paragraph} >
                            { description==='' ? '-' : description}
                        </p>
                      </div>
                        </div>
                        <div className={classes.sourceForSubTitles}>Requisitos</div>
                        <div className={classes.request}>Experiencia:
                          <div className={classes.contentForSubTitle}>{` ${experience}`}</div>
                        </div>

                        <div className={classes.secondBlockParagraph}>
                            <div className={classes.sourceForSubTitles}>Idioma:
                                <div className={classes.contentForSubTitle}>{` ${language.language}`}</div>
                            </div>
                            <div className={classes.sourceForSubTitles}>Certificado:
                                <div className={classes.contentForSubTitle}> {language.certificate? "Si" : "No"}</div>
                            </div>
                        </div>
                        {data.address && 
                          <Typography gutterBottom style={{marginBottom: theme.spacing(4)}}>
                            <div className={classes.firstBlockParagraph}>
                              <div className={classes.sourceForSubTitles}>Ubicación
                                {data.address.province && 
                                  <div className={classes.addressContainer}>
                                    <div>
                                      <Typography variant="subtitle2">
                                        {data.address.province.name}
                                        {data.address.postal_code && ` (${data.address.postal_code})`},
                                        {" " + data.address.city.name}
                                      </Typography>
                                      <Typography variant="body2">
                                        {data.address.district && `${data.address.district}, `}
                                        {`${data.address.street} ${data.address.number? data.address.number : '' }`}
                                      </Typography>
                                      {data.address.tower && <Typography variant="body2">
                                        Torre {data.address.tower}, Piso {data.address.floor}, Departamento {data.address.apartment}
                                      </Typography>}
                                    </div>
                                  </div>
                                }
                              </div>
                            </div>
                            {data.address.location && <GoogleMap height="300px" initialLatLng={data.address.location}/>}
                          </Typography>
                        }
                        {images && images.length > 0 && <div><div className={classes.sourceForSubTitles}>Imágenes</div>
                       <div className={classes.containerCustomCarousel }><CustomCarousel images={images} height={"250px"} slideLength={1}> </CustomCarousel></div></div>}
                    </Typography>
                    </>}
                    {showEnterprise && <>
                      <Typography gutterBottom>
                    <div className={classes.firstBlockParagraph}>
                      <div className={classes.sourceForSubTitles}>Rubro 
                        <div className={classes.contentForSubTitle}>{` ${sector}`}</div>
                      </div>

                    <div className={classes.sourceForSubTitles}>Tipo de Empresa
                      <div className={classes.contentForSubTitle}>{` ${data.enterprise.enterprise_type}`}</div>
                    </div>
                    </div>  
                  </Typography>
                  <Typography gutterBottom>
                  <div className={classes.firstBlockParagraph}>
                    <div className={classes.sourceForSubTitles}>Descripción
                      <p className={classes.paragraph} >
                          { data.enterprise.description? data.enterprise.description : ' - '}
                      </p>
                    </div>
                  </div>
                  <div className={classes.sourceForSubTitlesWebPage}>Página web
                    <div className={classes.contentForSubTitle}><span className={classes.link}>{data.enterprise.web_page? data.enterprise.web_page : '-'}</span></div>
                  </div>
                  
                  {data.enterprise.address && 
                    <Typography gutterBottom>
                      <div className={classes.firstBlockParagraph}>
                        <div className={classes.sourceForSubTitles}>Ubicación
                          {data.enterprise.address.province && 
                            <div className={classes.addressContainer}>
                              <div>
                                <Typography variant="subtitle2">
                                  {data.enterprise.address.province.name}
                                  {data.enterprise.address.postal_code && ` (${data.enterprise.address.postal_code})`},
                                  {" " + data.enterprise.address.city.name}
                                </Typography>
                                <Typography variant="body2">
                                  {data.enterprise.address.district && `${data.enterprise.address.district}, `}
                                  {`${data.enterprise.address.street} ${data.enterprise.address.number? data.enterprise.address.number : '' }`}
                                </Typography>
                                {data.enterprise.address.tower && <Typography variant="body2">
                                  Torre {data.enterprise.address.tower}, Piso {data.enterprise.address.floor}, Departamento {data.enterprise.address.apartment}
                                </Typography>}
                              </div>
                            </div>
                          }
                        </div>
                      </div>
                      {data.enterprise.address.location && <GoogleMap height="300px" initialLatLng={data.enterprise.address.location}/>}
                    </Typography>
                  }
                  {enterpriseImages && enterpriseImages.length > 0 && <><div className={classes.sourceForSubTitles}>Imágenes</div>
                  <div className={classes.containerCustomCarousel }><CustomCarousel images={enterpriseImages} height={"250px"} slideLength={1}> </CustomCarousel></div></>}
                  </Typography>
                </>}
                </DialogContent>
                {!showEnterprise &&
                  <DialogActions>
                      <div className={classes.buttons}>
                        <Button className={classes.textButton} onClick={handleClose} color="primary" variant="contained">
                          Volver
                        </Button>
                        <Button  className={classes.textButton} onClick={handleConfirm} color="primary" variant="contained" disabled={postulated}>
                          Postularme
                        </Button>
                </div>
                </DialogActions>
              }
            </Dialog>
        </div>
      </>
    );
}