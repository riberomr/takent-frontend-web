import { Tooltip } from '@material-ui/core'
import { format } from 'date-fns'
import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../constants/generalConstants/theme'
import { apiTakent } from '../../../../../services/API-TAKENT'
import MyPostulations from '../../../../ui/businessUI/CommonUser/Offers/MyPostulations'
import StateStepper from '../../../../ui/generalUI/StateStepper'

const STATES_POSTULATION_MAP = {
  pending: "Pendiente",
  pre_selected: "Preseleccionado",
  contacted: "Contactado",
  // discarded: "Descartado"
}

const MyPostulationsWrapper = () => {

  const [ postulationList, setPostulationList ] = useState([])
  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    setLoading(true)
    apiTakent.getMyPostulations()
    .then(response => {
      let formatedResponse = response.data.map(element => {
        return {
          ...element,
          postulation_date: format(new Date(element.postulation_date), "dd/MM/yyyy"),
          state: (<StateStepper states={Object.values(STATES_POSTULATION_MAP)} activeState={Object.keys(STATES_POSTULATION_MAP).indexOf(element.state)}/>),
          companyPosition: (<Tooltip title={`${element.enterprise} - ${element.position}`}><div style={{textOverflow: "ellipsis", overflow: "hidden", paddingRight: theme.spacing(3)}}>{`${element.enterprise} - ${element.position}`}</div></Tooltip>)
        }
      })
      setPostulationList(formatedResponse)
    })
    .catch(() => {
      enqueueSnackbar('Error al obtener mis postulaciones', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }, [enqueueSnackbar])

  return (
    <MyPostulations
      postulationList={postulationList}
      loading={loading}
    />
  )

}

export default MyPostulationsWrapper
