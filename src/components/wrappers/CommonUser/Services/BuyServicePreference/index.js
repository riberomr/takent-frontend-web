import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../../services/API-TAKENT'
import BuyServicePreferenceForm from '../../../../ui/businessUI/CommonUser/Services/BuyServicePreferenceForm'

const CommonUserBuyServicePreferenceWrapper = props => {

  const [ activeServices, setActiveServices ] = useState([])

  useEffect(() => {
    const params = {only_mine: true, state: "active", preference: false}
    apiTakent.getServices(params)
    .then(response => {
      setActiveServices(response.data.map(service => {
        return {code: service._id, name: service.title}
      }))
    })

  }, [])

  return (
    <BuyServicePreferenceForm
      activeServices={activeServices}
      {...props}
    />
  )

}

export default CommonUserBuyServicePreferenceWrapper
