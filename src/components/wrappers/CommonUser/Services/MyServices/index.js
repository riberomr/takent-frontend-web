import { differenceInCalendarDays, format } from 'date-fns'
import { useSnackbar } from 'notistack'
import React, { useContext, useEffect, useState } from 'react'
import { apiTakent } from '../../../../../services/API-TAKENT'
import { Store } from '../../../../../Store'
import MyServicesList from '../../../../ui/businessUI/CommonUser/Services/MyServicesList'
import RequestList from '../../../../ui/businessUI/CommonUser/Services/RequestList'
import _ from 'underscore'
import { Tooltip } from '@material-ui/core'
import getWhatsAppLink from '../../../../../helpers/customHelpers/whatsapp'

const TRADUCED_STATE_SERVICE = {
  'active': 'Activo',
  'inactive' : 'Inactivo',
  'finished': 'Finalizado'
}

const TRADUCED_STATE_SERVICE_REQUEST = {
  'pending': 'Pendiente',
  'completed' : 'Completada',
  'canceled': 'Cancelada'
}

const SnackBarMessageServiceState = {
  'active': 'Servicio activado con éxito',
  'inactive' : 'Servicio desactivado con éxito',
  'finished': 'Servicio finalizado con éxito',
}

const SnackBarMessageServiceRequestState = {
  'completed': 'Solicitud completada con éxito',
  'canceled' : 'Solicitud cancelada con éxito'
}

const CommonUserMyServicesWrapper = props => {

  const [serviceList, setServiceList] = useState([])
  const context = useContext(Store)
  const [stateFilter, setStateFilter] = useState('Seleccione')
  const [firstDateFilter, setFirstDateFilter] = useState(null)
  const [lastDateFilter, setLastDateFilter] = useState(null)
  const { enqueueSnackbar } = useSnackbar()
  const [ viewRequests, setViewRequests ] = useState(false)
  const [ idSelectedService, setIdSelectedService ] = useState(null)
  const [ requests, setRequests ] = useState([])
  const [ stateFilterRequest, setStateFilterRequest ] = useState("Seleccione")
  const [ userFilterRequest, setUserFilterRequest ] = useState("")
  const [ loading, setLoading ] = useState(false)
  const [ changeRequests, setChangeRequests ] = useState(null)
  const [ ratingStatistics, setRatingStatistics ] = useState({})

  const [ loadingServices, setLoadingServices ] = useState(false)
  const [ loadingRequests, setLoadingRequests ] = useState(false)

  const retraduceState = (stateReceived) => {
    const matrix = Object.entries(TRADUCED_STATE_SERVICE)
    let traducedState = ''
    matrix.forEach(array => {
      if(array[1]===stateReceived){
        traducedState = array[0]
      }
    })

    return traducedState
  }

  const handleChangeStateService = (serviceId, state) => {
    setLoading(true)
    apiTakent.putService(serviceId, { state: state })
    .then(() => {
      enqueueSnackbar(SnackBarMessageServiceState[state], { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al cambiar el estado del servicio', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const handleChangeRequestState = (serviceId, requestId, state) => {
    setLoading(true)
    apiTakent.putServiceRequest(serviceId, requestId, { state: state })
    .then(() => {
      setChangeRequests(Math.random())
      enqueueSnackbar(SnackBarMessageServiceRequestState[state], { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al cambiar el estado de la solicitud', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const sendWhatsapp = (phone) => {
    window.open(getWhatsAppLink(phone), "_blank")
  }

  useEffect(() => {
    setLoadingServices(true)
    let params = {only_mine: true, sort: "desc", publication_date: `${firstDateFilter},${lastDateFilter}`}
    if(stateFilter && stateFilter!== 'Seleccione' ) params = {...params, state: retraduceState(stateFilter)} 
    apiTakent.getServices(params)
      .then((res) => {
        let mappedData = res.data.map((element) => {
          return {
            ...element,
            titleTableWithEllipsis: (<div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
                      {element.preference && <Tooltip title="Servicio preferencial">
                        <i className="fas fa-crown" style={{paddingRight: "5px", color: "#F29222"}}></i>
                      </Tooltip>}
                      <span style={{maxWidth: "150px", textOverflow: "ellipsis", overflow: "hidden", whiteSpace: "nowrap"}}>{element.title}</span>
                   </div>),
            titleTable: (<div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            {element.preference && <Tooltip title="Servicio preferencial">
                  <i className="fas fa-crown" style={{paddingRight: "5px", color: "#F29222"}}></i>
                </Tooltip>}
                <span>{element.title}</span>
            </div>),
            costTable: `$ ${element.cost}`,
            publishedDays: differenceInCalendarDays(new Date(), new Date(element.publication_date)),
            publication_date: format(new Date(element.publication_date),'dd/MM/yyyy'),
            requests_quantity: element.service_requests.length,
            remaining_days: element.state === "finished"? 0 : (30 - differenceInCalendarDays(new Date(), new Date(element.publication_date))),
            state: TRADUCED_STATE_SERVICE[element.state]
          }
        })
        setServiceList(mappedData)
      })
      .finally(() => setLoadingServices(false))
  }, [firstDateFilter, lastDateFilter, stateFilter, loading])

  useEffect(() => {
    if(!idSelectedService) return
    setLoadingRequests(true)
    apiTakent.getServiceRequests(idSelectedService)
    .then(response => {
      let mappedData = response.data.map(request => {
        return {
          ..._.omit(request, "user"),
          request_date: format(new Date(request.request_date),'dd/MM/yyyy'),
          requestId: request._id,
          serviceId: idSelectedService,
          ...request.user,
          state: TRADUCED_STATE_SERVICE_REQUEST[request.state]
        }
      })
      mappedData.reverse()
      setRequests(mappedData)
    })
    .finally(() => setLoadingRequests(false))
  }, [idSelectedService, changeRequests])

  useEffect(() => {
    apiTakent.getRatingStatistics(context.user?._id || context.user?.data?._id)
    .then(response => {
      setRatingStatistics(response.data)
    })
    .catch(() => {
      enqueueSnackbar('Error al obtener estadísticas de usuario', { variant: 'error' })
    })
    // eslint-disable-next-line
  }, [])

  return (
    <>
    {!viewRequests && <MyServicesList
      ratingStatistics={ratingStatistics}
      serviceList={serviceList}
      stateFilter={stateFilter}
      setStateFilter={setStateFilter}
      firstDateFilter={firstDateFilter}
      setFirstDateFilter={setFirstDateFilter}
      lastDateFilter={lastDateFilter}
      setLastDateFilter={setLastDateFilter}
      setViewRequests={setViewRequests}
      setIdSelectedService={setIdSelectedService}
      handleChangeStateService={handleChangeStateService}
      loading={loadingServices}
      {...props}
    />}
    {viewRequests && <RequestList
      requests={requests}
      setViewRequests={setViewRequests}
      stateFilter={stateFilterRequest}
      setStateFilter={setStateFilterRequest}
      userFilter={userFilterRequest}
      setUserFilter={setUserFilterRequest}
      handleChangeRequestState={handleChangeRequestState}
      sendWhatsapp={sendWhatsapp}
      loading={loadingRequests}
    />}
    </>
  )

}

export default CommonUserMyServicesWrapper
