import { format } from 'date-fns'
import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../../services/API-TAKENT'
import CommonUserServiceRequests from '../../../../ui/businessUI/CommonUser/Services/ServiceRequests'
import Rating from '@material-ui/lab/Rating'

const STATES_SERVICE_REQUESTS_MAP = {
  pending: "Pendiente",
  completed: "Completado",
  canceled: "Cancelado"
}

const CommonUserServiceRequestsWrapper = props => {

  const [ serviceRequestsList, setServiceRequestsList ] = useState([])
  const [ changeServiceRequests, setChangeServiceRequests ] = useState(0)
  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    setLoading(true)
    apiTakent.getMyServiceRequests()
    .then(response => {
      setServiceRequestsList(response.data.map(element => {
        return {
          ...element,
          request_date: format(new Date(element.request_date), "dd/MM/yyyy"),
          title: element.service?.title,
          state: STATES_SERVICE_REQUESTS_MAP[element.state],
          score: element.score? <Rating value={element.score} size="large" readOnly/> : null
        }
      }))
    })
    .catch(() => {
      enqueueSnackbar('Error al obtener servicios solicitados', { variant: 'error' })
    })
    .finally(() => {
      setLoading(false)
    })
  // eslint-disable-next-line
  }, [changeServiceRequests])

  const handleChangeRequestState = (serviceId, requestId, state) => {
    apiTakent.putServiceRequest(serviceId, requestId, { state: state })
    .then(() => {
      setChangeServiceRequests(Math.random())
      enqueueSnackbar("Solicitud de servicio cancelada con éxito", { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al cancelar el estado de la solicitud de servicio', { variant: 'error' })
    })
  }

  const handleRateRequest = (serviceId, requestId, rate) => {
    apiTakent.putServiceRequest(serviceId, requestId, { score: rate})
    .then(() => {
      setChangeServiceRequests(Math.random())
      enqueueSnackbar("Solicitud valorada con éxito", { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al valorar solicitud', { variant: 'error' })
    })
  }

  return (
    <CommonUserServiceRequests
      serviceRequestsList={serviceRequestsList}
      handleChangeRequestState={handleChangeRequestState}
      handleRateRequest={handleRateRequest}
      loading={loading}
    />
  )

}

export default CommonUserServiceRequestsWrapper
