import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { apiTakent } from '../../../../../services/API-TAKENT'
import NewServiceForm from '../../../../ui/businessUI/CommonUser/Services/NewServiceForm'
import AlertAction from '../../../../ui/generalUI/AlertAction'

const CommonUserNewServiceWrapper = props => {

  const [ sectors, setSectors ] = useState([])
  const [ provinces, setProvinces ] = useState([])
  const [ selectedProvince, setSelectedProvince ] = useState(null)
  const [ cities, setCities ] = useState([])
  const [ userAddress, setUserAddress ] = useState({})

  const [ buyPreferenceDialogOpen, setBuyPreferenceDialogOpen ] = useState(false)
  const [ newService, setNewService ] = useState(null)

  const [ loadingSubmit, setLoadingSubmit ] = useState(false)

  const history = useHistory()

  const { enqueueSnackbar } = useSnackbar()

  const handleSubmit = type => {
    setBuyPreferenceDialogOpen(false)
    if(type === "accept") {
      history.push("/app/user/service/buy", newService)
    }
  }

  const actions = [
    {
      name: 'Cancelar',
      type: 'cancel',
      handleSubmit: handleSubmit
    },  
    {
      name: 'Aceptar',
      type: 'accept',
      handleSubmit: handleSubmit
    }
  ]

  useEffect(()=> {
    apiTakent.getSectors()
    .then((response) => {
      setSectors(response.data.map(item => {
        return item._id
      }))
    })
  }, [])

  useEffect(() => {
    apiTakent.getProvinces()
      .then(response => setProvinces(response.data))
  }, [])

  useEffect(() => {
    if(selectedProvince) {
      apiTakent.getCities(selectedProvince)
        .then(response => setCities(response.data))
    }
  }, [selectedProvince])

  useEffect(() => {
    apiTakent.getCommonUserData()
    .then(response => {
      if(response.data.address.province) {
        setUserAddress({...response.data.address, province: response.data.address.province.code, city: response.data.address.city.code})
      }
    })
  }, [])

  const submitService = (service) => {
    setLoadingSubmit(true)
    apiTakent.postService(service)
    .then(response => {
      setNewService(response.data._id)
      setBuyPreferenceDialogOpen(true)
      enqueueSnackbar('¡Servicio publicado con éxito!', { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al intentar publicar el servicio', { variant: 'error' })
    })
    .finally(() => setLoadingSubmit(false))
  }

  return (
    <>
      <NewServiceForm 
        sectors={sectors}
        provinces={provinces}
        cities={cities}
        setSelectedProvince={setSelectedProvince}
        userAddress={userAddress}
        submitService={submitService}
        loadingSubmit={loadingSubmit}
        {...props}
      />
      {buyPreferenceDialogOpen && <AlertAction
        isOpen={buyPreferenceDialogOpen}
        setIsOpen={setBuyPreferenceDialogOpen}
        actions={actions}
        type='doubt'
        title={"¿Hacer servicio preferencial?"}
        loading={false}
      />}
    </>
  )

}

export default CommonUserNewServiceWrapper
