//----- React Core Imports ------
import React, {useState, useEffect} from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import Rating from '@material-ui/lab/Rating'
//----- Custom Components -------
import { DialogActions } from './DialogActions'
import { DialogContent } from './DialogContent'
import { DialogTitle } from './DialogTitle'
import CustomCarousel from '../../../../../ui/generalUI/CustomCarousel'
// ----- Styles and MakeStyles ------
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
// ----- Helpers ------
import { publishedDaysService } from '../../../../../../helpers/customHelpers/publishedDays'
// ----- API Calls ------
import { apiTakent } from '../../../../../../services/API-TAKENT'
import GoogleMap from '../../../../../ui/generalUI/GoogleMaps/GoogleMap'
import { theme } from '../../../../../../constants/generalConstants/theme'

const useStyles = makeStyles(styles)

export const ShowOffer =(props)=> {

  const { data, open, setOpen, avatar, requestService } = props
  const [images, setImages] = useState([])
  const { 
    title='',
    description='-',
    cost='-',
    address={city:{name:''}, province:{name:''}},
    publication_date,
    sector,
    publication_user,
    score,
    score_count
  } = data //Desestructuración de props según respuesta del Endpoint
    const classes = useStyles()

    const handleClose = () => {
        setOpen(false);
    };

    const handleConfirm = () => {
      requestService(data._id)
      setOpen(false)
    }

  useEffect(() => {
    
    if(open)
    apiTakent.getServiceImages(data._id)
      .then(res => {
        setImages(res.data)
      })
  }
    , [data._id, open])

    return (<>
        <div className={classes.container}>
         <Dialog classes={{ root: classes.container }} onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
            <DialogTitle id="customized-dialog-title" onClose={handleClose}>
              <div className={classes.containerTitle}>
                <div className={classes.containerAvatarTextUbication}>
                 <div className={classes.containerAvatarAndText}>
                   <div className={classes.containerAvatar} >
                      <Avatar alt="Picture" src={avatar} className={classes.avatar} variant="square"></Avatar>
                   </div>
                   <div className={classes.containerText}>
                     <div style={{fontSize: '24px'}}> {title}</div>
                     <div className={classes.titleCompany}>{publication_user.surname+', '+publication_user.name}</div>
                     {score !== undefined && <div style={{display: "flex", marginTop: theme.spacing(1)}}>
                      <Rating value={score} size="medium" readOnly/>
                      <Typography variant="body1">({score_count})</Typography>
                     </div>}
                   </div>
                 </div>
                 <div className={classes.containerUbiAndPublic}>
                    <div className={classes.textUbication}> {address.city.name} - {address.province.name} <LocationOnIcon className={classes.locationIcon}/></div> 
                  <div className={classes.textPublication}> {publishedDaysService(publication_date)} </div>
                 </div>
                </div>
              </div>
            </DialogTitle>
              <div className={classes.horizontalLine}></div>
              <DialogContent className={classes.dialogContent} dividers>
                <Typography gutterBottom style={{display: "flex"}}>
                  <div className={classes.firstBlockParagraph}>
                    <div className={classes.sourceForSubTitles}>Costo
                      <div className={classes.contentForSubTitle}>{' $'+cost}</div>
                    </div>
                  </div>
                  <div className={classes.firstBlockParagraph}>
                    <div className={classes.sourceForSubTitles}>Rubro 
                      <div className={classes.contentForSubTitle}>{` ${sector}`}</div>
                    </div>
                  </div>
                </Typography>
                <Typography gutterBottom>
                    <div className={classes.firstBlockParagraph}>
                      <div className={classes.sourceForSubTitles}>Descripción
                        <p className={classes.paragraph} >
                            { description==='' ? '-' : description}
                        </p>
                      </div>
                    </div>
                    {address.province && 
                    <Typography gutterBottom>
                      <div className={classes.firstBlockParagraph}>
                        <div className={classes.sourceForSubTitles}>Ubicación
                          <div className={classes.addressContainer}>
                            <div>
                              <Typography variant="subtitle2">
                                {address.province.name}
                                {address.postal_code && ` (${address.postal_code})`},
                                {" " + address.city.name}
                              </Typography>
                              <Typography variant="body2">
                                {address.district && `${address.district}, `}
                                {`${address.street} ${address.number? address.number : ''}`}
                              </Typography>
                              {address.tower && <Typography variant="body2">
                                Torre {address.tower}, Piso {address.floor}, Departamento {address.apartment}
                              </Typography>}
                            </div>
                          </div>
                        </div>
                      </div>
                      {address.location && <GoogleMap height="300px" initialLatLng={address.location}/>}
                    </Typography>
                  }
                    <div className={classes.sourceForSubTitles}>Imágenes</div>
                    <div className={classes.containerCustomCarousel }>
                      <CustomCarousel images={images} height={"250px"} slideLength={1}> </CustomCarousel>
                    </div>
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <div className={classes.buttons}>
                      <Button className={classes.textButton} onClick={handleClose} color="primary" variant="contained">
                         Volver
                      </Button>
                      <Button  className={classes.textButton} onClick={handleConfirm} color="primary" variant="contained" >
                         Contactar
                      </Button>
               </div>
             </DialogActions>
            </Dialog>
        </div>
      </>
    );
}