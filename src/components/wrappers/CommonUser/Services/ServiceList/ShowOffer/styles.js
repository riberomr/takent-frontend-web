
export const styles = (theme) => ({

    root: {
      padding: '26px 16px 0px 16px',
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: 'black',
    },
     container:{
      borderWidth: '2px',
      borderColor: 'orange',
      '&& .MuiDialog-paperWidthSm': {
          width: '562px',
          maxHeight: '83%',
          display: 'flex',
          marginTop: '75px',
          borderWidth: '2px',
          borderStyle: 'solid',
          borderColor: 'orange',
          
      }
     },
    containerTitle:{
      display: "flex",
      flexDirection:'column',
      justifyContent: "space-between"

    },
    titleCompany:{
      fontSize: '16px',
    },
    textUbication:{
      fontWeight: 'bolder',
      fontSize: '14px',
      color: "Gray"
    },

    textPublication:{
      fontWeight: 'normal',
      fontSize: '14px',
      color: "#EEA908"
    },
    containerAvatarAndText:{
      display: "flex",
      justifyContent: "space-between",
      width: '80%'
    },
    containerAvatar:{
      width: '30%',
      borderRadius:'10px',
      display:'flex',   
      justifyContent:'center',
      alignItems:'center'
    },
    dialogContent: {
      border:'none',
      '&::-webkit-scrollbar': {
      width: '0.4em'
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'orange',
      outline: '1px solid slategrey',
      borderRadius: 'inherit'
    },
    },
    avatar:{
      width: '73%',
      height: '73px',
      display:'flex',   
      justifyContent:'center',
      borderRadius:'8px'
    },
    containerText:{
      display: "flex",
      alignItems:'flex-start',
      width: '70%',
      wordBreak:'break-word',
      flexDirection:'column'
    },
    containerUbiAndPublic:{
      padding: '11px 11px 10px 16px',
      justifyContent: "space-between",
      display: 'flex',
      alignItems:'center',
    },
    firstBlockParagraph:{
      fontWeight: 'bolder',
      width: '100%',
      display: "flex",
      justifyContent:'space-between',
      marginBottom: '26px'
    },
    secondBlockParagraph:{
      fontWeight: 'bolder',
      width: '80%',
      display: "flex",
      justifyContent:'space-between',
      marginBottom: '26px',
      padding: '12px 21px 0px 19px',
    },
    paragraph:{
    fontWeight: 'normal',
    marginBlockStart:'1em',
    marginBlockEnd:'1em',
    marginInlineStart:'2em',
    marginInlineEnd:'0px',
    },
    request:{
      padding: '12px 21px 15px 19px',
      fontWeight: 'bolder',
    },
    sourceForSubTitles:{
      padding: '0px 10px 0px 0px',
      fontWeight: 'bolder',
    },
    sourceForUbication:{
      fontWeight: 'normal',
      color: 'yellow'
    },
    contentForSubTitle:{
      display: 'contents',
      fontWeight: 'normal'
    },
    divider:{
      color: 'red',
    },
    buttons:{
      display: "flex",
      width: '80%',
      justifyContent: 'space-between'
    },
    textButton:{
      height: '32px',
      minWidth: '128px',
      backgroundColor:'primary',
      color: 'black',
      fontWeight: '600',
      fontSize: '16px',
      letterSpacing: '1.25px',
      lineHeight: '16px',
  },
  horizontalLine: {
    width: '90%',
    height: '1px',
    backgroundColor: '#F29222',
    margin: 'auto',
    marginTop: '10px',
    marginBottom: '10px',

 },
  containerCustomCarousel:{
    padding:'12px',
  },
  locationIcon:{
    color: 'orange'
  },
  addressContainer:{
    marginBlockStart:'1em',
    marginBlockEnd:'1em',
    marginInlineStart:'2em',
    marginInlineEnd:'0px',
  },
  });