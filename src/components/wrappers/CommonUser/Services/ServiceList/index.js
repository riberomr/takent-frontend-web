import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../../services/API-TAKENT'
import { differenceInCalendarDays, format } from 'date-fns'
import { cleanString } from '../../../../../helpers/customHelpers'
import ServiceList from '../../../../ui/businessUI/CommonUser/Services/ServiceList'
import queryString from 'query-string'

const ServiceListWrapper = (props) => {
    const [services, setServices] = useState([])
    const [totalElements, setTotalElements] = useState(0)
    const [totalPages, setTotalPages] = useState(0)
    const [searcherInputValue, setSearcherInputValue] = useState('')
    const [filters, setFilters] = useState(null)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
      let locationParams = {}
      if(props.location?.search) {
        locationParams = queryString.parse(props.location.search)
        const { latitude, longitude, radius, filter_by_preferred } = locationParams
        if(latitude && longitude && radius && filter_by_preferred) {
          locationParams = {
            location: `${latitude},${longitude},${radius}`,
            filter_by_preference: filter_by_preferred
          }
        }
      }
      let options = {sort: "random", state: "active", exclude_mine: true, ...locationParams}
      if(filters) {
        options = {...options, ...filters}
      }
      setLoading(true)
      apiTakent.getServices(options)
        .then((res) => {
          let mappedData = res.data.map((element) => {
            return {
              ...element,
              publishedDays: differenceInCalendarDays(new Date(), new Date(element.publication_date)),
              publication_date_formatted: format(new Date(element.publication_date),'dd/MM/yyyy'),
          }})
          if(searcherInputValue && searcherInputValue !== ''){
            mappedData = mappedData.filter((element) => (cleanString(element.title).includes(cleanString(searcherInputValue))
            || cleanString(element.sector).includes(cleanString(searcherInputValue)))
            )
          }
          setServices(mappedData)
          setTotalElements(mappedData.length)
          setTotalPages(Math.ceil(mappedData.length/5))
        })
        .finally(() => setLoading(false))
        // eslint-disable-next-line
      }, [searcherInputValue, filters])
    
    const handleChangeFilters = (filters) => {
      if(filters !== {} ) setFilters(filters)
      else setFilters(null)
    }
    
    return(
        <>
          <ServiceList
            loading={loading} 
            services={services}
            totalPages={totalPages}
            totalElements={totalElements}
            searcherInputValue={searcherInputValue}
            setSearcherInputValue={setSearcherInputValue}
            handleChangeFilters={handleChangeFilters}
          />
        </>
    )
}

export default ServiceListWrapper