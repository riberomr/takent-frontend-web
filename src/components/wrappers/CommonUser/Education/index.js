import React, { useState, useEffect, useContext } from 'react'
import Education from '../../../ui/businessUI/CommonUser/Education'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const CommonUserEducationWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [userEducation, setUserEducation] = useState(null)
    const [flagReload, setFlagReload] = useState(false)
    // const [loading, setLoading] = useState(true)

    useEffect(()=>{
        // setLoading(true)
        apiTakent.getCommonUserEducation()
            .then(res => { setUserEducation(res.data)})
            // .finally(() => setLoading(false))
    },[context, flagReload])
    
    const putFormUser = (values) => {
        // setLoading(true)
        apiTakent.postCommonUserEducation(values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Datos académicos cargados con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cargar datos académicos', { variant: 'error' }))
            // .finally(() => setLoading(false))   
    }

    const deleteEducation = (id) => {
        // setLoading(true)
        apiTakent.deleteCommonUserEducation(id)
        .then(()=> {
            setFlagReload(!flagReload)
            enqueueSnackbar('Dato académico eliminado', { variant: 'success' })
        })
        .catch(() => enqueueSnackbar('Error al intentar eliminar dato académico', { variant: 'error' }))
        // .finally(() => setLoading(false))   
    }

    const editEducation = (id, values) => {
        // setLoading(true)
        apiTakent.putCommonUserEducation(id, values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cambiar los datos', { variant: 'error' }))
            // .finally(() => setLoading(false))   
    }

    return(
        <Education {...props}
            // loading={loading}
            handleFormSubmit={putFormUser}
            userEducation={userEducation}
            editEducation={editEducation}
            deleteEducation={deleteEducation}
        />
    )
 }
 export default CommonUserEducationWrapper