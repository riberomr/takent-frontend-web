import React, { useState, useEffect, useContext } from 'react'
import Profile from '../../../ui/businessUI/CommonUser/Profile'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const CommonUserProfileWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [provinces, setProvinces] = useState(null)
    const [cities, setCities] = useState([])
    const [idProvince, setIdProvince] = useState(null)
    const [userData, setUserData] = useState(null)
    const [curriculumBase64, setCurriculumBase64] = useState('')
    const [uploadCurriculum, setUploadCurriculum] = useState(false)
    const [loading, setLoading] = useState(false)
    const [profilePhoto, setProfilePhoto] = useState(null);
    const [sectors, setSectors] = useState(null);

    const [ loadingSubmit, setLoadingSubmit ] = useState(false)

    useEffect(() => {
        apiTakent.getCommonUserData()
            .then(res => { setUserData(res.data) })
        apiTakent.getCurriculum()
            .then(res => {
                setCurriculumBase64(res.data)
            })
        apiTakent.getProfilePhotoCommonUser()
            .then( res => {
                context.setPhotoChange(Math.random())
                setProfilePhoto(res.data.profile_photo)
            })
    // eslint-disable-next-line
    }, [loading, context.setPhotoChange])

    useEffect(() => {
      apiTakent.getProfilePhotoCommonUser()
      .then(res => {
          setProfilePhoto(res.data.profile_photo)
      })
    }, [context.photoChange])

    useEffect(() => {
        if (!provinces)
            apiTakent.getProvinces()
                .then(res => setProvinces(res.data))
                .catch(() => setProvinces([]))
        if (idProvince)
            apiTakent.getCities(idProvince)
                .then(res => setCities(res.data))
                .catch(() => setCities([]))
        
        if (!sectors)
            apiTakent.getSectors()
            .then((response) => {setSectors(response.data.map(item => {
                return item._id}))
            })
            .catch(() => setSectors([]));
            
    }, [provinces, idProvince, sectors])

    const putFormUser = (values) => {
      setLoadingSubmit(true)
      apiTakent.putCommonUserData(values)
      .then(() => enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' }))
      .catch(() => enqueueSnackbar('Error al intentar cambiar los datos', { variant: 'error' }))
      .finally(() => setLoadingSubmit(false))
    }

    return (
        <Profile {...props}
          provinces={provinces}
          cities={cities}
          setIdProvince={setIdProvince}
          handleFormSubmit={putFormUser}
          userData={userData}
          pdf={curriculumBase64}
          sectors={sectors}
          setSectors={setSectors}
          setUploadCurriculum={setUploadCurriculum}
          uploadCurriculum={uploadCurriculum}
          loading={loading}
          setLoading={setLoading}
          profilePhoto={profilePhoto}
          loadingSubmit={loadingSubmit}
        />
    )
}
export default CommonUserProfileWrapper