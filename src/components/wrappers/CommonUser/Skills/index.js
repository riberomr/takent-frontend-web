import React, { useState, useEffect, useContext } from 'react'
import Skills from '../../../ui/businessUI/CommonUser/Skills'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const CommonUserSkillsWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [userSkills, setUserSkills] = useState(null)
    const [flagReload, setFlagReload] = useState(false)
    // const [loading, setLoading] = useState(true)

    useEffect(()=>{
        // setLoading(true)
        apiTakent.getCommonUserSkills()
            .then(res => { setUserSkills(res.data)})
            // .finally(() => setLoading(false))
    },[context, flagReload])


    const putFormUser = (values) => {
        // setLoading(true)
        apiTakent.postCommonUserSkills(values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Aptitud cargada con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cargar aptitud', { variant: 'error' }))
            // .finally(() => setLoading(false))   
    }

    const deleteSkill = (id) => {
        // setLoading(true)
        apiTakent.deleteCommonUserSkills(id)
        .then(()=> {
            setFlagReload(!flagReload)
            enqueueSnackbar('Aptitud eliminada', { variant: 'success' })
        })
        .catch(() => enqueueSnackbar('Error al intentar eliminar aptitud', { variant: 'error' }))
        // .finally(() => setLoading(false))   
    }

    const editSkill = (id, values) => {
        // setLoading(true)
        apiTakent.putCommonUserSkills(id, values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cambiar los datos', { variant: 'error' }))
            // .finally(() => setLoading(false))   
    }

    return(
        <Skills {...props}
            // loading={loading}
            handleFormSubmit={putFormUser}
            userSkills={userSkills}
            deleteSkill={deleteSkill}
            editSkill={editSkill}
        />
    )
 }
 export default CommonUserSkillsWrapper