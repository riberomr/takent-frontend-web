import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import CommonUserNotificationsConfiguration from '../../../ui/businessUI/CommonUser/NotificationsConfiguration'
import _ from 'underscore'

const dataDialog = {'title': 'Ayuda',
                    'data': {'Nuevas solicitudes de servicio': 'Te llegarán notificaciones cuando otro usuario solicite uno de los servicios que tenés publicados',
                    'Recordatorios de valoración de servicios': 'Te llegarán notificaciones para que puedas valorar al usuario prestador del servicio que recibiste',
                    'Sugerencias de servicio preferencial': 'Te llegarán notificaciones para recordarte que podés hacer preferencial a tu servicio publicado',
                    'Cambios de estados de postulaciones a ofertas': 'Te llegarán notificaciones cuando cambie el estado de una de tus postulaciones a ofertas laborales',
                    'Sugerencias de completado de datos personales': 'Te llegarán notificaciones sugiriendote que completes tus datos personales cuando no lo estén',
                    'Ofertas cerca de tu ubicación': 'Te llegarán notificaciones de ofertas cercanas a tu ubicación actual. Recordá que para que te lleguen notificaciones de ofertas según tu ubicación, tenés que cargar cuáles son tus rubros preferidos en la sección "Mis datos personales"'}
                  }

const CommonUserNotificationsConfigurationWrapper = props => {

  const [ notificationsConfiguration, setNotificationsConfiguration ] = useState(null)
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    apiTakent.getNotificationsConfiguration()
    .then(response => {
      setNotificationsConfiguration(_.omit(response.data, "_id"))
    })
    .catch(() => {
      enqueueSnackbar('Error al obtener configuración de notificaciones', { variant: 'error' })
    })
  }, [enqueueSnackbar])

  const handleConfigurationChange = (notification, value) => {
    setNotificationsConfiguration(prevState => {
      return {...prevState, [notification]: value}
    })
    apiTakent.putNotificationsConfiguration({[notification]: value})
    .then(() => {
      enqueueSnackbar('Configuración de notificaciones actualizada', { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al actualizar configuración de notificaciones', { variant: 'error' })
    })
  }

  if(!notificationsConfiguration)
    return null

  return (
    <CommonUserNotificationsConfiguration
      notificationsConfiguration={notificationsConfiguration}
      handleConfigurationChange={handleConfigurationChange}
      dataDialog={dataDialog}
    />
  )

}

export default CommonUserNotificationsConfigurationWrapper
