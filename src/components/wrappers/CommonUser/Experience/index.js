import React, { useState, useEffect, useContext } from 'react'
import Experience from '../../../ui/businessUI/CommonUser/Experience'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'

const CommonUserExperienceWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)
    const [userExperience, setUserExperience] = useState(null)
    const [flagReload, setFlagReload] = useState(false)

    useEffect(()=>{
        apiTakent.getCommonUserExperience()
            .then(res => { setUserExperience(res.data)})
    },[context, flagReload])


    const putFormUser = (values) => {

        apiTakent.postCommonUserExperience(values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Experiencia laboral cargada con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cargar experiencia laboral', { variant: 'error' }))   
    }

    const deleteExperience = (id) => {

        apiTakent.deleteCommonUserExperience(id)
        .then(()=> {
            setFlagReload(!flagReload)
            enqueueSnackbar('Experiencia laboral eliminada', { variant: 'success' })
        })
        .catch(() => enqueueSnackbar('Error al intentar eliminar experiencia laboral', { variant: 'error' }))   
    }

    const editExperience = (id, values) => {

        apiTakent.putCommonUserExperience(id, values)
            .then(()=> {
                context.setLeftDialog(null)
                setFlagReload(!flagReload)
                enqueueSnackbar('Datos cambiados con éxito', { variant: 'success' })
            })
            .catch(() => enqueueSnackbar('Error al intentar cambiar los datos', { variant: 'error' }))   
    }

    return(
        <Experience {...props}
            handleFormSubmit={putFormUser}
            userExperience={userExperience}
            deleteExperience={deleteExperience}
            editExperience={editExperience}
        />
    )
 }
 export default CommonUserExperienceWrapper