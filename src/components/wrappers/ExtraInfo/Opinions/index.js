import { useSnackbar } from 'notistack'
import React, { useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import Opinions from '../../../ui/businessUI/ExtraInfo/Opinions'

const OpinionsWrapper = () => {

  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  const postOpinion = async body => {
    setLoading(true)
    try {
      await apiTakent.postOpinion(body)
      enqueueSnackbar("Opinión enviada correctamente", {variant: "success"})
    }
    catch(e) {
      enqueueSnackbar("Hubo un error al intentar enviar la opinión", {variant: "error"})
    }
    setLoading(false)
  }

  return (
    <Opinions
      postMethod={postOpinion}
      loading={loading}
    />
  )

}

export default OpinionsWrapper
