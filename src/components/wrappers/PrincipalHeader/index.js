import React from 'react'
import PrincipalHeader from '../../ui/businessUI/PrincipalHeader'
const PrincipalHeaderWrapper = (props) => {

    return(
      <>
        <PrincipalHeader {...props}/>
      </>
    )
}

export default PrincipalHeaderWrapper