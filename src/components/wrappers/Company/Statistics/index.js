import { useSnackbar } from 'notistack'
import React, { useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import CompanyStatistics from '../../../ui/businessUI/Company/Offers/Statistics'

const STATS_MAP = {
  offers_per_month: "Ofertas publicadas por mes",
  most_popular_offers: "Ofertas con mayor cantidad de postulantes",
  offers_by_state: "Ofertas por estado",
  postulants_by_age: "Postulantes por edad",
  postulants_by_month_and_state: "Postulantes por mes y estado"
}

const STATS_COMBO_INFO = Object.entries(STATS_MAP).map(([key, value]) => {
  return {code: key, name: value}
})

const GRAPH_STATS_INFO = {
  offers_per_month: {
    graphType: "XYChart",
    x: "month",
    y: "quantity",
    labelY: "Mes",
    xAxisTitle: "Meses",
    yAxisTitle: "Cantidad de ofertas"
  },
  most_popular_offers: {
    graphType: "XYChart",
    x: "title",
    y: "postulants",
    labelY: "Oferta",
    xAxisTitle: "Ofertas",
    yAxisTitle: "Cantidad de postulantes",
    overflowY: true
  },
  offers_by_state: {
    graphType: "PieChart",
    category: "state",
    value: "quantity",
    colors_map: {
      "Activas": "#3CB371",
      "En borrador": "#B0C4DE",
      "Finalizadas": "#F08080",
      "Programadas": "#FFA07A",
      "En selección": "#ADD8E6"
    }
  },
  postulants_by_age: {
    graphType: "XYChart",
    x: "age",
    y: "quantity",
    labelY: "Edad",
    xAxisTitle: "Edades",
    yAxisTitle: "Cantidad de postulantes"
  },
  postulants_by_month_and_state: {
    graphType: "ClusteredColumnChart",
    category: "month",
    series: {
      pending: "Pendientes",
      pre_selected: "Preseleccionados",
      contacted: "Contactados",
      discarded: "Descartados"
    },
    colors_map: {
      pending: "#B0C4DE",
      pre_selected: "#3CB371",
      contacted: "#ADD8E6",
      discarded: "#F08080"
    },
    xAxisTitle: "Meses",
    yAxisTitle: "Cantidad de postulantes"
  }
}

const OFFER_STATES_TRANSLATION = {
  active: "Activas",
  in_selection: "En selección",
  finished: "Finalizadas",
  deleted: "Eliminadas",
  programmed: "Programadas",
  draft: "En borrador"
}

const CompanyStatisticsWrapper = () => {

  const [ currentStat, setCurrentStat ] = useState(STATS_COMBO_INFO[0].code)
  const [ currentGraphInfo, setCurrentGraphInfo ] = useState(null)
  const [ statInfo, setStatInfo ] = useState(null)
  const [ dateFilter, setDateFilter ] = useState({})
  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  const getStat = statName => {
    setLoading(true)
    apiTakent.getEnterpriseStatistic(statName, dateFilter)
    .then(response => {
      let data = response.data
      if(statName === "offers_by_state")
        data.forEach(stat => {
          stat.state = OFFER_STATES_TRANSLATION[stat.state]
        })
      setStatInfo(data)
      setCurrentGraphInfo(GRAPH_STATS_INFO[statName])
    })
    .catch(() => {
      enqueueSnackbar("Hubo un error al obtener los datos de la estadística", { variant: 'error' })
    })
    .finally(() => {
      setLoading(false)
    })
  }

  return (
    <CompanyStatistics
      statsComboInfo={STATS_COMBO_INFO}
      currentStat={currentStat}
      setCurrentStat={setCurrentStat}
      getStat={getStat}
      statInfo={statInfo}
      dateFilter={dateFilter}
      setDateFilter={setDateFilter}
      graphStatsInfo={currentGraphInfo}
      loading={loading}
    />
  )

}

export default CompanyStatisticsWrapper
