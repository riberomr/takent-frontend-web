import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import BuyOfferForm from '../../../ui/businessUI/Company/Offers/BuyOfferForm'

const CompanyBuyOfferWrapper = props => {

  const [ offerPacks, setOfferPacks ] = useState([])

  useEffect(() => {
    apiTakent.getOfferPacks()
    .then(response => {
      setOfferPacks(response.data)
    })
  }, [])

  return (
    <BuyOfferForm
      offerPacks={offerPacks}
      {...props}
    />
  )

}

export default CompanyBuyOfferWrapper
