import { useSnackbar } from 'notistack'
import React, { useContext, useEffect, useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import _ from 'underscore'
import EnterpriseUserNotificationsConfiguration from '../../../ui/businessUI/Company/NotificationsConfiguration'
import { Store } from '../../../../Store'

const EnterpriseUserNotificationsConfigurationWrapper = props => {

  const [ notificationsConfiguration, setNotificationsConfiguration ] = useState(null)
  const context = useContext(Store)
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    apiTakent.getNotificationsConfiguration()
    .then(response => {
      setNotificationsConfiguration(_.omit(response.data, "_id"))
    })
    .catch(() => {
      enqueueSnackbar('Error al obtener configuración de notificaciones', { variant: 'error' })
    })
  }, [enqueueSnackbar])

  const handleConfigurationChange = (notification, value) => {
    setNotificationsConfiguration(prevState => {
      return {...prevState, [notification]: value}
    })
    apiTakent.putNotificationsConfiguration({[notification]: value})
    .then(() => {
      enqueueSnackbar('Configuración de notificaciones actualizada', { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al actualizar configuración de notificaciones', { variant: 'error' })
    })
  }

  if(!notificationsConfiguration)
    return null

  return (
    <EnterpriseUserNotificationsConfiguration
      notificationsConfiguration={notificationsConfiguration}
      handleConfigurationChange={handleConfigurationChange}
      adminUser={context.user?.permission === "admin"}
    />
  )

}

export default EnterpriseUserNotificationsConfigurationWrapper
