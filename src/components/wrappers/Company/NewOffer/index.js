import { useSnackbar } from 'notistack'
import React, { useEffect, useState } from 'react'
import { apiTakent } from '../../../../services/API-TAKENT'
import NewOfferForm from '../../../ui/businessUI/Company/Offers/NewOfferForm'

const CompanyNewOfferWrapper = (props) => {

  const [ defaultSector, setDefaultSector ] = useState("")
  const [ enterpriseAddress, setEnterpriseAddress ] = useState({})
  const [ sectors, setSectors ] = useState([])
  const [ languages, setLanguages ] = useState([])
  const [ provinces, setProvinces ] = useState([])
  const [ selectedProvince, setSelectedProvince ] = useState(null)
  const [ cities, setCities ] = useState([])
  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  const experiences = [ "Ninguna experiencia", "Experiencia escasa", "Experiencia intermedia", "Mucha experiencia" ]
  const contractTypes = [ "Jornada parcial", "Jornada completa", "Por temporada", "Por obra", "Pasantía", "Por hora" ]
  
  useEffect(() => {
    apiTakent.getEnterprise()
    .then(response => {
      setDefaultSector(response.data.sector)
      if(response.data.address.province) {
        setEnterpriseAddress({...response.data.address, province: response.data.address.province.code, city: response.data.address.city.code})
      }
    })
  }, [])

  useEffect(()=> {
    apiTakent.getSectors()
    .then((response) => {
      setSectors(response.data.map(item => {
        return item._id
      }))
    })
  }, [])

  useEffect(() => {
    apiTakent.getLanguages()
    .then((response) => {
      setLanguages(response.data.map(item => {
        return item.name
      }))
    })
  }, [])

  useEffect(() => {
    apiTakent.getProvinces()
      .then(response => setProvinces(response.data))
  }, [])

  useEffect(() => {
    if(selectedProvince) {
      apiTakent.getCities(selectedProvince)
        .then(response => setCities(response.data))
    }
  }, [selectedProvince])

  const submitOffer = (offer, type) => {
    let textPositive = type === 'accept'? 'Oferta creada con éxito' : 'Borrador guardado con éxito'
    let textNegative = type === 'accept'? 'Error al intentar crear la oferta' : 'Error al intentar guardar borrador'

    setLoading(true)
    if(offer && (offer.idDraft || offer.idProgrammed)) {
      if(offer.idProgrammed) {
        textPositive = 'Oferta programada con éxito'
        textNegative = 'Error al intentar programar oferta'
      }
      let id = offer.idDraft? offer.idDraft : offer.idProgrammed
      delete offer.idDraft
      delete offer.idProgrammed
      let images = []
      if(offer.images && offer.images.length > 0){
        images = offer.images
        delete offer.images
      }
      else {
        apiTakent.deleteOfferImages(id)
      }
      apiTakent.putCompanyOffer(id, offer)
        .then(() => {
          enqueueSnackbar(textPositive, { variant: 'success' })
        })
        .then(() => {
          if(images.length>0) apiTakent.postOfferImages(id, { images: images })
        } )
        .catch(() => {
          enqueueSnackbar(textNegative, { variant: 'error' })
        })
        .finally(() => setLoading(false))
    }
    else {
      if(offer.state === 'programmed') {
        textPositive = 'Oferta programada con éxito'
        textNegative = 'Error al intentar programar oferta'
      }
      delete offer.idDraft
      delete offer.idProgrammed
      apiTakent.postCompanyOffer(offer)
        .then(() => {
          enqueueSnackbar(textPositive, { variant: 'success' })
        })
        .catch(() => {
          enqueueSnackbar(textNegative, { variant: 'error' })
        })
        .finally(() => setLoading(false))
    }
  }

  // Mas adelante agregar loading
  if(!defaultSector)
    return null

  return (
    <NewOfferForm
      defaultSector={defaultSector}
      enterpriseAddress={enterpriseAddress}
      sectors={sectors}
      experiences={experiences}
      contractTypes={contractTypes}
      languages={languages}
      provinces={provinces}
      cities={cities}
      setSelectedProvince={setSelectedProvince}
      submitOffer={submitOffer}
      loading={loading}
      {...props} 
    />
  )
}

export default CompanyNewOfferWrapper
