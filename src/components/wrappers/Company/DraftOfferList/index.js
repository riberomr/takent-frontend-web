import React, { useEffect, useState } from 'react'
import DraftOfferList from '../../../ui/businessUI/Company/Offers/DraftOfferList'
// import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'

const TRADUCED_STATE = {
  'draft': 'Borrador'
}

const CompanyDraftOfferListWrapper = () => {

  const [offerList, setOfferList] = useState([])
 
  const { enqueueSnackbar } = useSnackbar()
  const [loading, setLoading] = useState(false)

  const [ loadingDrafts, setLoadingDrafts ] = useState(false) 

  const handleDiscardDraft = (offerId, state) => {
    setLoading(true)
    apiTakent.deleteCompanyOffer(offerId)
    .then(() => {
      enqueueSnackbar('Borrador descartado con éxito', { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al descartar borrador', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  useEffect(() => {
    setLoadingDrafts(true)
    apiTakent.getOffers()
      .then((res) => {
        let mappedData = res.data
          .filter(element => element.state === 'draft')
          .map((element) => {
            return {
              ...element,
              state: TRADUCED_STATE[element.state]
            }
          })
          setOfferList(mappedData.reverse())
        })
      .finally(() => setLoadingDrafts(false))
  }, [loading])

  return (
    <DraftOfferList 
      offerList={offerList}
      handleDiscardDraft={handleDiscardDraft}
      loading={loadingDrafts}
    />
  )
}

export default CompanyDraftOfferListWrapper