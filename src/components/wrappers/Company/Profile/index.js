import React, { useState, useEffect, useContext } from 'react'
import Profile from '../../../ui/businessUI/Company/Profile'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import AlertAction from '../../../ui/generalUI/AlertAction'
import { Store } from '../../../../Store'


const CompanyProfileWrapper = (props) => {

    const [provinces, setProvinces] = useState([])
    const [cities, setCities] = useState([])
    const [idProvince, setIdProvince] = useState(null)
    const [enterprise, setEnterprise] = useState(null)
    const [sectors, setSectors] = useState([])
    const [photo, setPhoto] = useState('')
    const [images, setImages] = useState([])
    const { enqueueSnackbar } = useSnackbar()
    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [disabledStatus, setDisabledStatus] = useState(true)
    const [values, setValues] = useState(null)
    const context = useContext(Store)

    const enterpriseTypes = ['Reclutamiento','Empleador directo','Servicios temporales']

    useEffect(() => {
        apiTakent.getProvinces()
            .then(res => setProvinces(res.data))
            .catch(() => setProvinces([]))
        apiTakent.getSectors()
            .then((response) => {
                setSectors(response.data.map(item => {
                return item._id
            }))})
    },[])

    useEffect(()=> {
        apiTakent.getEnterprise()
            .then(res => {
                setCities([res.data.address.city])
                setEnterprise(res.data)
                setIdProvince(res.data.address.province.code)
            })
            .catch(() => setEnterprise(null))
    },[])

    useEffect(()=> {
        if(idProvince)
            apiTakent.getCities(idProvince)
            .then(res => setCities(res.data))
            .catch(() => setCities([]))
    },[idProvince])

    useEffect(()=>{
        if(idProvince)
            apiTakent.getProvinces()
               
    },[idProvince])
    
    useEffect(()=> {
        if(enterprise) {
            apiTakent.getEnterpriseProfilePhoto(enterprise._id)
            .then(res => {
                setPhoto(res.data)
            })
            .catch(() => setPhoto(''))

            apiTakent.getEnterpriseImages(enterprise._id)
                .then(res => {
                    setImages(res.data)
            })
            .catch(() => setImages([]))
        }
    },[enterprise])

    const submitChanges = (body, photo, images) => {
        apiTakent.putEnterpriseData(body)
            .then(() => enqueueSnackbar('Datos de empresa modificados con éxito', { variant: 'success' }))
            .catch(() => enqueueSnackbar('Error al intentar modificar datos de empresa', { variant: 'error' }))
        if(photo)
        apiTakent.postEnterpriseProfilePhoto(photo)
            .then(() => {
              enqueueSnackbar('Foto de perfil modificada con éxito', { variant: 'success' })
              context.setPhotoChange(Math.random())
            })
            .catch(() => enqueueSnackbar('Error al intentar modificar foto de perfil', { variant: 'error' }))
        if(images)
        apiTakent.postEnterpriseImages(images)
            .then(() => enqueueSnackbar('Imagenes cargadas con éxito', { variant: 'success' }))
            .catch(() => enqueueSnackbar('Error al intentar cargar imagenes', { variant: 'error' }))
    }

    const openAlert = (body, photo, images) => {
        setValues({body: body, photo: photo, images: images})
        setIsAlertOpen(true)
    }

    const handleSubmitAction = (type) => {

        if(type === 'accept') {
            submitChanges(values.body, values.photo, values.images)
            setIsAlertOpen(false)
            setDisabledStatus(!disabledStatus)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
            setValues(null)
        }
    }

    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    return(
        <>
            <Profile {...props} 
                admin={context?.user?.permission === "admin"}
                enterprise={enterprise} 
                provinces={provinces} 
                cities={cities} 
                sectors={sectors} 
                setIdProvince={setIdProvince}
                enterpriseTypes={enterpriseTypes}
                photo={photo}
                images={images}
                setPhoto={setPhoto}
                setImages={setImages}
                submitChanges={openAlert}
                disabledStatus={disabledStatus}
                setDisabledStatus={setDisabledStatus}
            />
            <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title='¿Desea confirmar los cambios?'
                loading={false}
            />
        </>  
    )
 }
 export default CompanyProfileWrapper