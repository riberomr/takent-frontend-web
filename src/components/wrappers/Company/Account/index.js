import React, { useContext, useEffect, useState } from 'react'
import Account from '../../../ui/businessUI/Company/Account'
import { apiTakent } from '../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../Store'
import { authService } from '../../../../services/auth.service'

const CompanyAccountWrapper = (props) => {

    const { enqueueSnackbar } = useSnackbar()
    const context = useContext(Store)

    const [ isVerified, setIsVerified ] = useState(null)

    useEffect(() => {
      const fetchVerified = async () => {
        setIsVerified((await apiTakent.getEnterprise())?.data.verified)
      }
      fetchVerified()
    }, [])

    const changePassword = async values => {
      try {
        await apiTakent.postChangePassword(values)
        context.setLeftDialog(null)
        enqueueSnackbar('Contraseña cambiada con éxito', { variant: 'success' })
        
      }
      catch(error) {
        enqueueSnackbar('Error al intentar cambiar la contraseña', { variant: 'error' })
      }
      await authService.login(
        JSON.parse(localStorage.getItem('takent_user'))._id, 
        values.new_password
      )
    }

    return(
        <Account {...props}
            handleFormSubmit={changePassword}
            isVerified={isVerified}
        />
    )
 }
 export default CompanyAccountWrapper