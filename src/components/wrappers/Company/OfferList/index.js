import React, { useEffect, useState, useContext } from 'react'
import OfferList from '../../../ui/businessUI/Company/Offers/OfferList'
// import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../services/API-TAKENT'
import { differenceInCalendarDays, differenceInYears, format } from 'date-fns'
import PostulantList from '../../../ui/businessUI/Company/Offers/PostulantList'
import _ from 'underscore'
import DownloadAttachment from '../../../ui/generalUI/DownloadAttachment'
import { Store } from '../../../../Store'
import ContactMailForm from '../../../ui/businessUI/Company/Offers/PostulantList/ContactMailForm'
import { useSnackbar } from 'notistack'
import getWhatsAppLink from '../../../../helpers/customHelpers/whatsapp'

const TRADUCED_STATE = {
  'active': 'Activa',
  'in_selection' : 'En selección',
  'finished': 'Finalizada',
  'deleted': 'Eliminada',
  'programmed' :  'Programada',
  'draft': 'Borrador'
}

const TRADUCED_STATE_POSTULANT = {
  "pending": "Pendiente",
  "pre_selected": "Preseleccionado",
  "contacted": "Contactado",
  "discarded": "Descartado"
}

const SnackBarMessageOfferState = {
  'active': 'Oferta activada con éxito',
  'in_selection' : 'La oferta pasó al estado en selección',
  'finished': 'Oferta finalizada con éxito',
  'deleted': 'Oferta eliminada con éxito'
}

const CompanyOfferListWrapper = () => {

  const [offerList, setOfferList] = useState([])
  const context = useContext(Store)
  const [stateFilter, setStateFilter] = useState('Seleccione')
  const [firstDateFilter, setFirstDateFilter] = useState(null)
  const [lastDateFilter, setLastDateFilter] = useState(null)
  const { enqueueSnackbar } = useSnackbar()
  const [ viewPostulants, setViewPostulants ] = useState(false)
  const [ idSelectedOffer, setIdSelectedOffer ] = useState(null)
  const [ postulants, setPostulants ] = useState([])
  const [ stateFilterPostulant, setStateFilterPostulant ] = useState([])
  const [loading, setLoading] = useState(false)
  const [ changePostulants, setChangePostulants ] = useState(null)

  const [ loadingOffers, setLoadingOffers ] = useState(false)
  const [ loadingPostulants, setLoadingPostulants ] = useState(false)

  const downloadCV = async (userId) => {
    const params = {user_id: userId}
    return apiTakent.getCurriculum(params)
  }

  const sendMail = (offerId, postulationId, comment = null) => {
    setLoading(true)
    apiTakent.postContactPostulant(offerId, postulationId, { comment: comment })
    .then(() => {
      context.setLeftDialog(null)
      enqueueSnackbar('Contacto por mail realizado con éxito', { variant: 'success' })
      setChangePostulants(Math.random())
    })
    .catch(() => {
      context.setLeftDialog(null)
      enqueueSnackbar('Error al contactar postulante', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const sendWhatsapp = (offerId, postulationId, phone, text = null, state) => {
    if(state === "Descartado") {
      context.setLeftDialog(null)
      enqueueSnackbar('Contacto por WhatsApp realizado con éxito', { variant: 'success' })
      window.open(getWhatsAppLink(phone, text), "_blank")
      return
    }
    setLoading(true)
    apiTakent.putPostulant(offerId, postulationId, {state: "contacted"})
    .then(() => {
      context.setLeftDialog(null)
      enqueueSnackbar('Contacto por WhatsApp realizado con éxito', { variant: 'success' })
      window.open(getWhatsAppLink(phone, text), "_blank")
      setChangePostulants(Math.random())
    })
    .catch(() => {
      context.setLeftDialog(null)
      enqueueSnackbar('Error al contactar postulante', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const contactPostulant = (data) => {
    context.setLeftDialog(
      <ContactMailForm 
        sendMail={sendMail}
        sendWhatsapp={sendWhatsapp}
        data={data} 
      />)
  }

  const preselectPostulant = (offerId, postulationId, comment=null) => {
    setLoading(true)
    apiTakent.postPreselectPostulant(offerId, postulationId, {comment: comment})
    .then(() => {
      enqueueSnackbar('Postulante preseleccionado', { variant: 'success' })
      setChangePostulants(Math.random())
    })
    .catch(() => {
      enqueueSnackbar('Error al preseleccionar postulante', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const discardPostulant = (offerId, postulationId, comment=null) => {
    setLoading(true)
    apiTakent.postDiscardPostulant(offerId, postulationId, {comment: comment})
    .then(() => {
      enqueueSnackbar('Postulante descartado', { variant: 'success' })
      setChangePostulants(Math.random())
    })
    .catch(() => {
      enqueueSnackbar('Error al descartar postulante', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  const handleChageStateOffer = (offerId, state, isProgrammed = false) => {
    setLoading(true)
    let body = { state: state }
    if(isProgrammed) body = {...body, publication_date: new Date()}
    apiTakent.putCompanyOffer(offerId, body)
    .then(() => {
      enqueueSnackbar(SnackBarMessageOfferState[state], { variant: 'success' })
    })
    .catch(() => {
      enqueueSnackbar('Error al cambiar el estado de la oferta', { variant: 'error' })
    })
    .finally(() => setLoading(false))
  }

  useEffect(() => {
    if(!idSelectedOffer) return
    setLoadingPostulants(true)
    apiTakent.getPostulants(idSelectedOffer)
    .then(response => {
      let mappedData = response.data.map(postulation => {
        return {
          ..._.omit(postulation, "postulant"),
          postulationId: postulation._id,
          offerId: idSelectedOffer,
          ...postulation.postulant,
          state: TRADUCED_STATE_POSTULANT[postulation.state],
          age: differenceInYears(new Date(), new Date(postulation.postulant.birth_date)),
          cv: (<DownloadAttachment downloadFunction={() => downloadCV(postulation.postulant._id)} disabled={!Boolean(postulation.postulant.curriculum)}/>)
        }
      })
      setPostulants(mappedData)
    })
    .finally(() => setLoadingPostulants(false))
  }, [idSelectedOffer, changePostulants])

  const retraduceState = (stateReceived) => {
    const matrix = Object.entries(TRADUCED_STATE)
    let tratucedState = ''
    matrix.forEach(array => {
      if(array[1]===stateReceived){
        tratucedState = array[0]
      }
    })

    return tratucedState
  }

  useEffect(() => {
    setLoadingOffers(true)
    let params = {publication_date: `${firstDateFilter},${lastDateFilter}`}
    if(stateFilter && stateFilter!== 'Seleccione' ) params = {...params, state: retraduceState(stateFilter)} 
    apiTakent.getOffers(params)
      .then((res) => {
        let mappedData = res.data.filter(element => element.state!=='draft')
        .map((element) => {
          return {
            ...element,
            publishedDays: differenceInCalendarDays(new Date(), new Date(element.publication_date)),
            publication_date: format(new Date(element.publication_date),'dd/MM/yyyy'),
            publication_date_not_formated: element.publication_date,
            postulants_quantity: element.postulants.length,
            remaining_days: element.state === "finished"? 0 : (element.state === 'programmed') || (element.state ==='deleted')? ' - ': (30 - differenceInCalendarDays(new Date(), new Date(element.publication_date))),
            state: TRADUCED_STATE[element.state]
          }
        })
        if(stateFilter === 'Seleccione') setOfferList(mappedData.filter(element => element.state !== 'Eliminada').reverse())
        else setOfferList(mappedData.reverse())
      })
      .finally(() => setLoadingOffers(false))
  }, [firstDateFilter, lastDateFilter ,stateFilter, loading])

  return (
    <>
    {!viewPostulants && <OfferList 
      offerList={offerList}
      stateFilter={stateFilter}
      setStateFilter={setStateFilter}
      firstDateFilter={firstDateFilter}
      setFirstDateFilter={setFirstDateFilter}
      lastDateFilter={lastDateFilter}
      setLastDateFilter={setLastDateFilter}
      setViewPostulants={setViewPostulants}
      setIdSelectedOffer={setIdSelectedOffer}
      handleChageStateOffer={handleChageStateOffer}
      loading={loadingOffers}
    />}
    {viewPostulants && <PostulantList
      setViewPostulants={setViewPostulants}
      postulants={postulants}
      stateFilter={stateFilterPostulant}
      setStateFilter={setStateFilterPostulant}
      contactPostulant={contactPostulant}
      preselectPostulant={preselectPostulant}
      discardPostulant={discardPostulant}
      loading={loadingPostulants}
    />}

    </>
  )
}

export default CompanyOfferListWrapper