import AllowedUserForm from '../../../../ui/businessUI/EnterpriseUser/AllowedUser';
import React from 'react'
import { apiTakent } from '../../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import { differenceInCalendarYears } from 'date-fns';

const TRADUCED_STATE = {
    'active': 'Activa',
    'inactive': 'Inactiva',
    'in_selection' : 'En selección',
    'finished': 'Finalizada',
    'deleted': 'Eliminada',
    // 'invalid': 'Suspendida'
  }

const AllowedUserFormWrapper = (props) => {
    const { setIsOpen, setLoading, setAllowedUsers } = props;
    const { enqueueSnackbar } = useSnackbar()

    const postFormUser = (values) => {
        setLoading(true);
        apiTakent.getAllowedUsers().then(res => {
            let data = res.data.map(elem => {
                return {
                    ...elem,
                    state: TRADUCED_STATE[elem.state],
                    age: differenceInCalendarYears(new Date(), new Date(elem.birth_date)) - 1,
                    position: elem.position !== undefined && elem.position !== '' ? elem.position : "-"
                }
            })
            setUsers(prevState => {
                return prevState.concat(data);
            });
            apiTakent.getEnterpriseUserData().then(res => {
                const admin = {
                ...res.data,
                nameUser: res.data.name,
                name: (<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <Tooltip title="Usuario administrador">
                        <i className="fas fa-crown" style={{ paddingRight: "5px", color: "#F29222" }}></i>
                    </Tooltip>
                    <span>{res.data.name}</span>
                </div>),
                state: TRADUCED_STATE[res.data.state],
                age: differenceInCalendarYears(new Date(), new Date(res.data.birth_date)) - 1,
                position: res.data.position !== undefined && res.data.position !== '' ? res.data.position : "-"
            };
            setUsers(prevState => {
                return prevState.concat([admin]).reverse()
            })
            });
        })
        .catch(() => enqueueSnackbar('Error en el registro', { variant: 'error' }))
        .finally(() => setLoading(false));
    }

    return (
        <AllowedUserForm 
        handleFormSubmit={postFormUser}
        />
    );
}

export default AllowedUserFormWrapper;