import React, { useContext, useEffect, useState } from 'react';
import { apiTakent } from '../../../../../services/API-TAKENT';
import AllowedUserList from '../../../../ui/businessUI/AllowedUser/ListUsers';
import { differenceInCalendarYears, differenceInYears } from 'date-fns';
import { useSnackbar } from 'notistack'
import { Tooltip } from '@material-ui/core';
import { Store } from '../../../../../Store';
import { authService } from '../../../../../services/auth.service';

const TRADUCED_STATE = {
    'active': 'Activa',
    'inactive': 'Inactiva',
}

const AllowedUserListWrapper = (...props) => {
    const { enqueueSnackbar } = useSnackbar();
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);

    const context = useContext(Store)

    useEffect(() => {
        setLoading(true);
        apiTakent.getEnterpriseUsers().then(res => {
            let data = res.data.map(elem => {
                return {
                    ...elem,
                    nameForTable: elem.permission === 'admin' ? (<div style={{ display: 'flex', justifyContent: 'center'}}>
                            <Tooltip title="Usuario administrador">
                                <i className="fas fa-crown" style={{ paddingRight: "5px", color: "#F29222"}}></i>
                            </Tooltip>
                            <span style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>{elem.name}</span>
                    </div>) : elem.name,
                    state: TRADUCED_STATE[elem.state],
                    age: differenceInYears(new Date(), new Date(elem.birth_date)),
                    position: elem.position !== undefined && elem.position !== '' ? elem.position : "-",
                    name: elem.name

                }
            });
            setUsers(data)
        })
        .finally(() => {
            setLoading(false);
        })

    }, []);

    const deleteAllowedUser = (id) => {
        setUsers([]);
        setLoading(true);
        apiTakent.deleteAllowedUser(id).then(res => {
            apiTakent.getEnterpriseUsers().then(res => {
                let data = res.data.map(elem => {
                    return {
                        ...elem,
                        nameForTable: elem.permission === 'admin' ? (<div style={{ display: 'flex', justifyContent: 'center'}}>
                            <Tooltip title="Usuario administrador">
                                <i className="fas fa-crown" style={{ paddingRight: "5px", color: "#F29222"}}></i>
                            </Tooltip>
                            <span style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>{elem.name}</span>
                        </div>) : elem.name,
                        state: TRADUCED_STATE[elem.state],
                        age: differenceInCalendarYears(new Date(), new Date(elem.birth_date)) - 1,
                        position: elem.position !== undefined && elem.position !== '' ? elem.position : "-",
                        name: elem.name
                    }
                });
                setUsers(data);
            })
                .catch(() => enqueueSnackbar('Error al intentar cargar usuarios colaboradores', { variant: 'error' }))
                .finally(() => setLoading(false));
            enqueueSnackbar('Usuario colaborador borrado con éxito', { variant: 'success' });
        })
            .catch(() => {
                enqueueSnackbar('Error al borrar usuario colaborador', { variant: 'error' })
            });
    }

    const doAdmin = account => {
      apiTakent.doAdmin(account)
      .then(() => {
        enqueueSnackbar('Usuario administrador cambiado con éxito. Cerrando sesión...', { variant: 'success' })
        new Promise(resolve => setTimeout(resolve, 3000))
        .then(() => {
          authService.logout()
          context.handleSetUser(null) 
        })
      })
      .catch(() => {
        enqueueSnackbar('Error al cambiar usuario administrador', { variant: 'error' })
      })
    }

    return (
        <>
          <AllowedUserList
            admin={context?.user?.permission === "admin"}
            allowedUsers={users}
            setAllowedUsers={setUsers}
            loading={loading}
            setLoading={setLoading}
            deleteAllowedUser={deleteAllowedUser}
            doAdmin={doAdmin}
          />
        </>
    );
}

export default AllowedUserListWrapper;