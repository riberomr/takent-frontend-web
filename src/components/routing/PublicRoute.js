import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { paths } from '../../constants/generalConstants/paths'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import SmallScreen from '../pages/SmallScreen'
// this is a "custom" route component. Allows passing custom props, and also checks if user is already logged
// in App.js the component is called
const PublicRoute = ({ component: Component, loggedIn, path, allScreenSize, ...rest }) => {

  const isXSWidth = useMediaQuery('(max-width:1024px)')
  if(allScreenSize || !(isXSWidth)) {
    if ((path === paths.public.login && loggedIn) || path === '/')
      return <Redirect to={paths.protected.home} />
    return <Route {...rest} render={props => <Component {...props} {...rest} />}/>
  }
  else {
    return <Route {...rest} render={() => <SmallScreen/>}/>
  }
}

export default PublicRoute
