import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { paths } from '../../constants/generalConstants/paths'

import { Store } from '../../Store'

const PrivateRoute = ({
  component: Component,
  def = paths.public.login,
  user_type,
  ...rest
}) => {
  const { user } = useContext(Store)


  // const userAuthorities = (user)? user.authorities : []

  let token = localStorage.getItem('takent_token')
  const parseJwt = () => {
      try {
        return JSON.parse(atob(token.split('.')[1]))
      } catch (e) {
        return null
      }
    }
  let tokenData = parseJwt()

  
  const isUserAllowed = tokenData? tokenData.sub.user_type : false

  return (
    <Route
      {...rest}
      render={props =>
        user && isUserAllowed ?
          <Component {...props} user_type={tokenData.sub.user_type}  /> :
          <Redirect to={def} />
      }
    />
  )
}
export default PrivateRoute
