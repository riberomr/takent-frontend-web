import { Button, Grid } from '@material-ui/core'
import { Formik } from 'formik'
import React, { useEffect, useRef } from 'react'
import TextField from '../../Inputs/CustomTextField'
import Select from '../../Selects/CustomSelect/CustomSelect'
import GoogleMap from '../GoogleMap'
import AddressFormSchema from '../../../../rules/validationSchema/AddressForm'

const variant = "standard"

const parseProvince = (province, provinces) => {
  return provinces.find(element => element.name === province)?.code
}

const parseCity = (city, cities) => {
  const formattedCity = city.toUpperCase()
  return cities.find(element => element.name === formattedCity)?.code || "14014010000"
}

const parsePostalCode = postalCode => {
  return postalCode?.substring(1) ?? ""
}

const AddressForm = props => {

  const {
    address, 
    setAddress, 
    provinces, 
    cities, 
    setSelectedProvince,
    submitChanged
  } = props

  const initialValues = {
    province: address.province || "",
    city: address.city || "",
    district: address.district || "",
    street: address.street || "",
    number: address.number || "",
    postal_code: address.postal_code || "",
    tower: address.tower || "",
    floor: address.floor || "",
    apartment: address.apartment || "",
  }

  const submitButton = useRef()

  const setLatLng = latLng => {
    setAddress(prevState => {
      return {...prevState, location: latLng}
    })
  }

  const setAddressInfo = (addressInfo, setFieldValue) => {
    const province = parseProvince(addressInfo.province, provinces)
    const city = parseCity(addressInfo.city, cities)
    const convertedAddress = {...addressInfo, province: province, city: city}
    setAddress(prevState => {
      return {...prevState, ...convertedAddress}
    })
    if(province) {
      setFieldValue("province", province)
      setSelectedProvince(province)
    }
    if(city) {
      setFieldValue("city", city)
    }
    setFieldValue("district", addressInfo.district)
    setFieldValue("street", addressInfo.street)
    setFieldValue("number", addressInfo.number)
    setFieldValue("postal_code", parsePostalCode(addressInfo.postal_code))
  }

  const handleChangeProvince = (value, setFieldValue) => {
    setFieldValue('city', '' )
    setSelectedProvince(value)
    setFieldValue('province', value )
  }

  const handleChangeStreet = (value, setFieldValue) => {
    if (!value) {
        setFieldValue('number', '')
        setFieldValue('tower', '')
        setFieldValue('floor', '')
        setFieldValue('apartment', '')
        setFieldValue('postal_code', '')
    }
    setFieldValue('street', value)
  }

  const submit = (values) => {
    return
  }

  useEffect(() => {
    if(!submitChanged) return
    submitButton.current.click()
  }, [submitChanged])

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={AddressFormSchema}
      onSubmit={submit}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        errors,
        touched,
        setFieldValue
      }) => (
        <form onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <GoogleMap
                interactive
                geocoding
                setExtLatLng={setLatLng}
                setExtAddressInfo={(addressInfo) => setAddressInfo(addressInfo, setFieldValue)}
              />
            </Grid>
            <Grid item xs={12}>
              <Select
                id="province"
                name="province"
                label="Provincia"
                variant={variant}
                disabled={!address.location}
                fullWidth
                options={provinces}
                value={values.province}
                onChange={event => handleChangeProvince(event.target.value, setFieldValue)}
                error={errors.province && touched.province}
                helperText={(errors.province && touched.province)? errors.province : ""}
              />
            </Grid>
            <Grid item xs={12}>
              <Select
                id="city"
                name="city"
                label="Localidad"
                variant={variant}
                disabled={values.province === ''}
                fullWidth
                options={cities}
                value={values.city}
                onChange={event => setFieldValue("city", event.target.value)}
                error={errors.city && touched.city}
                helperText={(errors.city && touched.city)? errors.city : ""}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                id='district'
                type='text'
                name='district'
                label="Barrio"
                variant={variant}
                disabled={values.city === ''}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.district}
                error={errors.district && touched.district}
                helperText={(errors.district && touched.district)? errors.district : ""}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id='street'
                type='text'
                name='street'
                label="Calle"
                variant={variant}
                disabled={values.city === ''}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={(event) => handleChangeStreet(event.target.value, setFieldValue)}
                onBlur={handleBlur}
                value={values.street}
                error={errors.street && touched.street}
                helperText={(errors.street && touched.street)? errors.street : ""}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id='number'
                type='number'
                name='number'
                label="Número"
                onlyNumbers
                variant={variant}
                InputProps={{disabled: values.city === ""}}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.number}
                error={errors.number && touched.number}
                helperText={(errors.number && touched.number)? errors.number : ""}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id='postal_code'
                type='number'
                name='postal_code'
                label="CP"
                onlyNumbers
                variant={variant}
                fullWidth
                InputLabelProps={{shrink: true}}
                InputProps={{disabled: values.city === ""}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.postal_code}
                error={errors.postal_code && touched.postal_code}
                helperText={(errors.postal_code && touched.postal_code)? errors.postal_code : ""}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id='tower'
                type='text'
                name='tower'
                label="Torre"
                variant={variant}
                InputProps={{disabled: values.number === ""}}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.tower}
                error={errors.tower && touched.tower}
                helperText={(errors.tower && touched.tower)? errors.tower : ""}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id='floor'
                type='number'
                name='floor'
                label="Piso"
                onlyNumbers
                variant={variant}
                InputProps={{disabled: values.number === ""}}
                disabled={values.street === ''}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.floor}
                error={errors.floor && touched.floor}
                helperText={(errors.floor && touched.floor)? errors.floor : ""}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id='apartment'
                type='text'
                name='apartment'
                label="Departamento"
                variant={variant}
                InputProps={{disabled: values.number === ""}}
                disabled={values.street === ''}
                fullWidth
                InputLabelProps={{shrink: true}}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.apartment}
                error={errors.apartment && touched.apartment}
                helperText={(errors.apartment && touched.apartment)? errors.apartment : ""}
              />
            </Grid>
          </Grid>
          <Button ref={submitButton} style={{display: "none"}} type="submit">Submit</Button>
        </form>
      )}
    </Formik>
  )

}

AddressForm.defaultProps = {
  address: {}
}

export default AddressForm
