import { CircularProgress, makeStyles } from '@material-ui/core'
import React, { useEffect, useRef, useState } from 'react'
import { styles } from './styles'

const API_KEY = process.env.REACT_APP_GMAPS_KEY

const useStyles = makeStyles(styles)

const typeComponentMap = {
  administrative_area_level_1: "province",
  administrative_area_level_2: "city",
  neighborhood: "district",
  route: "street",
  street_number: "number",
  postal_code: "postal_code"
}

const GoogleMap = props => {

  const { 
    width,
    height,
    initialLatLng,
    interactive,
    geocoding,
    infoWindowValue,
    setExtLatLng,
    setExtAddressInfo,
    disabled
  } = props

  const classes = useStyles()

  const googleMapRef = useRef()

  const [ googleMap, setGoogleMap ] = useState(null)
  const [ geocoder, setGeocoder ] = useState(null)

  const [ loading, setLoading ] = useState(true)

  const [ initialMarker, setInitialMarker ] = useState(null)
  const [ latLng, setLatLng ] = useState(null)

  const toAddress = components => {
    let address = {}
    Object.entries(typeComponentMap).forEach(([key, value]) => {
      const item = components.find(component => component.types.includes(key))
      address[value] = item?.long_name ?? ""
    })
    return address
  }

  useEffect(() => {
    setLoading(true)
    const googleMapScript = document.createElement("script")
    googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&libraries=&v=weekly`
    window.document.body.appendChild(googleMapScript)
    googleMapScript.onload = () => {
      setLoading(false)
    }

    return () => {
      window.document.body.removeChild(googleMapScript)
    }
  }, [])

  useEffect(() => {
    if(loading) return

    const map = new window.google.maps.Map(googleMapRef.current, {
      center: initialLatLng || { lat: -31.4135, lng: -64.18105 },
      zoom: 8,
    })
    setGoogleMap(map)

    if(initialLatLng) {
      const marker = new window.google.maps.Marker({
        position: initialLatLng,
        map: map
      })
      setInitialMarker(marker)

      if(infoWindowValue) {
        const infoWindow = new window.google.maps.InfoWindow()
        infoWindow.setContent(infoWindowValue)
        infoWindow.open(map, marker)
      }
    }

    if(interactive) {
      map.addListener("click", event => {
        const clickedLatLng = event.latLng.toJSON()
        setLatLng(clickedLatLng)
        if(setExtLatLng)
          setExtLatLng(clickedLatLng)
      })

      if(geocoding) {
        const geocoder = new window.google.maps.Geocoder()
        setGeocoder(geocoder)
      }
    }
    // eslint-disable-next-line
  }, [loading])

  useEffect(() => {
    if(!interactive || disabled) return

    if(!latLng) return

    if(initialMarker) initialMarker.setMap(null)

    const marker = new window.google.maps.Marker({
      position: latLng,
      map: googleMap
    })

    return () => {
      marker.setMap(null)
    }
    // eslint-disable-next-line
  }, [latLng])

  useEffect(() => {
    if(!interactive || !geocoding || disabled || !setExtAddressInfo) return

    if(!latLng) return

    geocoder.geocode({location: latLng}, (results, status) => {
      setExtAddressInfo(status === "OK"? toAddress(results[0].address_components) : null)
    })
    // eslint-disable-next-line
  }, [latLng])

  return (
    <div className={classes.mapContainer}>
      {loading && <CircularProgress/>}
      <div 
        id="google-map"
        ref={googleMapRef}
        style={{ width: width, height: height, display: loading? "none" : "block"}}
      />
    </div>
  )

}

GoogleMap.defaultProps = {
  width: "100%",
  height: "400px",
  initialLatLng: null,
  interactive: false,
  geocoding: false,
  infoWindow: "",
  setExtLatLng: null,
  setExtAddressInfo: null,
  disabled: false
}

export default GoogleMap
