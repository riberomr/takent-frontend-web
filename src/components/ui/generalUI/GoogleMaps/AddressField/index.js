import React from 'react'
import GoogleMap from '../GoogleMap'
import _ from 'underscore'

const formatAddress = address => {
  let result = `${address.province.name} (${address.postal_code}), ${address.city.name} \n`
  result += address.district? `${address.district}, ${address.street} ${address.number} \n` : ""
  result += address.tower? `Torre ${address.tower}, Departamento ${address.apartment}` : ""
  return result
}

const AddressField = props => {

  const { address } = props

  return (
    <GoogleMap
      initialLatLng={address.location}
      infoWindowValue={formatAddress(_.omit(address, "location"))}
    />
  )

}

export default AddressField
