import React from 'react'
import { MenuItem, FormControl, InputLabel, Select, FormHelperText, makeStyles } from '@material-ui/core'
import { styles } from './styles'

const useStyles = makeStyles(styles)

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250
      }
    },
    // Show dropdow at bottom of select
    getContentAnchorEl: null,
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "left"
    },
    MenuListProps: {
      tabIndex: "1"
    }
  };

const CustomSelect = (props) => {

    const classes = useStyles()

    const renderOptions = () => {
        if (props.options != null) {
            return (props.options.map((option, index) =>
                (<MenuItem key={index} value={(typeof option) == 'string'? option : option.code} className={classes.menuItem}>
                    {(typeof option) == 'string'? option : option.name}
                </MenuItem>)));
        }
    }

    return (
      <FormControl variant={props.variant} fullWidth error={props.error} className={props.className}>
          <InputLabel shrink>{props.label}</InputLabel>
          <Select
              multiple={props.multiple}
              id={props.id}
              name={props.name}
              disabled={props.disabled}
              variant={props.variant}
              fullWidth
              onChange={props.onChange}
              onLoad={props.onLoad}
              value={props.value}
              inputProps={{readOnly: props.readOnly}}
              MenuProps={{ ...MenuProps, autoFocus: true }}
              tabIndex="1"
          >       
              {props.children}
              {renderOptions()}
          </Select>
          <FormHelperText>{props.helperText}</FormHelperText>
      </FormControl>
    )
}

export default CustomSelect