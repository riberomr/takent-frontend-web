import { theme } from "../../../../../constants/generalConstants/theme"

export const styles = {
    menuItem: {
        '&:hover': {
            backgroundColor: theme.palette.primary.main
        },
        backgroundColor: 'white'
    }
}