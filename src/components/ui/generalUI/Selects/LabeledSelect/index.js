import React from 'react'
import Select, { components } from 'react-select'
import { theme } from '../../../../../constants/generalConstants/theme'
const labeled = {
  container: base => ({ ...base, width: '100%' }),
  menuPortal: base => ({ ...base, zIndex: '9999' }),
  control: (base, state) => {
    return {
      ...base,
      '&:hover': {
        borderColor: ''
      },
      padding: '5px',
      minHeight: '56px',
      fontFamily: 'Roboto',
      borderColor: state.isFocused ? theme.palette.primary.main : base.borderColor,
      boxShadow: state.isFocused ? `0 0 0 0.5px ${theme.palette.primary.main}` : base.boxShadow
    }
  },
  valueContainer: base => {
    return {
      ...base,
      overflow: 'visible'
    }
  },
  placeholder: base => {
    return {
      ...base,
      position: 'absolute',
      top: '-8px',
      background: '#fff',
      color: theme.palette.primary.main,
      fontSize: '12px',
      transition: 'all .1s ease',
      lineHeight: 1,
      padding: '0 5px',
      fontFamily: 'Roboto',
    }
  },
  option: (base, state) => {
    return { ...base, background: state.isSelected ? theme.palette.primary.main : base.background, fontFamily: 'Roboto' }
  },
}
const CustomValueContainer = ({ children, ...props }) => {
  return (
    <components.ValueContainer {...props}>
      <components.Placeholder {...props}>
        {props.selectProps.placeholder}
      </components.Placeholder>
      {React.Children.map(children, child => {
        return child && child.type !== components.Placeholder ? child : null
      })}
    </components.ValueContainer>
  )
}
const LabeledSelect = ({ styles, components, ...props }) => {
  return (
    <Select
      styles={{ ...labeled, ...styles }}
      components={{ ...components, ValueContainer: CustomValueContainer }}
      noOptionsMessage={() => 'Sin resultados'}
      menuPortalTarget={document.body}
      {...props}
    />
  )
}
export default LabeledSelect