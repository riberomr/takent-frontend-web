import React from 'react'
import { Dialog, DialogTitle, Typography, IconButton, makeStyles, Divider, DialogContent } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { styles } from './styles'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles(styles)

const DataDialog = (props) => {

    const { title, data, open, setOpen, checkIcon, ...other} = props
    const classes = useStyles()

    return (
        <Dialog 
            onClose={() => setOpen(false)} 
            open={open} 
            maxWidth={"xs"}
            fullWidth={true}
            className={classes.dialog}
            {...other}
        >
            <DialogTitle disableTypography className={classes.title}>
                <Typography variant="h5">
                    {title}
                </Typography>
                <IconButton onClick={() => setOpen(false)}>
                    <CloseIcon/>
                </IconButton>
            </DialogTitle>
            <DialogContent dividers classes={{dividers: classes.dividers}}>
                {data && Object.keys(data).map( (key, index) => (
                    <>
                        <div className={classes.itemContainer}>
                            <div className={classes.dataIcon}>
                                {checkIcon ? <CheckCircleIcon style={{color: '#F29222'}}/> : <FiberManualRecordIcon style={{color: '#F29222'}}/>}
                            </div>
                            <div className={classes.dataItem}>
                                <Typography gutterBottom variant="h6">
                                    {key}
                                </Typography>
                                <Typography variant="body1">
                                    {data[key]}
                                </Typography>
                            </div>
                        </div>
                        {index+1 !== Object.keys(data).length && <Divider light/>}
                    </>)
                )}
            </DialogContent>
        </Dialog>
    )
}

export default DataDialog