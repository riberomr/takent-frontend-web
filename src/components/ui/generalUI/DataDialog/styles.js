import { theme } from "../../../../constants/generalConstants/theme"

export const styles = {
    dialog: {
        marginTop: "48px"
    },
    itemContainer: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    dataIcon: {
        marginRight: '10px'
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    dataItem: {
        padding: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        overflowWrap: 'break-word'
    },
    dividers: {
        padding: '16px 24px',
        borderTop: `1px solid ${theme.palette.primary.main}`,
        borderBottom: `1px solid ${theme.palette.primary.main}`,
        '&::-webkit-scrollbar': {
            width: '0.4em'
            },
          '&::-webkit-scrollbar-track': {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
          },
          '&::-webkit-scrollbar-thumb': {
            backgroundColor: 'orange',
            outline: '1px solid slategrey',
            borderRadius: 'inherit'
          },
    },
}