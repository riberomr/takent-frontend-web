export const styles = {
    button:{
        width: '250px',
        height: '40px !important',
        borderRadius: '3px !important'
    },
    text:{
        color: 'black',
        fontWeight: '600',
        fontSize: '18px',
        letterSpacing: '1.25px',
        lineHeight: '16px',
    }
}
