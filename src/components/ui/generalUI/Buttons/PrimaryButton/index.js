import React from 'react'
// import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(styles)

const FormButton = (props) => {
    const { children, ...rest } = props
    const classes = useStyles()

  return (
    <Button 
        className={classes.button}
        {...rest}> 
            <span className={classes.text}>{children}
            </span>
    </Button>
  )
}

export default FormButton
