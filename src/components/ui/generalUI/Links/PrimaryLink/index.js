import React from 'react'
import { Link } from 'react-router-dom'

const StyledLink = (props) => {
    const {children, ...rest} = props
    return (
        <Link  {...rest}>
        <p>{children}</p>
        </Link>
    )
}

export default StyledLink