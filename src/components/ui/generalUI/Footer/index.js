import React from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { IconButton, Typography } from '@material-ui/core'
import InstagramIcon from '@material-ui/icons/Instagram'
import FacebookIcon from '@material-ui/icons/Facebook'
import MailIcon from '@material-ui/icons/Mail'

const useStyles = makeStyles(styles)

const Footer = () =>{
    
    const classes = useStyles()

    return(
        <div className={classes.container}>
          <div></div>
          <div style={{display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
            <Typography variant="caption">2020-2021 {"\u00A9"} Takent ARG, Inc. or its affiliates</Typography>
            <div>
              <Typography component="a" variant="caption" color="primary" className={classes.link} href="/termsAndConditions">Políticas de privacidad</Typography>|
              <Typography component="a" variant="caption" color="primary" className={classes.link} href="/support">Soporte</Typography>|
              <Typography component="a" variant="caption" color="primary" className={classes.link} href="/opinions">Dejanos tu opinión</Typography>
            </div>
          </div>
          <div className={classes.socialNetworks}>
            <IconButton href="mailto:takentuser@gmail.com"><MailIcon/></IconButton>
            <IconButton href="https://www.instagram.com/takentapp/" target="_blank"><InstagramIcon/></IconButton>
            <IconButton href="https://www.facebook.com/Takent-107583084366371" target="_blank"><FacebookIcon/></IconButton>
          </div>
        </div>
    )
}
export default Footer