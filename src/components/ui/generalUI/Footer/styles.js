import { theme } from "../../../../constants/generalConstants/theme";

export const styles = {
    container:{
        position: "relative",
        width:'100%',
        background: '#DCDCDC',
        height: '48px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        zIndex: theme.zIndex.drawer + 1,
        boxShadow: '0px -2px 4px -1px rgba(0,0,0,0.2), 0px -4px 5px 0px rgba(0,0,0,0.14), 0px -1px 10px 0px rgba(0,0,0,0.12)',
        boxSizing: 'border-box',
        flexShrink: 0,
        transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
    },
    socialNetworks: {
      display: 'flex',
      paddingRight: theme.spacing(2),
      marginLeft: '-112px'
    },
    link: {
      textDecoration: "underline",
      margin: theme.spacing(0, 0.5, 0, 0.5)
    }
}
