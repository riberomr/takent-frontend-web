import React, { useState } from 'react'
import { CircularProgress, IconButton, Tooltip } from '@material-ui/core'
import AttachFileIcon from '@material-ui/icons/AttachFile'
import { useSnackbar } from 'notistack'

const DownloadAttachment = props => {

  const { downloadFunction, ...other } = props
  const { enqueueSnackbar } = useSnackbar()

  const [ loading, setLoading ] = useState(false)

  const handleClick = async () => {
    setLoading(true)
    downloadFunction()
    .then(response => {
      if(!response.data.base64)
        return enqueueSnackbar('El postulante no tiene cargado su CV', { variant: 'info' })
      const linkSource = `data:application/pdf;base64, ${response.data.base64}`
      const downloadLink = document.createElement("a")
      const fileName = response.data.name
      downloadLink.href = linkSource
      downloadLink.download = fileName
      downloadLink.click()
    })
    .finally(() => setLoading(false))
  }

  return (
    <Tooltip title="Descargar CV">
      <IconButton 
        color="primary"
        component="span"
        onClick={handleClick}
        {...other}
      >
        {loading? <CircularProgress size={20} style={{color: "#F29222"}}/> : <AttachFileIcon/>}
      </IconButton>
    </Tooltip>
  )

}

export default DownloadAttachment
