import { Button, CircularProgress } from '@material-ui/core'
import React from 'react'

const LoadingButton = ({
  children,
  loading,
  disabled,
  style,
  loadingIconSize,
  ...other
}) => {

  let loadingSize
  if(loadingIconSize) {
    loadingSize = loadingIconSize
  }
  else {
    switch(other.size) {
      case "small":
        loadingSize = 16
        break
      case "medium":
        loadingSize = 19
        break
      case "large":
        loadingSize = 22
        break
      default:
        loadingSize = 19
    }
  }

  return (
    <Button
      style={{color: "black", ...style}}
      startIcon={loading && <CircularProgress style={{color: "black"}} size={loadingSize}/>}
      disabled={disabled || loading}
      {...other}
    >
      {children}
    </Button>
  )

}

LoadingButton.defaultProps = {
  loading: false,
  variant: "contained",
  color: "primary"
}

export default LoadingButton
