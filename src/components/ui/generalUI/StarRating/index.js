import React from 'react'
import Rating from '@material-ui/lab/Rating'

const StarRating = props => {

  const { submit, ...other } = props

  return (
    <Rating
      {...other}
      name="rating-service"
      onChange={submit? (event, value) => submit(value) : null}
    />
  )

}

StarRating.defaultProps = {
  precision: 1,
  size: "medium"
}

export default StarRating
