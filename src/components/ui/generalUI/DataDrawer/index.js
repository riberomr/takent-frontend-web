import {
  Avatar, 
  Divider,
  Drawer,
  IconButton, 
  makeStyles, 
  Typography, 
  List, ListItem, 
  ListItemText,
  Collapse } from '@material-ui/core'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import clsx from 'clsx'
import React,{useState} from 'react'
import { theme } from '../../../../constants/generalConstants/theme'
import CustomCarousel from '../CustomCarousel'
import { styles } from './styles'
import CloseIcon from '@material-ui/icons/Close'
import { format } from 'date-fns'
import GoogleMap from '../GoogleMaps/GoogleMap'

// Example of use

// const data = {
//   title: "Oferta de ejemplo",
//   content: {
//     "Autor": "Jeremias Gonzalez",
//     "Descripción": "Se busca desarrollador backend"
//   },
//   images: Array(3).fill("https://estaticos.muyinteresante.es/media/cache/760x570_thumb/uploads/images/article/59a3df3f5bafe86dc90b4c63/internet-de-las-cosas_0.jpg")
// }

// <DataDrawer
//   anchor="right"
//   isOpen={isDrawerOpen}
//   setIsOpen={setIsDrawerOpen}
//   variant="temporary"
//   data={data}
// />

const useStyles = makeStyles(styles)

const DataDrawer = props => {

  const { anchor, isOpen, setIsOpen, data, ...other } = props

  const classes = useStyles()
   const [openEducation, setOpenEducation] = useState(false);
   const [openExperience, setOpenExperience] = useState(false);
   const [openLenguages, setOpenLanguages] = useState(false);
   const [openSkills, setOpenSkills] = useState(false);
   
   const handleClickEducation = () => {
    setOpenEducation(!openEducation);
  };

  const handleClickExperiencie = () => {
    setOpenExperience(!openExperience);
  };

  const handleClickLenguages = () => {
    setOpenLanguages(!openLenguages);
  };

  const handleClickSkills = () => {
    setOpenSkills(!openSkills);
  };

  const handleClose = () => {
    setIsOpen(false)
    setOpenEducation(false)
    setOpenExperience(false)
    setOpenLanguages(false)
    setOpenSkills(false)
  }
  return (
    <Drawer
      anchor={anchor || "right"}
      open={isOpen}
      onClose={() => handleClose()}
      PaperProps={{style: {backgroundColor: 'white'}}}
      {...other}
    >
      <div className={clsx(classes.data, {
        [classes.fullData]: anchor === "top" || anchor === "bottom",
      })}
      >
        <div className={classes.title}>
          <div className={classes.titleDivFlex}>
            {data.profilePhoto && <Avatar src={data.profilePhoto ?? null} style={{marginRight: theme.spacing(2), height: '60px', width: '60px'}}/>}
            <Typography variant="h5">{data.title}</Typography>
          </div>
          <IconButton onClick={() => setIsOpen(false)}>
            <CloseIcon />
          </IconButton>
        </div>
        <Divider classes={{root: classes.divider}} />
        {data.content && Object.keys(data.content).map((key, index) => {
          if(Array.isArray(data.content[key])) {
            let key1 = Object.keys(data.content[key][0])[0]
            let key2 = Object.keys(data.content[key][1])[0]

            return (<><div className={clsx(classes.contentItem, classes.arrayContent)} key={index}>
              <div className={classes.subItem}>
                <Typography variant="h6">{key1}</Typography>
                <Typography variant="body1">{data.content[key][0][key1] || data.content[key][0][key1] === 0 ? data.content[key][0][key1] : '-'}</Typography>
              </div>
              <div className={classes.subItem}>
                <Typography variant="h6">{key2}</Typography>
                <Typography variant="body1">{data.content[key][1][key2] || data.content[key][1][key2] === 0? data.content[key][1][key2] : '-'}</Typography>
              </div>
            </div>
            <Divider light style={{marginTop: theme.spacing(2)}} />
            </>)
            }
          else 
            return (<div className={classes.contentItem} key={index}>
                <Typography variant="h6">{key}</Typography>
                <Typography variant="body1">{data.content[key] || data.content[key] === 0? data.content[key] : '-'}</Typography>
                <Divider light style={{marginTop: theme.spacing(2)}} />
              </div>)
          }
        )}
        {data.location && <>
          <div className={classes.locationContainer}>
            <Typography variant="h6" gutterBottom>Ubicación</Typography>
            <Typography variant="subtitle2">
              {data.location.province.name}
              {data.location.postal_code && ` (${data.location.postal_code})`},
              {" " + data.location.city.name}
            </Typography>
            <Typography variant="body2">
              {data.location.district && `${data.location.district}, `}
              {`${data.location.street} ${data.location.number? data.location.number : ''}`}
            </Typography>
            {data.location.tower && <Typography variant="body2">
              Torre {data.location.tower}, Piso {data.location.floor}, Departamento {data.location.apartment}
            </Typography>}
          </div>
          <Divider light style={{marginTop: theme.spacing(2)}} />
          {data.location.location && <GoogleMap height="300px" initialLatLng={data.location.location}/>}
          </>}
          {data?.education && <Typography className={classes.locationContainer} variant="h6">{'Currículum virtual'}</Typography>}
          {data && data.education &&
          <List subheader={<li />}>
            <div className={classes.divListTitle} onClick={handleClickEducation} ><Typography variant="body2">{`Datos académicos`}</Typography>{openEducation ? <ExpandLess /> : <ExpandMore />}</div>
            {data.education.length !== 0 && data.education.map((data) => (
              <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openEducation} timeout="auto" unmountOnExit>  
                  <div className={classes.listItem}>
                    <ListItemText>{format(new Date(data.start_date), 'dd/MM/yyyy') + " - " + (data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : "Actualidad")}</ListItemText>
                    <ListItemText>Título: <span className={classes.spanDataCollapse}>{data.title}</span></ListItemText>
                    <ListItemText>Institución: <span className={classes.spanDataCollapse}>{data.institution}</span></ListItemText>
                    <ListItemText>Nivel alcanzado: <span className={classes.spanDataCollapse}>{data.level}</span></ListItemText>
                    {data.description &&<ListItemText>Comentario: <span className={classes.spanDataCollapse}>{data.description}</span></ListItemText>}
                  </div>
                </Collapse>
              </ListItem>
             ))}
             {data.education.length === 0 && 
             <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openEducation} timeout="auto" unmountOnExit>
                    <ListItemText><span className={classes.spanNoDataCollapse}>No agregó información</span></ListItemText>
                </Collapse>
              </ListItem>}
          </List>}

          {data && data.experience &&
          <List subheader={<li />}>
            <div className={classes.divListTitle} onClick={handleClickExperiencie}><Typography variant="body2">{`Experiencia`}</Typography> {openExperience ? <ExpandLess /> : <ExpandMore />}</div>
            {data.experience.length !== 0 &&  data.experience.map((data) => (
              <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openExperience} timeout="auto" unmountOnExit>
                  <div className={classes.listItem}>
                    <ListItemText>{format(new Date(data.start_date), 'dd/MM/yyyy') + " - " + (data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : "Actualidad")}</ListItemText>
                    <ListItemText>Empresa: <span className={classes.spanDataCollapse}>{data.company}</span></ListItemText>
                    <ListItemText>Puesto: <span className={classes.spanDataCollapse}>{data.position}</span></ListItemText>
                    {data.description &&<ListItemText>Comentario: <span className={classes.spanDataCollapse}>{data.description}</span></ListItemText>}
                    {data.reference && Object.keys(data.reference).length !== 0 && <ListItemText>Referencia: <span className={classes.spanDataCollapse}>{data.reference.name}{data.reference.phone? ' - '+data.reference.phone: ''}</span></ListItemText>}
                  </div>
                </Collapse>
                </ListItem>
             ))}
             {data.experience.length === 0 && 
             <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openExperience} timeout="auto" unmountOnExit>
                    <ListItemText><span className={classes.spanNoDataCollapse}>No agregó información</span></ListItemText>
                </Collapse>
              </ListItem>}
          </List>}

          {data && data.language &&
          <List subheader={<li />}>
            <div className={classes.divListTitle} onClick={handleClickLenguages}><Typography variant="body2">{`Idiomas`}</Typography> {openLenguages ? <ExpandLess /> : <ExpandMore />}</div>
            {data.language.length !== 0 && data.language.map((data) => (
              <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openLenguages} timeout="auto" unmountOnExit>
                  <div className={classes.listItem}>
                    <ListItemText>{data.language}</ListItemText>
                    <ListItemText>Nivel de comunicación: <span className={classes.spanDataCollapse}>{data.communication_level}</span></ListItemText>
                    <ListItemText>Nivel de escritura: <span className={classes.spanDataCollapse}>{data.writing_level}</span></ListItemText>
                    <ListItemText>Certificado: <span className={classes.spanDataCollapse}>{data.certificate? 'Si' : 'No'}</span></ListItemText>
                    {data.description && <ListItemText>Comentario: <span className={classes.spanDataCollapse}>{data.description}</span></ListItemText>}
                  </div>
                </Collapse>
              </ListItem>
             ))}
             {data.language.length === 0 && 
             <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openLenguages} timeout="auto" unmountOnExit>
                    <ListItemText><span className={classes.spanNoDataCollapse}>No agregó información</span></ListItemText>
                </Collapse>
              </ListItem>}
          </List>}

          {data && data.skill &&
          <List subheader={<li />}>
            <div className={classes.divListTitle} onClick={handleClickSkills}><Typography variant="body2">{`Aptitudes`}</Typography> {openSkills ? <ExpandLess /> : <ExpandMore />}</div>
            {data.skill.length !== 0 && data.skill.map((data) => (
              <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openSkills} timeout="auto" unmountOnExit>
                  <div className={classes.listItem}>
                    <ListItemText>{data.name}</ListItemText>
                    {data.description && <ListItemText>Comentario: <span className={classes.spanDataCollapse}>{data.description}</span></ListItemText>}
                  </div>
                </Collapse>
              </ListItem>
             ))}
             {data.skill.length === 0 && 
             <ListItem classes={{root: classes.listItemPadding}}>
                <Collapse className={classes.collapseWidth} in={openSkills} timeout="auto" unmountOnExit>
                    <ListItemText><span className={classes.spanNoDataCollapse}>No agregó información</span></ListItemText>
                </Collapse>
              </ListItem>}
          </List>}
          
        {data.images &&
          <div className={classes.contentItem}>
            <Typography variant="h6" gutterBottom>Imágenes</Typography>
            <CustomCarousel
              images={data.images}
              slideLength={2}
              height={250}
              autoPlay={false}
              animation={"slide"}
              navButtonsAlwaysVisible={true}
            />
          </div>
        }
      </div>
    </Drawer>
  )
}

export default DataDrawer
