import React from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles(styles)

const AlertAction = (props) => {
    //actions will be buttons
    //type will define the icon, can be succes, error, warning, doubt, doubt2
    const { actions, type, title, subtitle, isOpen, setIsOpen, loading, moreThanTwoActions, children } = props

    const classes = useStyles(moreThanTwoActions)

    const srcImgLogo = `/images/AlertIcons/${type}.png`

    const handleSubmit = (actionFunction, type) => {
        actionFunction(type)
    }

    const handleClose = () => {
        setIsOpen(false)
    }
  return (
        <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={isOpen} className={classes.container}>
            <div className={classes.mainContent}>
                <div className={classes.containerIcon}>
                    <img className={classes.imgIcon} src={srcImgLogo} alt="takent" />
                </div>
                <div className={classes.titleContainer}>
                    <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
                    {subtitle && <span className={classes.spanSubtitle}>{subtitle}</span>}
                </div>
                {children}
                <div className={classes.buttonContainer}>
                    {actions.length > 0 ? actions.map((action, index) => { // this fuction will map more than 1 button, and can be extended
                        return (
                        <Button
                            key={index}
                            variant="contained"
                            disabled={loading}
                            color="primary"
                            onClick={() => handleSubmit(action.handleSubmit, action.type)}
                            className={classes.submitButton}
                        >
                            {loading ? <CircularProgress size={20} /> : action.name}
                        </Button>
                        )
                    }) : null}
                </div>
            </div>
        </Dialog> 
  )
}

export default AlertAction
