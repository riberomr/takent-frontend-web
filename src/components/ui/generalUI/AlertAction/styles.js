export const styles = (moreThanTwoActions) => ({
    button:{
        width: '250px',
        height: '40px !important',
        borderRadius: '3px !important'
    },
    text:{
        color: 'black',
        fontWeight: '600',
        fontSize: '14px',
        letterSpacing: '1.25px',
        lineHeight: '16px',
    },
    imgIcon:{
        width: '90px'
    },
    containerIcon:{
        display: 'flex',
        justifyContent: 'center'
    },
    container:{
        border: 'solid 1px'
    },
    buttonContainer:{
        display: 'flex',
        justifyContent: 'space-around',
        minWidth: '300px'
    },
    mainContent:{
        margin: '25px'
    },
    titleContainer:{
        display: 'flex',
        justifyContent: 'center',
        margin: '10px 0px 14px 0px',
        flexDirection: 'column',
        textAlign: 'center'
    },
    spanSubtitle:{
        fontFamily: 'roboto',
        marginBottom: '15px'
    },
    submitButton: {
        fontFamily: 'Roboto',
        color: 'black',
        width: moreThanTwoActions? 'unset' : '120px'
    }
})
