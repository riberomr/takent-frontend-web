export const styles = (hoverable, height) => ({
  slideItem: {
    backgroundColor: 'lightgrey',
    height: '100%',
    width: '100%',
    overflow: 'hidden',
    position: 'relative',
    backgroundSize: 'contain', //cover
    cursor: 'pointer',
    '&:hover': {
      opacity: '0.95'
    }
  },
  slide: {
    position: 'relative',
  },
  slideGrid: {
    height: '100%'
  }
})