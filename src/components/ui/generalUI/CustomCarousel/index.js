import React, { useEffect, useState } from 'react'
import Carousel from 'react-material-ui-carousel'
import { Card, CardMedia, Grid, makeStyles } from '@material-ui/core'
import { styles } from './styles'

const WHITE_IMAGE_URL = "https://wwwidentoes-ans9ff0uc6be.stackpathdns.com/wp-content/plugins/accelerated-mobile-pages/images/SD-default-image.png"

const useStyles = makeStyles(styles)

const Slide = (props) => {

  const { images, height } = props
  

  const classes = useStyles()

  let items = []

  for(let i = 0; i < images.length; i++) {
    const item = images[i]

    const media = (
      <Grid item xs={12 / images.length} key={i}>
        <CardMedia
          className={classes.slideItem}
          image={item}
        >
        </CardMedia>
      </Grid>
    )
    items.push(media)
  }

  return (
    <Card raised className={classes.slide}>
      <Grid container spacing={0} className={classes.slideGrid} style={{height: height}}>
        {items}
      </Grid>
    </Card>
  )
}

const CustomCarousel = (props) => {

  const { images, slideLength, height, ...other } = props

  const [ slides, setSlides ] = useState(
    [(<Slide 
        key={0} 
        images={Array(slideLength).fill(WHITE_IMAGE_URL)} 
        height={height? height : '400px'}/>
    )])

  const step = slideLength? slideLength : 3

  useEffect(() => {
    if(images.length === 0) {
      setSlides(
        [(<Slide
            key={0}
            images={Array(slideLength).fill(WHITE_IMAGE_URL)}
            height={height? height : '400px'}
          />)])
    }
    else {
      let slidesToView = []
      let i = 0
      while(i < images.length) {
        let slideImages = images.slice(i, i + step)
        
          if(slideImages.length !== slideLength) {
           slideImages = [...slideImages, ...Array(slideLength - slideImages.length).fill(WHITE_IMAGE_URL)]
        }
        const slide = (
          <Slide
            key={i}
            images={slideImages}
            height={height? height : '400px'}
          />
        )
        slidesToView.push(slide)
        i = i + step
      }
      setSlides(slidesToView)
    }
  }, [images, slideLength, step, height])

  return (
    <Carousel key={slides.length} {...other}>
      {slides}
    </Carousel>
  )
}

export default CustomCarousel
