import {
    Divider,
    Drawer,
    IconButton,
    makeStyles,
    Tooltip,
    Typography } from '@material-ui/core'
import clsx from 'clsx'
import React, { useEffect, useState } from 'react'
import { styles } from './styles'
import CloseIcon from '@material-ui/icons/Close'
import AllowedUserForm from '../../../../ui/businessUI/EnterpriseUser/AllowedUser';
import { useSnackbar } from 'notistack';
import { apiTakent } from '../../../../../services/API-TAKENT'
import { differenceInCalendarYears } from 'date-fns';

const useStyles = makeStyles(styles);

const TRADUCED_STATE = {
    'active': 'Activa',
    'inactive': 'Inactiva',
    'in_selection': 'En selección',
    'finished': 'Finalizada',
    'deleted': 'Eliminada',
}

const FormDrawerAllowedUsers = (props) => {
    const classes = useStyles();

    const { anchor, isOpen, setIsOpen, data, loading, setLoading, users, setUsers, ...other } = props;

    const [ usersChanged, setUsersChanged ] = useState(null)

    const handleClose = () => {
        setIsOpen(false);
    }

    const { enqueueSnackbar } = useSnackbar()

    const handleSubmit = values => {
      setLoading(true)
      apiTakent.postAllowedUsers(values)
      .then(() => {
        setIsOpen(false)
        enqueueSnackbar('Usuario colaborador registrado con éxito', { variant: 'success' })
      })
      .catch(() => {
        enqueueSnackbar('Error al intentar registrar usuario colaborador', { variant: 'error' })
      })
      .finally(() => {
        setLoading(false)
        setUsersChanged(Math.random())
      })
    }

    useEffect(() => {
      apiTakent.getEnterpriseUsers()
      .then(res => {
        let data = res.data.map(elem => {
          return {
            ...elem,
            nameForTable: elem.permission === 'admin' ? (<div style={{ display: 'flex' }}>
                <Tooltip title="Usuario administrador">
                    <i className="fas fa-crown" style={{ paddingRight: "5px", color: "#F29222", marginLeft: '20px' }}></i>
                </Tooltip>
                <span style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', marginLeft: '25px' }}>{elem.name}</span>
            </div>) : elem.name,
            state: TRADUCED_STATE[elem.state],
            age: differenceInCalendarYears(new Date(), new Date(elem.birth_date)) - 1,
            position: elem.position !== undefined && elem.position !== '' ? elem.position : "-",
            name: elem.name
          }
        })
        setUsers(data)
      })
    }, [usersChanged, setUsers])

    return (
        <Drawer
            anchor={anchor || "right"}
            open={isOpen}
            onClose={() => handleClose()}
            PaperProps={{ style: { backgroundColor: 'white' } }}
            {...other}
        >
            <div className={clsx(classes.data, {
                [classes.fullData]: anchor === "top" || anchor === "bottom",
            })}
            >
                <div className={classes.title}>
                    <div className={classes.titleDivFlex}>
                        <Typography variant="h5">{data.title}</Typography>
                    </div>
                    <IconButton onClick={() => setIsOpen(false)}>
                        <CloseIcon />
                    </IconButton>

                </div>
                <Divider classes={{ root: classes.divider }} />

                <AllowedUserForm
                    handleFormSubmit={handleSubmit}
                    loading={loading}
                    setLoading={setLoading}
                    setIsOpen={setIsOpen}
                />
            </div>

        </Drawer>
    );
}

export default FormDrawerAllowedUsers;