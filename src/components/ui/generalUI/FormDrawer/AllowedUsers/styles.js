import { theme } from "../../../../../constants/generalConstants/theme";

export const styles = {
  data : {
    width: (window.innerWidth / 2) + "px",
    maxWidth: "500px",
    padding: theme.spacing(4)
  },
  fullData : {
    width: "auto",
    height: "250px"
  },
  divider: {
    background: theme.palette.primary.main
  },
  contentItem: {
    marginTop: theme.spacing(2)
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(2)
  },
  arrayContent: {
    display:'flex',
    justifyContent: 'space-between'
  },
  subItem: { 
    display: 'grid',
    width: '50%'
  },
  locationContainer: {
    marginTop: '10px'
  },
  listItem: {
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'initial',
    alignItems: 'flex-start',
    border: 'solid 2px '+theme.palette.primary.main,
    borderRadius: '15px',
    width: '90%',
    padding: '10px'
  },
  divListTitle:{
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  listItemPadding:{
    paddingTop: '4px',
    paddingBottom: '4px'
  },
  collapseWidth: {
    width: 'inherit'
  },
  spanDataCollapse:{
    fontWeight: 'normal'
  },
  spanNoDataCollapse:{
    textAlign: 'center',
    color: '#b2b2b2',
    fontWeight: 'normal'
  },
  titleDivFlex: {
    display: 'flex',
    alignItems: 'center'
  }
}