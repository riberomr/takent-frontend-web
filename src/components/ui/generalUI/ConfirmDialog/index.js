import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import { styles } from './styles'
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';

const useStyles = makeStyles(styles)

export const ConfirmDialog =(props)=> {

  const classes = useStyles();
  const {setOpenModalConfirm, openModalConfirm, success=true, acceptFunction = null } = props;
  const messageSuccessPart1 = "¡Tu postulación está en marcha!"
  const messageSuccessPart2 ="¡Éxitos!"
  const urlSuccess="/images/AlertIcons/success.png"
  const urlError= "/images/AlertIcons/error.png"
  const messageError= "¡Algo falló! Lo sentimos..."
  let messageResult=''
  let  urlResult=''

  const handleClose = () => {
    if(acceptFunction) acceptFunction()
    setOpenModalConfirm(false)
  };
    if(success){
        messageResult=messageSuccessPart1
        urlResult=urlSuccess
    }else{
        messageResult=messageError
        urlResult=urlError
    }
      
  return (
    <Dialog classes={{ root: classes.dialog }} onClose={handleClose} aria-labelledby="simple-dialog-title" open={openModalConfirm}>
        <div className={classes.container}>
          <div className={classes.containerAvatar}>
              <div className={classes.avatar}>
                <Avatar className ={classes.image} alt="logo-maletin" src={urlResult} variant="square" />
              </div>
          </div>
          <div className={classes.containerText}>
                <span>{messageResult}</span>
                {success && <span className={classes.secondSpan}>{messageSuccessPart2}</span>}
          </div>
          <div className={classes.containerButton} >
            <Button  
            className={classes.textButton}
            onClick={handleClose}>Aceptar</Button> 
          </div> 
      </div>
    </Dialog>
  );
}