
export const styles = () => ({

dialog: {
    '&& .MuiDialog-paperWidthSm': {
      borderRadius:'8px'   
     }
 },   
container: {
 width: '350px',
 height: '240px',
 borderWidth: '1px',
 borderStyle: 'solid',
 borderColor: 'Gray',
 borderRadius:'4px'
},
containerAvatar:{
 height:'33%',
 width:'100%',
 justifyContent:'center',
 display: 'flex',
 alignItems: 'center',  
},
avatar:{
    justifyContent:'center',
    display: 'flex',
    alignItems: 'center', 
    width:'100%',
    paddingTop: '50px',
    paddingLeft: '10px'
   },
image:{
  width: '20%',
  height: '100%',
  borderRadius:'0'
   },
containerText:{
 fontSize: '16px',
 fontWeight: 'bold',
 fontFamily: 'Roboto', 
 height:'33%',
 width:'100%',
 justifyContent:'center',
 display: 'flex',
 flexDirection: 'column',
 alignItems: 'center', 
},
containerButton:{
 height:'33%',
 width:'100%',
 justifyContent:'center',
 display: 'flex',
 alignItems: 'center', 
},
textButton:{
 fontFamily: 'Roboto', 
 backgroundColor: '#F29222',
 color: 'black',
 fontWeight: '600',
 fontSize: '18px',
 letterSpacing: '1.25px',
 lineHeight: '16px',
  },
  secondSpan:{
    marginTop: '10px'
  }

});