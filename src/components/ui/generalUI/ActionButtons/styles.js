export const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'space-evenly',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'flex-end',
      '& :not(:first-child)' : {
        marginLeft: '20px'
      }
    }
  },
  buttons:{
    boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
    minHeight: '40px',
    minWidth: '40px',
    borderRadius: '100%',
    backgroundColor: 'white',
    color: 'orange',
    '&&:hover ' : {
      backgroundColor: 'orange',
      color: 'white'
    },
  }
})