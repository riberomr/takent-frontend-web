import React from 'react'
import Button from '@material-ui/core/Button'
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined'
import EventBusyIcon from '@material-ui/icons/EventBusy'
import PeopleIcon from '@material-ui/icons/People'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd'
import EditIcon from '@material-ui/icons/Edit'
import Tooltip from '@material-ui/core/Tooltip'
import clsx from 'clsx'
// import { Store } from '../../../../Store'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import RefreshIcon from '@material-ui/icons/Refresh'
import CachedIcon from '@material-ui/icons/Cached'
import CloseIcon from '@material-ui/icons/Close'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

const useStyles = makeStyles(styles)

const ActionButtons = (props) => {
  //this component is very very specific, i mean, at the moment i wasn't think about flexibility
  //actionButtonsProps is an object that conteins more props, that's the reason because i made double destructuring below

  const classes = useStyles()
  const { actionButtonsProps, rowData, className } = props
  const { 
    firstTooltip,
    secondTooltip, 
    thirdTooltip, 
    fourthTooltip,
    fifthTooltip,
    fifthTooltiBiz,
    sixthTooltip,
    seventhTooltip,
    eighthTooltip,
    firstFunction, 
    secondFunction, 
    thirdFunction, 
    fourthFunction,
    fifthFunction,
    sixthFunction,
    seventhFunction,
    eighthFunction
  } = actionButtonsProps
  // const context = useContext(Store)
  return (
    <div id='action-buttons' className={clsx(classes.container, className)}>
      {rowData.state !== 'Borrador' && rowData.state !== 'Programada' && rowData.state !== 'Eliminada' &&
        <Tooltip title={firstTooltip}>
          <Button
            className={classes.buttons}
            onClick={() => { firstFunction(rowData) }}
            >
            <PeopleIcon />
          </Button>
        </Tooltip>
      }
      {rowData.state === 'Borrador' && 
        <Tooltip title={firstTooltip}>
          <Button
            className={classes.buttons}
            onClick={() => { firstFunction(rowData) }}
            >
            <DescriptionOutlinedIcon />
          </Button>
        </Tooltip>
      }
      {rowData.state !== 'Borrador' && 
        <Button
          className={classes.buttons}
          onClick={() => secondFunction(rowData)}
          >
          <Tooltip title={secondTooltip}>
            <DescriptionOutlinedIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Borrador' && 
        <Tooltip title={secondTooltip}>
          <Button
            className={classes.buttons}
            onClick={() => secondFunction(rowData)}
            >
              <CloseIcon />
          </Button>
        </Tooltip>
      }
      {rowData.state === 'Activa' && 
        <Button
          className={classes.buttons}
          onClick={() => thirdFunction(rowData)}
          >
          <Tooltip title={thirdTooltip}>
            <AssignmentIndIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'En selección' && 
        <Button
          className={classes.buttons}
          onClick={() => fifthFunction(rowData)}
          >
          <Tooltip title={fifthTooltip}>
            <RefreshIcon/>
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Programada' && 
        <Button
          className={classes.buttons}
          onClick={() => fifthFunction(rowData)}
          >
          <Tooltip title={fifthTooltiBiz}>
            <RefreshIcon/>
          </Tooltip>
        </Button>
      }
      {(rowData.state === 'Activa' || rowData.state === 'En selección') &&
        <Button
          className={classes.buttons}
          onClick={() => fourthFunction(rowData)}
          >
          <Tooltip title={fourthTooltip}>
            <EventBusyIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Finalizada' &&
        <Button
          className={classes.buttons}
          onClick={() => sixthFunction(rowData)}
          >
          <Tooltip title={sixthTooltip}>
            <CachedIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Eliminada' &&
        <Button
          className={classes.buttons}
          onClick={() => sixthFunction(rowData)}
          >
          <Tooltip title={sixthTooltip}>
            <CachedIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Programada' &&
        <Button
          className={classes.buttons}
          onClick={() => seventhFunction(rowData)}
          >
          <Tooltip title={seventhTooltip}>
            <EditIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Programada' &&
        <Button
          className={classes.buttons}
          onClick={() => eighthFunction(rowData)}
          >
          <Tooltip title={eighthTooltip}>
            <DeleteForeverIcon />
          </Tooltip>
        </Button>
      }
    </div>
  )
}

export default ActionButtons