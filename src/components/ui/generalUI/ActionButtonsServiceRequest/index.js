import React from 'react'
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import BlockIcon from '@material-ui/icons/Block'
import VisibilityIcon from '@material-ui/icons/Visibility'
import StarRateIcon from '@material-ui/icons/StarRate'

const useStyles = makeStyles(styles)

const ActionButtonsServiceRequest = (props) => {

  const classes = useStyles()
  const { actionButtonsServiceProps, rowData, className } = props
  const { 
    firstTooltip,
    secondTooltip,
    thirdTooltip,
    firstFunction, 
    secondFunction,
    thirdFunction
  } = actionButtonsServiceProps
  return (
    <div id='action-buttons' className={clsx(classes.container, className)}>
      <Button
        className={classes.buttons}
        onClick={() => firstFunction(rowData)}
        >
        <Tooltip title={firstTooltip}>
          <VisibilityIcon/>
        </Tooltip>
      </Button>
      {rowData.state === 'Pendiente' && 
        <Button
          className={classes.buttons}
          onClick={() => secondFunction(rowData)}
          >
          <Tooltip title={secondTooltip}>
            <BlockIcon/>
          </Tooltip>
        </Button>
      }
      {['Completado', 'Cancelado'].includes(rowData.state) && !rowData.score && 
        <Button
          className={classes.buttons}
          onClick={() => thirdFunction(rowData)}
          >
          <Tooltip title={thirdTooltip}>
            <StarRateIcon/>
          </Tooltip>
        </Button>
      }
    </div>
  )
}

export default ActionButtonsServiceRequest
