import React from 'react'
import TextField from '@material-ui/core/TextField'


const FormInput = (props) => {

    const {error, errorMessage } = props
    return (
        <div >
        <TextField
            helperText = {error && errorMessage}
            error = {error}
            {...props}
        />
        </div>
    )
}

export default FormInput