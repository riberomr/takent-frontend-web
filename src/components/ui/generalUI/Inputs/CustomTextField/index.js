import React from 'react'
import { TextField } from '@material-ui/core'

const CustomTextField = (props) => {

    const { onlyNumbers, onlyLetters, lettersAndNumbers, className, ...other} = props
    const regexOnlyNumbers = /^[0-9]+$/;
    const regexOnlyLetters = /^[a-zA-ZÀ-ÖØ-öø-ÿ ']+$/;
    const regexLettersAndNumbers = /^[a-zA-ZÀ-ÖØ-öø-ÿ0-9 ']+$/;
    

    const handlePaste = (e) => {
        if ((onlyNumbers && !regexOnlyNumbers.test(e.clipboardData.getData('Text'))) ||
            (onlyLetters && !regexOnlyLetters.test(e.clipboardData.getData('Text'))) ||
            (lettersAndNumbers && !regexLettersAndNumbers.test(e.clipboardData.getData('Text')))) {
            e.preventDefault()
        }
    }


    const handleKeyPress = (e) => {
        if ((onlyNumbers && !regexOnlyNumbers.test(e.key)) ||
            (onlyLetters && !regexOnlyLetters.test(e.key)) ||
            (lettersAndNumbers && !regexLettersAndNumbers.test(e.key))) {
            e.preventDefault()
        }
    }

    return (
        <TextField
            className={className}
            onKeyPress={(onlyLetters || onlyNumbers || lettersAndNumbers)? handleKeyPress : null}
            onPaste={(onlyLetters || onlyNumbers || lettersAndNumbers)? handlePaste : null}
            {...other}
        />
    )
}

export default CustomTextField