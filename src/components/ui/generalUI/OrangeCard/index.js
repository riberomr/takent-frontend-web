import React from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import { Button, Card, CardActions, CardContent, Typography } from '@material-ui/core'

const useStyles = makeStyles(styles)

const OrangeCard = (props) =>{

    const classes = useStyles()
    const { title, description, buttonText, redirectTo} = props

    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography gutterBottom variant="h5">{title}</Typography>
          <Typography variant="body1">{description}</Typography>
        </CardContent>
        <CardActions style={{display: 'flex', justifyContent: 'center'}}>
          <Link to={redirectTo} className={classes.linkDecoration}>
            <Button variant="contained" color="primary" style={{color: 'black'}}>{buttonText}</Button>
          </Link>
        </CardActions>
      </Card>
    )
}
export default OrangeCard
