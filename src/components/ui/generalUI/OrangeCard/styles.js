import { theme } from "../../../../constants/generalConstants/theme";

export const styles = {
    container:{
        display: 'flex',
        backgroundColor: 'orange',
        width: '100%',
        padding: '28px',
        margin: 'auto',
        borderRadius: '8px'
    },
    textContainer:{
        width: '70%',
    },
    title:{
        fontFamily: 'Roboto',
        fontSize: 'large',
        fontWeight: 'bold',
        textDecoration: 'underline'
    },
    description:{
        fontFamily: 'Roboto',
        fontSize: 'large',
    },
    buttonContainer:{
        width: '30%',
        display: 'flex',
        justifyContent: 'center'
    },
    button:{
        width: 'unset',
        minHeight: '44px'
    },
    linkDecoration:{
        textDecoration: 'none'
    },
    card: {
      borderRadius: '8px',
      border: '2px solid ' + theme.palette.primary.main,
      boxShadow: '7px 7px 7px rgba(0, 0, 0, 0.2)'
    }
}
