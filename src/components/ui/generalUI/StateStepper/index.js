import { makeStyles, Step, StepLabel, Stepper } from '@material-ui/core'
import React from 'react'
import { theme } from '../../../../constants/generalConstants/theme'
import { styles } from './styles'

const useStyles = makeStyles(styles)

const StateStepper = props => {

  const classes = useStyles()

  const { states, activeState, orientation="horizontal" } = props
  
  return (
    <>
      {activeState === -1 && "No has sido seleccionado"}
      {activeState !== -1 && <div className={classes.root}>
        <Stepper
          activeStep={activeState}
          orientation={orientation}
          style={{backgroundColor: "transparent", padding: theme.spacing(3, 0, 3, 0)}}
        >
          {
            states.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))
          }
        </Stepper>
      </div>}
    </>
  )

}

export default StateStepper
