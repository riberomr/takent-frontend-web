import React from 'react'
import Button from '@material-ui/core/Button'
import EventBusyIcon from '@material-ui/icons/EventBusy'
import PeopleIcon from '@material-ui/icons/People'
import Tooltip from '@material-ui/core/Tooltip'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import RefreshIcon from '@material-ui/icons/Refresh'
import CachedIcon from '@material-ui/icons/Cached'
import BlockIcon from '@material-ui/icons/Block'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import DescriptionOutlinedIcon from '@material-ui/icons/DescriptionOutlined'

const useStyles = makeStyles(styles)

const ActionButtonsService = (props) => {
  //this component is very very specific, i mean, at the moment i wasn't think about flexibility
  //actionButtonsServiceProps is an object that conteins more props, that's the reason because i made double destructuring below

  const classes = useStyles()
  const { actionButtonsServiceProps, rowData, className } = props
  const { 
    firstTooltip,
    secondTooltip, 
    thirdTooltip, 
    fourthTooltip,
    fifthTooltip,
    sixthTooltip,
    seventhTooltip,
    firstFunction, 
    secondFunction, 
    thirdFunction, 
    fourthFunction,
    fifthFunction,
    sixthFunction,
    seventhFunction} = actionButtonsServiceProps
  return (
    <div id='action-buttons' className={clsx(classes.container, className)}>
      {!rowData.preference && rowData.state === 'Activo' && <Tooltip title={seventhTooltip}>
        <Button
          className={classes.buttons}
           onClick={() => { seventhFunction(rowData) }}
          >
          <AttachMoneyIcon />
        </Button>
      </Tooltip>}
      <Tooltip title={firstTooltip}>
        <Button
          className={classes.buttons}
           onClick={() => { firstFunction(rowData) }}
          >
          <PeopleIcon />
        </Button>
      </Tooltip>
      <Button
        className={classes.buttons}
         onClick={() => secondFunction(rowData)}
        >
        <Tooltip title={secondTooltip}>
          <DescriptionOutlinedIcon/>
        </Tooltip>
      </Button>
      {rowData.state === 'Activo' && 
        <Button
          className={classes.buttons}
          onClick={() => thirdFunction(rowData)}
          >
          <Tooltip title={thirdTooltip}>
            <BlockIcon/>
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Inactivo' && 
        <Button
          className={classes.buttons}
          onClick={() => fifthFunction(rowData)}
          >
          <Tooltip title={fifthTooltip}>
            <RefreshIcon/>
          </Tooltip>
        </Button>
      }
      {(rowData.state === 'Activo' || rowData.state === 'Inactivo') &&
        <Button
          className={classes.buttons}
          onClick={() => fourthFunction(rowData)}
          >
          <Tooltip title={fourthTooltip}>
            <EventBusyIcon />
          </Tooltip>
        </Button>
      }
      {rowData.state === 'Finalizado' &&
        <Button
          className={classes.buttons}
          onClick={() => sixthFunction(rowData)}
          >
          <Tooltip title={sixthTooltip}>
            <CachedIcon />
          </Tooltip>
        </Button>
      }
    </div>
  )
}

export default ActionButtonsService
