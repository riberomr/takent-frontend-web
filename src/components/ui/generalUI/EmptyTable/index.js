import React from 'react'
import { makeStyles } from '@material-ui/core'
import { styles } from './styles'

const useStyles = makeStyles(styles)

export default function EmptyTable({ message = '' }) {

  const classes = useStyles()

  return (
    <div className={classes.noDataTable}>
      {message}
    </div>
  )
}