export const styles = {
    noDataTable: {
        textAlign: 'center',
        padding: '2rem',
        color: '#b2b2b2',
        minHeight: '350px'
    }
}