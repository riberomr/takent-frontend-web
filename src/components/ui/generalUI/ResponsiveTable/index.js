import React from 'react'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import TableRow from '@material-ui/core/TableRow'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Chip from '@material-ui/core/Chip'
import EmptyTable from '../EmptyTable'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import { CircularProgress } from '@material-ui/core'


const useStyles = makeStyles(styles)

const ResponsiveTable = ({ 
  title,
  noDataMessage,
  header,
  rows = [],
  setPagOptions,
  pagOptions,
  order,
  orderBy,
  styleTable,
  actions,
  onClick = null,
  loading = false,
  disableMaxWidth = false,
  handleSortClick = () => { } }) => {

  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(5)
  const isXSSize = useMediaQuery('(max-width:600px)')
  const { widthColum = null } = styleTable || {}
  const classes = useStyles()
  const handleChangePage = (event, page) => {
    setPagOptions ?
      setPagOptions({ ...pagOptions, page: page }) :
      setPage(page)
  }

  const handleChangeRowsPerPage = event => {
    if (setPagOptions) {
      setPagOptions({ ...pagOptions, pageSize: event.target.value })
    }
    else {
      setRowsPerPage(event.target.value)
      setPage(0)
    }
  }

  if(loading) return (
    <div style={{display: "flex", justifyContent: "center", alignItems: "center", marginTop: "20px", height: "350px"}}>
      <CircularProgress size={80}/>
    </div>
  )

  if (!rows || rows.length === 0) return <EmptyTable message={noDataMessage ? noDataMessage : 'No hay información para mostrar'} />

  const rowsToMap = setPagOptions ? rows.content : rows.slice(page * rowsPerPage, rowsPerPage * (page + 1))

  return isXSSize ? (
    <div className='responsive-table'>
      <table>
        <caption>{title}</caption>
        <thead className={classes.thead}>
          <tr>
            {header.map((head, index) => (
              <th key={index} scope="col">
                {head.label}
              </th>
            ))}
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {rowsToMap
            .map((row, index) => (
              <tr key={index}>
                {header.map((head, index) => (
                  <td key={index} data-label={head.label} className={classes.tdMobile} onClick={onClick? () => { onClick(row) } : null}>
                    {(head.opts && head.opts.type === 'chip') ?
                      <Chip
                        style={{
                          background: head.opts.values[row[head.id]].ligth,
                          display: 'flex',
                          color: head.opts.values[row[head.id]].dark,
                          fontSize: '14px',
                          lineHeight: '18px',
                          letterSpacing: '0.25px',
                          width: '40%'
                        }}
                        label={row[head.id]} /> : row.format && typeof row[head.id] === 'number' ?
                        row.format(row[head.id]) : row[head.id] || '-'
                    }
                  </td>
                ))}
                {actions && <td>{React.cloneElement(actions, { rowData: row })} </td>}
              </tr>
            ))}
        </tbody>
      </table>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25, 100]}
        component="div"
        labelRowsPerPage="Resultados por página"
        labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
        count={setPagOptions ? rows.totalElements : rows.length}
        rowsPerPage={setPagOptions ? pagOptions.pageSize : rowsPerPage}
        page={setPagOptions ? pagOptions.page : page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  ) : (
      <Paper className={classes.root}>
        <TableContainer id='responsiveTableTableContainer' className={classes.container}>
          <Table stickyHeader aria-label="sticky table" className={classes.table}>
            <TableHead id='responsiveTableTableHeader'>
              <TableRow className={classes.tr}>
                {header.map(column => (
                  <TableCell
                    className={classes.th}
                    key={column.id}
                    align={column.alignHeader}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.sortable ?
                      <TableSortLabel
                        active={orderBy === column.id}
                        direction={orderBy === column.id ? order : 'asc'}
                        onClick={() => handleSortClick(column.id)}
                      >
                        {column.label}
                      </TableSortLabel>
                      : column.label
                    }
                  </TableCell>

                ))}
                {actions && <TableCell className={classes.actionsCell} align={'center'}>Acciones</TableCell>}
              </TableRow>
            </TableHead>
            <TableBody>
              {rowsToMap
                .map((row, index) => {
                  return (
                    <TableRow classes={{ root: classes.tableRow }} className={classes.tr} hover role="checkbox" tabIndex={-1} key={index} onClick={onClick? () => { onClick(row) } : null} style={{cursor: onClick? "pointer" : "auto"}}>
                      {header.map((column, index) => {
                        const value = row[column.id]
                        return (
                          <TableCell className={clsx(widthColum, classes.td)} key={column.id} align={column.alignRow}>
                            {(column.opts && column.opts.type === 'chip') ?
                              <Chip
                                style={{
                                  background: column.opts.values[value].ligth,
                                  display: 'flex',
                                  color: column.opts.values[value].dark,
                                  fontSize: '14px',
                                  lineHeight: '18px',
                                  letterSpacing: '0.25px',
                                  marginLeft: column.sortable? "-26px" : "0px"
                                }}
                                label={value} /> : column.format && typeof value === 'number' ? column.format(value)
                                : <div style={{maxWidth: (!disableMaxWidth || column.maxWidth)? (column.maxWidth ?? '185px') : null, overflow: 'hidden',
                                textOverflow: 'ellipsis', whiteSpace: 'nowrap', marginLeft: column.sortable? "-26px" : "0px"}}>{value}</div>
                            }
                          </TableCell>
                        )
                      })}
                      {actions && <TableCell>
                        {actions && React.cloneElement(actions, { rowData: row, className: classes.actions })}
                      </TableCell>}
                    </TableRow>
                  )
                })}

            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          id='responsiveTablePagination'
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          labelRowsPerPage="Resultados por página"
          labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
          count={setPagOptions ? rows.totalElements : rows.length}
          rowsPerPage={setPagOptions ? pagOptions.pageSize : rowsPerPage}
          page={setPagOptions ? pagOptions.page : page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          classes={{ input: classes.input, spacer: classes.spacer, root: classes.tablePagination }}
        />
      </Paper>
    )
}

export default ResponsiveTable