export const styles = {
  root: {
    width: '100%',
    minHeight: 350,
    border: '1px solid #ccc',
    marginTop: '20px',
    borderRadius: '4px',
    boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)'
  },
  tablePagination:{
    borderTop: '1px solid #ccc'
  },
  container: {
    height: 422,
    borderRadius: '4px',
    // minHeight: 440,
    
  },
  tableRow: {
    '&&:hover td #action-buttons': {
      visibility: 'visible'
    }
  },
  actions: {
    visibility: 'hidden'
  },
  actionsCell: {
    width: '180px',
    backgroundColor: '#F29222',
    fontSize: '.93em',
    letterSpacing: '.1em',
    color: '#fff'
  },
  spacer: {
    flex: 0
  },
  input: {
    '&& + p': {
      flex: 1,
      textAlign: 'right'
    }
  },
  tdMobile:{
    display: 'flex !important',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  table:{
    // border: '1px solid #ccc',
    borderCollapse: 'collapse',
    margin: '0',
    padding: '0',
    width: '100%',
    tableLayout: 'auto',
    backgroundColor: '#f8f8f8',
    '@media screen and (max-width: 600px)': {
      border: 0,
      '& caption':  {
        fontSize: '1.3em'
        }
    },
  },
  tr:{
    backgroundColor: '#f8f8f8',
    // border: '1px solid #ddd',
    padding: '.35em',
    '@media screen and (max-width: 600px)': {
      borderBottom: '3px solid #ddd',
      display: 'block',
      marginBottom: '.625em'
    },
  },
  th: {
    backgroundColor: '#F29222',
    padding: '.625em',
    // textAlign: 'center',
    fontSize: '.93em',
    letterSpacing: '.1em',
    // textTransform: 'uppercase',
    color: '#fff'
  },
  td: {
    padding: '.625em',
    // textAlign: 'center'
    '@media screen and (max-width: 600px)': {
      borderBottom: '1px solid #ddd',
      display: 'block',
      fontSize: '.8em',
      textAlign: 'right'
    },
    '@media screen and (max-width: 600px) & ::before': {
      content: 'attr(data-label)',
      float: 'left',
      fontWeight: 'bold',
      textTransform: 'uppercase'
    },
    '@media screen and (max-width: 600px) & :last-child': {
        borderBottom: '0'
    }
  },
  thead: {
    border: 'none',
    clip: 'rect(0 0 0 0)',
    height: '1px',
    margin: '-1px',
    overflow: 'hidden',
    padding: '0',
    position: 'absolute',
    width: '1px',
  }
}