import { Button, DialogContent, DialogContentText, Tooltip } from '@material-ui/core'
import { IconButton } from '@material-ui/core'
import { DialogTitle } from '@material-ui/core'
import { Dialog, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React, { useState } from 'react'
import { DialogActions } from '../../../wrappers/CommonUser/Offers/OfferList/ShowOffer/DialogActions'
import { styles } from './styles'
import AttachFileIcon from '@material-ui/icons/AttachFile'
import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../services/API-TAKENT'

const useStyles = makeStyles(styles)

const ALLOWED_TYPES = ['pdf']

const AlertUploadCV = ({
  open,
  setOpen,
  postulate,
  data
}) => {

  const srcIcon = "/images/AlertIcons/error.png"

  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()

  const [ selectedCurriculum, setSelectedCurriculum ] = useState(null)
  const [ curriculumName, setCurriculumName ] = useState(null)

  const handleCVSelected = event => {
    const files = event.target.files
    const file = files[0]

    let [type, extension] = file.type.split('/')

    if (!ALLOWED_TYPES.includes(extension) || type !== 'application' || !file.type || !file) {
      return enqueueSnackbar('Error al cargar curriculum, debe ser formato PDF.', { variant: 'error' })
    }

    if(((file.size/1024)/1024)>5) return enqueueSnackbar('Error al cargar curriculum, el tamaño excede los 5 Mb.', { variant: 'error' })

    if (files && file) {
      var reader = new FileReader()
      reader.onload = function(readerEvt) {
        var binaryString = readerEvt.target.result
        setSelectedCurriculum(btoa(binaryString))
        setCurriculumName(file.name)
      }
      reader.readAsBinaryString(file)
    }
  }

  const handlePostulate = async () => {
    if ( selectedCurriculum ) {
      await apiTakent.postCurriculum({
        name: curriculumName,
        base64: selectedCurriculum
      })
    }
    await postulate(data._id)
  }

  return (
    <Dialog
      maxWidth='sm'
      fullWidth
      open={open}
      onClose={() => setOpen(false)}
      className={classes.dialog}
    >
      <div className={classes.iconContainer}>
        <img src={srcIcon} className={classes.icon} alt=''></img>
      </div>
      <DialogTitle disableTypography>
        <Typography variant='h5' align='center'>Currículum requerido</Typography>
      </DialogTitle>
      <DialogContent>
        <DialogContentText>Esta oferta requiere tener un CV en formato PDF cargado. Cargalo ahora y completá tu postulación</DialogContentText>
        <div className={classes.curriculumContainer}>
          <Typography variant='body2'>{curriculumName || 'No cargaste un currículum'}</Typography>
          <input accept='application/pdf' id='icon-button-cv' type='file' className={classes.input} onChange={handleCVSelected}/>
          <Tooltip title='Cargar currículum'>
            <label htmlFor='icon-button-cv'>
              <IconButton color='primary' component='span'>
                <AttachFileIcon/>
              </IconButton>
            </label>
          </Tooltip>
        </div>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button
          color='primary'
          variant='contained'
          onClick={() => setOpen(false)}
          className={classes.button}
        >
          Cerrar
        </Button>
        <Button
          color='primary'
          variant='contained'
          onClick={handlePostulate}
          className={classes.button}
          disabled={!selectedCurriculum}
        >
          Postularme
        </Button>
      </DialogActions>
    </Dialog>
  )

}

export default AlertUploadCV
