import { theme } from "../../../../constants/generalConstants/theme";

export const styles = {
  dialog: {
    marginTop: '48px',
  },
  iconContainer: {
    marginTop: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: '90px'
  },
  dialogActions: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(1),
  },
  button: {
    width: '120px',
    color: 'black !important'
  },
  curriculumContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  input: {
    display: 'none'
  }
}
