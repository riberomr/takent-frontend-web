import { theme } from "../../../../constants/generalConstants/theme"

export const styles = {
  dialog: {
      marginTop: "48px"
  },
  itemContainer: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center'
  },
  dataIcon: {
      marginRight: '10px'
  },
  title: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between'
  },
  dividers: {
      padding: '16px 24px',
      borderTop: `1px solid ${theme.palette.primary.main}`,
      borderBottom: `1px solid ${theme.palette.primary.main}`,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    '& button': {
      width: '150px',
      height: '40px',
      margin: theme.spacing(2)
    }
  }
}