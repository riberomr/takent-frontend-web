import { Dialog, DialogContent, DialogTitle, Grid, IconButton, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import { styles } from './styles'
import CloseIcon from '@material-ui/icons/Close'
import { Formik } from 'formik'
import Select from '../Selects/CustomSelect/CustomSelect'
import TextField from '../Inputs/CustomTextField'
import FormButton from '../Buttons/PrimaryButton'
import AddressForm from '../../../rules/validationSchema/AddressForm'
import GoogleMap from '../GoogleMaps/GoogleMap'
import { placesHelper } from '../../../../helpers/customHelpers/places'

const useStyles = makeStyles(styles)
const variant = "standard"

const AddressDialog = (props) => {

  const classes = useStyles()

  const { isOpen, setIsOpen, address, setAddress, provinces, cities, setSelectedProvince, ...other} = props

  const setLocation = location => {
    setAddress(prevState => {
      return {...prevState, location: location}
    })
  }

  const setAddressInfo = (addressInfo, setFieldValue) => {
    const province = placesHelper.parseProvince(addressInfo.province, provinces)
    if(province) {
      setFieldValue("province", province)
      setSelectedProvince(province)
    }
    placesHelper.parseCity(addressInfo.city, province)
    .then(city => {
      setFieldValue("city", city)
    })
    setFieldValue("district", addressInfo.district)
    setFieldValue("street", addressInfo.street)
    setFieldValue("number", addressInfo.number)
    setFieldValue("postal_code", placesHelper.parsePostalCode(addressInfo.postal_code))
  }

  const handleChangeProvince = (value, setFieldValue) => {
    setFieldValue('city', '' )
    setSelectedProvince(value)
    setFieldValue('province', value )
  }

  const handleChangeStreet = (value, setFieldValue) => {
    if (!value) {
        setFieldValue('number', '')
        setFieldValue('tower', '')
        setFieldValue('floor', '')
        setFieldValue('apartment', '')
        setFieldValue('postal_code', '')
    }
    setFieldValue('street', value)
  }

  const initialValues = {
    province: address.province || "",
    city: address.city || "",
    district: address.district || "",
    street: address.street || "",
    number: address.number || "",
    postal_code: address.postal_code || "",
    tower: address.tower || "",
    floor: address.floor || "",
    apartment: address.apartment || ""
  }

  const submit = (values) => {
    const address = {
      'district': values.district !==""? values.district : null,
      'street': values.street !==""? values.street : null,
      'number': values.number !==""? values.number : null,
      'apartment': values.apartment !==""? values.apartment : null,
      'floor': values.floor !==""? values.floor : null,
      'postal_code': values.postal_code !==""? values.postal_code : null,
      'province': values.province !== ""? values.province : null,
      'city': values.city !==""? values.city : null,
      'tower': values.tower !==""? values.tower : null
    }
    setAddress(prevState => {
      return { ...prevState, ...address}
    })
    setIsOpen(false)
  }

  return (
    <Dialog
      onClose={() => setIsOpen(false)}
      open={isOpen}
      maxWidth="sm"
      fullWidth={true}
      className={classes.dialog}
      {...other}
    >
      <DialogTitle disableTypography className={classes.title}>
        <Typography variant="h5">
            Ubicación
        </Typography>
        <IconButton onClick={() => setIsOpen(false)}>
            <CloseIcon/>
        </IconButton>
      </DialogTitle>
      <DialogContent dividers classes={{dividers: classes.dividers}}>
        <Formik
          initialValues={initialValues}
          onSubmit={values => submit(values)}
          validationSchema={AddressForm}
        >
          {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              errors,
              touched,
              setFieldValue
          }) => (
          <form onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <GoogleMap
                  height="300px"
                  initialLatLng={address.location}
                  interactive
                  geocoding
                  setExtLatLng={setLocation}
                  setExtAddressInfo={(addressInfo) => setAddressInfo(addressInfo, setFieldValue)}
                />
              </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <Select
                    id="province"
                    name="province"
                    label="Provincia"
                    variant={variant}
                    disabled={!Boolean(address.location)}
                    fullWidth
                    options={provinces}
                    value={values.province}
                    onChange={event => handleChangeProvince(event.target.value, setFieldValue)}
                    error={errors.province && touched.province}
                    helperText={(errors.province && touched.province)? errors.province : ""}
                  />
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <Select
                    id="city"
                    name="city"
                    label="Localidad"
                    variant={variant}
                    disabled={values.province === ''}
                    fullWidth
                    options={cities}
                    value={values.city}
                    onChange={event => setFieldValue("city", event.target.value)}
                    error={errors.city && touched.city}
                    helperText={(errors.city && touched.city)? errors.city : ""}
                  />
                </Grid>
                <Grid item xs={5} className={classes.gridItem}>
                  <TextField
                    id='district'
                    type='text'
                    name='district'
                    label="Barrio"
                    variant={variant}
                    disabled={values.city === ''}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.district}
                    error={errors.district && touched.district}
                    helperText={(errors.district && touched.district)? errors.district : ""}
                  />
                </Grid>
                <Grid item xs={4} className={classes.gridItem}>
                  <TextField
                    id='street'
                    type='text'
                    name='street'
                    label="Calle"
                    variant={variant}
                    disabled={values.city === ''}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={(event) => handleChangeStreet(event.target.value, setFieldValue)}
                    onBlur={handleBlur}
                    value={values.street}
                    error={errors.street && touched.street}
                    helperText={(errors.street && touched.street)? errors.street : ""}
                  />
                </Grid>
                <Grid item xs={3} className={classes.gridItem}>
                  <TextField
                    id='number'
                    type='text'
                    name='number'
                    label="Número"
                    onlyNumbers
                    variant={variant}
                    fullWidth
                    disabled={values.city === ''}
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.number}
                    error={errors.number && touched.number}
                    helperText={(errors.number && touched.number)? errors.number : ""}
                  />
                </Grid>
                <Grid item xs={3} className={classes.gridItem}>
                  <TextField
                    id='postal_code'
                    type='text'
                    name='postal_code'
                    label="CP"
                    onlyNumbers
                    variant={variant}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    InputProps={{disabled: values.city === ""}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.postal_code}
                    error={errors.postal_code && touched.postal_code}
                    helperText={(errors.postal_code && touched.postal_code)? errors.postal_code : ""}
                  />
                </Grid>
                <Grid item xs={3} className={classes.gridItem}>
                  <TextField
                    id='tower'
                    type='text'
                    name='tower'
                    label="Torre"
                    variant={variant}
                    InputProps={{disabled: values.number === ""}}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.tower}
                    error={errors.tower && touched.tower}
                    helperText={(errors.tower && touched.tower)? errors.tower : ""}
                  />
                </Grid>
                <Grid item xs={3} className={classes.gridItem}>
                  <TextField
                    id='floor'
                    type='text'
                    name='floor'
                    label="Piso"
                    onlyNumbers
                    variant={variant}
                    InputProps={{disabled: values.number === ""}}
                    disabled={values.street === ''}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.floor}
                    error={errors.floor && touched.floor}
                    helperText={(errors.floor && touched.floor)? errors.floor : ""}
                  />
                </Grid>
                <Grid item xs={3} className={classes.gridItem}>
                  <TextField
                    id='apartment'
                    type='text'
                    name='apartment'
                    label="Departamento"
                    variant={variant}
                    InputProps={{disabled: values.number === ""}}
                    disabled={values.street === ''}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.apartment}
                    error={errors.apartment && touched.apartment}
                    helperText={(errors.apartment && touched.apartment)? errors.apartment : ""}
                  />
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.buttonContainer}>
                    <FormButton
                        onClick={() => setIsOpen(false)}
                        variant="contained"
                        color="primary"
                        >
                        Cancelar
                    </FormButton>
                    <FormButton
                        type="submit"
                        variant="contained"
                        color="primary"
                        >
                        Guardar
                    </FormButton>
                  </div>
                </Grid>
            </Grid>
          </form>)}
        </Formik>
      </DialogContent>
    </Dialog>
  )
}

export default AddressDialog