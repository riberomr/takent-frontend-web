import { makeStyles, createStyles } from '@material-ui/core/styles'
import { theme } from '../../../../constants/generalConstants/theme'

export const useStyles = makeStyles(() => createStyles({
  nav: {
    padding: '10px',
    color: '#6F6F6F'

  },
  container:{
    zIndex: 99
  },
  navLink: {
    textDecoration: 'none',
    color: 'inherit',
  },
  iconLabel: {
    display: 'flex',
    alignItems: 'center'
  },
  span: {
    marginLeft: '10px'
  },
  navItem: {
    width: '239.82px',
    borderRadius: '0px 20px 20px 0px',
  },
  navItemActive: {
    background: theme.palette.primary.main,
    '&&:hover': {
      paddingLeft: theme.spacing(2),
      background: theme.palette.primary.main
    }
  },
  subItem: {
    marginLeft: '35px'
  },
  dropdown: {
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold'
  }
})
)