import React from 'react'
import {
  List,
  ListItem,
  Collapse,
  ListItemIcon,
  ListItemText,
  Tooltip
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import { useStyles } from './styles'
import { theme } from '../../../../constants/generalConstants/theme'

const MakeNavItems = props => {
  const classes = useStyles()
  let { items, handleSetOpen, isDrawerOpen } = props // open or close dropdown items

  return (
    items.filter(i => !i.hidden).map(item => {
      const user_type = item.user_type || ''
      const userType = localStorage.getItem('type')
      const isUserAllowed = (userType === user_type)
      if(!isUserAllowed) return null

      if(item.dropdown){
        return (
          <React.Fragment key={item.id}>
            <ListItem button onClick = {() => handleSetOpen(item.id)} divider style={{paddingLeft: theme.spacing(3)}}>
              <Tooltip title={item.id}>
                <ListItemIcon>{item.icon}</ListItemIcon>
              </Tooltip>
              <ListItemText primary={item.id} primaryTypographyProps={{noWrap: true}}/>
            </ListItem>
            <Collapse
              in={item.opened && isDrawerOpen}
              timeout="auto"
            >
              <List disablePadding>
                {
                  item.children.map((c, index) => {
                    const subItemUser = c.user_type || ''
                    const isUserAllowed = (userType === subItemUser)
                    if(!isUserAllowed) return null
                    return(
                      <Link className={classes.navLink} to={c.link} key={c.id}>
                        <ListItem
                          button
                          className={`${c.active? classes.navItemActive : null}`}
                          divider={index === item.children.length - 1}
                        >
                          <ListItemText primaryTypographyProps={{noWrap: true}}>{c.id}</ListItemText>
                        </ListItem>
                      </Link>
                    )
                  })
                }
              </List>
            </Collapse>
          </React.Fragment>
        )
      }
      return (
        <Link className={classes.navLink} to={item.link} key={item.id}>
          <ListItem
            button
            className={`${item.active? classes.navItemActive : null}`}
            divider
            style={{paddingLeft: theme.spacing(3)}}
          >
            <Tooltip title={item.id}>
              <ListItemIcon style={{color: item.active? 'black' : '#F29222'}}>{item.icon}</ListItemIcon>
            </Tooltip>
            <ListItemText primaryTypographyProps={{noWrap: true}}>{item.id}</ListItemText>
          </ListItem>
        </Link>
      )
    })
  )
}

export default MakeNavItems
