import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React, { useState } from 'react'
import StarRating from '../StarRating'
import { styles } from './styles'

const useStyles = makeStyles(styles)

const RatingDialog = props => {

  const { open, setOpen, submitFunction } = props

  const [ rating, setRating ] = useState(null)

  const classes = useStyles()

  const handleSubmit = () => {
    submitFunction(rating)
    setRating(null)
    setOpen(false)
  }

  const handleClose = () => {
    setRating(null)
    setOpen(false)
  }

  return (
    <Dialog
      maxWidth="xs"
      open={open}
      fullWidth
      onClose={handleClose}
      className={classes.dialog}
    >
      <DialogTitle disableTypography>
        <Typography variant="h5" align="center">Valoración de servicio</Typography>
      </DialogTitle>
      <DialogContent style={{display: "flex", justifyContent: "center"}}>
        <StarRating size="large" submit={value => setRating(value)}/>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button color="primary" variant="contained" onClick={handleClose} className={classes.button}>
            Cancelar
        </Button>
        <Button color="primary" variant="contained" onClick={handleSubmit} className={classes.button} disabled={rating === null}>
            Valorar
        </Button>
      </DialogActions>
    </Dialog>
  )

}

export default RatingDialog
