import React, { useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import EditIcon from '@material-ui/icons/Edit'
import ClearIcon from '@material-ui/icons/Clear'
import Button from '@material-ui/core/Button'
import { shortText } from '../../../../helpers/generalHelpers/shortText'
import DataDialog from '../DataDialog'

const useStyles = makeStyles(styles)

const CardItem = (props) => {
    const { data, displayData, title, subtitle, labelText1, valueText1, valueText1Shortable = true, labelText2, valueText2, valueText2Shortable = true, editItem, deleteItem } = props

    const [ dialogOpen, setDialogOpen] = useState(false)

    const classes = useStyles()

    const renderTitle = (value) => {
        if(typeof value === "string") {
            return (<span>
                        {shortText(26, value) || ''}
                    </span>)
        }
        else {
            return value
        }
    }

    return (
        <>
            <div className={classes.content} onClick={() => setDialogOpen(true)}>
                <div className={classes.mainContent}>
                    <div className={classes.title}>
                        {renderTitle(title)}
                    </div>
                    <span className={classes.subtitle}>
                        {shortText(32,subtitle) || ''}
                    </span>
                    <span className={classes.text}>
                        {labelText1 || ''} {shortText(10,valueText1, valueText1Shortable) || ''}
                    </span>
                    <span className={classes.text}>
                        {labelText2 || ''} {shortText(10,valueText2, valueText2Shortable) || ''}
                    </span>
                </div>
                <div className={classes.actionsContent}>
                    <Button onClick={() => editItem(data)}>
                        <EditIcon/>
                    </Button>
                    <Button onClick={() => deleteItem(data.id)}>
                        <ClearIcon/>
                    </Button>
                </div>
            </div>
            <DataDialog
                title={displayData.title}
                data={displayData.content}
                open={dialogOpen}
                setOpen={setDialogOpen}
                checkIcon={true}
            />
        </>
    )
}

export default CardItem