import { theme } from '../../../../constants/generalConstants/theme'

export const styles = {

    content:{
        display: 'flex',
        border: 'solid 2px '+theme.palette.primary.main,
        borderRadius: '15px',
        width: '90%',
        marginBottom: '20px',
        boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
        '&:hover': {
            backgroundColor: 'rgba(242, 146, 34, 0.1)',
            cursor: 'pointer'
        }
    },
    mainContent:{
        margin: '8px 8px 8px 20px',
        display: 'flex',
        flexDirection: 'column',
        width: '75%'
    },
    actionsContent:{
        margin: '8px',
        display: 'grid',
        width: '20%',
        justifyContent: 'right'
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: '18px',
        margin: '2px 2px 10px 2px'
    },
    title: {
        fontFamily: 'Roboto',
        fontSize: '22px',
        margin: '10px 2px 2px 2px',
        fontWeight: 'bold'
    },
    subtitle: {
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '2px 2px 15px 2px',
        color: 'grey'
    }
}