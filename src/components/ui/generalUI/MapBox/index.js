import React, { useRef, useEffect, useState } from 'react'
import mapboxgl from 'mapbox-gl'
import clsx from 'clsx'
import LocationOffIcon from '@material-ui/icons/LocationOff'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import SaveIcon from '@material-ui/icons/Save'
import MyLocationIcon from '@material-ui/icons/MyLocation'
import DeleteIcon from '@material-ui/icons/Delete'
import { Button, Tooltip } from '@material-ui/core'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(styles)

mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA'

const MapBox = (props) => {
  const mapContainerRef = useRef(null)

  const {className, handleSubmit, location, defaultLocation, handleReset, disabled} = props

  const classes = useStyles({location})
  const [lng, setLng] = useState(location? location.lng : defaultLocation.lng)
  const [lat, setLat] = useState(location? location.lat : defaultLocation.lat)
  const [zoom, setZoom] = useState(location? location.zoom : 11)

  // Initialize map when component mounts
  useEffect(() => {
    let lngU = location? location.lng : defaultLocation.lng
    let latU = location? location.lat : defaultLocation.lat
    let zoomU = location? location.zoom : 11
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lngU, latU],
      zoom: zoomU
    })

    map.on('move', () => {
        setLng(map.getCenter().lng.toFixed(4))
        lngU = map.getCenter().lng.toFixed(4)
        setLat(map.getCenter().lat.toFixed(4))
        latU = map.getCenter().lat.toFixed(4)
        setZoom(map.getZoom().toFixed(2))
        zoomU = map.getZoom().toFixed(2)
    })

    // Clean up on unmount
    return () => map.remove()
  }, [defaultLocation, location])

  const deleteLocation = () => {
    handleSubmit('delete', )
  }

  const saveLocation = () =>{
    handleSubmit('save', { lat: lat, lng: lng, zoom: zoom})
  }

  const returnHome = () => {
    handleReset()
  }
  return (
      <div className={clsx(classes.mainContainer, className)}>
        <div className={classes.mapContainer} ref={mapContainerRef}>
            {
                !location? 
                <LocationOffIcon className={classes.notMarked}/> :
                <LocationOnIcon className={classes.marked}/>
            }
        </div>
        <div className={classes.buttonContainer}>
            <Button disabled={disabled} onClick={() => saveLocation()}><Tooltip title='Guardar localización'><SaveIcon className={classes.iconButton}/></Tooltip></Button>
            <Button disabled={!location} onClick={() => returnHome()}><Tooltip title='Localización guardada'><MyLocationIcon className={classes.iconButtonReset}/></Tooltip></Button>
            <Button disabled={disabled} onClick={() => deleteLocation()}><Tooltip title='Eliminar localización'><DeleteIcon className={classes.iconButton}/></Tooltip></Button>
        </div>
      </div>
  )
}

export default MapBox