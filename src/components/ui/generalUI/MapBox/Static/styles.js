export const styles = {
    mainContainer:{
        display: 'flex',
        width: '100%'
    },
    mapContainer: {
        position: 'relative',
        width: '100%',
        '&& .mapboxgl-ctrl-attrib-inner': {
            display: 'none'
        },
        'pointer-events': 'none'
    },
    notMarked:{
        position: 'absolute',
        top: '50%',
        right: '50%',
        color: 'grey'
    },
    marked:{
        position: 'absolute',
        top: '50%',
        right: '50%',
        color: 'orange'
    },
    sidebarStyle: {
        display: 'inline-block',
        position: 'absolute',
        top: '0',
        left: '0',
        margin: '12px',
        backgroundColor: '#404040',
        color: '#ffffff',
        zIndex: '1 !important',
        padding: '6px',
        fontWeight: 'bold'
      },
    buttonContainer:{
        display: 'grid',
        width: '10%'
    },
    iconButton:{
        color: 'orange'
    },
    iconButtonReset:{
        color: ({location})=> location? 'orange' : 'grey'
    }
}