import React, { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl'
import clsx from 'clsx'
import LocationOffIcon from '@material-ui/icons/LocationOff'
import LocationOnIcon from '@material-ui/icons/LocationOn'

import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(styles)

mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA'

const MapBoxStatic = (props) => {
  const mapContainerRef = useRef(null)

  const {className, location, defaultLocation} = props

  const classes = useStyles({location})

  // Initialize map when component mounts
  useEffect(() => {
    let lngU = location? location.lng : defaultLocation.lng
    let latU = location? location.lat : defaultLocation.lat
    let zoomU = location? location.zoom : 11
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lngU, latU],
      zoom: zoomU
    })

    // Clean up on unmount
    return () => map.remove()
  }, [defaultLocation, location])

  return (
      <div className={clsx(classes.mainContainer, className)}>
        <div className={classes.mapContainer} ref={mapContainerRef}>
            {
                !location? 
                <LocationOffIcon className={classes.notMarked}/> :
                <LocationOnIcon className={classes.marked}/>
            }
        </div>
      </div>
  )
}

export default MapBoxStatic