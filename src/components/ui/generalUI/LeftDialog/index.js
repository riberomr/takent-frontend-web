import React, { useContext } from 'react'
import Dialog from '@material-ui/core/Dialog'
import CloseIcon from '@material-ui/icons/Close'
import Slide from '@material-ui/core/Slide'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../Store'

const useStyles = makeStyles(styles)

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />
});

const LeftDialog = () => {

  const {
    leftDialog,
    setLeftDialog
  } = useContext(Store)

  const classes = useStyles()

  const handleClose = () => {
    setLeftDialog(null)
  }

  return (
    <div className={classes.divContainer}>
      <Dialog
        open={!!leftDialog}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        classes={{
          scrollPaper: classes.scrollPaper,
          paper: classes.paper,
          paperScrollPaper: classes.paperScrollPaper }}
      >
        <CloseIcon
          onClick={handleClose}
          className={classes.closeButton}
        />
        {leftDialog || <></>}
      </Dialog>
    </div>
  )
}

export default LeftDialog