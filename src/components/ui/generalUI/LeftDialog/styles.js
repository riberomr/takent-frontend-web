export const styles = {
    scrollPaper:{
        justifyContent: 'flex-end',
        // maxHeight: 'calc(100% - 48px)'
    },
    paper:{
        margin: '0px',
        top: '0px',
        position: 'fixed',
        width: '40vw',
        // height: '100vh',
        //border: 'solid 1px',
        zIndex: 999999,
        borderRadius: '0'
    },
    closeButton: {
        position: 'absolute',
        cursor: 'pointer',
        top: '1.3rem',
        right: '1rem',
        color: 'rgba(0,0,0, 0.54)',
        margin: '0.3rem',
        zIndex: '50',
        '&:hover': {
          background: 'rgba(0,0,0, 0.07)',
          borderRadius: '4%',
        }
    },
    divContainer:{
      zIndex: 9999
    },
    paperScrollPaper:{
      minHeight: '100vh'
    }
}
