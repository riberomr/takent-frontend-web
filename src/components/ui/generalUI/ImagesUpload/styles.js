import { theme } from "../../../../constants/generalConstants/theme";

export const styles = {
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  input: {
    display: 'none'
  },
  actions: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    margin: theme.spacing(4),
    '& label': {
      width: '70px',
      height: '70px'
    }
  }
}