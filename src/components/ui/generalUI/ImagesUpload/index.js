import { Fab, makeStyles } from '@material-ui/core'
import { AddPhotoAlternate, DeleteForever } from '@material-ui/icons'
import { useSnackbar } from 'notistack'
import React from 'react'
// import { theme } from '../../../../constants/generalConstants/theme'
import CustomCarousel from '../CustomCarousel'
import { styles } from './styles'

const ALLOWED_TYPES = ['jpeg', 'png']

const useStyles = makeStyles(styles)

const ImagesUpload = (props) => {

  const { height, carouselLength, images, setImages, imagesLimit, disabled
    // ...other
  } = props
  const { enqueueSnackbar } = useSnackbar()

  const classes = useStyles()

  const handleUploadChange = event => {
    if(!event.target.files.length)
      return enqueueSnackbar('Error al cargar imagen, formato inválido.', { variant: 'error' })
    const file = event.target.files[0]
    if(((file.size / 1024 ) / 1024) > 5)
      return enqueueSnackbar('Error al cargar imagen, el tamaño excede los 5 Mb.', { variant: 'error' })
    let [type, extension] = file.type.split('/')
    if (!ALLOWED_TYPES.includes(extension) || type !== 'image' || !file.type) {
      return enqueueSnackbar(`La imagen debe tener formato ${ALLOWED_TYPES.map(element => {
        return element.toUpperCase()
      }).join(" o ")}`, { variant: 'error' })
    }
    const reader = new FileReader()
    reader.onloadend = (e) => {
      setImages(prevState => {
        return [...prevState, reader.result]
      })
    }
    reader.readAsDataURL(file)
  }

  const handleUploadClick = event => {
    if(imagesLimit && images.length >= imagesLimit) {
      enqueueSnackbar(`Solo se pueden cargar hasta ${imagesLimit} imágenes`, { variant: 'error' })
      event.preventDefault()
    }
    event.target.value = ""
  }

  const handleDeleteClick = () => {
    setImages(prevState => {
      let newState = [...prevState]
      newState.pop()
      return newState
    })
  }

  return (
    <>
      <div className={classes.root}>
        <div style={{width: '100%'}}>
          <CustomCarousel
            images={images}
            autoPlay={false}
            animation={"slide"}
            navButtonsAlwaysVisible={true}
            height={height}
            slideLength={carouselLength}
          />
        </div>
        <div className={classes.actions}>
          <input
            id="button-upload-image"
            type="file"
            multiple
            accept="image/*"
            className={classes.input}
            onChange={handleUploadChange}
            onClick={handleUploadClick}
            disabled={disabled}
          />
          <label htmlFor="button-upload-image">
            <Fab disabled={disabled} component="span" color="primary">
              <AddPhotoAlternate/>
            </Fab>
          </label>
          <label onClick={disabled? null : handleDeleteClick}>
            <Fab disabled={disabled} component="span" color="primary">
              <DeleteForever/>
            </Fab>
          </label>
        </div>
      </div>
    </>
  )
}

export default ImagesUpload