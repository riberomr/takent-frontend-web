import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import TextField from '@material-ui/core/TextField'
import { format, addHours } from 'date-fns'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import commonUserEducation from '../../../../../rules/validationSchema/commonUserEducation'
import { Grid, FormControlLabel, Checkbox } from '@material-ui/core'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'

const useStyles = makeStyles(styles)

const EducationForm = (props) => {
    const { data, handleFormSubmit, closeLeftDialog, action, id } = props
    const NEW = 'NEW'
    const classes = useStyles()
    const [educationLevels, setEducationLevels] = useState([])
    const [levelSelected, setLevelSelected] = useState(data? data.level : "")
    const variant = 'standard'

    let initialValues = {
        institution: '',
        title:'',
        start_date: '',
        end_date: '',
        level: '',
        description: '',
        checkPresentEndDate: false
    }
    if(data) {
        //este parche tiene que irse despues agrego 4 horas para que me tome el día bien
        initialValues = {
            institution: data.institution,
            title: data.title,
            start_date: format(
                addHours(new Date(`${data.start_date}`), 4),
                'yyyy-MM-dd'
              ),
            end_date: data.end_date? format(
                addHours(new Date(`${data.end_date}`), 4),
                'yyyy-MM-dd'
            ) : '',
            level: data.level,
            description: data.description,
            checkPresentEndDate: data.end_date? false : true
        }
    }   
 
    useEffect(()=>{
        apiTakent.getEducationLevels()
            .then((res) => {
                const levels = res.data.map((level) => {return level.name})
                setEducationLevels(levels)
            })
    },[])

    const handleEducationLevelChange = (value, setFieldValue) => {
        if(value) {
            setLevelSelected(value)
            setFieldValue('level', value)
        }
        else {
            setLevelSelected('')
            setFieldValue('level', '')}
    }

    const handleCheckPresentEndDate = (checked, setFieldValue) => {
        setFieldValue('checkPresentEndDate', checked)
        setFieldValue('end_date', '')
    }

    const handleSubmitForm = (values) => {
        //este parche tiene que irse despues agrego 4 horas para que me tome el día bien
        let valuesToSend = {
            title: values.title,
            institution: values.institution,
            start_date: format(
                addHours(new Date(`${values.start_date}`), 4),
                'yyyy-MM-dd'
              ),
            end_date: values.end_date? format(
                addHours(new Date(`${values.end_date}`), 4),
                'yyyy-MM-dd'
            ) : null,
            level: values.level,
            description: values.description
        }
        if(action!==NEW && id)
            handleFormSubmit(id, valuesToSend)
        else handleFormSubmit(valuesToSend)
    }
    
    return(<>
        <Formik
            initialValues={initialValues}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserEducation}
        >
        {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue
        }) => (
            <form onSubmit={handleSubmit}>
                <div className={classes.formRoot}>
                    <Grid container spacing={5}>
                        <Grid item xs={12} className={classes.gridItem}>
                            <span className={classes.titleLabel}>{action===NEW? 'Nuevo dato académico' : 'Editar dato académico'}</span>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='title'
                                type='text'
                                name='title'
                                label="Título o certificado"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.title}
                                error={errors.title && touched.title}
                                helperText={(errors.title && touched.title)? errors.title : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='institution'
                                type='text'
                                name='institution'
                                label="Institución"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.institution}
                                error={errors.institution && touched.institution}
                                helperText={(errors.institution && touched.institution)? errors.institution : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <Select
                                id="level"
                                name="level"
                                label="Nivel alcanzado"
                                variant={variant}
                                fullWidth
                                options={educationLevels}
                                value={levelSelected}
                                onLoad={(event) => handleEducationLevelChange(event.target.value, setFieldValue)}
                                onChange={(event) => handleEducationLevelChange(event.target.value, setFieldValue)}
                                error={errors.level && touched.level}
                                helperText={(errors.level && touched.level)? errors.level : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField 
                                id='start_date'
                                type='date'
                                name='start_date'
                                label="Fecha de inicio"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.start_date}
                                error={errors.start_date && touched.start_date}
                                helperText={(errors.start_date && touched.start_date)? errors.start_date : ""}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField 
                                    id='end_date'
                                    type='date'
                                    name='end_date'
                                    label="Fecha de finalización"
                                    variant={variant}
                                    fullWidth
                                    InputProps={{disabled: values.checkPresentEndDate}}
                                    InputLabelProps={{shrink: true}}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.end_date}
                                    error={errors.end_date && touched.end_date}
                                    helperText={(errors.end_date && touched.end_date)? errors.end_date : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                                            checkedIcon={<CheckBoxIcon fontSize="small" color="primary"/>}
                                            name="checkPresentEndDate"
                                            checked={values.checkPresentEndDate}
                                            onChange={(event) => handleCheckPresentEndDate(event.target.checked, setFieldValue)}
                                        />
                                    }
                                    label="Actualidad"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItemCentered}>
                                <TextField
                                    multiline
                                    rows={4}
                                    id='description'
                                    type='text'
                                    name='description'
                                    label="Comentario"
                                    variant={variant}
                                    fullWidth
                                    InputLabelProps={{shrink: true}}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.description}
                                    error={errors.description && touched.description}
                                    helperText={(errors.description && touched.description)? errors.description : ""}
                                />
                            </Grid>
                    </Grid>
                    <div className={classes.buttonContainer}>
                        <FormButton
                            onClick={()=> closeLeftDialog()}
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Cancelar
                        </FormButton>
                        <FormButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Guardar
                        </FormButton>
                    </div>
                </div>
            </form>
        )}
        </Formik>
        </>
    )
}

export default EducationForm