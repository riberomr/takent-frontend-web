import React, { useContext, useState, useEffect } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../../Store'
import EducationForm from './EducationForm'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import Button from '@material-ui/core/Button'
import AlertAction from '../../../generalUI/AlertAction'
import CardItem from '../../../generalUI/CardItem'
import { v4 } from 'uuid'
import { format } from 'date-fns'

const useStyles = makeStyles(styles)

const Education = (props) => {
    const NEW = 'NEW'
    const EDIT = 'EDIT'
    const {userEducation, handleFormSubmit, deleteEducation, editEducation
        // , loading
        } = props
    const context = useContext(Store)
    const classes = useStyles()

    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [id, setId] = useState(null)
    const [values, setValues] = useState(null)

    const closeLeftDialog = () => {
        context.setLeftDialog(null)
    }

    //this function open the alert and receive and set the an id if from the form
    const openAlert = (id, values) => {
        if(id) setId(id)
        if(values) setValues(values)    
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {

        if(type === 'accept' && id && !values) {
            deleteEducation(id)
            setIsAlertOpen(false)
            setId(null)
        }
        if(type === 'accept' && id && values) {
            setIsAlertOpen(false)
            editEducation(id, values)
            setValues(null)
            setId(null)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    }
    
    useEffect(()=> {
        if(!isAlertOpen) setValues(null)
    },[isAlertOpen])


    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const newEducation = () => {
        context.setLeftDialog(<EducationForm handleFormSubmit={handleFormSubmit} closeLeftDialog={closeLeftDialog} action={NEW}/>)
    }

    const editItem = (editableData) => {
        context.setLeftDialog(
        <EducationForm 
            handleFormSubmit={openAlert} 
            data={editableData} 
            closeLeftDialog={closeLeftDialog} 
            action={EDIT}
            id={editableData.id}
            />)
    }

    const getDisplayData = (data) => {
        let displayData = {
            title: "Dato académico",
            content: {
                "Título": data.title,
                "Institución": data.institution,
                "Periodo": "Desde " + format(new Date(data.start_date), 'dd/MM/yyyy') + " hasta " + (data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : "Actualidad"),
                "Nivel": data.level
            }
        }
        if(data.description) {
            displayData.content = {
                ...displayData.content,
                "Comentario": data.description,
            }
        }
        return displayData
    }

    return (
        <div className={classes.content}>
            <div className={classes.titleContainer}>
                <span className={classes.titleText}>Mis datos académicos</span>
                <Button onClick={()=> newEducation()}>
                    <AddCircleOutlineIcon fontSize='large'/>
                </Button>
            </div>
            <div className={classes.mainContent}>
                <div className={classes.educations}>
                    {userEducation && userEducation.length>0 &&
                    //  !loading &&
                        userEducation.map((data, index)=> {
                            return(
                                    <div key={v4()}>
                                        {(index % 2 === 0) && 
                                            <CardItem 
                                                key={index} 
                                                index={index} 
                                                data={data}
                                                displayData={getDisplayData(data)}
                                                title={data.title}
                                                subtitle={data.institution}
                                                valueText1={`${data.start_date? format(new Date(data.start_date), 'dd/MM/yyyy') : '-'} - 
                                                            ${data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : 'Actualidad'}`}
                                                valueText1Shortable={false}
                                                editItem={editItem}
                                                deleteItem={openAlert} 
                                                closeLeftDialog={closeLeftDialog} 
                                            />
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className={classes.educations}>
                        {userEducation && userEducation.length>0 &&
                        // !loading &&
                        userEducation.map((data, index)=> {
                                return(
                                        <div key={v4()} >
                                            {(index % 2 !== 0) && 
                                                <CardItem 
                                                    key={index} 
                                                    index={index} 
                                                    data={data}
                                                    displayData={getDisplayData(data)}
                                                    title={data.title}
                                                    subtitle={data.institution}
                                                    valueText1={`${data.start_date? format(new Date(data.start_date), 'dd/MM/yyyy') : '-'} - 
                                                                ${data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : 'Actualidad'}`}
                                                    valueText1Shortable={false}
                                                    editItem={editItem}
                                                    deleteItem={openAlert} 
                                                    closeLeftDialog={closeLeftDialog} 
                                                />
                                            }
                                        </div>
                                    )
                                }
                            )
                        }
                    </div>
                </div>
            {(!userEducation || userEducation.length===0) && 
                <div>
                 <span className={classes.noData}>No agregaste información.</span>
                </div>
            }
            {isAlertOpen && <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title={values? '¿Desea confirmar los cambios?' : '¿Desea eliminar estos datos académicos?'}
                loading={false}
            />}
        </div>
    )
}

export default Education