import React, { useState } from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import commonUserChangePass from '../../../../../rules/validationSchema/commonUserChangePass'
import { Grid, TextField } from '@material-ui/core'
import { authService } from '../../../../../../services/auth.service'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles(styles)

const ChangePassForm = (props) => {
    const { handleFormSubmit, closeLeftDialog } = props
    const classes = useStyles()

    const [showPass, setShowPass] = useState(false)
    const [showNewPass, setShowNewPass] = useState(false)
    const [showRepeatNewPass, setShowRepeatNewPass] = useState(false)
    const { enqueueSnackbar } = useSnackbar()

    const variant = 'standard'

    const user = JSON.parse(localStorage.getItem('takent_user'))

    let initialValues = {
        pass: '',
        newPass: '',
        repeatNewPass: '',
    }  

    const handleSubmitForm = async values => {
      try {
        await authService.checkPass(user._id, values.pass)
      }
      catch(error) {
        return enqueueSnackbar("La contraseña ingresada no es correcta", { variant: "error" })
      }
      let valuesToSend = {
          new_password: values.newPass,
      }
      handleFormSubmit(valuesToSend)
    }
    
    const handleMouseDownPassword = (event) => {
        event.preventDefault()
      }

    return(<>
        <Formik
            initialValues={initialValues}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserChangePass}
        >
        {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            errors,
            touched
        }) => (
            <form onSubmit={handleSubmit}>
                <div className={classes.formRoot}>
                    <Grid container spacing={5}>
                        <Grid item xs={12} className={classes.gridItem}>
                            <span className={classes.titleLabel}>{'Cambiar contraseña'}</span>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='pass'
                                type={showPass ? 'text' : 'password'}
                                name='pass'
                                label="Contraseña actual"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.pass}
                                error={errors.pass && touched.pass}
                                helperText={(errors.pass && touched.pass)? errors.pass : ""}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={() => setShowPass(!showPass)}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showPass ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='newPass'
                                type={showNewPass ? 'text' : 'password'}
                                name='newPass'
                                label="Nueva contraseña"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.newPass}
                                error={errors.newPass && touched.newPass}
                                helperText={(errors.newPass && touched.newPass)? errors.newPass : ""}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={() => setShowNewPass(!showNewPass)}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showNewPass ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='repeatNewPass'
                                type={showRepeatNewPass ? 'text' : 'password'}
                                name='repeatNewPass'
                                label="Repetir nueva contraseña"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.repeatNewPass}
                                error={errors.repeatNewPass && touched.repeatNewPass}
                                helperText={(errors.repeatNewPass && touched.repeatNewPass)? errors.repeatNewPass : ""}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={() => setShowRepeatNewPass(!showRepeatNewPass)}
                                                onMouseDown={handleMouseDownPassword}
                                            >
                                                {showRepeatNewPass ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                    </Grid>
                    <div className={classes.buttonContainer}>
                        <FormButton
                            onClick={()=> closeLeftDialog()}
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Cancelar
                        </FormButton>
                        <FormButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Guardar
                        </FormButton>
                    </div>
                </div>
            </form>
        )}
        </Formik>
        </>
    )
}

export default ChangePassForm