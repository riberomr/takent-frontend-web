export const styles = {
    content:{
        margin: '0px 50px 0px 50px',
        width: '90%'
    },
    titleText:{
        fontFamily: 'Roboto',
        fontSize: '18px',
        fontWeight: 'bold'
    },
    mainContent:{
        paddingTop: '15px',
        display: 'flex',
        justifyContent: 'space-between'
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    languages:{
        width: '50%'
    },
    noData:{
        fontSize: '18px',
        fontFamily: 'Roboto',
        color: 'grey'
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    languageName: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'
    }
}