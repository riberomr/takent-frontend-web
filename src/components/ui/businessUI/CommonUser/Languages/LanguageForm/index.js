import React, { useState } from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import commonUserLanguages from '../../../../../rules/validationSchema/commonUserLanguages'
import { Grid, TextField, FormControlLabel, Checkbox } from '@material-ui/core'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'

const useStyles = makeStyles(styles)

const LanguageForm = (props) => {
    const { data, handleFormSubmit, closeLeftDialog, action, languages, languageLevels, id } = props
    const NEW = 'NEW'
    const classes = useStyles()
    const [language, setLanguage] = useState(data? data.language : "")
    const [languageWritingLevel, setLanguageWritingLevel] = useState(data? data.writing_level : "")
    const [languageSpeakingLevel, setLanguageSpeakingLevel] = useState(data? data.communication_level : "")
    const variant = 'standard'

    const getOptionsLanguages = () => {
        if(languages) {
            return languages.map((language) => {
                return language.name
            })
        }
    }

    const getOptionsLanguageLevels = () => {
        if(languageLevels){
            return languageLevels.map((languageLevel) => {
                return languageLevel._id
            })
        }
    }

    let initialValues = {
        language:'',
        communication_level: '',
        writing_level: '',
        description: '',
        certificate: false
    }
    if(action!==NEW && data) {
        initialValues = {
            language: data.language,
            communication_level: data.communication_level,
            writing_level: data.writing_level,
            description: data.description,
            certificate: data.certificate
        }
    }
 
    const handleSubmitForm = (values) => {

        if(action!==NEW && id)
            handleFormSubmit(id, values)
        else handleFormSubmit(values)
    }

    const handleLanguageChange = (value, setFieldValue) => {
        setLanguage(value)
        setFieldValue('language', value)
    }

    const handleLanguageSpeakingLevelChange = (value, setFieldValue) => {

        setLanguageSpeakingLevel(value)
        setFieldValue('communication_level', value)
    }

    const handleLanguageWritingLevelChange = (value, setFieldValue) => {

        setLanguageWritingLevel(value)
        setFieldValue('writing_level', value)
    }
    
    return(<>
        <Formik
            initialValues={initialValues}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserLanguages}
        >
        {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue
        }) => (
            <form onSubmit={handleSubmit}>
                <div className={classes.formRoot}>
                    <Grid container spacing={5}>
                    <Grid item xs={12} className={classes.gridItem}>
                            <span className={classes.titleLabel}>{action===NEW? 'Nuevo idioma ' : 'Editar idioma'}</span>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <Select
                                id="language"
                                name="language"
                                label="Idioma"
                                variant={variant}
                                fullWidth
                                options={getOptionsLanguages()}
                                value={language}
                                onChange={(event) => handleLanguageChange(event.target.value, setFieldValue)}
                                error={errors.language && touched.language}
                                helperText={(errors.language && touched.language)? errors.language : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <Select
                                id="communication_level"
                                name="communication_level"
                                label="Nivel de comunicación"
                                variant={variant}
                                fullWidth
                                options={getOptionsLanguageLevels()}
                                value={languageSpeakingLevel}
                                onChange={(event) => handleLanguageSpeakingLevelChange(event.target.value, setFieldValue)}
                                error={errors.communication_level && touched.communication_level}
                                helperText={(errors.communication_level && touched.communication_level)? errors.communication_level : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <Select
                                id="writing_level"
                                name="writing_level"
                                label="Nivel de escritura"
                                variant={variant}
                                fullWidth
                                options={getOptionsLanguageLevels()}
                                value={languageWritingLevel}
                                onChange={(event) => handleLanguageWritingLevelChange(event.target.value, setFieldValue)}
                                error={errors.writing_level && touched.writing_level}
                                helperText={(errors.writing_level && touched.writing_level)? errors.writing_level : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                multiline
                                rows={4}
                                id='description'
                                type='text'
                                name='description'
                                label="Comentario"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                placeholder={"Por ejemplo, podés agregar tu certificación o que viviste en el extranjero"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.description}
                                error={errors.description && touched.description}
                                helperText={(errors.description && touched.description)? errors.description : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                                            checkedIcon={<CheckBoxIcon fontSize="small" color="primary"/>}
                                            id="certificate"
                                            name="certificate"
                                            checked={values.certificate}
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                        />
                                    }
                                    label="Tengo certificado"
                                />
                            </Grid>
                    </Grid>
                    <div className={classes.buttonContainer}>
                        <FormButton
                            onClick={()=> closeLeftDialog()}
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Cancelar
                        </FormButton>
                        <FormButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Guardar
                        </FormButton>
                    </div>
                </div>
            </form>
        )}
        </Formik>
        </>
    )
}

export default LanguageForm