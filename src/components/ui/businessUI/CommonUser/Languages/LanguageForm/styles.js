import { theme } from "../../../../../../constants/generalConstants/theme"

export const styles = {
    buttonContainer:{
        backgroundColor: 'white',
        paddingTop: '30px',
        paddingBottom: '30px',
        position: 'sticky',
        bottom: '0px',
        display: 'flex',
        justifyContent: 'space-around',
    },
    labelContainer:{
        width: '35%',
        display: 'grid'
    },
    inputContainter:{
        width: '65%'
    },
    textField:{
        width: '100%'
    },
    buttons: {
        width: '150px',
        height: '40px'
    },
    formLabel:{
        fontFamily: 'Roboto',
        fontSize: '16px'
    },
    titleLabel:{
        fontFamily: 'Roboto',
        fontSize: '16px',
        fontWeight: 'bold',
        paddingBottom: '10px',
        display: 'flex'
    },
    errorMessage: {
        fontSize: '12px',
        color: 'rgb(244, 67, 54)',
        fontFamily: 'Roboto',
        marginTop: '8px'
    },
    formRoot: {
        flexGrow: 1,
        padding: theme.spacing(6),
        paddingBottom: "0"
    },
    gridItem: {
        display: 'flex',
        flexGrow: 1,
    },
    gridItemCentered: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
}
