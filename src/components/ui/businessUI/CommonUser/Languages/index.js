import React, { useContext, useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../../Store'
import LanguageForm from './LanguageForm'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import Button from '@material-ui/core/Button'
import { v4 } from 'uuid'
import AlertAction from '../../../generalUI/AlertAction'
import CardItem from '../../../generalUI/CardItem'
import { Chip } from '@material-ui/core'
import DoneIcon from '@material-ui/icons/Done'
//import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(styles)

const Languages = (props) => {
    const NEW = 'NEW'
    const EDIT = 'EDIT'
    const {userLanguages, handleFormSubmit, deleteLanguage, languages, languageLevels, editLanguage
        // , loading
    } = props
    const context = useContext(Store)
    const classes = useStyles()

    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [id, setId] = useState(null)
    const [values, setValues] = useState(null)

    const closeLeftDialog = () => {
        context.setLeftDialog(null)
    }

    //this function open the alert and receive and set the an id if from the form
    const openAlert = (id, values) => {
        if(id) setId(id)
        if(values) setValues(values)    
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {

        if(type === 'accept' && id && !values) {
            deleteLanguage(id)
            setIsAlertOpen(false)
            setId(null)
        }
        if(type === 'accept' && id && values) {
            setIsAlertOpen(false)
            editLanguage(id, values)
            setValues(null)
            setId(null)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    }


    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const newLanguage= () => {
        context.setLeftDialog(
            <LanguageForm 
                languages={languages}
                languageLevels={languageLevels} 
                handleFormSubmit={handleFormSubmit} 
                closeLeftDialog={closeLeftDialog} 
                action={NEW}
            />
        )
    }

    const editItem = (editableData) => {
        context.setLeftDialog(
            <LanguageForm
                id={editableData.id} 
                languages={languages}
                languageLevels={languageLevels}
                handleFormSubmit={openAlert} 
                data={editableData} 
                closeLeftDialog={closeLeftDialog} 
                action={EDIT}
            />
        )
    }

    const getDisplayData = (data) => {
        let displayData = {
            title: "Idioma",
            content: {
                "Idioma": data.language,
                "Nivel de comunicación": data.communication_level,
                "Nivel de escritura": data.writing_level,
                "Certificado": data.certificate? "Si" : "No",
            }
        }
        if(data.description) {
            displayData.content = {
                ...displayData.content,
                "Comentario": data.description
            }
        }
        return displayData
    }


    return (
        <div className={classes.content}>
            <div className={classes.titleContainer}>
                <span className={classes.titleText}>Idiomas</span>
                <Button onClick={()=> newLanguage()}>
                    <AddCircleOutlineIcon fontSize='large'/>
                </Button>
            </div>
            <div className={classes.mainContent}>
            <div className={classes.languages}>
                {userLanguages && userLanguages.length>0 &&
                //  !loading &&
                    userLanguages.map((data, index)=> {
                        return(
                                <div key={v4()}>
                                    {(index % 2 === 0) && 
                                        <CardItem 
                                            key={index} 
                                            index={index} 
                                            data={data}
                                            displayData={getDisplayData(data)}
                                            title={(
                                                <div className={classes.title}>
                                                    <span className={classes.languageName}>{data.language}</span>
                                                    {data.certificate && 
                                                        <Chip 
                                                            label="Certificado" 
                                                            size="small" 
                                                            color="primary" 
                                                            icon={<DoneIcon />}
                                                        />
                                                    }
                                                </div>
                                            )}
                                            labelText1='Nivel de comunicación:'
                                            valueText1={data.communication_level}
                                            labelText2='Nivel de escritura:'
                                            valueText2={data.writing_level}
                                            editItem={editItem}
                                            deleteItem={openAlert} 
                                            closeLeftDialog={closeLeftDialog} 
                                        />
                                    }
                                </div>
                            )
                        })
                    }
                </div>
                <div className={classes.languages}>
                    {userLanguages && userLanguages.length>0 && 
                    // !loading &&
                        userLanguages.map((data, index)=> {
                            return(
                                    <div key={v4()} >
                                        {(index % 2 !== 0) && 
                                            <CardItem 
                                                key={index} 
                                                index={index} 
                                                data={data}
                                                displayData={getDisplayData(data)}
                                                title={(
                                                    <div className={classes.title}>
                                                        <span className={classes.languageName}>{data.language}</span>
                                                        {data.certificate && 
                                                            <Chip 
                                                                label="Certificado" 
                                                                size="small" 
                                                                color="primary" 
                                                                icon={<DoneIcon />}
                                                            />
                                                        }
                                                    </div>
                                                )}
                                                labelText1='Nivel de comunicación:'
                                                valueText1={data.communication_level}
                                                labelText2='Nivel de escritura:'
                                                valueText2={data.writing_level}
                                                editItem={editItem}
                                                deleteItem={openAlert} 
                                                closeLeftDialog={closeLeftDialog} 
                                             />
                                        }
                                    </div>
                                )
                            }
                        )
                    }
                </div>
            </div>
            {(!userLanguages || userLanguages.length === 0) &&
            //  !loading &&
                <div>
                 <span className={classes.noData}>No agregaste información.</span>
                </div>
            }
            {/* {loading && <CircularProgress/>} */}
            {isAlertOpen && <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title={values? '¿Desea confirmar los cambios?' : '¿Desea eliminar este idioma?'}
                loading={false}
            />}
        </div>
    )
}

export default Languages