import { Button, Chip, CircularProgress, Container, Grid, makeStyles, ThemeProvider, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { styles } from './styles'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import PaymentIcon from '@material-ui/icons/Payment'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import queryString from 'query-string'

const useStyles = makeStyles(styles)

const BuyServicePreferenceForm = props => {

  const classes = useStyles()

  const { activeServices, location } = props
  const [ selectedService, setSelectedService ] = useState(null)
  const [ loading, setLoading ] = useState(false)
  const [ preferenceId, setPreferenceId ] = useState()
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    if(location.state && activeServices.map(activeService => activeService.code).includes(location.state))
      setSelectedService(location.state)
  }, [location.state, activeServices])

  const handleConfirm = () => {
    const beforeButton = document.getElementsByClassName('mercadopago-button')
    if(beforeButton.length > 0)
      beforeButton[0].remove()
    setLoading(true)
    apiTakent.postGenerateMPPayment({concept_id: "SERVICE", service_id: selectedService})
    .then(response => {
      setPreferenceId(response.data.response.id)
      setLoading(false)
    })
  }

  useEffect(() => {
    const payment = queryString.parse(location.search)
    if(payment.preference_id){
      if(payment.payment_status === "approved"){
        enqueueSnackbar('¡Servicio preferencial comprado con éxito!', { variant: 'success' })
      }
      else if(payment.payment_status === "pending"){
        enqueueSnackbar('Pago pendiente, en cuanto se acredite el pago se actualizará el servicio', { variant: 'warning' })
      }
      else{
        enqueueSnackbar('Error al procesar el pago', { variant: 'error' })
      }
    }
  }, [enqueueSnackbar, location.search])

  useEffect(() => {
    if(!preferenceId){
      const beforeButton = document.getElementsByClassName('mercadopago-button')
      if(beforeButton.length > 0)
        beforeButton[0].remove()
      return
    }
    const scriptElemen = document.getElementById('mp-script')
    if(scriptElemen)
      scriptElemen.remove()
    const beforeButton = document.getElementsByClassName('mercadopago-button')
    if(beforeButton.length > 0)
      beforeButton[0].remove()
    const script = document.createElement('script');
    script.src = 'https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js';
    script.async = false;
    script.setAttribute('id', 'mp-script')
    script.setAttribute('data-preference-id', preferenceId);
    document.getElementById('mercadoForm').appendChild(script);
  }, [preferenceId])

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            <ShoppingCartIcon color="primary" style={{paddingRight: '5px'}}/>
            <Typography variant="h5">Pagar servicio preferencial</Typography>
          </div>
          <Chip label="Valor de servicio preferencial: $200" color="primary"></Chip>
        </div>
        <Grid container spacing={5}>
          <Grid item xs={12} md={12}>
            <Select
              label="Seleccioná uno de tus servicios activos"
              variant={"standard"}
              fullWidth
              options={activeServices}
              value={selectedService}
              onChange={e => setSelectedService(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} md={12} style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
            <Button
              variant="contained"
              color="primary"
              startIcon={<PaymentIcon/>}
              disabled={!selectedService}
              onClick={handleConfirm}
            >
              Confirmar compra
            </Button>
          </Grid>
        </Grid>
        <form method="GET" id="mercadoForm" style={{marginTop: theme.spacing(4), display: "flex", justifyContent: 'center'}}>
          {loading && <CircularProgress/>}
        </form>
      </Container>
    </ThemeProvider>
  )
}

export default BuyServicePreferenceForm
