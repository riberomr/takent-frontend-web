import React, { useState } from 'react'
import { makeStyles, Container, ThemeProvider, Typography } from '@material-ui/core'
import { theme } from '../../../../../../constants/generalConstants/theme'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import ViewListIcon from '@material-ui/icons/ViewList';
import { format, addHours, addDays } from 'date-fns'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import { styles } from './styles'
import DataDrawer from '../../../../generalUI/DataDrawer'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import AlertAction from '../../../../generalUI/AlertAction'
import { useHistory } from 'react-router-dom'
import ActionButtonsService from '../../../../generalUI/ActionButtonsService'
import _ from 'underscore'
import Rating from '@material-ui/lab/Rating'

const useStyles = makeStyles(styles)

const MyServicesList = props => {

  const {
    ratingStatistics,
    serviceList,
    stateFilter,
    setStateFilter,
    setFirstDateFilter,
    setLastDateFilter,
    setViewRequests,
    setIdSelectedService,
    handleChangeStateService,
    loading
  } = props

  const classes = useStyles(styles)

  const [ isDrawerOpen, setIsDrawerOpen ] = useState(false)
  const [ drawerData, setDrawerData ] = useState({})

  const [ confirmSelectionOpen, setConfirmSelectionOpen ] = useState(false)
  const [ handleDiscard, setHandleDiscard ] = useState(null)
  const [ confirmationMessage, setConfirmationMessage ] = useState(null)

  const history = useHistory()

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmSelection = (_id, state) => {
    let action
    switch(state) {
      case "active":
        action = "Activar"
        break
      case "inactive":
        action = "Desactivar"
        break
      case "finished":
        action = "Finalizar"
        break
      default:
        action = "No se"
    }
    setConfirmationMessage(`¿${action} servicio?`)
    setConfirmSelectionOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        handleChangeStateService(_id, state)
        setConfirmSelectionOpen(false)
      }
      if(type === 'cancel') {
        setConfirmSelectionOpen(false)
      }
    })
  }

  const handleViewService = (rowData) => {
    if(!rowData){
      return
    }
    let displayValues = {
      title: rowData.titleTable,
      content: {
        "Estado": rowData.state,
        "Second Row": [
          { "Fecha de publicación": rowData.publication_date },
          { "Días restantes": rowData.remaining_days }
        ],
        "Third Row": [
          {"Pendientes": (rowData.service_requests_pending && rowData.service_requests_pending !== 0) ? rowData.service_requests_pending : "0"},
          {"Completadas": (rowData.service_requests_completed && rowData.service_requests_completed !== 0) ? rowData.service_requests_completed : "0"}
        ],
        "Costo": rowData.costTable,
        "Rubro": rowData.sector,
        "Descripción": rowData.description,
      },
      location: rowData.address,
    }
    setDrawerData(displayValues)
    setIsDrawerOpen(true)
    apiTakent.getServiceImages(rowData._id)
    .then(response => {
      setDrawerData(prevState => {
        return {...prevState, images: response.data}
      })
    })
  }

  const handleViewRequests = rowData => {
    setIdSelectedService(rowData._id)
    setViewRequests(true)
  }

  const chipColors = {
      GREEN: '#E8F5E9',
      B_GREEN: '#4CAF50',
      RED: '#FEEBEE',
      B_RED: '#F2453D',
      YELLOW: '#FFF8E1',
      B_YELLOW: '#FFC107',
      BLUE: '#E3F2FD',
      B_BLUE: '#2C98F0'
    }

  const CHIP_STATE_SERVICE_LIST = { type: 'chip', values: 
      {
        'Activo': { ligth : chipColors.GREEN, dark : chipColors.B_GREEN},
        'Inactivo': { ligth : chipColors.YELLOW, dark : chipColors.B_YELLOW},
        'Finalizado' : { ligth : chipColors.RED, dark : chipColors.B_RED},
      }
  }

  const header = [
      createHeader('titleTableWithEllipsis', 'center', 'left', false, 'Título', false),
      createHeader('costTable', 'center', 'center', false, 'Costo', false),
      createHeader('requests_quantity', 'center', 'center', false, 'Solicitudes', false),
      createHeader('publication_date', 'center', 'center', false, 'Fecha de publicación', false),
      createHeader('remaining_days', 'center', 'center', false, 'Días restantes', false),
      createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_SERVICE_LIST)
  ]

  const actionButtonsServiceProps = {
      firstTooltip: 'Ver solicitudes de servicio',
      secondTooltip: 'Ver servicio',
      thirdTooltip: 'Desactivar servicio',
      fourthTooltip: 'Finalizar servicio',
      fifthTooltip: 'Activar servicio',
      sixthTooltip: 'Republicar servicio',
      seventhTooltip: 'Pagar servicio preferencial',
      firstFunction: rowData => handleViewRequests(rowData),
      secondFunction: rowData => handleViewService(rowData),
      thirdFunction: rowData => confirmSelection(rowData._id, 'inactive'),
      fourthFunction: rowData => confirmSelection(rowData._id, 'finished'),
      fifthFunction: rowData => confirmSelection(rowData._id, 'active'),
      sixthFunction: rowData => history.push("/app/user/service/new", _.omit(rowData, "titleTable")),
      seventhFunction: rowData => history.push("/app/user/service/buy", rowData._id)
    }
  
  const handleChangeFirstDate = (date) => {
      setFirstDateFilter(format(addHours(new Date(date), 3),'yyyy/MM/dd'))
  }
  const handleChangeLastDate = (date) => {
      setLastDateFilter(format(addHours(addDays(new Date(date), 1), 3),'yyyy/MM/dd'))
  }
  const handleChangeState = (ev) => {
      setStateFilter(ev.target.value)
  }

  return(
    <ThemeProvider theme={theme}>
      <Container classes={{root: classes.marginBottom}} maxWidth="md">
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
              <ViewListIcon color="primary" style={{paddingRight: '5px'}}/>
              <Typography variant="h5">Mis servicios</Typography>
          </div>
        </div>
        <div className={classes.ratingContainer}>
            <Typography variant='body1' style={{marginRight: theme.spacing(1)}}><strong>Valoración promedio</strong></Typography>
            {ratingStatistics.average_score !== undefined && <Rating value={ratingStatistics.average_score} size="large" readOnly style={{marginRight: theme.spacing(2)}}/>}
            <Typography variant='body1'><strong>Cantidad de valoraciones:</strong> {ratingStatistics.total_assessments}</Typography>
        </div>
        <div className={classes.filtersContainer}>
            <div className={classes.itemFilterContainer}>
              <TextField
                  id='firstDate'
                  type='date'
                  name='firstDate'
                  label="Inicio periodo"
                  fullWidth
                  variant='standard'
                  InputLabelProps={{shrink: true}}
                  onChange={(ev)=> handleChangeFirstDate(ev.target.value)}
              />
            </div>
              <div className={classes.itemFilterContainer}>
                <TextField
                  id='lastDate'
                  type='date'
                  name='lastDate'
                  label="Fin periodo"
                  fullWidth
                  variant='standard'
                  InputLabelProps={{shrink: true}}
                  onChange={(ev)=> handleChangeLastDate(ev.target.value)}
                />
              </div>
              <div className={classes.itemFilterContainer}>
                <Select
                  id="status"
                  name="status"
                  label="Estado"
                  variant='standard'
                  options={['Seleccione', 'Activo', 'Inactivo', 'Finalizado']}
                  value={stateFilter}
                  onChange={(ev) => handleChangeState(ev)}
                />
              </div>
            </div>
            <ResponsiveTable
              noDataMessage={'No se encontraron servicios'}
              header={header}
              rows={serviceList}
              actions={actionButtonsServiceProps ? <ActionButtonsService
              actionButtonsServiceProps={actionButtonsServiceProps}
              /> : null}
              loading={loading}
            />
            <DataDrawer
              anchor="right"
              isOpen={isDrawerOpen}
              setIsOpen={setIsDrawerOpen}
              variant="temporary"
              data={drawerData}
            />
      </Container>
      {confirmSelectionOpen && <AlertAction
        isOpen={confirmSelectionOpen}
        setIsOpen={setConfirmSelectionOpen}
        actions={actions}
        type='doubt'
        title={confirmationMessage}
        loading={false}
      />}
  </ThemeProvider>
)

}

export default MyServicesList
