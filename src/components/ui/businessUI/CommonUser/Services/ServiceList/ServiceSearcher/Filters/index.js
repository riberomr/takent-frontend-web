import React, { useEffect, useState } from 'react'
import { Dialog, DialogTitle, DialogActions, DialogContent, FormControlLabel, Checkbox, Slider} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { styles } from './styles'
import Button from '../../../../../../generalUI/Buttons/PrimaryButton'
import Select from '../../../../../../generalUI/Selects/CustomSelect/CustomSelect'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles(styles)

const SERVICE_COST = {
  "Hasta $1.000": "0,1000",
  "Entre $1.000 y $3.000": "1000,3000",
  "Entre $3.000 y $5.000": "3000,5000",
  "Más de $5.000": "5000,"
}

const maxDistanceMarks = [
  {
    value: 1,
    label: "1km"
  },
  {
    value: 2,
    label: "2km"
  },
  {
    value: 3,
    label: "5km"
  },
  {
    value: 4,
    label: "10km",
  },
  {
    value: 5,
    label: "25km"
  },
  {
    value: 6,
    label: "50km"
  },
  {
    value: 7,
    label: "100km"
  },
  {
    value: 8,
    label: "200km"
  }
]

const maxDistanceMap = {
  1: 1000,
  2: 2000,
  3: 5000,
  4: 10000,
  5: 25000,
  6: 50000,
  7: 100000,
  8: 200000
}

const Filters = (props) => {

    const variant = 'standard'
    const { open, handleClose, title , setSelectedProvince, sectors, provinces, cities, selectedProvince, handleChangeFilters } = props
    const [selectedCities, setSelectedCities] = useState([])
    const [selectedSectors, setSelectedSectors] = useState([])
    const [selectedCost, setSelectedCost] = useState('')
    const [ filterByCurrLocation, setFilterByCurrLocation ] = useState(false)
    const [ maxRatio, setMaxRatio ] = useState(2)
    const { enqueueSnackbar } = useSnackbar()
    const classes = useStyles()
    const [filteredValues, setFilteredValues] = useState({})

    useEffect(() => {
      if(!open) {
        setSelectedSectors(filteredValues.selectedSectors || [])
        setSelectedCost(filteredValues.selectedCost || "")
        setSelectedProvince(filteredValues.selectedProvince || "")
        setSelectedCities(filteredValues.selectedCities || [])
        setFilterByCurrLocation(filteredValues.filterByCurrLocation || false)
        setMaxRatio(filteredValues.maxRatio || 2)
      }
      // eslint-disable-next-line
    }, [open])

    const handleSumit = async () => {
        let filters = {}
        if(selectedProvince !== '') filters = {...filters, 'address.province': selectedProvince}
        if(selectedSectors.length>0) filters = {...filters, 'sector': selectedSectors.join(",")}
        if(selectedCities.length>0) filters = {...filters, 'address.city': selectedCities.join(",")}
        if(selectedCost !== '') filters = {...filters, 'cost': SERVICE_COST[selectedCost]}
        if(filterByCurrLocation) {
          const result = await navigator.permissions.query({name: "geolocation"})
          if(result.state === "granted") {
            const position = await new Promise((resolve, reject) => {
              navigator.geolocation.getCurrentPosition(resolve, reject)
            })
            const ratio = maxDistanceMap[maxRatio]
            filters = {...filters, location: `${position.coords.latitude},${position.coords.longitude},${ratio}`, filter_by_preference: false}
          }
          else if(result.state === "prompt") {
            navigator.geolocation.getCurrentPosition(() => {return})
            return
          }
          else {
            enqueueSnackbar('Error al obtener ubicación actual, chequeá los permisos del explorador', { variant: 'error' })
            return
          }
        }
        handleChangeFilters(filters)
        setFilteredValues({
          selectedSectors,
          selectedCost,
          selectedProvince,
          selectedCities,
          filterByCurrLocation,
          maxRatio
        })
        handleClose()
    }

    const handleSelectProvince = (id) => {
      if(id === '') {
        setSelectedCities([])
        setSelectedProvince(id)
      }
      setSelectedProvince(id)
    }
    const handleCleanFilters = () => {
      setSelectedCities([])
      setSelectedSectors([])
      setSelectedProvince(null)
      setSelectedCost('')
    }

    const handleCheckCurrLocation = event => {
      setFilterByCurrLocation(event.target.checked)
      if(event.target.checked) {
        setSelectedProvince("")
      }
      else {
        setMaxRatio(2)
      }
    }

    return (<Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        classes={{paper: classes.dialogPaper }}
        BackdropProps={{invisible: true}}
      >
        <DialogTitle>{title}</DialogTitle>
      
        <DialogContent>
            <Select
                id="sector"
                name="sector"
                label="Rubro"
                variant={variant}
                fullWidth
                multiple
                options={sectors}
                value={selectedSectors}
                onChange={(event) => setSelectedSectors(event.target.value)}
            />
            <Select
                id="cost"
                name="cost"
                label="Costo"
                variant={variant}
                fullWidth
                options={Object.keys(SERVICE_COST)}
                value={selectedCost}
                onChange={(event) => setSelectedCost(event.target.value)}
              />
            <Select
                id="province"
                name="province"
                label="Provincia"
                disabled={filterByCurrLocation}
                variant={variant}
                fullWidth
                options={provinces}
                value={selectedProvince}
                onChange={(event) => handleSelectProvince(event.target.value)}
            />
            <Select
                id="city"
                name="city"
                label="Localidad"
                disabled={!selectedProvince || selectedProvince === ''}
                variant={variant}
                fullWidth
                multiple
                options={cities}
                value={selectedCities}
                onChange={(event) => setSelectedCities(event.target.value)}
            />
            <FormControlLabel
              control={<Checkbox color="primary" checked={filterByCurrLocation} onChange={handleCheckCurrLocation}/>}
              label="Filtrar por ubicación actual"
            />
            <Slider
              value={maxRatio}
              min={1}
              max={8}
              step={null}
              marks={maxDistanceMarks}
              onChange={(event, value) => setMaxRatio(value)}
              disabled={!filterByCurrLocation}
            />
        </DialogContent>
        <DialogActions className={classes.containerButtons}>
          <Button 
              className={classes.buttonStyles} 
              onClick={handleCleanFilters}
              variant='contained' 
              color='primary'
            >
              Borrar
          </Button>
          <Button 
            className={classes.buttonStyles} 
            onClick={handleSumit}
            variant='contained' 
            color='primary'
          >
            Filtrar
          </Button>
        </DialogActions>   
      </Dialog>)
}

export default Filters