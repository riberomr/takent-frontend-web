import React, { useState, useEffect } from 'react'
import { Carousel } from "react-responsive-carousel"
import { makeStyles } from '@material-ui/core/styles'
import { IconButton, CircularProgress } from '@material-ui/core/'
import {
  ChevronLeft as ChevronLeftIcon,
  ChevronRight as ChevronRightIcon,
} from '@material-ui/icons/'
import ServiceCard from './ServiceCard'
import ServiceSearcher from './ServiceSearcher'

import "react-responsive-carousel/lib/styles/carousel.min.css"

const useStyles = makeStyles((theme) => ({
  root: {
    height: 180,
  },
  wrapper: {
    width: 100 + theme.spacing(2),
  },
  paper: {
    zIndex: 1,
    position: 'relative',
    margin: theme.spacing(1),
  },
  svg: {
    width: 100,
    height: 100,
  },
  polygon: {
    fill: theme.palette.common.white,
    stroke: theme.palette.divider,
    strokeWidth: 1,
  },
  container:{
      '& .control-dots, .carousel-status, .thumbs-wrapper, .axis-vertical, .control-arrow': {
          display: 'none'
      },
      '& .slide': {
        background: 'unset'
      },
      width: '160px',
      display: 'grid',
      justifyContent: 'center'
  },
  main:{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '50px'
  },
  carouselContainer:{
      display: 'grid',
      justifyContent:' center'
  },
  titleText: {
    fontFamily: 'Roboto',
    fontSize: '24px',
    fontWeight: 'bold'
    },
  paginationText:{
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '16px',
    lineHeight: '19px',
    color: '#6F6F6F',
    marginTop: '8px'
  },
  fakeScrollButton:{
    minWidth: '0px'
  },
  containerFakeScroll:{
    display: 'flex'
  },
  listContainer:{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '75%',
    height: '450px',
    overflow: 'auto',
    '&::-webkit-scrollbar': {
      width: '0.4em'
      },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'orange',
      outline: '1px solid slategrey',
      borderRadius: 'inherit'
    },
  },
  offerText:{
    fontFamily: 'Roboto',
    fontSize: '20px',
    fontWeight: 'bold',
    width: '83%',
    paddingBottom: '20px'
  },
  noData:{
    fontFamily: 'Roboto',
    fontSize: '16px',
    color: 'grey'
  },
  paginationContainer:{
    marginTop: '30px'
  }
}))

const ServiceList = (props) => {

    const classes = useStyles();
    const { services, totalPages, totalElements, searcherInputValue, setSearcherInputValue, handleChangeFilters, loading } = props
    const [page, setPage] = useState(1)
    const [firstElement, setFirstElement] = useState(1)
    const [lastElement, setLastElement] = useState(totalElements>5? 5 : totalElements%5 === 0? 5 : totalElements%5)

    useEffect(() => {
      if(!services || services.length === 0) {
        setFirstElement(0)
        setPage(0)
      }
      else {
        setFirstElement(1)
        setPage(1)
      }
    },[services])

    useEffect(()=>{
      if(totalElements === 0) setLastElement(0)
      else setLastElement(totalElements>5? 5 : totalElements%5 === 0? 5 : totalElements%5)
    },[totalElements])

    const handlePages = (newPage) => {
      const lessThan5 = totalElements%5
      setPage(newPage)
      setFirstElement(newPage===1? 1 : ((newPage-1)*5)+1)
      if(newPage === totalPages) setLastElement(lessThan5 !==0? ((newPage-1)*5)+lessThan5 : ((newPage-1)*5)+5)
      else setLastElement(((newPage-1)*5)+5)
    }

    const pathImgArray = [
      '/images/Workers/sj-1.png',
      '/images/Workers/sj-2.png',
      '/images/Workers/sj-3.png',
      '/images/Workers/sj-4.png',
      '/images/Workers/sj-5.png',
      '/images/Workers/sj-6.png',
      '/images/Workers/sj-7.png',
      '/images/Workers/sj-8.png',
      '/images/Workers/sj-9.png',
      '/images/Workers/sj-10.png',
      '/images/Workers/sj-11.png',
      '/images/Workers/sj-12.png',
      '/images/Workers/sj-13.png',
      '/images/Workers/sj-14.png',
      '/images/Workers/sj-15.png',
      '/images/Workers/sj-16.png',
      '/images/Workers/sj-17.png',
      '/images/Workers/sj-18.png',
      '/images/Workers/sj-19.png',
      '/images/Workers/sj-20.png',
      '/images/Workers/sj-21.png',
      '/images/Workers/sj-22.png',
      '/images/Workers/sj-23.png',
      '/images/Workers/sj-24.png',
      '/images/Workers/sj-25.png',
      '/images/Workers/sj-26.png',
    ]

    return (
      <div className={classes.main}>
        <div className={classes.carouselContainer}>
          <Carousel 
            autoPlay 
            infiniteLoop
            className={classes.container}
          >
              {pathImgArray.map((item, index)=>{
                return(<div key={index}>
                  <img alt="" src={item}/>
                </div>)
              })}
          </Carousel>
        </div>
      <p className={classes.titleText}>¡Encontrá los servicios que necesitás!</p>
      <ServiceSearcher searcherInputValue={searcherInputValue} setSearcherInputValue={setSearcherInputValue} handleChangeFilters={handleChangeFilters}/>
      <p className={classes.offerText}>Servicios encontrados</p>
      <div className={classes.listContainer}>
        {services && services.length > 0 && services.map((item, index) => {
          if(firstElement <= index+1 && index+1 <= lastElement) {
            return(
              <ServiceCard key={index} data={item}/>
            )
          }
          else return null
        })}
        {loading && <CircularProgress/>}
        {(!services || services.length === 0) && !loading && <div className={classes.noData}>No se encontraron servicios</div>}
      </div>
          <div className={classes.paginationContainer}> 
            <span className={classes.paginationText} >{(firstElement)+' - '+(lastElement)+' de '+totalElements}</span>
            <IconButton 
              className={classes.buttonPage} 
              disabled={(page === 1 || page === 0)}
              onClick={() => handlePages(page-1)}
            >
              <ChevronLeftIcon style={{ color: '#6F6F6F' }}/>
            </IconButton>
            <span className={classes.paginationText} >{page}</span>
            <IconButton 
              className={classes.buttonPage}  
              disabled={(page===totalPages)} 
              onClick={() => handlePages(page+1)}
            >
              <ChevronRightIcon style={{ color: '#6F6F6F' }}/>
            </IconButton>
          </div>
      </div>
    )
}

export default ServiceList