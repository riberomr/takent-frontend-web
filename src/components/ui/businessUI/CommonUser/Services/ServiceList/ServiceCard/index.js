import React, { useState, useEffect } from 'react'
import { publishedDaysService } from '../../../../../../../helpers/customHelpers'
import { styles } from './styles'
import { apiTakent } from '../../../../../../../services/API-TAKENT'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import Grid from '@material-ui/core/Grid'
import clsx from 'clsx'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import { ShowOffer }  from '../../../../../../wrappers/CommonUser/Services/ServiceList/ShowOffer/index'
import { Typography } from '@material-ui/core'
import { useSnackbar } from 'notistack'
import getWhatsAppLink from '../../../../../../../helpers/customHelpers/whatsapp'
import Rating from '@material-ui/lab/Rating'

const useStyles = makeStyles(styles)

const ServiceCard = (props) => {
    const { data, className } = props
    const [servicePhoto, setServicePhoto] = useState(null)
    const [open, setOpen] = useState(false)
    const classes = useStyles()
    const { enqueueSnackbar } = useSnackbar()

    useEffect(()=>{
        if(data.publication_user._id) apiTakent.getProfilePhotoCommonUser(data.publication_user._id)
          .then((res)=> {
            setServicePhoto(res.data.profile_photo)
          })
    },[data.publication_user._id])
    
    const handleClickOpen = () => {
        setOpen(true)   
    }

    const requestService = _id =>{
        apiTakent.requestService(_id)
        .then(() => {
          enqueueSnackbar('Servicio solicitado con éxito', { variant: 'success' })
          window.open(getWhatsAppLink(data.publication_user.phone), "_blank")
        })
        .catch(()=>{
          enqueueSnackbar('Error al solicitar el servicio', { variant: 'error' })
        })
      }

    return (<>
            <ShowOffer 
                data={data} 
                open={open} 
                setOpen={setOpen} 
                avatar={servicePhoto}
                requestService={requestService}
            />
            <div className={clsx(classes.content, className)} onClick={() => {handleClickOpen()}}>        
                <Grid container>
                    <Grid item xs={3} className={classes.avatarContainer}>
                        <Avatar
                            variant='rounded'
                            className={classes.avatar}
                            alt={data.publication_user? data.publication_user.surname +' '+ data.publication_user.name : ''} 
                            src={servicePhoto}
                        />
                    </Grid>
                    <Grid item xs={6} className={classes.mainOffer}>
                        <p title={data.title} className={classes.text}>{data.title}</p>
                        <p className={classes.subText}>{data.sector}</p>
                        <div style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
                          <p className={classes.date}>{publishedDaysService(data.publication_date)}</p>
                          {data.score !== undefined && <div style={{display: "flex", alignItems: "center"}}>
                            <Rating value={data.score} size="medium" readOnly/>
                            <Typography variant="body2">({data.score_count})</Typography>
                          </div>}
                        </div>
                    </Grid>
                    <Grid item xs={3} className={classes.locationContainer}>
                      <div style={{display:'flex', alignItems: 'center', width: '-webkit-fill-available'}}>
                        <LocationOnIcon className={classes.locationIcon} />
                        <div className={classes.subLocationConatiner}>
                            <p title={data.address.city.name} className={classes.location}>{data.address.city.name},</p>
                            <p title={data.address.province.name} className={classes.location}>{data.address.province.name}</p>
                        </div>
                      </div>
                      {/* <div style={{margin: '0px 14px 14px 0px', alignSelf: 'flex-end'}}>
                        {data.requested && <Chip size="small" color="primary" label="Solicitado"/>}
                      </div> */}
                    </Grid>
                </Grid>
            </div>
   </> )
}

export default ServiceCard