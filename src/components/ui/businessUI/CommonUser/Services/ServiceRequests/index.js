import { Container, ThemeProvider, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import ViewListIcon from '@material-ui/icons/ViewList'
import { makeStyles } from '@material-ui/styles'
import { styles } from './styles'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import moment from 'moment'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import DataDrawer from '../../../../generalUI/DataDrawer'
import ActionButtonsServiceRequest from '../../../../generalUI/ActionButtonsServiceRequest'
import AlertAction from '../../../../generalUI/AlertAction'
import RatingDialog from '../../../../generalUI/RatingDialog'

const useStyles = makeStyles(styles)

const chipColors = {
  GREEN: '#E8F5E9',
  B_GREEN: '#4CAF50',
  RED: '#FEEBEE',
  B_RED: '#F2453D',
  YELLOW: '#FFF8E1',
  B_YELLOW: '#FFC107',
  BLUE: '#E3F2FD',
  B_BLUE: '#2C98F0'
}

const CHIP_STATE_SERVICE_REQUEST = { type: 'chip', values: 
  {
    'Pendiente': { ligth : chipColors.YELLOW, dark : chipColors.B_YELLOW},
    'Completado': { ligth : chipColors.GREEN, dark : chipColors.B_GREEN},
    'Cancelado' : { ligth : chipColors.RED, dark : chipColors.B_RED},
  }
}

const getLabelDate = (date) => {
  if(!date) return
  let labelDate = date.slice(8,10)+'/'+date.slice(5,7)+'/'+date.slice(2,4)
  return labelDate
}

const header = [
  createHeader('request_date', 'center', 'center', false, 'Fecha de solicitud', false),
  createHeader('title', 'center', 'center', false, 'Servicio', false, null, "350px"),
  createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_SERVICE_REQUEST)
]

const getObjectToCompare = (obj, orderBy) => {
  let res = obj[orderBy]
  switch(orderBy) {
    case "request_date":
      res = moment(res, "DD/MM/YYYY")
      break
    default:
  }
  return res
}

const descendingComparator = (a, b, orderBy) => {
  const elementA = getObjectToCompare(a, orderBy)
  const elementB = getObjectToCompare(b, orderBy)

  if(elementA < elementB) return -1
  if(elementA > elementB) return 1
  return 0
}

const getComparator = (order, orderBy) => {
  return order === "desc" ? (a, b) => descendingComparator(a, b, orderBy) : (a, b) => -descendingComparator(a, b, orderBy)
}

const CommonUserServiceRequests = props => {

  const classes = useStyles()

  const { serviceRequestsList, handleChangeRequestState, handleRateRequest, loading } = props

  const [ isDrawerOpen, setIsDrawerOpen ] = useState(false)
  const [ drawerData, setDrawerData ] = useState({})
  const [ order, setOrder ] = useState("asc")
  const [ orderBy, setOrderBy ] = useState(null)
  const [ sortedServiceRequestList, setSortedServiceRequestList ] = useState(null)
  const [ handleDiscard, setHandleDiscard ] = useState(null)
  const [ confirmSelectionOpen, setConfirmSelectionOpen ] = useState(false)
  const [ rateServiceDialogOpen, setRateServiceDialogOpen ] = useState(false)
  const [ rateFunction, setRateFunction ] = useState(null)

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmSelection = (serviceId, requestId, state) => {
    setConfirmSelectionOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        handleChangeRequestState(serviceId, requestId, state)
        setConfirmSelectionOpen(false)
      }
      if(type === 'cancel') {
        setConfirmSelectionOpen(false)
      }
    })
  }

  useEffect(() => {
    if(!orderBy) {
      setSortedServiceRequestList(serviceRequestsList)
      return
    }
    setSortedServiceRequestList([...serviceRequestsList].sort(getComparator(order, orderBy)))
  }, [serviceRequestsList, orderBy, order])

  const handleViewServiceRequest = rowData => {
    if(!rowData){
      return
    }
    let lastArrayState = rowData.states_history.length - 1

    let stateAndLastChanges = lastArrayState !== 0? `${rowData.state}, última modificación ${getLabelDate(rowData.states_history[lastArrayState].date)}` : rowData.state
    
    let displayValues = {
      title: rowData.title,
      content: {
          "Fecha de solicitud": rowData.request_date,
          "Oferente": `${rowData.service.publication_user.name} ${rowData.service.publication_user.surname}`,
          "Descripción": rowData.service.description,
          "Costo": `$${rowData.service.cost}`,
          "Rubro": rowData.service.sector,
          "Estado de solicitud": stateAndLastChanges,
          "Valoración": rowData.score
      },
      location: rowData.service.address
    }
    setDrawerData(displayValues)
    setIsDrawerOpen(true)
  }

  const handleRateService = (serviceId, requestId) => {
    const rateFunction = rate => {
      handleRateRequest(serviceId, requestId, rate)
    }
    setRateFunction(() => rateFunction)
    setRateServiceDialogOpen(true)
  }

  const handleSortClick = id => {
    const isAsc = orderBy === id && order === "asc"
    setOrder(isAsc? "desc" : "asc")
    setOrderBy(id)
  }

  const actionButtonsServiceRequestProps = {
    firstTooltip: 'Ver solicitud de servicio',
    secondTooltip: 'Cancelar solicitud de servicio',
    thirdTooltip: 'Valorar solicitud de servicio',
    firstFunction: rowData => handleViewServiceRequest(rowData),
    secondFunction: rowData => confirmSelection(rowData.service._id, rowData._id, 'canceled'),
    thirdFunction: rowData => handleRateService(rowData.service._id, rowData._id)
  }

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md" style={{marginBottom: theme.spacing(4)}}>
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
              <ViewListIcon color="primary" style={{paddingRight: '5px'}}/>
              <Typography variant="h5">Servicios solicitados</Typography>
          </div>
        </div>
        <ResponsiveTable
          noDataMessage={'No solicitaste ningún servicio. ¡No esperes más!'}
          header={header}
          rows={sortedServiceRequestList}
          handleSortClick={handleSortClick}
          orderBy={orderBy}
          order={order}
          actions={actionButtonsServiceRequestProps ? <ActionButtonsServiceRequest
            actionButtonsServiceProps={actionButtonsServiceRequestProps}
            /> : null}
          loading={loading}
        />
      </Container>
      <DataDrawer
        anchor="right"
        isOpen={isDrawerOpen}
        setIsOpen={setIsDrawerOpen}
        variant="temporary"
        data={drawerData}
      />
      {confirmSelectionOpen && <AlertAction
          isOpen={confirmSelectionOpen}
          setIsOpen={setConfirmSelectionOpen}
          actions={actions}
          type='doubt'
          title={"¿Estás seguro de cancelar la solicitud de este servicio?"}
          loading={false}
        />}
        <RatingDialog
          open={rateServiceDialogOpen}
          setOpen={setRateServiceDialogOpen}
          submitFunction={rateFunction}
        />
    </ThemeProvider>
  )

}

export default CommonUserServiceRequests
