import { Button, Container, IconButton, makeStyles, TextField, ThemeProvider, Tooltip, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import CheckIcon from '@material-ui/icons/Check'
import clsx from 'clsx'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import AlertAction from '../../../../generalUI/AlertAction'
import CancelIcon from '@material-ui/icons/Cancel'
import ContactPhoneIcon from '@material-ui/icons/ContactPhone'

const useStyles = makeStyles(styles)

const ActionButtons = props => {

  const { actionButtonsProps, rowData, className } = props
  const { firstTooltip, secondTooltip, thirdTooltip, firstFunction, secondFunction, thirdFunction } = actionButtonsProps

  const classes = makeStyles({
    container: {
      display: 'flex',
      justifyContent: 'space-evenly',
      [theme.breakpoints.down('md')]: {
        justifyContent: 'flex-end',
        '& :not(:first-child)' : {
          marginLeft: '20px'
        }
      }
    },
    buttons:{
      boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
      minHeight: '40px',
      minWidth: '40px',
      borderRadius: '100%',
      backgroundColor: 'white',
      color: 'orange',
      '&&:hover ' : {
        backgroundColor: 'orange',
        color: 'white'
      },
    }
  })()
  
  return (
    <div id='action-buttons' className={clsx(classes.container, className)}>
      <Tooltip title={firstTooltip}>
        <Button
          className={classes.buttons}
          onClick={() => firstFunction(rowData)}
        >
          <ContactPhoneIcon />
        </Button>
      </Tooltip>
      <Tooltip title={secondTooltip}>
        <Button
          className={classes.buttons}
           onClick={() => { secondFunction(rowData) }}
           disabled={["Completada", "Cancelada"].includes(rowData.state)}
          >
          <CheckIcon />
        </Button>
      </Tooltip>
      <Tooltip title={thirdTooltip}>
        <Button
          className={classes.buttons}
           onClick={() => { thirdFunction(rowData) }}
           disabled={["Completada", "Cancelada"].includes(rowData.state)}
          >
          <CancelIcon />
        </Button>
      </Tooltip>
    </div>
  )
}

const RequestList = props => {

  const { 
    requests, 
    setViewRequests,
    stateFilter,
    setStateFilter,
    userFilter,
    setUserFilter,
    handleChangeRequestState,
    sendWhatsapp,
    loading
  } = props

  const classes = useStyles()

  const [ confirmSelectionOpen, setConfirmSelectionOpen ] = useState(false)
  const [ handleDiscard, setHandleDiscard ] = useState(null)
  const [ confirmationMessage, setConfirmationMessage ] = useState(null)
  const [ requestsFiltered, setRequestsFiltered] = useState([])

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmSelection = (serviceId, requestId, state) => {
    let action
    switch(state) {
      case "completed":
        action = "Completar"
        break
      case "canceled":
        action = "Cancelar"
        break
      default:
        action = "No se"
    }
    setConfirmationMessage(`¿${action} solicitud?`)
    setConfirmSelectionOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        handleChangeRequestState(serviceId, requestId, state)
        setConfirmSelectionOpen(false)
      }
      if(type === 'cancel') {
        setConfirmSelectionOpen(false)
      }
    })
  }

  const chipColors = {
    GREEN: '#E8F5E9',
    B_GREEN: '#4CAF50',
    RED: '#FEEBEE',
    B_RED: '#F2453D',
    GREY: '#E8E8E8',
    B_GREY: '#808080',
    BLUE: '#E3F2FD',
    B_BLUE: '#2C98F0'
  }

  const CHIP_STATE_REQUEST_LIST = {
    type: 'chip', 
    values: { 
      'Pendiente' : { ligth : chipColors.GREY, dark : chipColors.B_GREY},
      'Completada' : { ligth : chipColors.GREEN, dark : chipColors.B_GREEN},
      'Cancelada': { ligth : chipColors.RED, dark : chipColors.B_RED}
    }
  }

  const header = [
    createHeader('request_date', 'center', 'center', false, "Fecha de solicitud"),
    createHeader('name', 'center', 'center', false, 'Nombre', false),
    createHeader('surname', 'center', 'center', false, 'Apellido', false),
    createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_REQUEST_LIST),
  ]

  const actionButtonsProps = {
    firstTooltip: 'Contactar solicitante',
    secondTooltip: 'Completar solicitud',
    thirdTooltip: 'Cancelar solicitud',
    firstFunction: rowData => sendWhatsapp(rowData.phone),
    secondFunction: (rowData) => confirmSelection(rowData.serviceId, rowData.requestId, 'completed'),
    thirdFunction: (rowData) => confirmSelection(rowData.serviceId, rowData.requestId, 'canceled'),
  }

  useEffect(()=> {
    let filtered = requests
    if(stateFilter !== "Seleccione")
      filtered = filtered.filter(request => request.state === stateFilter)
    if(userFilter !== "")
      filtered = filtered.filter(request => `${request.name} ${request.surname}`.toLowerCase().includes(userFilter.toLowerCase()))
    setRequestsFiltered(filtered)
  }, [requests, stateFilter, userFilter])
  
  return (
    <ThemeProvider theme={theme}>
      <Container classes={{root: classes.marginBottom}} maxWidth="md">
        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', marginBottom : theme.spacing(4)}}>
          <Tooltip title="Volver a servicios">
            <IconButton color="primary" component="span" onClick={() => setViewRequests(false)}>
              <ArrowBackIcon/>
            </IconButton>
          </Tooltip>
          <Typography variant="h5">Solicitudes de servicio</Typography>
        </div>
        <div className={classes.filtersContainer}>
          <div className={classes.itemFilterContainer}>
            <Select
              id="status"
              name="status"
              label="Estado"
              variant='standard'
              options={['Seleccione', 'Pendiente', 'Completada', 'Cancelada']}
              value={stateFilter}
              onChange={event => setStateFilter(event.target.value)}
            />
          </div>
          <div className={classes.itemFilterContainer}>
            <TextField
              id='user'
              type='text'
              name='user'
              label="Buscar solicitante"
              fullWidth
              variant="standard"
              InputLabelProps={{shrink: true}}
              onChange={event => setUserFilter(event.target.value)}
              value={userFilter}
            />
          </div>
        </div>
        <ResponsiveTable
          noDataMessage={!requests || requests.length === 0? 'No existen solicitudes' : 'No se encontraron solicitudes según los filtros seleccionados'}
          header={header}
          rows={requestsFiltered}
          actions={actionButtonsProps ? <ActionButtons
          actionButtonsProps={actionButtonsProps}
          /> : null}
          loading={loading}
        />
      </Container>
      {confirmSelectionOpen && <AlertAction
          isOpen={confirmSelectionOpen}
          setIsOpen={setConfirmSelectionOpen}
          actions={actions}
          type='doubt'
          title={confirmationMessage}
          loading={false}
        />}
    </ThemeProvider>
  )
}

export default RequestList
