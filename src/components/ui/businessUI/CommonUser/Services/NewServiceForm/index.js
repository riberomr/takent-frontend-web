import { Container, Fab, Grid, InputAdornment, makeStyles, ThemeProvider, Tooltip, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import AddIcon from '@material-ui/icons/Add'
import { Formik } from 'formik'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import AddressDialog from '../../../../generalUI/AddressDialog'
import { useSnackbar } from 'notistack'
import ImagesUpload from '../../../../generalUI/ImagesUpload'
import AlertAction from '../../../../generalUI/AlertAction'
import _ from 'underscore'
import CommonUserNewServiceForm from '../../../../../rules/validationSchema/commonUserNewServiceForm'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import HomeIcon from '@material-ui/icons/Home'
import LoadingButton from '../../../../generalUI/LoadingButton'

const useStyles = makeStyles(styles)

const NewServiceForm = props => {

  const {
    sectors,
    provinces,
    cities,
    setSelectedProvince,
    location,
    userAddress,
    submitService,
    loadingSubmit
  } = props

  const classes = useStyles()
  const fieldVariant = "standard"

  const [ isCancelOpen, setIsCancelOpen ] = useState(false)
  const [ isConfirmationOpen, setIsConfirmationOpen ] = useState(false)
  const [ isAddressDialogOpen, setIsAddressDialogOpen ] = useState(false)
  const [ uploadedImages, setUploadedImages ] = useState([])
  const [ addressEmpty, setAddressEmpty ] = useState(false)
  const [ address, setAddress ] = useState(() => {
    let initialAddress = {}
    if(location.state?.address?.province)
    {
      initialAddress = {
        ...location.state.address,
        province: location.state.address.province.code,
        city: location.state.address.city.code,
      }
    }
    return initialAddress
  })

  const { enqueueSnackbar } = useSnackbar()

  const initialValues = {
    title: location.state?.title ?? "",
    sector: location.state?.sector ?? "",
    cost: location.state?.cost ?? "",
    description: location.state?.description ?? ""
  }

  useEffect(() => {
    if(location.state?._id) {
      apiTakent.getServiceImages(location.state._id)
      .then(response => {
        setUploadedImages(response.data)
      })
    }
  }, [location.state])

  useEffect(() => {
    if(address.province && addressEmpty){
      setAddressEmpty(false)
    }
    setSelectedProvince(address.province)
  }, [address, addressEmpty, setSelectedProvince])

  const handleSubmit = () => {
    if(!address.province) {
      setAddressEmpty(true)
      return
    }
    setIsConfirmationOpen(true)
  }

  const submit = (values) => {
    if(!address.province) {
      setAddressEmpty(true)
      return
    }
    const submitJson = {
      ...values,
      address: address? _.omit(address, "province") : null,
      images: uploadedImages? uploadedImages : null
    }
    submitService(submitJson)
    setUploadedImages([])
    setAddress({})
  }

  const getCancelAlertActions = (resetForm) => {

    const handleSubmit = (type) => {
      if(type === "accept") {
        resetForm()
        setAddressEmpty(false)
      }
      setIsCancelOpen(false)
      window.scrollTo(0, 0)
    }

    return [
      {
        name: 'No',
        type: 'cancel',
        handleSubmit: handleSubmit
      },
      {
          name: 'Si',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]
  }

  const getConfirmationAlertActions = (resetForm, values) => {

    const handleSubmit = (type) => {
      if(type === "accept") {
        submit(values)
        resetForm()
      }
      setIsConfirmationOpen(false)
      window.scrollTo(0, 0)
    }

    return [
      {
          name: 'Cancelar',
          type: 'cancel',
          handleSubmit: handleSubmit
      },  
      {
          name: 'Aceptar',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]
  }

  const handleClickSetUserAddress = () => {
    if(userAddress.province) {
      setAddress(userAddress)
      setSelectedProvince(userAddress.province)
    }
    else {
      enqueueSnackbar('No tenés un domicilio cargado en tu perfil', { variant: 'error' })
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
        <div className={classes.title}>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
              <AddIcon color="primary" style={{paddingRight: '5px'}}/>
              <Typography variant="h5">Publicar servicio</Typography>
            </div>
        </div>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={CommonUserNewServiceForm}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
            resetForm
          }) => (
            <form onSubmit={handleSubmit}>
              <Grid container spacing={5}>
                <Grid item xs={12}>
                  <TextField
                    id='title'
                    type='text'
                    name='title'
                    label="Título"
                    variant={fieldVariant}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.title}
                    error={errors.title && touched.title}
                    helperText={touched.title && errors.title? errors.title : ""}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <Select
                    id="sector"
                    name="sector"
                    label="Rubro"
                    variant={fieldVariant}
                    fullWidth
                    options={sectors}
                    value={values.sector}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.sector && touched.sector}
                    helperText={(errors.sector && touched.sector)? errors.sector : ""}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    id='cost'
                    type='text'
                    name='cost'
                    label="Costo"
                    variant={fieldVariant}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.cost}
                    error={errors.cost && touched.cost}
                    helperText={touched.cost && errors.cost? errors.cost : ""}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <AttachMoneyIcon color="primary"/>
                        </InputAdornment>
                      )
                    }}
                    onlyNumbers
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id='description'
                    type='text'
                    name='description'
                    label="Descripción"
                    placeholder={ "Acá podés escribir una descripción de tu servicio y también incluir a qué hace referencia el costo del mismo. Ej: Una sesión, consulta, diagnostico, etc."}
                    variant={fieldVariant}
                    multiline
                    rows={4}
                    fullWidth
                    InputLabelProps={{shrink: true}}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                    error={touched.description && errors.description}
                    helperText={touched.description && errors.description? errors.description : ""}
                  />
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Ubicación</Typography>
                    <div className={classes.addressContainer}>
                      {address.province? (
                        <div>
                          <Typography variant="subtitle2">
                            {provinces.find(element => element.code === address.province)?.name}
                            {address.postal_code && ` (${address.postal_code})`},
                            {" " + cities.find(element => element.code === address.city)?.name}
                          </Typography>
                          <Typography variant="body2">
                            {address.district && `${address.district}, `}
                            {`${address.street} ${address.number? address.number : '' }`}
                          </Typography>
                          {address.tower && <Typography variant="body2">
                            Torre {address.tower}, Piso {address.floor}, Departamento {address.apartment}
                          </Typography>}
                        </div>): (
                          <Typography variant="subtitle2" color="textSecondary">No se añadió ninguna ubicación</Typography>
                        )}
                      <div style={{marginTop: theme.spacing(2)}}>
                        <Tooltip title="Utilizar domicilio de mi perfil">
                          <Fab size="small" color="primary" style={{marginRight: theme.spacing(2)}} onClick={() => handleClickSetUserAddress()}>
                            <HomeIcon />
                          </Fab>
                        </Tooltip>
                        <Tooltip title="Añadir nueva ubicación">
                          <Fab size="small" color="primary" style={{marginRight: theme.spacing(2)}} onClick={() => setIsAddressDialogOpen(true)}>
                            <EditIcon />
                          </Fab>
                        </Tooltip>
                        <Tooltip title="Eliminar ubicación">
                          <Fab size="small" color="primary" onClick={() => setAddress({})}>
                            <DeleteIcon />
                          </Fab>
                        </Tooltip>
                      </div>
                      {addressEmpty && <Typography variant="body2" className={classes.error}>
                        Debés seleccionar una ubicación
                      </Typography>}
                    </div>
                    <AddressDialog
                      isOpen={isAddressDialogOpen}
                      setIsOpen={setIsAddressDialogOpen}
                      address={address}
                      setAddress={setAddress}
                      provinces={provinces}
                      cities={cities}
                      setSelectedProvince={setSelectedProvince}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Imágenes</Typography>
                    <Typography variant="body2" color="textSecondary" style={{marginBottom: theme.spacing(3)}}>
                      Se pueden cargar hasta 6 imágenes (Tamaño máximo: 5 MB)
                    </Typography>
                    <ImagesUpload
                      images={uploadedImages}
                      setImages={setUploadedImages}
                      height={"300px"}
                      carouselLength={2}
                      imagesLimit={6}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="body2"><strong>Importante</strong>: Los datos del servicio no se podrán modificar una vez publicado</Typography>
                    <div className={classes.buttonContainer}>
                      <LoadingButton
                        onClick={() => setIsCancelOpen(true)}
                        size="large"
                      >
                        Cancelar
                      </LoadingButton>
                      <LoadingButton
                        type="submit"
                        loading={loadingSubmit}
                        size="large"
                      >
                        Publicar
                      </LoadingButton>
                    </div>
                  </Grid>
              </Grid>
              {isCancelOpen && <AlertAction
                isOpen={isCancelOpen}
                setIsOpen={setIsCancelOpen}
                actions={getCancelAlertActions(resetForm)}
                type='doubt'
                title={"¿Estás seguro de cancelar?"}
                loading={false}
              />}
              {isConfirmationOpen && <AlertAction
                isOpen={isConfirmationOpen}
                setIsOpen={setIsConfirmationOpen}
                actions={getConfirmationAlertActions(resetForm, values)}
                type='doubt'
                title={"¿Publicar servicio?"}
                loading={false}
              />}
            </form>
          )}
        </Formik>
      </Container>
    </ThemeProvider>
  )
}

export default NewServiceForm
