import { theme } from '../../../../../../../constants/generalConstants/theme'

export const styles = {

    content:{
        display: 'flex',
        border: 'solid 2px '+theme.palette.primary.main,
        borderRadius: '15px',
        width: '94%',
        marginBottom: '20px',
        boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
        '&:hover': {
            backgroundColor: 'rgba(242, 146, 34, 0.1)',
            cursor: 'pointer'
        },
        height: '120px'
    },
    mainContent:{
        margin: '8px 8px 8px 20px',
        display: 'grid',
        width: '80%'
    },
    actionsContent:{
        margin: '8px',
        display: 'grid',
        width: '20%',
        justifyContent: 'right'
    },
    text:{
        fontFamily: 'Roboto',
        fontSize: '18px',
        fontWeight: 'bold',
        margin: '14px 0px 14px 0px',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'  
    },
    title: {
        fontFamily: 'Roboto',
        fontSize: '22px',
        margin: '10px 2px 2px 2px',
        fontWeight: 'bold'
    },
    subtitle: {
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '2px 2px 15px 2px',
        color: 'grey'
    },
    mainOffer:{

    },
    avatarContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar:{
        width: '100px',
        height: '100px'
    },
    subText: {
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '0px',
        color: 'grey'
    },
    date:{
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '14px 0px 14px 0px',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        color: 'orange'  
    },
    location:{
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '14px 0px 0px 0px',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        paddingRight: '25px'
    },
    locationContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    subLocationConatiner: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: '15px',
        width: 'inherit'
    },
    locationIcon: {
        color: 'orange'
    }
}