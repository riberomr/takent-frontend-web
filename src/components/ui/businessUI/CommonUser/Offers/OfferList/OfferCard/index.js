import React, { useState, useEffect } from 'react'
import { publishedDaysOffer } from '../../../../../../../helpers/customHelpers'
import { styles } from './styles'
import { apiTakent } from '../../../../../../../services/API-TAKENT'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import Grid from '@material-ui/core/Grid'
import clsx from 'clsx'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import { ShowOffer }  from '../../../../../../wrappers/CommonUser/Offers/OfferList/ShowOffer/index'
import { ConfirmDialog } from '../../../../../generalUI/ConfirmDialog/index'
import { Chip, Tooltip } from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import { theme } from '../../../../../../../constants/generalConstants/theme'
import AlertUploadCV from '../../../../../generalUI/AlertUploadCV'

const useStyles = makeStyles(styles)

const OfferCard = (props) => {
    const { data, className, setPostulationChanged } = props
    const [enterprisePhoto, setEnterprisePhoto] = useState(null)
    const [openModalConfirm, setOpenModalConfirm]=useState(false)
    const [ showConfirmationCV, setShowConfirmationCV ] = useState(false)
    const [open, setOpen] = useState(false)
    const [success, setSuccess]= useState(null)
    const classes = useStyles()

    useEffect(()=>{
        if(data.enterprise._id) apiTakent.getEnterpriseProfilePhoto(data.enterprise._id)
          .then((res)=> {
            setEnterprisePhoto(res.data.profile_photo)
          })
    },[data.enterprise._id])
    
    const handleClickOpen = () => {
        setOpen(true)   
    };

    const postulatedCommonUser=(_id)=>{
        apiTakent.postulationCommonUser(_id)
        .then(() => {
            setSuccess(true)
            setOpenModalConfirm(true)
        })
        .catch(error => {
          if ( error.response.data?.message.includes('curriculum required') ) {
            setShowConfirmationCV(true)
            setSuccess(true)
          } else {
            setSuccess(false)
            setOpenModalConfirm(true)
          }
        })
      }

    return (<>
            <ConfirmDialog openModalConfirm={openModalConfirm}  setOpenModalConfirm={setOpenModalConfirm} success={success} acceptFunction={() => setPostulationChanged(Math.random()) }/>
            <AlertUploadCV
              open={showConfirmationCV}
              setOpen={setShowConfirmationCV}
              postulate={postulatedCommonUser}
              data={data}
            />
            <ShowOffer
                data={data} 
                open={open} 
                setOpen={setOpen} 
                setOpenModalConfirm={setOpenModalConfirm} 
                avatar={enterprisePhoto}
                postulatedCommonUser={postulatedCommonUser}
            />
            <div className={clsx(classes.content, className)} onClick={() => {handleClickOpen()}}>        
                <Grid container>
                    <Grid item xs={3} className={classes.avatarContainer}>
                        <Avatar
                            variant='rounded'
                            className={classes.avatar}
                            alt={data.enterprise.fantasy_name? data.enterprise.fantasy_name : data.enterprise.business_name} 
                            src={enterprisePhoto}
                        />
                    </Grid>
                    <Grid item xs={6} className={classes.mainOffer}>
                        <p title={data.title} className={classes.text}>{data.title}</p>
                        <div style={{display: "flex", alignItems: "center", justifyContent: "flex-start"}}>
                          {data.enterprise.verified && 
                          <Tooltip title="Empresa verificada">
                            <CheckCircleIcon
                              fontSize="small"
                              style={{marginRight: theme.spacing(1), color: "#1DA1F2"}}
                            />
                          </Tooltip>}
                          <p className={classes.subText}>{data.enterprise.fantasy_name? data.enterprise.fantasy_name : data.enterprise.business_name}</p>
                        </div>
                        <p className={classes.date}>{publishedDaysOffer(data.publication_date)}</p>
                    </Grid>
                    <Grid item xs={3} className={classes.locationContainer}>
                      <div style={{display:'flex', alignItems: 'center', width: '-webkit-fill-available'}}>
                        <LocationOnIcon className={classes.locationIcon} />
                        <div className={classes.subLocationConatiner}>
                            <p title={data.address.city.name} className={classes.location}>{data.address.city.name},</p>
                            <p title={data.address.province.name} className={classes.location}>{data.address.province.name}</p>
                        </div>
                      </div>
                      <div style={{margin: '0px 14px 14px 0px', alignSelf: 'flex-end'}}>
                        {data.postulated === true && <Chip size="small" color="primary" label="Postulado"/>}
                      </div>
                    </Grid>
                </Grid>
            </div>
   </> )
}

export default OfferCard