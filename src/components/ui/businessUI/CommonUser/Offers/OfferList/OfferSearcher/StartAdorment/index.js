import React from 'react'
import { InputAdornment, IconButton } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'

const StartAdorment = ({onClick}) => {
  return (
    <InputAdornment onClick={onClick}>
      <IconButton>
        <SearchIcon />
      </IconButton>
    </InputAdornment>
  )
}

export default StartAdorment