import { theme } from '../../../../../../../constants/generalConstants/theme'

export const styles = {
    input: {
      width: '65%',
      height: '44px',
      borderRadius: '3px',
      background: '#FFF',
      boxSizing: 'border-box'
    },
    searcherContainer:{
      display: 'flex',
      height: '80px',
      width: '75%',
      justifyContent: 'space-around',
      alignItems: 'center',
      border: 'solid 2px '+theme.palette.primary.main,
      borderRadius: '8px',
      padding: '.35em',
      boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
      marginRight: '15px'
    },
    button:{
      height: '44px',
      borderRadius: '3px'
    },
    filterButton:{
      height: '44px',
      width: '65px',
      borderRadius: '3px'
    },
    mainContainer:{
      width: '100%',
      marginBottom: '50px',
      display:'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    advancedSearchContainer:{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      cursor: 'pointer'
    },
    advancedSearchSpan: {
      fontSize: '18px',
      // textShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
      color: theme.palette.primary.main
    }
  }