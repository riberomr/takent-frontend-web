import React, { useState, useEffect } from 'react'
import { Input, Tooltip } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { styles } from './styles'
import StartAdornment from './StartAdorment'
import FilterListIcon from '@material-ui/icons/FilterList'
import Button from '../../../../../generalUI/Buttons/PrimaryButton'
import Filters from './Filters'
import { apiTakent } from '../../../../../../../services/API-TAKENT'

const ENTER = 13

const useStyles = makeStyles(styles)

const OfferSearcher = (props) => {

    const { searcherInputValue, setSearcherInputValue, handleChangeFilters } = props
    const [ internalInputValue, setInternalInpuValue ] = useState(searcherInputValue? searcherInputValue : '')
    const [ openFilters, setOpenFilters ] = useState(false)
    const [ sectors, setSectors ] = useState([])
    const [ languages, setLanguages ] = useState([])
    const [ provinces, setProvinces ] = useState([])
    const [ selectedProvince, setSelectedProvince ] = useState('')
    const [ remote, setRemote ] = useState('')
    const [ cities, setCities ] = useState([])

    const classes = useStyles()

    const handleChange = event => {
        setInternalInpuValue(event.target.value)
      }
    
    const handlePressSearch = () => {
        if(internalInputValue === '') setSearcherInputValue(null)
        setSearcherInputValue(internalInputValue)
    }

    const handleKeyDown = (event) => {
        if (event.keyCode === ENTER) {
            document.getElementById('searcher_button').click()
        }
    }

    const handleFilter = () => {
        setOpenFilters(true)
    }

    const handleClose = () => {
        setOpenFilters(false)
    }

    useEffect(()=> {
        apiTakent.getSectors()
        .then((response) => {
          setSectors(response.data.map(item => {
            return item._id
          }))
        })
      }, [])
    
      useEffect(() => {
        apiTakent.getLanguages()
        .then((response) => {
          setLanguages(response.data.map(item => {
            return item.name
          }))
        })
      }, [])
    
      useEffect(() => {
        apiTakent.getProvinces()
          .then(response => setProvinces(response.data))
      }, [])
    
      useEffect(() => {
        if(selectedProvince) {
          apiTakent.getCities(selectedProvince)
            .then(response => setCities(response.data))
        }
        else setCities([])
      }, [selectedProvince])
    
    return (
        <div className={classes.mainContainer}>
            <div className={classes.searcherContainer}>
                <Input
                    autoComplete='off'
                    autoCapitalize='off'
                    autoCorrect='off'
                    autoFocus
                    value={internalInputValue}
                    className={classes.input}
                    placeholder="Ingresá el puesto o empresa a buscar"
                    startAdornment={<StartAdornment onClick={() => handlePressSearch()}/>}
                    onKeyDown={handleKeyDown}
                    onChange={handleChange}
                />
                <Button
                    id="searcher_button"
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={() => handlePressSearch()}
                >
                    Buscar
                </Button>
                <Tooltip title='Búsqueda avanzada'>
                  <Button
                    className={classes.filterButton}
                    variant="contained"
                    color="primary"
                    onClick={handleFilter}
                    >
                    <FilterListIcon/>
                  </Button>
                </Tooltip>
            </div>
            <Filters 
                open={openFilters} 
                handleClose={handleClose} 
                title={'Búsqueda avanzada'}
                selectedProvince={selectedProvince}
                setSelectedProvince={setSelectedProvince}
                sectors={sectors}
                languages={languages}
                provinces={provinces}
                cities={cities}
                handleChangeFilters={handleChangeFilters}
                remote={remote}
                setRemote={setRemote}
            />
        </div>
    )
}

export default OfferSearcher