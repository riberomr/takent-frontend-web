import React, { useEffect, useState } from 'react'
import { Dialog, DialogTitle, DialogActions, DialogContent, FormControlLabel, Checkbox, Slider} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { styles } from './styles'
import Button from '../../../../../../generalUI/Buttons/PrimaryButton'
import Select from '../../../../../../generalUI/Selects/CustomSelect/CustomSelect'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles(styles)

const maxDistanceMarks = [
  {
    value: 1,
    label: "1km"
  },
  {
    value: 2,
    label: "2km"
  },
  {
    value: 3,
    label: "5km"
  },
  {
    value: 4,
    label: "10km",
  },
  {
    value: 5,
    label: "25km"
  },
  {
    value: 6,
    label: "50km"
  },
  {
    value: 7,
    label: "100km"
  },
  {
    value: 8,
    label: "200km"
  }
]

const maxDistanceMap = {
  1: 1000,
  2: 2000,
  3: 5000,
  4: 10000,
  5: 25000,
  6: 50000,
  7: 100000,
  8: 200000
}

const Filters = (props) => {
    
        
    const experiences = [ "Ninguna experiencia", "Experiencia escasa", "Experiencia intermedia", "Mucha experiencia" ]
    const contractTypes = [ "Jornada parcial", "Jornada completa", "Por temporada", "Por obra", "Pasantía", "Por hora" ]

    const variant = 'standard'
    const { open, handleClose, title , setSelectedProvince, sectors, languages, provinces, cities, selectedProvince, handleChangeFilters, remote, setRemote } = props
    const [ selectedCities, setSelectedCities ] = useState([])
    const [ selectedSectors, setSelectedSectors] = useState([])
    const [ selectedExperiences, setSelectedExperiences ] = useState([])
    const [ selectedContractTypes, setSelectedContractTypes ] = useState([])
    const [ selectedLanguages, setSelectedLanguages ] = useState([])
    const [ filterByCurrLocation, setFilterByCurrLocation ] = useState(false)
    const [ maxRatio, setMaxRatio ] = useState(2)
    const { enqueueSnackbar } = useSnackbar()
    const classes = useStyles()
    const [filteredValues, setFilteredValues] = useState({})

    useEffect(() => {
      if(!open) {
        setSelectedSectors(filteredValues.selectedSectors || [])
        setSelectedContractTypes(filteredValues.selectedContractTypes || [])
        setRemote(filteredValues.remote || "")
        setSelectedExperiences(filteredValues.selectedExperiences || [])
        setSelectedLanguages(filteredValues.selectedLanguages || [])
        setSelectedProvince(filteredValues.selectedProvince || "")
        setSelectedCities(filteredValues.selectedCities || [])
        setFilterByCurrLocation(filteredValues.filterByCurrLocation || false)
        setMaxRatio(filteredValues.maxRatio || 2)
      }
      // eslint-disable-next-line
    }, [open])

    const handleSumit = async () => {
        let filters = {}

        if(selectedLanguages.length>0) filters = {...filters, 'language.language': selectedLanguages.join(",") }
        if(selectedProvince !== '') filters = {...filters, 'address.province': selectedProvince}
        if(selectedSectors.length>0) filters = {...filters, 'sector': selectedSectors.join(",")}
        if(selectedExperiences.length>0) filters = {...filters, 'experience': selectedExperiences.join(",")}
        if(selectedContractTypes.length>0) filters = {...filters, 'contract_type': selectedContractTypes.join(",")}
        if(selectedCities.length>0) filters = {...filters, 'address.city': selectedCities.join(",")}
        if(remote !== '') filters = {...filters, remote: remote === 'Si'? true : false}
        
        if(filterByCurrLocation) {
          const result = await navigator.permissions.query({name: "geolocation"})
          if(result.state === "granted") {
            const position = await new Promise((resolve, reject) => {
              navigator.geolocation.getCurrentPosition(resolve, reject)
            })
            const ratio = maxDistanceMap[maxRatio]
            filters = {...filters, location: `${position.coords.latitude},${position.coords.longitude},${ratio}`, filter_by_preference: false}
          }
          else if(result.state === "prompt") {
            navigator.geolocation.getCurrentPosition(() => {return})
            return
          }
          else {
            enqueueSnackbar('Error al obtener ubicación actual, chequeá los permisos del explorador', { variant: 'error' })
            return
          }
        }
   
        handleChangeFilters(filters)
        setFilteredValues({
          selectedSectors,
          selectedContractTypes,
          remote,
          selectedExperiences,
          selectedLanguages,
          selectedProvince,
          selectedCities,
          filterByCurrLocation,
          maxRatio
        })
        handleClose()
    }

    const handleSelectProvince = (id) => {
      if(id === '') {
        setSelectedCities([])
        setSelectedProvince(id)
      }
      setSelectedProvince(id)
    }
    const handleCleanFilters = () => {
      setSelectedCities([])
      setSelectedSectors([])
      setSelectedExperiences([])
      setSelectedContractTypes([])
      setSelectedLanguages([])
      setSelectedProvince(null)
      setRemote('')
      setFilterByCurrLocation(false)
      setMaxRatio(2)
    }

    const handleCheckCurrLocation = event => {
      setFilterByCurrLocation(event.target.checked)
      if(event.target.checked) {
        setSelectedProvince("")
      }
      else {
        setMaxRatio(2)
      }
    }

    return (<Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        classes={{paper: classes.dialogPaper }}
        BackdropProps={{invisible: true}}
      >
        <DialogTitle>{title}</DialogTitle>
      
        <DialogContent>
            <Select
                id="sector"
                name="sector"
                label="Rubro"
                variant={variant}
                fullWidth
                multiple
                options={sectors}
                value={selectedSectors}
                onChange={(event) => setSelectedSectors(event.target.value)}
            />
            <Select
                id="contract_type"
                name="contract_type"
                label="Tipo de contrato"
                variant={variant}
                fullWidth
                multiple
                options={contractTypes}
                value={selectedContractTypes}
                onChange={(event) => setSelectedContractTypes(event.target.value)}
            />
            <Select
                id="remote"
                name="remote"
                label="Trabajo remoto"
                variant={variant}
                fullWidth
                options={['Si', 'No']}
                value={remote}
                onChange={(event) => setRemote(event.target.value)}
            />
            <Select
                id="experience"
                name="experience"
                label="Experiencia"
                variant={variant}
                fullWidth
                multiple
                options={experiences}
                value={selectedExperiences}
                onChange={(event) => setSelectedExperiences(event.target.value)}
            />
            <Select
                id="language"
                name="language"
                label="Idioma"
                variant={variant}
                fullWidth
                multiple
                options={languages}
                value={selectedLanguages}
                onChange={(event) => setSelectedLanguages(event.target.value)}
            />
            <Select
                id="province"
                name="province"
                label="Provincia"
                disabled={filterByCurrLocation}
                variant={variant}
                fullWidth
                options={provinces}
                value={selectedProvince}
                onChange={(event) => handleSelectProvince(event.target.value)}
            >
            </Select>
            <Select
                id="city"
                name="city"
                label="Localidad"
                disabled={!selectedProvince || selectedProvince === ''}
                variant={variant}
                fullWidth
                multiple
                options={cities}
                value={selectedCities}
                onChange={(event) => setSelectedCities(event.target.value)}
            />
            <FormControlLabel
              control={<Checkbox color="primary" checked={filterByCurrLocation} onChange={handleCheckCurrLocation}/>}
              label="Filtrar por ubicación actual"
            />
            <Slider
              value={maxRatio}
              min={1}
              max={8}
              step={null}
              marks={maxDistanceMarks}
              onChange={(event, value) => setMaxRatio(value)}
              disabled={!filterByCurrLocation}
            />
        </DialogContent>
        <DialogActions className={classes.containerButtons}>
          <Button 
              className={classes.buttonStyles} 
              onClick={handleCleanFilters}
              variant='contained' 
              color='primary'
            >
              Borrar
          </Button>
          <Button 
            className={classes.buttonStyles} 
            onClick={handleSumit}
            variant='contained' 
            color='primary'
          >
            Filtrar
          </Button>
        </DialogActions>   
      </Dialog>)
}

export default Filters