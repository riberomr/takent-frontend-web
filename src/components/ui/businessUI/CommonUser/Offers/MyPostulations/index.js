import { Container, makeStyles, ThemeProvider, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import ContactsIcon from '@material-ui/icons/Contacts'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import DataDrawer from '../../../../generalUI/DataDrawer'
import moment from 'moment'

const useStyles = makeStyles(styles)

const header = [
  createHeader('postulation_date', 'center', 'center', true, 'Fecha de postulación', true),
  createHeader('companyPosition', 'center', 'center', false, 'Empresa - Puesto', false, null, "300px"),
  createHeader('state', 'center', 'center', true, 'Estado', true),
]

const getObjectToCompare = (obj, orderBy) => {
  let res = obj[orderBy]
  switch(orderBy) {
    case "postulation_date":
      res = moment(res, "DD/MM/YYYY")
      break
    case "state":
      res = res.props.activeState
      break
    default:
  }
  return res
}

const descendingComparator = (a, b, orderBy) => {
  const elementA = getObjectToCompare(a, orderBy)
  const elementB = getObjectToCompare(b, orderBy)

  if(elementA < elementB) return -1
  if(elementA > elementB) return 1
  return 0
}

const getComparator = (order, orderBy) => {
  return order === "desc" ? (a, b) => descendingComparator(a, b, orderBy) : (a, b) => -descendingComparator(a, b, orderBy)
}

const MyPostulations = props => {

  const classes = useStyles()

  const { postulationList, loading } = props

  const [ isDrawerOpen, setIsDrawerOpen ] = useState(false)
  const [ drawerData, setDrawerData ] = useState({})
  const [ order, setOrder ] = useState("asc")
  const [ orderBy, setOrderBy ] = useState(null)
  const [ sortedPostulationList, setSortedPostulationList ] = useState(null)

  useEffect(() => {
    if(!orderBy) {
      setSortedPostulationList(postulationList)
      return
    }
    setSortedPostulationList([...postulationList].sort(getComparator(order, orderBy)))
  }, [postulationList, orderBy, order])

  const handleViewPostulation = rowData => {
    if(!rowData){
      return
    }
    let displayValues = {
      title: rowData.position,
      content: {
          "Título": rowData.title,
          "Descripción": rowData.description,
          "Empresa": rowData.enterprise,
          "Estado de postulación": rowData.state
      },
      location: rowData.address
    }
    setDrawerData(displayValues)
    setIsDrawerOpen(true)
  }

  const handleSortClick = id => {
    const isAsc = orderBy === id && order === "asc"
    setOrder(isAsc? "desc" : "asc")
    setOrderBy(id)
  }

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md" style={{marginBottom: theme.spacing(4)}}>
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
              <ContactsIcon color="primary" style={{paddingRight: '5px'}}/>
              <Typography variant="h5">Mis postulaciones</Typography>
          </div>
        </div>
        <ResponsiveTable
          noDataMessage={'No te postulaste a ninguna oferta. ¡No esperés más!'}
          header={header}
          rows={sortedPostulationList}
          onClick={rowData => handleViewPostulation(rowData)}
          handleSortClick={handleSortClick}
          orderBy={orderBy}
          order={order}
          loading={loading}
          disableMaxWidth={true}
        />
      </Container>
      <DataDrawer
        anchor="right"
        isOpen={isDrawerOpen}
        setIsOpen={setIsDrawerOpen}
        variant="temporary"
        data={drawerData}
      />
    </ThemeProvider>
  )

}

export default MyPostulations
