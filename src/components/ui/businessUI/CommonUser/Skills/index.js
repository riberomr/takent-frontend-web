import React, { useContext, useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../../Store'
import SkillForm from './SkillForm'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import Button from '@material-ui/core/Button'
import { v4 } from 'uuid'
import AlertAction from '../../../generalUI/AlertAction'
import CardItem from '../../../generalUI/CardItem'
// import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(styles)

const Skills = (props) => {
    const NEW = 'NEW'
    const EDIT = 'EDIT'
    const {userSkills, handleFormSubmit, deleteSkill, editSkill
        // , loading
        }= props
    const context = useContext(Store)
    const classes = useStyles()

    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [id, setId] = useState(null)
    const [values, setValues] = useState(null)

    const closeLeftDialog = () => {
        context.setLeftDialog(null)
    }

    //this function open the alert and receive and set the an id if from the form
    const openAlert = (id, values) => {
        if(id) setId(id)
        if(values) setValues(values)    
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {

        if(type === 'accept' && id && !values) {
            deleteSkill(id)
            setIsAlertOpen(false)
            setId(null)
        }
        if(type === 'accept' && id && values) {
            setIsAlertOpen(false)
            editSkill(id, values)
            setValues(null)
            setId(null)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    }


    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const newSkill= () => {
        context.setLeftDialog(
            <SkillForm 
                handleFormSubmit={handleFormSubmit} 
                closeLeftDialog={closeLeftDialog} 
                action={NEW}
            />
        )
    }

    const editItem = (editableItem) => {
        context.setLeftDialog(
            <SkillForm 
                handleFormSubmit={openAlert} 
                data={editableItem} 
                closeLeftDialog={closeLeftDialog} 
                action={EDIT}
                id={editableItem.id}
            />
        )
    }

    const getDisplayData = (data) => {
        let displayData = {
            title: "Aptitud",
            content: {
                "Aptitud": data.name
            }
        }
        if(data.description) {
            displayData.content = {
                ...displayData.content,
                "Comentario": data.description
            }
        }
        return displayData
    }

    return (
        <div className={classes.content}>
            <div className={classes.titleContainer}>
                <span className={classes.titleText}>Aptitudes</span>
                <Button onClick={()=> newSkill()}>
                    <AddCircleOutlineIcon fontSize='large'/>
                </Button>
            </div>
            <div className={classes.mainContent}>
            <div className={classes.skills}>
                {userSkills && userSkills.length>0 && 
                // !loading && 
                    userSkills.map((data, index)=> {
                        return(
                                <div key={v4()} >
                                    {(index % 2 === 0) && 
                                        <CardItem 
                                            key={index} 
                                            index={index} 
                                            data={data}
                                            displayData={getDisplayData(data)}
                                            title={data.name}
                                            editItem={editItem}
                                            deleteItem={openAlert} 
                                            closeLeftDialog={closeLeftDialog} 
                                        />
                                    }
                                </div>
                            )
                        })
                    }
                </div>
                <div className={classes.skills}>
                    {userSkills && userSkills.length>0 && 
                    // !loading && 
                        userSkills.map((data, index)=> {
                            return(
                                    <div key={v4()} >
                                        {(index % 2 !== 0) && 
                                            <CardItem 
                                                key={index} 
                                                index={index} 
                                                data={data}
                                                displayData={getDisplayData(data)}
                                                title={data.name}
                                                editItem={editItem}
                                                deleteItem={openAlert} 
                                                closeLeftDialog={closeLeftDialog} 
                                            />
                                    }
                                    </div>
                                )
                            }
                        )
                    }
                </div>
            </div>
            {(!userSkills || userSkills.length === 0) && 
            // !loading && 
                <div className={classes.noDataContainer}>
                 <span className={classes.noData}>No agregaste información.</span>
                </div>
            }
            {/* {loading && <CircularProgress/>} */}
            {isAlertOpen && <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title={values? '¿Desea confirmar los cambios?' : '¿Desea eliminar esta aptitud?'}
                loading={false}
            />}
        </div>
    )
}

export default Skills