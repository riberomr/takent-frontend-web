import React from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import commonUserSkills from '../../../../../rules/validationSchema/commonUserSkills'
import { Grid } from '@material-ui/core'

const useStyles = makeStyles(styles)

const LanguageForm = (props) => {
    const { data, handleFormSubmit, closeLeftDialog, action, id } = props
    const NEW = 'NEW'
    const classes = useStyles()
    const variant = 'standard'

    let initialValues = {
        name:'',
        description: ''
    }
    if(action!==NEW && data) {
        initialValues = {
            name: data.name,
            description: data.description,
        }
    }   
 
    const handleSubmitForm = (values) => {

        if(action!==NEW && id)
            handleFormSubmit(id, values)
        else handleFormSubmit(values)
    }
    
    return(<>
        <Formik
            initialValues={initialValues}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserSkills}
        >
        {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            errors,
            touched,
        }) => (
            <form onSubmit={handleSubmit}>
                <div className={classes.formRoot}>
                    <Grid container spacing={5}>
                        <Grid item xs={12}>
                            <span className={classes.titleLabel}>{action===NEW? 'Nueva aptitud' : 'Editar aptitud'}</span> 
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='name'
                                type='text'
                                name='name'
                                label="Aptitud"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.name}
                                error={errors.name && touched.name}
                                helperText={(errors.name && touched.name)? errors.name : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                multiline
                                rows={4}
                                id='description'
                                type='text'
                                name='description'
                                label="Comentario"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.description}
                                error={errors.description && touched.description}
                                helperText={(errors.description && touched.description)? errors.description : ""}
                            />
                        </Grid>
                    </Grid>
                    <div className={classes.buttonContainer}>
                        <FormButton
                            onClick={()=> closeLeftDialog()}
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Cancelar
                        </FormButton>
                        <FormButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Guardar
                        </FormButton>
                    </div>
                </div>
            </form>
        )}
        </Formik>
        </>
    )
}

export default LanguageForm