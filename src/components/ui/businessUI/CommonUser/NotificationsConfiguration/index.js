import { Container, FormControlLabel, Grid, Slider, Switch, ThemeProvider, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import NotificationsIcon from '@material-ui/icons/Notifications'
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import DataDialog from '../../../generalUI/DataDialog'

const useStyles = makeStyles(styles)

const maxDistanceMarks = [
  {
    value: 1,
    label: "300m"
  },
  {
    value: 2,
    label: "1km"
  },
  {
    value: 3,
    label: "2km"
  },
  {
    value: 4,
    label: "5km"
  },
  {
    value: 5,
    label: "10km",
  },
  {
    value: 6,
    label: "25km"
  },
  {
    value: 7,
    label: "50km"
  },
  {
    value: 8,
    label: "100km"
  },
  {
    value: 9,
    label: "200km"
  }
]

const maxDistanceMap = {
  1: 300,
  2: 1000,
  3: 2000,
  4: 5000,
  5: 10000,
  6: 25000,
  7: 50000,
  8: 100000,
  9: 200000
}

const CommonUserNotificationsConfiguration = props => {

  const classes = useStyles()

  const { notificationsConfiguration, handleConfigurationChange, dataDialog } = props
  const [dialogOpen, setDialogOpen] = useState(false);
  const [near_offers, setNearOffers] = useState(notificationsConfiguration.near_offers)

  useEffect(() => {
    if(notificationsConfiguration.near_offers !== near_offers){
      handleConfigurationChange("near_offers", near_offers)
    }
  // eslint-disable-next-line
  }, [near_offers.active])

  const handleOpen = () => {
    setDialogOpen(true);
  }

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
       <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            <NotificationsIcon color="primary" style={{paddingRight: '5px'}}/>
            <Typography variant="h5" style={{paddingRight: '15px'}}>Notificaciones</Typography>
            <Tooltip title="Ayuda" style={{color: 'orange'}} onClick={handleOpen}>             
                <HelpOutlineIcon  />                   
            </Tooltip>
          </div>
        </div>
        <Grid container spacing={5}>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="requested_service"
                  checked={notificationsConfiguration.requested_service}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Nuevas solicitudes de servicio"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="request_assessment"
                  checked={notificationsConfiguration.request_assessment}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Recordatorios de valoración de servicios obtenidos"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="suggest_preferred_service"
                  checked={notificationsConfiguration.suggest_preferred_service}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Sugerencias de servicio preferencial"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="state_postulation_changed"
                  checked={notificationsConfiguration.state_postulation_changed}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Cambios de estado de postulaciones a ofertas"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="suggest_info_completion"
                  checked={notificationsConfiguration.suggest_info_completion}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Sugerencia para completar tus datos personales"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="near_offers"
                  checked={near_offers.active}
                  color="primary"
                  onChange={event => setNearOffers(prevState => ({...prevState, active: event.target.checked}))}
                />
              }
              label={
                <div style={{display: "flex", alignItems: "center", justifyContent: "flex-start"}}>
                  <Typography variant="body1" component="span" style={{marginRight: theme.spacing(1)}}>Ofertas cerca de tu ubicación</Typography>
                  <Tooltip title='Recordá que para que te lleguen notificaciones de ofertas según tu ubicación, tenés que cargar cuáles son tus rubros preferidos en la sección "Mis datos personales"'>             
                    <HelpOutlineIcon color="primary"/>                   
                  </Tooltip>
                </div>
              }
            />
            <div>
              <Typography id="max-radius" variant="caption">Distancia máxima</Typography>
              <Slider
                value={Object.entries(maxDistanceMap).filter(([key, value]) => value === near_offers.radius)[0][0]}
                min={1}
                max={9}
                step={null}
                marks={maxDistanceMarks}
                onChange={(event, value) => setNearOffers(prevState => ({...prevState, radius: maxDistanceMap[value]}))}
                onChangeCommitted={() => handleConfigurationChange("near_offers", near_offers)}
                disabled={!notificationsConfiguration.near_offers.active}
                aria-labelledby="max-radius"
              />
            </div>
          </Grid>
        </Grid>
        <DataDialog
                title={dataDialog.title}
                data={dataDialog.data}
                open={dialogOpen}
                setOpen={setDialogOpen}
            />
      </Container>
    </ThemeProvider>
  )

}

export default CommonUserNotificationsConfiguration
