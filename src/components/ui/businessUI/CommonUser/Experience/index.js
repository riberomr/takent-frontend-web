import React, { useContext, useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../../Store'
import ExperienceForm from './ExperienceForm'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import Button from '@material-ui/core/Button'
import AlertAction from '../../../generalUI/AlertAction'
import { v4 } from 'uuid'
import CardItem from '../../../generalUI/CardItem'
import { format } from 'date-fns'
// import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(styles)

const Experience = (props) => {
    const NEW = 'NEW'
    const EDIT = 'EDIT'
    const {userExperience, handleFormSubmit, deleteExperience, editExperience
        // , loading
        } = props
    const context = useContext(Store)
    const classes = useStyles()

    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [id, setId] = useState(null)
    const [values, setValues] = useState(null)

    const closeLeftDialog = () => {
        context.setLeftDialog(null)
    }

    //this function open the alert and receive and set the an id if from the form
    const openAlert = (id, values) => {
        if(id) setId(id)
        if(values) setValues(values)    
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {

        if(type === 'accept' && id && !values) {
            deleteExperience(id)
            setIsAlertOpen(false)
            setId(null)
        }
        if(type === 'accept' && id && values) {
            setIsAlertOpen(false)
            editExperience(id, values)
            setValues(null)
            setId(null)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    }


    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const newExperience = () => {
        context.setLeftDialog(<ExperienceForm handleFormSubmit={handleFormSubmit} closeLeftDialog={closeLeftDialog} action={NEW}/>)
    }
    
    const editItem = (editableData) => {
        context.setLeftDialog(
            <ExperienceForm 
                handleFormSubmit={openAlert} 
                data={editableData} 
                closeLeftDialog={closeLeftDialog} 
                action={EDIT}
                id={editableData.id}
            />)
    }

    const getDisplayData = (data) => {
        let displayData = {
            title: "Experiencia laboral",
            content: {
                "Puesto": data.position,
                "Empresa": data.company,
                "Periodo": "Desde " + format(new Date(data.start_date), 'dd/MM/yyyy') + " hasta " + (data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : "Actualidad")
            }
            
        }
        if(data.description) {
            displayData.content = {
                ...displayData.content,
                "Comentario": data.description,
            }
        }
        if(data.reference.name) {
            displayData.content = {
                ...displayData.content,
                "Referencia": `${data.reference.name} - ${data.reference.phone}`
            }
        }
        return displayData
    }

    return (
        <div className={classes.content}>
            <div className={classes.titleContainer}>
                <span className={classes.titleText}>Mi experiencia laboral</span>
                <Button onClick={()=> newExperience()}>
                    <AddCircleOutlineIcon fontSize='large'/>
                </Button>
            </div>
            <div className={classes.mainContent}>
                <div className={classes.experiences}>
                    {userExperience && userExperience.length>0 &&
                    //  !loading &&
                        userExperience.map((data, index)=> {
                            return(
                                    <div key={v4()}>
                                        {(index % 2 === 0) && 
                                            <CardItem 
                                                key={index} 
                                                index={index} 
                                                data={data}
                                                displayData={getDisplayData(data)}
                                                title={data.position}
                                                subtitle={data.company}
                                                valueText1={`${data.start_date? format(new Date(data.start_date), 'dd/MM/yyyy') : '-'} - 
                                                            ${data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : 'Actualidad'}`}
                                                valueText1Shortable={false}
                                                editItem={editItem}
                                                deleteItem={openAlert} 
                                                closeLeftDialog={closeLeftDialog} 
                                            />
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className={classes.experiences}>
                        {userExperience && userExperience.length>0 &&
                        // !loading &&
                        userExperience.map((data, index)=> {
                                return(
                                        <div key={v4()} >
                                            {(index % 2 !== 0) && 
                                                <CardItem 
                                                    key={index} 
                                                    index={index} 
                                                    data={data}
                                                    displayData={getDisplayData(data)}
                                                    title={data.position}
                                                    subtitle={data.company}
                                                    valueText1={`${data.start_date? format(new Date(data.start_date), 'dd/MM/yyyy') : '-'} - 
                                                                ${data.end_date? format(new Date(data.end_date), 'dd/MM/yyyy') : 'Actualidad'}`}
                                                    valueText1Shortable={false}
                                                    editItem={editItem}
                                                    deleteItem={openAlert} 
                                                    closeLeftDialog={closeLeftDialog} 
                                                />
                                            }
                                        </div>
                                    )
                                }
                            )
                        }
                    </div>
                </div>
            {(!userExperience|| userExperience.length===0) && 
            // !loading &&
                <div>
                 <span className={classes.noData}>No agregaste información.</span>
                </div>
            }
            {/* {loading && <CircularProgress/>} */}
            {isAlertOpen && <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title={values? '¿Desea confirmar los cambios?' : '¿Desea eliminar esta experiencia laboral?'}
                loading={false}
            />}
        </div>
    )
}

export default Experience