import React from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import { format, addHours } from 'date-fns'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import commonUserExperience from '../../../../../rules/validationSchema/commonUserExperience'
import { Grid, FormControlLabel, Checkbox } from '@material-ui/core'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'

const useStyles = makeStyles(styles)

const ExperienceForm = (props) => {
    const { data, handleFormSubmit, closeLeftDialog, action, id } = props
    const NEW = 'NEW'
    const classes = useStyles()
    const variant = 'standard'

    let initialValues = {
        company: '',
        position:'',
        start_date: '',
        end_date: '',
        description: '',
        nameReference: '',
        phoneReference: '',
        checkPresentEndDate: false
    }
    if(data) {
        //este parche tiene que irse despues agrego 4 horas para que me tome el día bien
        initialValues = {
            company: data.company,
            position: data.position,
            start_date: format(
                addHours(new Date(`${data.start_date}`), 4),
                'yyyy-MM-dd'
              ),
            end_date: data.end_date? format(
                addHours(new Date(`${data.end_date}`), 4),
                'yyyy-MM-dd'
            ) : '',
            nameReference: data.reference.name,
            phoneReference: data.reference.phone,
            description: data.description,
            checkPresentEndDate: data.end_date? false : true
        }
    }

    const handleCheckPresentEndDate = (checked, setFieldValue) => {
        setFieldValue('checkPresentEndDate', checked)
        setFieldValue('end_date', '')
    }
 
    const handleSubmitForm = (values) => {
        //este parche tiene que irse despues agrego 4 horas para que me tome el día bien
        let valuesToSend = {
            company: values.company,
            position: values.position,
            start_date: format(
                addHours(new Date(`${values.start_date}`), 4),
                'yyyy-MM-dd'
              ),
            end_date: values.end_date? format(
                addHours(new Date(`${values.end_date}`), 4),
                'yyyy-MM-dd'
            ) : null,
            reference: {
              name: values.nameReference, 
              phone: values.phoneReference
            },
            description: values.description
        }

        if(action!==NEW && id) {
            handleFormSubmit(id, valuesToSend)
            return
        }

        handleFormSubmit(valuesToSend)
    }
    
    return(<>
        <Formik
            initialValues={initialValues}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserExperience}
        >
        {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue
        }) => (
            <form onSubmit={handleSubmit}>
                <div className={classes.formRoot}>
                    <Grid container spacing={5}>
                        <Grid item xs={12} className={classes.gridItem}>
                            <span className={classes.titleLabel}>{action===NEW? 'Nueva experiencia laboral' : 'Editar experiencia laboral'}</span>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='position'
                                type='text'
                                name='position'
                                label="Puesto"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.position}
                                error={errors.position && touched.position}
                                helperText={(errors.position && touched.position)? errors.position : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='company'
                                type='text'
                                name='company'
                                label="Empresa"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.company}
                                error={errors.company && touched.company}
                                helperText={(errors.company && touched.company)? errors.company : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField 
                                id='start_date'
                                type='date'
                                name='start_date'
                                label="Fecha de inicio"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.start_date}
                                error={errors.start_date && touched.start_date}
                                helperText={(errors.start_date && touched.start_date)? errors.start_date : ""}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField 
                                    id='end_date'
                                    type='date'
                                    name='end_date'
                                    label="Fecha de finalización"
                                    variant={variant}
                                    fullWidth
                                    InputProps={{disabled: values.checkPresentEndDate}}
                                    InputLabelProps={{shrink: true}}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.end_date}
                                    error={errors.end_date && touched.end_date}
                                    helperText={(errors.end_date && touched.end_date)? errors.end_date : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            icon={<CheckBoxOutlineBlankIcon fontSize="small"/>}
                                            checkedIcon={<CheckBoxIcon fontSize="small" color="primary"/>}
                                            id="checkPresentEndDate"
                                            name="checkPresentEndDate"
                                            checked={values.checkPresentEndDate}
                                            onBlur={handleBlur}
                                            onChange={(event) => handleCheckPresentEndDate(event.target.checked, setFieldValue)}
                                        />
                                    }
                                    label="Actualidad"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                multiline
                                rows={4}
                                id='description'
                                type='text'
                                name='description'
                                label="Comentario"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.description}
                                error={errors.description && touched.description}
                                helperText={(errors.description && touched.description)? errors.description : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <span className={classes.formLabel}><span style={{fontWeight: 'bold'}}>{'Referencia '}</span></span>
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='nameReference'
                                type='text'
                                name='nameReference'
                                label="Nombre"
                                variant={variant}
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.nameReference}
                                error={errors.nameReference && touched.nameReference}
                                helperText={(errors.nameReference && touched.nameReference)? errors.nameReference : ""}
                            />
                        </Grid>
                        <Grid item xs={12} className={classes.gridItem}>
                            <TextField
                                id='phoneReference'
                                type='number'
                                name='phoneReference'
                                label="Teléfono"
                                variant={variant}
                                onlyNumbers
                                fullWidth
                                InputLabelProps={{shrink: true}}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phoneReference}
                                error={errors.phoneReference && touched.phoneReference}
                                helperText={(errors.phoneReference && touched.phoneReference)? errors.phoneReference : ""}
                            />
                        </Grid>
                    </Grid>
                    <div className={classes.buttonContainer}>
                        <FormButton
                            onClick={()=> closeLeftDialog()}
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Cancelar
                        </FormButton>
                        <FormButton
                            type="submit"
                            variant="contained"
                            color="primary"
                            className={classes.buttons}
                            >
                            Guardar
                        </FormButton>
                    </div>
                </div>
            </form>
        )}
        </Formik>
        </>
    )
}

export default ExperienceForm