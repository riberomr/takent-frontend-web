import React, { useContext, useState } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
  Badge,
  Avatar,
  Popover,
  ButtonGroup,
  Button,

} from '@material-ui/core'
import { useSnackbar } from 'notistack'
import { Store } from '../../../../../../../Store'
import { apiTakent } from '../../../../../../../services/API-TAKENT'
import FolderOpenIcon from '@material-ui/icons/FolderOpen'
import AlertAction from '../../../../../generalUI/AlertAction'

const ALLOWED_TYPES = ['jpeg', 'png']

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    '& > *': {
      margin: theme.spacing(1)
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  textField: {
    margin: '0.5rem',
  },
  grid: {
    margin: '0.5rem',
  },
  large: {
    //marginTop: '7.5rem',
    width: theme.spacing(25),
    height: theme.spacing(25),
    border: '1px solid rgba(0,0,0, 0.15)',

  },
  inputFileStyle: {
    display: 'none',
  }
}))

const SmallAvatar = withStyles(() => ({
  root: {
    width: 35,
    height: 35,
    marginLeft: '1rem',
    marginTop: '1rem',
    backgroundColor: '#777',
  }
}))(Avatar)


const AvatarProfile = (props) => {
  const { disabledStatus, profilePhoto } = props
  const classes = useStyles()
  const context = useContext(Store)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const { enqueueSnackbar } = useSnackbar()
  const [isAlertOpen, setIsAlertOpen] = useState(false)

   //this function open the alert and receive and set the an id if from the form
   const openAlert = () => {   
    setIsAlertOpen(true)
}

//this function handle the action of the actionButtons by type
const handleSubmitAction = (type) => {

    if(type === 'accept') {
        handleDeleteImage()
        setIsAlertOpen(false)
    }
    
    if(type === 'cancel') {
        setIsAlertOpen(false)
    }
}


//these are the actionButtons, an array of objects with properties
const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleSubmitAction
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleSubmitAction
    }  
]

  const convertImage = event => {
    // setError(false)
    if (!event.target.files.length) return enqueueSnackbar('Error al cargar foto de perfil, formato inválido.', { variant: 'error' })
    let file = event.target.files[0]
    if(((file.size/1024)/1024)>5) return enqueueSnackbar('Error al cargar foto de perfil, el tamaño excede los 5 Mb.', { variant: 'error' })
    let [type, extension] = file.type.split('/')
    if (!ALLOWED_TYPES.includes(extension) || type !== 'image' || !file.type) {
      // setError(true)
      enqueueSnackbar('Error al cargar foto de perfil', { variant: 'error' })
      setAnchorEl(null)
      return
    }

    let reader = new FileReader()
    reader.onloadend = function () {
      let values = { profile_photo: reader.result }
      apiTakent.postProfilePhotoCommonUser(values)
        .then(() => {
          context.handleSetUser({...context.user, profile_photo: reader.result})
          enqueueSnackbar('Foto de perfil cargada con éxito', { variant: 'success' })
          context.setPhotoChange(Math.random())
          handleClose()
        })
        .catch(() => enqueueSnackbar('Error al cargar foto de perfil', { variant: 'error' }))
    }
    reader.readAsDataURL(file)
  }

  const handleDeleteImage = () => {
    apiTakent.deleteProfilePhotoCommonUser()
      .then(() => {
        enqueueSnackbar('Foto de perfil eliminada con éxito', { variant: 'success' })
        let newValues = { ...context.user, profile_photo: null }
        context.handleSetUser(newValues)
        context.setPhotoChange(Math.random())
        handleClose()
      })
      .catch(() => enqueueSnackbar('Error al eliminar foto de perfil', { variant: 'error' }))
  }

  const handleClick = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined

  return (
    <>
      <Badge
        overlap="circle"
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        badgeContent={
          <SmallAvatar alt="Remy Sharp">
            <Button
              aria-describedby={id}
              variant="contained"
              color="primary"
              onClick={handleClick}
              disabled={disabledStatus}
            >
              <FolderOpenIcon />
            </Button>
            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <ButtonGroup
                orientation="vertical"
                color="primary"
                aria-label="vertical outlined primary button group"
              >
                <Button
                  onClick={() => {
                    document.getElementById('inputFile').click()
                  }}
                >Subir</Button>
                <Button
                  disabled={!profilePhoto}
                  onClick={() => {
                    openAlert()
                  }}
                >Eliminar</Button>
              </ButtonGroup>
            </Popover>
          </SmallAvatar>
        }
      >
        <Avatar src={profilePhoto? profilePhoto : ''} className={classes.large}/>
        <input
          type="file"
          id="inputFile"
          accept="image/jpeg,image/png"
          className={classes.inputFileStyle}
          onChange={convertImage}
        />
      </Badge>
      {isAlertOpen && 
        <AlertAction
          isOpen={isAlertOpen}
          setIsOpen={setIsAlertOpen}
          actions={actions}
          type='doubt'
          title={'¿Estás seguro de eliminar la foto de perfil?'}
          loading={false}
        />}
    </>
  )
}

export default AvatarProfile