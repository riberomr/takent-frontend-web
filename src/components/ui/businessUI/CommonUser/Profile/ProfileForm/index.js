import React, { useState } from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import Chip from '@material-ui/core/Chip'
import AvatarProfile from './AvatarProfile'
import { format, addHours } from 'date-fns'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import commonUserProfile from '../../../../../rules/validationSchema/commonUserProfile'
import { UploadPDF } from '../../../../../../helpers/generalHelpers/UploadPDF/UploadPDF'
import { DownloadPDF } from '../../../../../../helpers/generalHelpers/DownloadPDF/DownloadPDF'
import { Grid } from '@material-ui/core'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import LoadingButton from '../../../../generalUI/LoadingButton'
import { placesHelper } from '../../../../../../helpers/customHelpers/places'
import GoogleMap from '../../../../generalUI/GoogleMaps/GoogleMap'

const useStyles = makeStyles((theme) => (styles))

const ProfileForm = (props) => {
    const {
        provinces,
        cities,
        setIdProvince,
        userData,
        handleFormSubmit,
        pdf,
        sectors,
        setUploadCurriculum,
        uploadCurriculum,
        loading,
        setLoading,
        disabledStatus,
        setDisabledStatus,
        profilePhoto,
        loadingSubmit
    } = props
    const classes = useStyles();

    const [location, setLocation] = useState(userData.address.location? userData.address.location : null)

    const [ cancelChanged, setCancelChanged ] = useState(null)


    let initialValuesCommonUser
    const variant = 'standard'

    if (userData) {
        initialValuesCommonUser = {
            name: userData.name,
            surname: userData.surname,
            description: userData.description,
            birth_date: format(
                addHours(new Date(`${userData.birth_date}`), 4),
                'yyyy-MM-dd'
            ),
            sector: userData.preferred_services,
        }
        if (userData.address) {
            initialValuesCommonUser = {
                ...initialValuesCommonUser,
                district: userData.address.district ? userData.address.district : '',
                street: userData.address.street ? userData.address.street : '',
                number: userData.address.number ? userData.address.number : '',
                floor: userData.address.floor ? userData.address.floor : '',
                apartment: userData.address.apartment ? userData.address.apartment : '',
                postal_code: userData.address.postal_code ? userData.address.postal_code : '',
                province: userData.address.province ? userData.address.province.code : '',
                city: userData.address.city ? userData.address.city.code : '',
                tower: userData.address.tower ? userData.address.tower : ''
            }
        }
        if (userData.phone) initialValuesCommonUser = { ...initialValuesCommonUser, phone: userData.phone }
    } else {
        initialValuesCommonUser = {
            name: '',
            surname: '',
            description: '',
            birth_date: '',
            district: '',
            street: '',
            number: '',
            floor: '',
            apartment: '',
            postal_code: '',
            province: '',
            city: '',
            phone: '',
            sector: [],
        }
    }

    const handleProvinceChange = (value, setFieldValue) => {
        setFieldValue('city', '')
        setIdProvince(value)
        setFieldValue('province', value)
    }
    const handleCityChange = (value, setFieldValue) => {
        setFieldValue('city', value)
    }

    const handleSectorChange = (value, setFieldValue, options) => {      
        if (value.length <= 3) setFieldValue('sector', value);
    }

    const handleSubmitForm = (values) => {
        let valuesToSend = {
            name: values.name,
            surname: values.surname,
            description: values.description,
            birth_date: format(
                addHours(new Date(`${values.birth_date}`), 4),
                'yyyy-MM-dd'
            ),
            preferred_services: values.sector,
        }
        valuesToSend = {
            ...valuesToSend,
            address: {
                district: values.district !== '' ? values.district : null,
                street: values.street !== '' ? values.street : null,
                number: values.number !== '' ? values.number : null,
                apartment: values.apartment !== '' ? values.apartment : null,
                floor: values.floor !== '' ? values.floor : null,
                postal_code: values.postal_code !== '' ? values.postal_code : null,
                city: values.city !== '' ? values.city : null,
                location: location? location : null,
            }
        }
        valuesToSend = {
            ...valuesToSend,
            phone: values.phone !== '' ? values.phone : null
        }
        handleFormSubmit(valuesToSend)
    }

    const handleCancel = (values, setFieldValue) => {
        setDisabledStatus(!disabledStatus)
        setCancelChanged(Math.random())
        Object.keys(values).map(key => {
            return Object.keys(initialValuesCommonUser).map(key2 => {
                if (key === key2) return setFieldValue(`${key}`, initialValuesCommonUser[key2])
                return null
            })
        })
    }

    const setAddressInfo = (addressInfo, setFieldValue) => {
      const province = placesHelper.parseProvince(addressInfo.province, provinces)
      if(province) {
        setFieldValue("province", province)
        setIdProvince(province)
      }
      placesHelper.parseCity(addressInfo.city, province)
      .then(city => {
        setFieldValue("city", city)
      })
      setFieldValue("district", addressInfo.district)
      setFieldValue("street", addressInfo.street)
      setFieldValue("number", addressInfo.number)
      setFieldValue("postal_code", placesHelper.parsePostalCode(addressInfo.postal_code))
    }

    return (<>
        <div className={classes.root} onLoad={setIdProvince(initialValuesCommonUser.province)}>
            <Grid container spacing={3}>
                <Grid item xs={9}>
                    <span className={classes.mainTitle}>Mis datos personales</span>
                </Grid>
                <Grid item xs={3}>
                    <FormButton
                        onClick={() => { setDisabledStatus(!disabledStatus) }}
                        variant="contained"
                        color="primary"
                        disabled={!disabledStatus}
                        className={classes.buttonWidth}
                    >
                        Editar
                    </FormButton>
                </Grid>
            </Grid>
        </div>
        {userData && <Formik initialValues={initialValuesCommonUser}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={commonUserProfile}>
            {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                errors,
                touched,
                setFieldValue
            }) => (
                <form onSubmit={handleSubmit}>
                    <div className={classes.formRoot}>
                        <Grid container spacing={5}>
                            <Grid item xs={4} className={classes.gridItemCentered}>
                                <AvatarProfile profilePhoto={profilePhoto} disabledStatus={disabledStatus} />
                            </Grid>
                            <Grid item xs={8} className={classes.gridItemCentered}>
                                <TextField
                                    multiline
                                    rows={4}
                                    id='description'
                                    type='text'
                                    name='description'
                                    label="Descripción"
                                    variant={variant}
                                    inputProps={{maxLength: 300}}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.description}
                                    error={errors.description && touched.description}
                                    helperText={(errors.description && touched.description) ? errors.description : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItemGrid}>
                                <div className={classes.gridItem}>
                                    <span className={classes.inlineElement}><b>Cargá tu CV:</b></span>
                                    <UploadPDF
                                        uploadCurriculum={uploadCurriculum}
                                        setUploadCurriculum={setUploadCurriculum}
                                        loading={loading}
                                        setLoading={setLoading}
                                        disabledStatus={disabledStatus}
                                        cancelChanged={cancelChanged}
                                    />
                                </div>
                                <DownloadPDF
                                    pdf={pdf}
                                    loading={loading}
                                    classes={classes}
                                    setLoading={setLoading}
                                    disabledStatus={disabledStatus}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItemGrid}>
                                <div className={classes.gridItem}>
                                    <span className={classes.inlineElement}><b>¿Qué rubros preferís?</b></span>                          
                                </div>
                                <br />
                                <Select
                                    id="sector"
                                    name="sector"
                                    label="Preferencias"
                                    readOnly={disabledStatus}
                                    variant={variant}
                                    multiple
                                    fullWidth
                                    options={sectors}
                                    value={values.sector}
                                    onChange={(event) => handleSectorChange(event.target.value, setFieldValue, values.sector)}
                                    
                                />
                                
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                            
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                            {values.sector.map(elem => {
                                    return (<Chip color="primary" label={elem} />)
                                })}
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='name'
                                    type='text'
                                    name='name'
                                    label="Nombre"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.name}
                                    error={errors.name && touched.name}
                                    helperText={(errors.name && touched.name) ? errors.name : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='surname'
                                    type='text'
                                    name='surname'
                                    label="Apellido"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.surname}
                                    error={errors.surname && touched.surname}
                                    helperText={(errors.surname && touched.surname) ? errors.surname : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='phone'
                                    type='number'
                                    name='phone'
                                    label="Teléfono"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    onlyNumbers
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.phone}
                                    error={errors.phone && touched.phone}
                                    helperText={(errors.phone && touched.phone) ? errors.phone : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='birth_date'
                                    type='date'
                                    name='birth_date'
                                    label="Fecha de nacimiento"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.birth_date}
                                    error={errors.birth_date && touched.birth_date}
                                    helperText={(errors.birth_date && touched.birth_date) ? errors.birth_date : ""}
                                />
                            </Grid>
                            <Grid container item spacing={5} xs={12} sm={12} md={6} className={classes.customGrid}>
                              <Grid item xs={12} className={classes.gridItem}>
                                  <Select
                                    id="province"
                                    name="province"
                                    label="Provincia"
                                    readOnly={disabledStatus}
                                    variant={variant}
                                    fullWidth
                                    options={provinces}
                                    value={values.province}
                                    onChange={(event) => handleProvinceChange(event.target.value, setFieldValue)}
                                    error={errors.name && touched.name}
                                    helperText={(errors.province && touched.province) ? errors.province : ""}
                                  />
                              </Grid>
                              <Grid item xs={12} className={classes.gridItem}>
                                <Select
                                  id="city"
                                  name="city"
                                  label="Localidad"
                                  readOnly={disabledStatus}
                                  disabled={values.province === ''}
                                  variant={variant}
                                  fullWidth
                                  options={cities}
                                  value={values.city}
                                  onChange={(event) => handleCityChange(event.target.value, setFieldValue)}
                                  error={errors.city && touched.city}
                                  helperText={(errors.city && touched.city) ? errors.city : ""}
                                />
                              </Grid>
                              <Grid item xs={4} className={classes.gridItem}>
                                <TextField
                                  id='district'
                                  type='text'
                                  name='district'
                                  label="Barrio"
                                  variant={variant}
                                  InputProps={{ readOnly: disabledStatus }}
                                  disabled={values.city === ''}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.district}
                                  error={errors.district && touched.district}
                                  helperText={(errors.district && touched.district) ? errors.district : ""}
                                />
                              </Grid>
                              <Grid item xs={4} className={classes.gridItem}>
                                <TextField
                                  id='street'
                                  type='text'
                                  name='street'
                                  label="Calle"
                                  variant={variant}
                                  InputProps={{ readOnly: disabledStatus }}
                                  disabled={values.city === ''}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.street}
                                  error={errors.street && touched.street}
                                  helperText={(errors.street && touched.street) ? errors.street : ""}
                                />
                              </Grid>
                              <Grid item xs={4} className={classes.gridItem}>
                                <TextField
                                  id='number'
                                  type='number'
                                  name='number'
                                  label="Número"
                                  variant={variant}
                                  onlyNumbers
                                  InputProps={{ readOnly: disabledStatus }}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.number}
                                  error={errors.number && touched.number}
                                  helperText={(errors.number && touched.number) ? errors.number : ""}
                                />
                              </Grid>
                              <Grid item xs={3} className={classes.gridItem}>
                                <TextField
                                  id='postal_code'
                                  type='number'
                                  name='postal_code'
                                  label="CP"
                                  onlyNumbers
                                  disabled={values.city === ''}
                                  variant={variant}
                                  InputProps={{ readOnly: disabledStatus }}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.postal_code}
                                  error={errors.postal_code && touched.postal_code}
                                  helperText={(errors.postal_code && touched.postal_code) ? errors.postal_code : ""}
                                />
                              </Grid>
                              <Grid item xs={3} className={classes.gridItem}>
                                <TextField
                                  id='tower'
                                  type='text'
                                  name='tower'
                                  label="Torre"
                                  variant={variant}
                                  disabled={values.street === ''}
                                  InputProps={{ readOnly: disabledStatus }}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.tower}
                                  error={errors.tower && touched.tower}
                                  helperText={(errors.tower && touched.tower) ? errors.tower : ""}
                                />
                              </Grid>
                              <Grid item xs={3} className={classes.gridItem}>
                                <TextField
                                  id='floor'
                                  type='number'
                                  name='floor'
                                  label="Piso"
                                  variant={variant}
                                  disabled={values.street === ''}
                                  InputProps={{ readOnly: disabledStatus }}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.floor}
                                  error={errors.floor && touched.floor}
                                  helperText={(errors.floor && touched.floor) ? errors.floor : ""}
                                />
                              </Grid>
                              <Grid item xs={3} className={classes.gridItem}>
                                <TextField
                                  id='apartment'
                                  type='text'
                                  name='apartment'
                                  label="Departamento"
                                  variant={variant}
                                  disabled={values.street === ''}
                                  InputProps={{ readOnly: disabledStatus }}
                                  fullWidth
                                  InputLabelProps={{ shrink: true }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.apartment}
                                  error={errors.apartment && touched.apartment}
                                  helperText={(errors.apartment && touched.apartment) ? errors.apartment : ""}
                                />
                              </Grid>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6}>
                              <GoogleMap
                                height="350px"
                                initialLatLng={location}
                                interactive
                                geocoding
                                setExtLatLng={setLocation}
                                setExtAddressInfo={addressInfo => setAddressInfo(addressInfo, setFieldValue)}
                                disabled={disabledStatus}
                              />
                            </Grid>
                            {!disabledStatus && <Grid item xs={12} className={classes.buttonContainer}>
                                <LoadingButton
                                    onClick={() => handleCancel(values, setFieldValue)}
                                    disabled={disabledStatus}
                                    className={classes.buttonWidth}
                                >
                                    Cancelar
                              </LoadingButton>
                                <LoadingButton
                                    type="submit"
                                    disabled={disabledStatus}
                                    loading={loadingSubmit}
                                    className={classes.buttonWidth}
                                >
                                    Guardar
                              </LoadingButton>
                            </Grid>}
                        </Grid>
                    </div>
                </form>
            )}
        </Formik>}
    </>
    )
}

export default ProfileForm