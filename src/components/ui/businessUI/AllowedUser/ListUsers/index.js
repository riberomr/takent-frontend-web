import { Button, Container, makeStyles, ThemeProvider, Tooltip, Typography } from '@material-ui/core'
import React, { useState } from 'react'
import { theme } from '../../../../../constants/generalConstants/theme'
import { createHeader } from '../../../../../helpers/generalHelpers/table'
import ResponsiveTable from '../../../generalUI/ResponsiveTable'
import clsx from 'clsx'
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled'
import DataDrawer from '../../../generalUI/DataDrawer'
import PersonIcon from '@material-ui/icons/Person'
import { styles } from './styles';
import FormButton from '../../../generalUI/Buttons/PrimaryButton'
import DrawerAllowedUsers from '../../../generalUI/FormDrawer/AllowedUsers'
import ValidPassDialog from '../ValidPassDialog'

const useStyles = makeStyles(styles);

const ActionButtons = props => {

    const { admin, actionButtonsProps, rowData, className } = props
    const { firstTooltip, secondTooltip, thirdTooltip, firstFunction, secondFunction, thirdFunction } = actionButtonsProps

    const classes = makeStyles({

        container: {
            display: 'flex',
            justifyContent: 'space-evenly',
            [theme.breakpoints.down('md')]: {
                justifyContent: 'flex-end',
                '& :not(:first-child)': {
                    marginLeft: '20px'
                }
            }
        },
        buttons: {
            boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
            minHeight: '40px',
            minWidth: '40px',
            borderRadius: '100%',
            backgroundColor: 'white',
            color: 'orange',
            '&&:hover ': {
                backgroundColor: 'orange',
                color: 'white'
            },
        }
    })()

    return (
        <div id='action-buttons' className={clsx(classes.container, className)}>
            <Tooltip title={firstTooltip}>
                <Button
                    className={classes.buttons}
                    onClick={() => firstFunction(rowData)}
                >
                    <PersonIcon />
                </Button>
            </Tooltip>
            {admin && rowData.permission !== "admin" && <Tooltip title={secondTooltip}>
                <Button
                    className={classes.buttons}
                    onClick={() => { secondFunction(rowData) }}
                >
                    <PersonAddDisabledIcon />
                </Button>
            </Tooltip>}
            {admin && rowData.permission !== "admin" && rowData.state !== "Inactiva" && <Tooltip title={thirdTooltip}>
                <Button
                    className={classes.buttons}
                    onClick={() => { thirdFunction(rowData) }}
                >
                    <i className="fas fa-crown"></i>
                </Button>
            </Tooltip>}
        </div>
    )
}

const AllowedUserList = props => {

    const { admin, allowedUsers, setAllowedUsers, loading, setLoading, deleteAllowedUser, doAdmin } = props

    const classes = useStyles();
    const [drawerData, setDrawerData] = useState({});
    const [isDrawerOpen, setIsDrawerOpen] = useState(false);
    const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
    const [doAdminConfirmationOpen, setDoAdminConfirmationOpen] = useState(false);
    const [isViewDrawerOpen, setIsViewDrawerOpen] = useState(false);
    const [row, setRow] = useState(null);

    const handleViewAllowedUser = (rowData) => {
        if (!rowData) {
            return
        }
        let displayValues = {
            title: rowData.permission === 'admin' ? 'Datos del administrador' : 'Datos del colaborador',
            content: {
                "Correo electrónico": rowData._id,
                "Nombre": rowData.name,
                "Apellido": rowData.surname,
                "Puesto": rowData.position,
                "Teléfono": rowData.phone,
                "Edad": rowData.age,
            },
        }
        setDrawerData(displayValues)
        setIsViewDrawerOpen(true)
    }

    const deleteUser = (rowData) => {
        setDeleteConfirmationOpen(true);
        setRow(rowData);
    }

    const doAdminUser = rowData => {
        setDoAdminConfirmationOpen(true);
        setRow(rowData);
    }

    const chipColors = {
        GREEN: '#E8F5E9',
        B_GREEN: '#4CAF50',
        RED: '#FEEBEE',
        B_RED: '#F2453D',
        GREY: '#E8E8E8',
        B_GREY: '#808080',
        BLUE: '#E3F2FD',
        B_BLUE: '#2C98F0'
    }

    const CHIP_STATE_ALLOWED_USER_LIST = {
        type: 'chip',
        values: {
            'Activa': { ligth: chipColors.GREEN, dark: chipColors.B_GREEN },
            'Inactiva': { ligth: chipColors.RED, dark: chipColors.B_RED }
        }
    }

    const header = [
        createHeader('nameForTable', 'center', 'center', false, 'Nombre', false),
        createHeader('surname', 'center', 'center', false, 'Apellido', false),
        createHeader('position', 'center', 'center', false, 'Puesto', false),
        createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_ALLOWED_USER_LIST),
    ]

    const actionButtonsProps = {
        firstTooltip: 'Ver usuario',
        secondTooltip: 'Eliminar usuario',
        thirdTooltip: 'Hacer administrador',
        firstFunction: (rowData) => handleViewAllowedUser(rowData),
        secondFunction: (rowData) => deleteUser(rowData),
        thirdFunction: (rowData) => doAdminUser(rowData),
    }

    const handleClickNewColaborator = () => {
        setIsDrawerOpen(true);
    }

    return (
        <>
            <ThemeProvider theme={theme}>
                <DataDrawer
                    anchor="right"
                    isOpen={isViewDrawerOpen}
                    setIsOpen={setIsViewDrawerOpen}
                    vatiant="temporary"
                    data={drawerData}
                />
                <DrawerAllowedUsers
                    anchor="right"
                    isOpen={isDrawerOpen}
                    setIsOpen={setIsDrawerOpen}
                    variant="temporary"
                    data={{ title: "Nuevo colaborador" }}
                    setUsers={setAllowedUsers}
                    setLoading={setLoading}
                    loading={loading}
                />
                <Container classes={{ root: classes.marginBottom }} maxWidth="md">
                    <div style={{ display: 'flex', alignItems: 'start', justifyContent: 'flex-start', marginBottom: theme.spacing(4) }}>
                        <div className={classes.title}>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-start' }}>
                                <Typography className={classes.mainTitle}>Colaboradores</Typography>
                            </div>
                        </div>
                        {admin && <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <FormButton
                                variant="contained"
                                color="primary"
                                onClick={handleClickNewColaborator}
                            >
                                Nuevo colaborador
                        </FormButton>
                        </div>}

                    </div>
                    <ResponsiveTable
                        noDataMessage={!allowedUsers || allowedUsers.length === 0 ? 'No existen colaboradores registrados.' : 'No se encontraron colaboradores.'}
                        header={header}
                        rows={allowedUsers}
                        actions={actionButtonsProps ? <ActionButtons
                            admin={admin}
                            actionButtonsProps={actionButtonsProps}
                        /> : null}
                        loading={loading}
                    />
                </Container>
                {deleteConfirmationOpen && <ValidPassDialog
                    open={deleteConfirmationOpen}
                    title={"Eliminar colaborador"}
                    subtitle={"Ingresá tu contraseña actual para confirmar la eliminación."}
                    setOpen={setDeleteConfirmationOpen}
                    action={deleteAllowedUser}
                    row={row}
                />}
                {doAdminConfirmationOpen && <ValidPassDialog
                    open={doAdminConfirmationOpen}
                    title={"Hacer administrador"}
                    subtitle={"Ingresá tu contraseña actual para confirmar el cambio de administrador."}
                    setOpen={setDoAdminConfirmationOpen}
                    action={doAdmin}
                    row={row}
                />}
            </ThemeProvider>
        </>
    )
}

export default AllowedUserList;