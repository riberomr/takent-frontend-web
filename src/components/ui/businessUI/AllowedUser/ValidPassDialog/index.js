import React, { useState } from 'react'
import { Dialog, DialogTitle, makeStyles, Typography, DialogContent, DialogContentText, TextField, IconButton, InputAdornment, Button, DialogActions } from '@material-ui/core'
import { styles } from './styles'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import { authService } from '../../../../../services/auth.service'

const useStyles = makeStyles(styles)

const DeleteAccountDialog = (props) => {

    const { open, title, subtitle, setOpen, action, row, ...other } = props
    const classes = useStyles()
    const srcIcon = "/images/AlertIcons/warning.png"
    const [ password, setPassword ] = useState("")
    const [ showPassword, setShowPassword ] = useState(false)
    const [ passwordIncorrect, setPasswordIncorrect ] = useState(false)
    const user = JSON.parse(localStorage.getItem('takent_user'));

    const handlePasswordChange = (event) => {
        setPassword(event.target.value)
        setPasswordIncorrect(false)
    }
    
    const handleSubmit = () => {
        authService.checkPass(user._id, password)
        .then(() => {
            action(row._id);
            setOpen(false);
        })
        .catch(() => {
            setPasswordIncorrect(true)
        })
    }

    return (
        <Dialog
            maxWidth="sm"
            fullWidth
            open={open}
            onClose={() => setOpen(false)}
            className={classes.dialog}
            {...other}
        >
                <div className={classes.iconContainer}>
                    <img src={srcIcon} className={classes.icon} alt=''></img>
                </div>
                <DialogTitle disableTypography>
                    <Typography variant="h5" align="center">
                        {title}
                    </Typography>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText align="left">
                        {subtitle}
                    </DialogContentText>
                    <TextField
                        type={showPassword ? 'text' : 'password'}
                        label="Contraseña"
                        variant="standard"
                        fullWidth
                        margin="dense"
                        InputLabelProps={{shrink: true}}
                        value={password}
                        error={passwordIncorrect}
                        helperText={passwordIncorrect? "La contraseña ingresada no es correcta" : ""}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => setShowPassword(!showPassword)}
                                    >
                                        {showPassword? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        onChange={handlePasswordChange}
                    />
                </DialogContent>
                <DialogActions className={classes.dialogActions}>
                    <Button color="primary" variant="contained" onClick={() => setOpen(false)} className={classes.button}>
                        Cancelar
                    </Button>
                    <Button color="primary" variant="contained" onClick={handleSubmit} className={classes.button}>
                        Aceptar
                    </Button>
                </DialogActions>
        </Dialog>
    )
}

export default DeleteAccountDialog