export const styles = {
    headerContainer:{
        display: 'grid',
        justifyContent: 'center',
        justifyItems: 'center',
    },
    container:{
        display: 'grid',
        justifyContent: 'center'
    },
    formContainer:{
        display:'grid',
    },
    goLoginContainer:{
        display:' flex',
        justifyContent: 'center',
        marginBottom: '65px',
        marginTop: '65px'
    },
    buttonContainer:{
        display:' flex',
        justifyContent: 'center',
        marginBottom: '24px',
        marginTop: '12px'
    },
    imgLogo:{
        width: '100%',
        maxWidth: '200px',
    },
    welcomeText:{
        fontSize: '28px',
        fontFamily: 'Roboto',
        fontWeight: '700',
        lineHeight: '36px',
        letterSpacing: '-0.04em',
        margin: '0px 20px 0px 20px',
        textAlign: 'center'
    },
    formLabel:{
        paddingBottom: '8px',
        fontSize: '14px',
        lineHeight: '20px',
        letterSpacing: '0.25px',
        fontWeight: '700',
        fontFamily: 'Roboto'
    },
    formItemContainer:{
        paddingBottom: '24px',
        display: 'grid'
    },
    endingText:{
        fontSize: '12px',
        fontWeight: '400',
        lineHeight: '16px',
        letterSpacing: '0.25px',
        fontFamily: 'Roboto'
    },
    formItemText:{
        textAlign: 'center',
        paddingBottom: '12px'
    },
    goLoginSpan:{
        fontSize: '16px',
        fontWeight: '400',
        lineHeight: '24px',
        letterSpacing: '0px',
        fontFamily: 'Roboto',
    },
    linkText:{
        color: '#F29222',
        marginLeft: '5px',
        fontFamily: 'Roboto',
        textDecoration: 'none'
    },
    linkDecoration:{
        textDecoration: 'none'
    },
    errorMessage: {
        fontSize: '12px',
        color: 'rgb(244, 67, 54)',
        fontFamily: 'Roboto',
        marginTop: '8px'
    },
    centerAling:{
        display: 'flex',
        justifyContent: 'center'
    },
    formItem: {
        paddingBottom: '8px',
        paddingTop: '8px'
    },
    footerContainer:{
        display: 'flex',
        justifyContent: 'flex-end'
    },
    textWidth:{
        width:'60%',
        textAlign: 'center'
    },
    mainContainer:{
      paddingTop: '100px',
      minHeight: 'calc(100vh - 148px)'
    },
    footer:{
        position: 'absolute',
        left: 0,
        bottom: 0
    }
}