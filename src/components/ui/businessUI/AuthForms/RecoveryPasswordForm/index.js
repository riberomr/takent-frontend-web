import React from 'react'
import { Formik } from 'formik'
import FormButton from '../../../generalUI/Buttons/PrimaryButton'
import TextField from '@material-ui/core/TextField'
import { Link } from 'react-router-dom'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import recovery from '../../../../rules/validationSchema/recovery'
import { paths } from '../../../../../constants/generalConstants/paths'
import Footer from '../../../generalUI/Footer'
import { theme } from '../../../../../constants/generalConstants/theme'
import { ThemeProvider } from '@material-ui/core/styles'
import HeaderLayout from '../../../../pages/Auth/GridLayout/HeaderLayout'

const useStyles = makeStyles(styles)

const RecoveryPasswordForm= props => {
  let { handleFormSubmit, isSubmitted } = props

  const variant = 'standard'
  const classes = useStyles()
  return (
    <ThemeProvider theme={theme}>
      <HeaderLayout/>
                    <div className={classes.mainContainer}>
                        <div className={classes.headerContainer} >
                            <div className={classes.textWidth}>
                              {!isSubmitted &&
                              <span className={classes.welcomeText}>Escribí tu email, te enviaremos un correo electrónico con el cual podrás gestionar la recuperación de la contraseña</span>
                              }
                              {isSubmitted==='OK' &&
                              <span className={classes.welcomeText}>Se ha enviado un email a tu casilla de correo electrónico, con él seguirás tu gestión de recuperación de contraseña.</span>
                              }
                              {isSubmitted==='ERROR' &&
                              <span className={classes.welcomeText}>Hubo un error al enviar el email, intentá recargar la página y completar el formulario nuevamente.</span>
                              }
                            </div>
                        </div>
                            {!isSubmitted && <Formik
                                initialValues={{ account: ''}}
                                onSubmit={handleFormSubmit}
                                validationSchema={recovery}
                              >
                                {({
                                  values,
                                  handleChange,
                                  handleBlur,
                                  handleSubmit,
                                  isSubmitting,
                                  errors,
                                  touched
                                  /* and other goodies */
                                }) => (
                                  <div className={classes.container}>
                                    <form onSubmit={handleSubmit} className={classes.form}>
                                      <div className={classes.formItem}>
                                      <TextField
                                        id="account"
                                        type="text"
                                        name="account"
                                        label="Correo electrónico"
                                        variant={variant}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.account}
                                        fullWidth
                                      //   disabled={!navigator.onLine}
                                      />
                                      {errors.account && touched.account ? (
                                            <div className={classes.errorMessage}>{errors.account}</div>
                                          ) : null}
                                      </div>
                                      <div className={classes.formItem}>
                                        <FormButton
                                          type="submit"
                                          variant="contained"
                                          color="primary"
                                          disabled={isSubmitting}>
                                            Enviar mail
                                        </FormButton>
                                      </div>
                                      <div className={classes.centerAling}>
                                        <Link style={{textDecoration: 'none'}}
                                          to={paths.public.login}
                                        >
                                          <p className={classes.linkText}>Volver</p>
                                        </Link>
                                      </div>
                                    </form>
                                  </div>
                                )}
                              </Formik> }
                            {isSubmitted && 
                              <div className={classes.goLoginContainer}>
                                <Link to={paths.public.login} style={{textDecoration: 'none'}}>
                                    <span className={`${classes.linkText} ${classes.goLoginSpan}`}>Volver</span>
                                </Link>
                            </div>}
                            
                    </div>
                    <Footer/>  
        </ThemeProvider>
  )
}

export default RecoveryPasswordForm

