export const styles = {
    headerContainer:{
        display: 'grid',
        justifyContent: 'center',
        justifyItems: 'center',
        paddingTop:'100px'
    },
    container:{
        display: 'grid',
        justifyContent: 'center',
        justifyItems: 'center',
        paddingTop:'40px',
        margin: '0 35% 0px 35%'
    },
    formContainer:{
        display:'grid',
    },
    goLoginContainer:{
        display:' flex',
        justifyContent: 'center',
        paddingTop: '0px'
    },
    goLoginContainerSuccess:{
        display:' flex',
        justifyContent: 'center',
        marginBottom: '100px',
        marginTop: '100px'
    },
    buttonContainer:{
        display:' flex',
        justifyContent: 'center',
        marginBottom: '24px',
        marginTop: '12px'
    },
    buttonContainerAlone:{
        display:' flex',
        justifyContent: 'center',
        marginBottom: '24px',
        marginTop: '12px'
    },
    imgLogo:{
        width: '100%',
        maxWidth: '200px',
    },
    welcomeText:{
        fontSize: '28px',
        fontFamily: 'Roboto',
        fontWeight: '700',
        lineHeight: '36px',
        letterSpacing: '-0.04em',
        paddingTop: '40px',
        textAlign: 'center'
    },
    formLabel:{
        paddingBottom: '8px',
        fontSize: '14px',
        lineHeight: '20px',
        letterSpacing: '0.25px',
        fontWeight: '700',
        fontFamily: 'Roboto'
    },
    formItemContainer:{
        paddingBottom: '24px',
        display: 'grid'
    },
    endingText:{
        textAlign: 'center',
        fontSize: '12px',
        fontWeight: '400',
        lineHeight: '16px',
        letterSpacing: '0.25px',
        fontFamily: 'Roboto'
    },
    formItemText:{
        textAlign: 'center',
        paddingBottom: '12px'
    },
    goLoginSpan:{
        fontSize: '16px',
        fontWeight: '400',
        lineHeight: '24px',
        letterSpacing: '0px',
        fontFamily: 'Roboto'
    },
    linkText:{
        color: '#F29222',
        marginLeft: '5px'
    },
    linkDecoration:{
        textDecorationColor: '#F29222'
    },
    halfRow:{
        width: '40%'
    },
    sixtyPercertRow:{
        width: '60%'
    },
    thirdPartRow:{
        width: '33%'
    },
    multiLabelContainer:{
        display: 'flex',
        justifyContent: 'space-between',
        marginTop: '8px'
    },
    successText:{
        fontSize: '20px',
        fontFamily: 'Roboto',
        fontWeight: '700',
        lineHeight: '36px',
        letterSpacing: '-0.04em',
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center'
    },
    successDiv:{
        paddingTop: '50px',
        display: 'grid',
        justifyContent: 'center'
    },
    succesImgAndSpan:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgSuccess:{
        width: '70px',
        height: '50px',
        paddingLeft: '10px'
    },
    imgSuccessText:{
        fontSize: '28px',
        fontFamily: 'Roboto',
        fontWeight: '700',
        lineHeight: '36px',
        letterSpacing: '-0.04em',
        textAlign: 'center'
    },
    succesDivSpan:{
        display: 'block',
        justifyContent: 'center',
        paddingTop: '30px'
    },
    formRootFirst: {
        flexGrow: 1,
        padding: '50px 35% 0px 35%'
    },
    formRootLast: {
        flexGrow: 1,
        padding: '0px 25% 15px 25%'
    },
    formRoot: {
        flexGrow: 1,
        padding: '0px 35% 0px 35%'
    },
    gridItem: {
        display: 'flex',
        flexGrow: 1
    },
    gridItemCentered: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorMessage: {
        fontSize: '14px',
        color: 'rgb(244, 67, 54)',
        fontFamily: 'Roboto'
    },
    centerAlign:{
        justifyContent: 'center',
        display: 'flex'
    },
    mainContainer:{
        minHeight: 'calc(100vh - 48px)'
    },
    horizontalLine: {
        width: '70%',
        height: '1px',
        backgroundColor: '#F29222',
        margin: 'auto',
        marginTop: '45px',
        marginBottom: '45px',

     }
}