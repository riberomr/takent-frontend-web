import React, { useEffect, useState } from 'react'
import Footer from '../../../generalUI/Footer'
import { paths } from '../../../../../constants/generalConstants/paths'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Formik } from 'formik'
import FormButton from '../../../generalUI/Buttons/PrimaryButton'
import { Link } from 'react-router-dom'
import { theme } from '../../../../../constants/generalConstants/theme'
import { ThemeProvider } from '@material-ui/core/styles'
import newCompanyForm  from '../../../../rules/validationSchema/newCompanyForm'
import { format, addHours } from 'date-fns'
import { Tooltip, Grid, InputAdornment, Typography } from '@material-ui/core'
import HelpIcon from '@material-ui/icons/Help'
import { apiTakent } from '../../../../../services/API-TAKENT'
import TextField from '../../../generalUI/Inputs/CustomTextField'
import Select from '../../../generalUI/Selects/CustomSelect/CustomSelect'
import AvatarProfile from '../../../generalUI/AvatarProfile'
import HeaderLayout from '../../../../pages/Auth/GridLayout/HeaderLayout'
import GoogleMap from '../../../generalUI/GoogleMaps/GoogleMap'
import { placesHelper } from '../../../../../helpers/customHelpers/places'
import LoadingButton from '../../../generalUI/LoadingButton'

const useStyles = makeStyles(styles)

const SignInForm = (props) => {

    const { type, submitNewUser, provinces, isSubmitted, cities, setIdProvince, sectors, enterpriseTypes, loadingSubmit } = props

    const [userExists, setUserExists] = useState(false)
    const [firstStep, setFirstStep] = useState(false)
    const [profilePhoto, setProfilePhoto] = useState(null)
    const [location, setLocation] = useState(null)
    const classes = useStyles({isSubmitted})
    const initialValuesUser = {
        email: '',
        password: '',
        password2: '',
        lastName: '',
        position: '',
        name: '',
        tower: '',
        apartment: '',
        street: '',
        floor: '',
        district: '',
        number: '',
        phone: '',
        city: '',
        province: '',
        birthDay: '',
        business_name: '',
        fantasy_name: '',
        sector: '',
        enterprise_type: '',
        description: '',
        contact_email: '',
        enterprise_phone: '',
        web_page: '',
    }
    const disabledStatus = false
    useEffect(()=>{
        window.scrollTo(0, 0)
    },[firstStep])

    const validateUserExists = async (user) => {
        let exists = false
        await apiTakent.getUserExists(user).then(res => {
            exists = res.data.exists
        })
        return exists
    }

    const handleErrorsFirstStep = (errors, setFieldTouched) => {
        let flag = true
        const errorsFirstStep = ['email', 'password', 'password2', 'name','lastName', 'position', 'birthDay', 'phone']

        Object.keys(errors).forEach(element => {
            if(errorsFirstStep.includes(element)){
                flag = false
                setFieldTouched(element, true)
            }
        })
        setFirstStep(flag)
    }

    const handleProvinceChange = (value, setFieldValue) => {
        setFieldValue('city', '' )
        setIdProvince(value)
        setFieldValue('province', value )
    }

    const handleCityChange = (value, setFieldValue) => {
        setFieldValue('city', value )
    }

    const handleSectorChange = (value, setFieldValue) => {
        setFieldValue('sector', value )
    }
    const handleETChange = (value, setFieldValue) => {
        setFieldValue('enterprise_type', value )
    }

    const handleStreetChange = (value, setFieldValue) => {
      if (!value) {
        setFieldValue('number', '')
        setFieldValue('tower', '')
        setFieldValue('floor', '')
        setFieldValue('apartment', '')
        setFieldValue('postal_code', '')
      }
      setFieldValue('street', value)
    }

    const handleEmailBlur = async (event, handleBlur, hasError) => {
        handleBlur(event)
        if(!hasError) {
            const exists = await validateUserExists(event.target.value)
            setUserExists(exists)
        }
        else {
            setUserExists(false)
        }
    }

    const handleSubmit = (values) => {
        //how backend receives the body
        if(userExists || !firstStep)
            return
        let valuesToSend = {
            'user': { 
                'account': values.email,
                'password': values.password,
                'surname': values.lastName,
                'position': values.position,
                'name': values.name,
                'birth_date': format(
                    addHours(new Date(`${values.birthDay}`), 4),
                    'yyyy-MM-dd'),
                'phone': values.phone !==''? values.phone : null,
            },
            'enterprise': {
                'business_name': values.business_name,
                'fantasy_name': values.fantasy_name,
                'sector': values.sector,
                'enterprise_type': values.enterprise_type,
                'address': {
                    'district': values.district !==''? values.district : null,
                    'street': values.street !==''? values.street : null,
                    'number': values.number !==''? values.number : null,
                    'apartment': values.apartment !==''? values.apartment : null,
                    'floor': values.floor !==''? values.floor : null,
                    'postal_code': values.postal_code !==''? values.postal_code : null,
                    'city': values.city !==''? values.city : null,
                    'tower': values.tower !==''? values.tower : null,
                    'location': location? location : null,
                },
                'description': values.description !==''? values.description : null,
                'contact_email': values.contact_email !==''? values.contact_email : null,
                'profile_photo': profilePhoto? profilePhoto.profile_photo : null,
                'contact_phone': values.enterprise_phone !== ''? values.enterprise_phone : null,
                'web_page': values.web_page !== ''? values.web_page : null,
            }
            
        }
        //to backend, you must separate strings with underscore like next line
   
        submitNewUser(valuesToSend)
    }

    const variant = 'standard'

    const setAddressInfo = (addressInfo, setFieldValue) => {
      const province = placesHelper.parseProvince(addressInfo.province, provinces)
      if(province) {
        setFieldValue("province", province)
        setIdProvince(province)
      }
      placesHelper.parseCity(addressInfo.city, province)
      .then(city => {
        setFieldValue("city", city)
      })
      setFieldValue("district", addressInfo.district)
      setFieldValue("street", addressInfo.street)
      setFieldValue("number", addressInfo.number)
      setFieldValue("postal_code", placesHelper.parsePostalCode(addressInfo.postal_code))
    }
    
    return(
        <ThemeProvider theme={theme}>
            <HeaderLayout/>
            <div className={classes.mainContainer}> 
                <div className={classes.headerContainer}>
                    {type==='COMPANY' && !isSubmitted && <>
                        <Typography variant="h4">¡Registrá gratis a tu empresa para publicar ofertas laborales!</Typography>
                        {!firstStep && <Typography variant="h5" style={{paddingTop: theme.spacing(4)}}>Paso 1: Datos del administrador</Typography>}
                        {firstStep && <Typography variant="h5" style={{paddingTop: theme.spacing(4)}}>Paso 2: Datos de la empresa</Typography>}
                        </>
                    }
                    {isSubmitted==='ERROR' &&
                        <span className={classes.welcomeText}>Hubo un error al crear tu cuenta, intentá recargar la página y completar el registro nuevamente.</span>
                    }
                </div>
                {isSubmitted==='OK' &&
                    <div className={classes.successDiv}>
                        <div className={classes.succesImgAndSpan}>
                            <span className={classes.imgSuccessText}>Registro de cuenta exitoso!</span>
                            <img className={classes.imgSuccess} src="/images/AlertIcons/success.png" alt="takent" />
                        </div>
                        <div className={classes.succesDivSpan}>
                            <span className={classes.successText}>
                                Enviamos un mail a tu dirección de correo electrónico.
                                </span>
                            <span className={classes.successText}>
                                Hacé click en el botón de abajo para finalizar el registro de la cuenta.</span>
                        </div>
                    </div>
                }
                {!isSubmitted && <Formik
                    initialValues={initialValuesUser}
                    onSubmit={values => handleSubmit(values)}
                    validationSchema={newCompanyForm}
                    >
                    {({
                        values,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        errors,
                        touched,
                        setFieldValue,
                        setFieldTouched
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <div className={classes.formRootFirst}>
                                <Grid container spacing={5}>
                                {!firstStep && <>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            autoFocus
                                            id='email'
                                            type='text'
                                            name='email'
                                            label="Correo electrónico"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={(event) => handleEmailBlur(event, handleBlur, errors.email)}
                                            value={values.email}
                                            error={touched.email && (errors.email || userExists)? true : false}
                                            helperText={((errors.email || userExists) && touched.email)? errors.email || "El correo electrónico ingresado ya existe": ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='name'
                                            type='text'
                                            name='name'
                                            label="Nombre"
                                            onlyLetters
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.name}
                                            error={errors.name && touched.name}
                                            helperText={(errors.name && touched.name)? errors.name : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='lastName'
                                            type='text'
                                            name='lastName'
                                            label="Apellido"
                                            onlyLetters
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.lastName}
                                            error={errors.lastName && touched.lastName}
                                            helperText={(errors.lastName && touched.lastName)? errors.lastName : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='position'
                                            type='text'
                                            name='position'
                                            label="Puesto"
                                            lettersAndNumbers
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.position}
                                            error={errors.position && touched.position}
                                            helperText={(errors.position && touched.position)? errors.position : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='phone'
                                            name='phone'
                                            type='text'
                                            onlyNumbers
                                            label="Teléfono"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.phone}
                                            error={errors.phone && touched.phone}
                                            helperText={(errors.phone && touched.phone)? errors.phone : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='birthDay'
                                            type='date'
                                            name='birthDay'
                                            label="Fecha de nacimiento"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.birthDay}
                                            error={errors.birthDay && touched.birthDay}
                                            helperText={(errors.birthDay && touched.birthDay)? errors.birthDay : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItemCentered}>
                                        <TextField
                                            id='password'
                                            type='password'
                                            name='password'
                                            label="Contraseña"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus, 
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <Tooltip title="Se requiere entre 8 y 15 caracteres, al menos un número, una mayúscula y una minúscula">
                                                            <HelpIcon/>
                                                        </Tooltip>
                                                    </InputAdornment>
                                                )
                                            }}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.password}
                                            error={errors.password && touched.password}
                                            helperText={(errors.password && touched.password)? errors.password : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='password2'
                                            type='password'
                                            name='password2'
                                            label="Confirmar contraseña"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.password2}
                                            error={errors.password2 && touched.password2}
                                            helperText={(errors.password2 && touched.password2)? errors.password2 : ""}
                                        />
                                    </Grid>
                                    </>}
                                </Grid>
                            </div>
                            <div className={classes.formRootFirst}>
                                <Grid container spacing={5}>
                                    {firstStep && <>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='business_name'
                                            type='text'
                                            name='business_name'
                                            label="Razón social"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.business_name}
                                            error={errors.business_name && touched.business_name}
                                            helperText={(errors.business_name && touched.business_name)? errors.business_name : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='fantasy_name'
                                            type='text'
                                            name='fantasy_name'
                                            label="Nombre de fantasía"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.fantasy_name}
                                            error={errors.fantasy_name && touched.fantasy_name}
                                            helperText={(errors.fantasy_name && touched.fantasy_name)? errors.fantasy_name : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <Select
                                            id="sector"
                                            name="sector"
                                            label="Sector"
                                            readOnly={disabledStatus}
                                            variant={variant}
                                            fullWidth
                                            options={sectors}
                                            value={values.sector}
                                            onChange={(event) => handleSectorChange(event.target.value, setFieldValue)}
                                            error={errors.sector && touched.sector}
                                            helperText={(errors.sector && touched.sector)? errors.sector : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <Select
                                            id="enterprise_type"
                                            name="enterprise_type"
                                            label="Tipo de empresa"
                                            readOnly={disabledStatus}
                                            variant={variant}
                                            fullWidth
                                            options={enterpriseTypes}
                                            value={values.enterprise_type}
                                            onChange={(event) => handleETChange(event.target.value, setFieldValue)}
                                            error={errors.enterprise_type && touched.enterprise_type}
                                            helperText={(errors.enterprise_type && touched.enterprise_type)? errors.enterprise_type : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItemCentered}>
                                        <AvatarProfile 
                                            disabledStatus={disabledStatus} 
                                            setProfilePhoto={setProfilePhoto}
                                            profilePhoto={profilePhoto}
                                            />
                                    </Grid> 
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='description'
                                            type='text'
                                            name='description'
                                            label="Descripción"
                                            multiline
                                            rows={4}
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.description}
                                            error={errors.description && touched.description}
                                            helperText={(errors.description && touched.description)? errors.description : ""}
                                        />
                                    </Grid>
                                </>}
                                </Grid>
                            </div>
                            {firstStep && <Grid spacing={5}>
                                <div className={classes.horizontalLine}></div>
                            </Grid>}
                            <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    {firstStep && <>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='contact_email'
                                            type='text'
                                            name='contact_email'
                                            label="Mail de contacto"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            value={values.contact_email}
                                            error={touched.contact_email && errors.contact_email ? true : false}
                                            helperText={(errors.contact_email && touched.contact_email)? errors.contact_email : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='enterprise_phone'
                                            type='number'
                                            name='enterprise_phone'
                                            label="Teléfono"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            value={values.enterprise_phone}
                                            error={touched.enterprise_phone && errors.enterprise_phone ? true : false}
                                            helperText={(errors.enterprise_phone && touched.enterprise_phone)? errors.enterprise_phone : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <TextField
                                            id='web_page'
                                            type='text'
                                            name='web_page'
                                            label="Página web"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            value={values.web_page}
                                            error={touched.web_page && errors.web_page ? true : false}
                                            helperText={(errors.web_page && touched.web_page)? errors.web_page : ""}
                                        />
                                    </Grid>
                                    </>}
                                </Grid>
                            </div>
                            {firstStep && <Grid spacing={5}>
                                <div className={classes.horizontalLine}></div>
                            </Grid>}
                            <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    {firstStep && <>
                                    <Grid item xs={12}>
                                      <GoogleMap
                                        height="300px"
                                        interactive
                                        geocoding
                                        setExtLatLng={setLocation}
                                        setExtAddressInfo={addressInfo => setAddressInfo(addressInfo, setFieldValue)}
                                      />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <Select
                                            id="province"
                                            name="province"
                                            label="Provincia"
                                            readOnly={disabledStatus}
                                            disabled={!location}
                                            variant={variant}
                                            fullWidth
                                            options={provinces}
                                            value={values.province}
                                            onChange={(event) => handleProvinceChange(event.target.value, setFieldValue)}
                                            error={errors.province && touched.province}
                                            helperText={(errors.province && touched.province)? errors.province : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.gridItem}>
                                        <Select
                                            id="city"
                                            name="city"
                                            label="Localidad"
                                            readOnly={disabledStatus}
                                            variant={variant}
                                            disabled={values.province === ''}
                                            fullWidth
                                            options={cities}
                                            value={values.city}
                                            onChange={(event) => handleCityChange(event.target.value, setFieldValue)}
                                            error={errors.city && touched.city}
                                            helperText={(errors.city && touched.city)? errors.city : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={5} className={classes.gridItem}>
                                        <TextField
                                            id='district'
                                            type='text'
                                            name='district'
                                            label="Barrio"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus, disabled: values.city === ''}}
                                            disabled={values.city === ''}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.district}
                                            error={errors.district && touched.district}
                                            helperText={(errors.district && touched.district)? errors.district : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={4} className={classes.gridItem}>
                                        <TextField
                                            id='street'
                                            type='text'
                                            name='street'
                                            label="Calle"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            disabled={values.city === ''}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={(event) => handleStreetChange(event.target.value, setFieldValue)}
                                            onBlur={handleBlur}
                                            value={values.street}
                                            error={errors.street && touched.street}
                                            helperText={(errors.street && touched.street)? errors.street : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={3} className={classes.gridItem}>
                                        <TextField
                                            id='number'
                                            type='text'
                                            name='number'
                                            label="Número"
                                            onlyNumbers
                                            variant={variant}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.number}
                                            error={errors.number && touched.number}
                                            helperText={(errors.number && touched.number)? errors.number : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={3} className={classes.gridItem}>
                                        <TextField
                                            id='postal_code'
                                            type='text'
                                            name='postal_code'
                                            label="CP"
                                            onlyNumbers
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            disabled={values.city === ''}
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.postal_code}
                                            error={errors.postal_code && touched.postal_code}
                                            helperText={(errors.postal_code && touched.postal_code)? errors.postal_code : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={3} className={classes.gridItem}>
                                        <TextField
                                            id='tower'
                                            type='text'
                                            name='tower'
                                            label="Torre"
                                            variant={variant}
                                            disabled={values.number === ''}
                                            InputProps={{disabled: values.number === ''}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.tower}
                                            error={errors.tower && touched.tower}
                                            helperText={(errors.tower && touched.tower)? errors.tower : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={3} className={classes.gridItem}>
                                        <TextField
                                            id='floor'
                                            type='text'
                                            name='floor'
                                            label="Piso"
                                            onlyNumbers
                                            variant={variant}
                                            InputProps={{disabled: values.number === ''}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.floor}
                                            error={errors.floor && touched.floor}
                                            helperText={(errors.floor && touched.floor)? errors.floor : ""}
                                        />
                                    </Grid>
                                    <Grid item xs={3} className={classes.gridItem}>
                                        <TextField
                                            id='apartment'
                                            type='text'
                                            name='apartment'
                                            label="Departamento"
                                            variant={variant}
                                            InputProps={{disabled: values.number === ''}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.apartment}
                                            error={errors.apartment && touched.apartment}
                                            helperText={(errors.apartment && touched.apartment)? errors.apartment : ""}
                                        />
                                    </Grid>
                                    </>}
                                </Grid>
                            </div>
                            {firstStep && <Grid spacing={5}>
                                <div className={classes.horizontalLine}></div>
                            </Grid>}
                            <div className={classes.formRootLast}>
                                <Grid container spacing={5}>
                                    {firstStep && <>
                                    <Grid item xs={12} className={classes.gridItemCentered}>
                                        <span className={classes.endingText}>
                                            Al hacer clic en <span style={{'fontWeight': 'bold'}}>Registrarte</span>, aceptás los<span><Link className={classes.linkText} to='/termsAndConditions'>Términos y Condiciones de Uso</Link></span> de Takent.<br/>
                                            Para obtener más información acerca de cómo Takent recopila, utiliza, comparte y protege tus datos personales, consulta la<span><Link className={classes.linkText} to='/termsAndConditions'>Política de Privacidad de Takent</Link></span>.
                                        </span>
                                    </Grid>
                                    <Grid item xs={12} className={classes.buttonContainer}>
                                      <LoadingButton
                                        size="large"
                                        onClick={() => setFirstStep(false)}
                                        style={{marginRight: theme.spacing(2), width: "150px"}}
                                      >
                                        Volver
                                      </LoadingButton>
                                      <LoadingButton
                                        type="submit"
                                        size="large"
                                        loading={loadingSubmit}
                                        style={{width: "150px"}}
                                      >
                                        Registrarte
                                      </LoadingButton>
                                    </Grid>
                                    </>}
                                    {!firstStep && <Grid item xs={12} className={classes.buttonContainerAlone}>
                                        <FormButton
                                            variant="contained"
                                            color="primary"
                                            onClick={()=> handleErrorsFirstStep(errors, setFieldTouched)}
                                        >
                                            Siguiente
                                        </FormButton>
                                    </Grid>}
                                    <Grid item xs={12}>
                                        <div className={classes.goLoginContainer}>
                                            <span className={classes.goLoginSpan}>¿Ya tenés cuenta?</span>
                                            <Link to={paths.public.login} className={classes.linkDecoration}>
                                                <span className={`${classes.linkText} ${classes.goLoginSpan}`}>¡Iniciá sesión!</span>
                                            </Link>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </form>
                    )}
                </Formik>}
                {isSubmitted==='ERROR' && <div className={classes.goLoginContainer}>
                    <span className={classes.goLoginSpan}>¿Ya tenés cuenta?</span>
                    <Link to={paths.public.login} className={classes.linkDecoration}>
                        <span className={`${classes.linkText} ${classes.goLoginSpan}`}>¡Iniciá sesión!</span>
                    </Link>
                </div>}
                {isSubmitted==='OK' &&
                    <div className={classes.goLoginContainerSuccess}>            
                        <FormButton
                            onClick={()=> document.getElementById('linkDisabled').click()}
                            variant="contained"
                            color="primary"
                            >
                            Finalizar
                        </FormButton>
                        <Link id='linkDisabled' to={paths.public.login} className={classes.linkDisabled}/>  
                    </div>}
            </div>
            <Footer/>
        </ThemeProvider>)
}

export default SignInForm