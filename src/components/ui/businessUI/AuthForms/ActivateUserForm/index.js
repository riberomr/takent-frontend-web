import { Button, Container, Grid, ThemeProvider, Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import { paths } from '../../../../../constants/generalConstants/paths'
import { theme } from '../../../../../constants/generalConstants/theme'
import HeaderLayout from '../../../../pages/Auth/GridLayout/HeaderLayout'
import Footer from '../../../generalUI/Footer'
import CircularProgress from '@material-ui/core/CircularProgress'

const ActivateUserForm = ({
  user,
  alert,
  message,
  loading
}) => {

  return (
    <ThemeProvider theme={theme}>
      <HeaderLayout/>
      <Container maxWidth="lg" style={{paddingTop: '100px', minHeight: 'calc(100vh - 48px)', textAlign: "center"}}>
        {loading && <CircularProgress style={{margin: "auto"}}/>}
        {!loading && <Grid container spacing={2}>
          <Grid item xs={12} style={{display: 'flex', justifyContent: 'center'}}>
            <img alt="message" src={alert} style={{width: '100px', height:'75px'}}/>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" align="center">{message}</Typography>
          </Grid>
          <Grid item xs={12} style={{display: 'flex', justifyContent: 'center'}}>
            <Link to={paths.public.login} style={{textDecoration: 'none', color: 'inherit'}}>
              <Button variant="contained" color="primary" style={{color: 'black'}}>{user? "Iniciar sesión" : "Volver"}</Button>
            </Link>
          </Grid>
        </Grid>}
      </Container>
      <Footer/>
    </ThemeProvider>
  )

}

export default ActivateUserForm
