import { theme } from "../../../../../constants/generalConstants/theme"

export const styles = {
    textLink:{
        fontFamily: 'Roboto'
    },
    container:{
        width: '80%',
        minWidth: '400px',
        backgroundColor: 'white',
        borderRadius: '8px',
        border: '2px solid ' + theme.palette.primary.main,
        boxShadow: '7px 7px 7px rgba(0, 0, 0, 0.2)'
    },
    form:{
        margin:'25px',
        padding: '10px'
    },
    input:{
        borderColor: '#F29222'
    },
    centerAling:{
        justifyContent: 'center',
        display: 'flex'
    },
    errorMessage: {
        fontSize: '14px',
        color: 'rgb(244, 67, 54)',
        fontFamily: 'Roboto'
    },
    linkText:{
        fontFamily: 'Roboto',
        color: 'black',
    }
}