import React from 'react'
import { Formik } from 'formik'
import TextField from '@material-ui/core/TextField'
import { Link } from 'react-router-dom'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import login from '../../../../rules/validationSchema/login'
import { paths } from '../../../../../constants/generalConstants/paths'
import { Grid, Typography } from '@material-ui/core'
import { theme } from '../../../../../constants/generalConstants/theme'
import LoadingButton from '../../../generalUI/LoadingButton'

const useStyles = makeStyles(styles)

const LoginForm = props => {
  let { handleSubmit, isError, setIsError, loading } = props
  const classes = useStyles()

  const handleChangeCleanError = (value, setFieldValue, name) => {
    setIsError(false)
    setFieldValue(name, value)
  }
  

  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      onSubmit={values => handleSubmit(values)}
      validationSchema={login}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        errors,
        touched,
        setFieldValue
        /* and other goodies */
      }) => (
        <div className={classes.container}>
          <form onSubmit={handleSubmit} className={classes.form}>
            <Grid container spacing={5}>
              <Grid item xs={12}>
                <TextField
                  id="email"
                  type="text"
                  name="email"
                  label="Correo electrónico"
                  variant="standard"
                  InputLabelProps={{shrink: true}}
                  onChange={ev => handleChangeCleanError(ev.target.value, setFieldValue, 'email')}
                  onBlur={handleBlur}
                  value={values.email}
                  fullWidth
                  InputProps={{classes: { root: classes.input }}}
                  error={errors.email && touched.email}
                  helperText={(errors.email && touched.email)? errors.email: ""}
              />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="password"
                  type="password"
                  name="password"
                  label="Contraseña"
                  variant="standard"
                  InputLabelProps={{shrink: true}}
                  onChange={ev  => handleChangeCleanError(ev.target.value, setFieldValue, 'password')}
                  onBlur={handleBlur}
                  value={values.password}
                  fullWidth
                  InputProps={{classes: { root: classes.input }}}
                  error={errors.password && touched.password}
                  helperText={(errors.password && touched.password)? errors.password: ""}
                  />
              </Grid>
              <Grid item xs={12} className={classes.centerAling}>
                <LoadingButton
                  type="submit"
                  size="large"
                  loading={loading}
                >
                  Iniciar sesión
                </LoadingButton>
              </Grid>
            </Grid>
            {isError && <div className={classes.centerAling}>
              <p className={classes.errorMessage}>
                Correo y/o contraseña incorrectos
              </p>
            </div>}
            <Typography variant="body1" align="center" style={{marginTop: theme.spacing(2)}}>
              ¿Olvidaste tu contraseña? Hacé click <Link to={paths.public.recovery} style={{textDecoration: 'none', color: '#F29222'}}><b>aquí</b></Link>!
            </Typography>
          </form>
        </div>
      )}
    </Formik>
  )
}

export default LoginForm