import { theme } from "../../../../../constants/generalConstants/theme"

export const styles = {
    iconContainer: {
        marginTop: theme.spacing(2),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        width: '90px'
    },
    dialogActions: {
        display: 'flex',
        justifyContent: 'center',
        margin: theme.spacing(1),
    },
    button: {
        color: 'black !important'
    }
}
