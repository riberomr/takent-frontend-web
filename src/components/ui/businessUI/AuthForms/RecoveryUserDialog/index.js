import React from 'react'
import { Dialog, DialogTitle, makeStyles, Typography, DialogContent, DialogContentText, Button, DialogActions } from '@material-ui/core'
import { styles } from './styles'
import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../../services/API-TAKENT'
import { Link } from 'react-router-dom'

const useStyles = makeStyles(styles)

const RecoveryUserDialog = (props) => {

    const { open, setOpen, user, ...other } = props
    const classes = useStyles()
    const srcIcon = "/images/AlertIcons/warning.png"
    const { enqueueSnackbar } = useSnackbar()

    const recoverAccount = () => {
        apiTakent.postRecoverAccount({account: user})
        .then(() => {
            enqueueSnackbar('Email de recuperación enviado con éxito', { variant: 'success' })
        })
        .catch(() => {
            enqueueSnackbar('Error al intentar enviar el email de recuperación', { variant: 'error' })
        })
        setOpen(false)
    }

    return (
        <Dialog
            maxWidth="md"
            fullWidth
            open={open}
            onClose={() => setOpen(false)}
            {...other}
        >
                <div className={classes.iconContainer}>
                    <img src={srcIcon} className={classes.icon} alt=''></img>
                </div>
                <DialogTitle disableTypography>
                    <Typography variant="h5" align="center">
                        Cuenta eliminada
                    </Typography>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText align="left">
                        Esta cuenta se encuentra eliminada. Te podemos enviar un correo electrónico para que puedas recuperarla.
                    </DialogContentText>
                    <DialogContentText align="left">
                        Al recuperar tu cuenta aceptás los <span><Link to="/termsAndConditions" style={{color: "#F29222"}}>Términos y Condiciones de Uso de Takent</Link></span>. 
                        Para obtener más información acerca de cómo Takent recopila, utiliza, comparte y protege tus datos personales, consultá la
                        <span> <Link to="/termsAndConditions" style={{color: "#F29222"}}>Política de Privacidad de Takent</Link></span>.
                    </DialogContentText>
                </DialogContent>
                <DialogActions className={classes.dialogActions}>
                    <Button color="primary" variant="contained" onClick={() => setOpen(false)} className={classes.button}>
                        Cancelar
                    </Button>
                    <Button color="primary" variant="contained" onClick={recoverAccount} className={classes.button}>
                        Recuperar
                    </Button>
                </DialogActions>
        </Dialog>
    )
}

export default RecoveryUserDialog
