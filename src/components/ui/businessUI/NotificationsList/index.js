import { Badge, ClickAwayListener, Divider, Grow, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Paper, Popper, Typography } from '@material-ui/core'
import React from 'react'
import { styles } from './styles'
import NotificationsIcon from '@material-ui/icons/Notifications'
import { theme } from '../../../../constants/generalConstants/theme'

const useStyles = makeStyles(styles)

const Notification = props => {

  const {
    type = null,
    title = "-",
    description = "-",
    date = "-",
    viewed = false,
    entity = null,
    info = null,
    onClick = null
  } = props

  return (
    <ListItem button onClick={() => onClick(type, entity, info)}>
      <ListItemAvatar>
          <Badge color="error" variant="dot" invisible={viewed}>
            <NotificationsIcon style={{color: "#F29222"}}/>
          </Badge>
      </ListItemAvatar>
      <ListItemText 
        primary={title}
        secondary={
          <div style={{display: "flex", flexDirection: "column"}}>
            <Typography component="span" variant="body2" color="textPrimary" gutterBottom>{description}</Typography>
            <Typography component="span" variant="caption" color="textSecondary" align="right">{date}</Typography>         
          </div>
        }
      />
    </ListItem>
  )
}

const NotificationsList = props => {

  const classes = useStyles()

  const { 
    open,
    setOpen,
    anchorEl,
    notifications = [],
    onClickNotification
  } = props

  return (
    <Popper 
      open={open}
      anchorEl={anchorEl.current}
      transition
      disablePortal
      placement="bottom-end"
    >
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{ transformOrigin: placement === 'bottom-end' ? 'center top' : 'center bottom' }}
        >
          <Paper elevation={8}>
            <ClickAwayListener onClickAway={() => setOpen(false)}>
              <List className={classes.root}>
                {notifications.length === 0 && 
                  <ListItem>
                    <ListItemText secondary="No tenés notificaciones nuevas"/>
                  </ListItem>
                }
                {notifications.length !== 0 && 
                  notifications.map((notification, index) => (
                    <>
                      <Notification
                        key={index}
                        type={notification.type}
                        title={notification.title}
                        description={notification.description}
                        date={notification.date}
                        viewed={notification.viewed}
                        entity={notification.entity}
                        info={notification.info}
                        onClick={onClickNotification}
                      />
                      {index + 1 !== notifications.length && <Divider variant="inset" style={{background: theme.palette.primary.main}}/>}
                    </>
                  ))
                }
              </List>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  )

}

export default NotificationsList
