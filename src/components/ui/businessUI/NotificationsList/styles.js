export const styles = {
  root: {
    width: "500px",
    maxHeight: "70vh",
    overflow: "auto",
    '&::-webkit-scrollbar': {
      width: '0.4em'
    },
    '&::-webkit-scrollbar-track': {
      boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
      webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'orange',
      outline: '1px solid slategrey',
      borderRadius: 'inherit'
    },
  },
}
