import { theme } from "../../../../../constants/generalConstants/theme";

export const styles = {
  title: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(4)
  },
  error: {
    marginTop: theme.spacing(2),
    color: '#f44336'
  },
  paper: {
    padding: theme.spacing(4)
  },
  buttonContainer: {
    padding: theme.spacing(4, 0, 2, 0),
    display: 'flex',
    justifyContent: 'center',
    '& button': {
      width: '150px',
      height: '40px',
      margin: theme.spacing(2)
    }
  },
  addressContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > *': {
      marginRight: theme.spacing(2)
    },
    '& > button': {
      marginTop: theme.spacing(2)
    }
  }
}
