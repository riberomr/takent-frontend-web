import { Container, FormControlLabel, Grid, Switch, ThemeProvider, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React from 'react'
import { theme } from '../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import NotificationsIcon from '@material-ui/icons/Notifications'

const useStyles = makeStyles(styles)

const EnterpriseUserNotificationsConfiguration = props => {

  const classes = useStyles()

  const { notificationsConfiguration, handleConfigurationChange, adminUser } = props

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
       <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            <NotificationsIcon color="primary" style={{paddingRight: '5px'}}/>
            <Typography variant="h5">Notificaciones</Typography>
          </div>
        </div>
        <Grid container spacing={5}>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="few_available_publications"
                  checked={notificationsConfiguration.few_available_publications}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Pocas ofertas disponibles"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="offers_nearing_to_finish"
                  checked={notificationsConfiguration.offers_nearing_to_finish}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Ofertas próximas a finalizar"
            />
          </Grid>
          <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="suggest_publish_new_offer"
                  checked={notificationsConfiguration.suggest_publish_new_offer}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Sugerencias de publicación de nuevas ofertas"
            />
          </Grid>
          {adminUser && <Grid item sm={12} md={6}>
            <FormControlLabel
              control={
                <Switch
                  id="suggest_register_new_member"
                  checked={notificationsConfiguration.suggest_register_new_member}
                  color="primary"
                  onChange={event => handleConfigurationChange(event.target.id, event.target.checked)}
                />
              }
              label="Sugerencias de registro de nuevo usuario colaborador"
            />
          </Grid>}
        </Grid>
      </Container>
    </ThemeProvider>
  )

}

export default EnterpriseUserNotificationsConfiguration
