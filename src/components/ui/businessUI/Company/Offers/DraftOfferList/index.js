import React, { useState } from 'react'
import { makeStyles, Container, ThemeProvider, Typography } from '@material-ui/core'
import { theme } from '../../../../../../constants/generalConstants/theme'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import ActionButtons from '../../../../generalUI/ActionButtons'
import ViewListIcon from '@material-ui/icons/ViewList'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import { styles } from './styles'
import AlertAction from '../../../../generalUI/AlertAction'
import { useHistory } from 'react-router-dom'

const useStyles = makeStyles(styles)

const DraftOfferList = (props) => {
  const { 
      offerList,
      handleDiscardDraft,
      loading
  } = props
  
  const classes = useStyles(styles)

  const [ confirmSelectionOpen, setConfirmSelectionOpen ] = useState(false)
  const [ handleDiscard, setHandleDiscard ] = useState(null)
  const [ confirmationMessage, setConfirmationMessage ] = useState(null)

  const history = useHistory()

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmSelection = (_id) => {
    setConfirmationMessage('¿Eliminar borrador?')
    setConfirmSelectionOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        handleDiscardDraft(_id)
        setConfirmSelectionOpen(false)
      }
      if(type === 'cancel') {
        setConfirmSelectionOpen(false)
      }
    })
  }    

    const header = [
        createHeader('title', 'center', 'left', false, 'Título', false),
    ]

    const actionButtonsProps = {
        firstTooltip: 'Publicar',
        secondTooltip: 'Eliminar',
        firstFunction: rowData => history.push("/app/company/offer/new", {...rowData, idDraft: rowData._id}),
        secondFunction: rowData => confirmSelection(rowData._id)
      }
    return(
        <ThemeProvider theme={theme}>
              <Container classes={{root: classes.marginBottom}} maxWidth="md">
                <div className={classes.title}>
                    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <ViewListIcon color="primary" style={{paddingRight: '5px'}}/>
                        <Typography variant="h5">Borradores</Typography>
                    </div>
                </div>
                <ResponsiveTable
                    title={'Titulin'}
                    noDataMessage={'No se encontraron borradores.'}
                    header={header}
                    rows={offerList}
                    actions={actionButtonsProps ? <ActionButtons
                    actionButtonsProps={actionButtonsProps}
                    /> : null}
                    loading={loading}
                />
        </Container>
        {confirmSelectionOpen && <AlertAction
          isOpen={confirmSelectionOpen}
          setIsOpen={setConfirmSelectionOpen}
          actions={actions}
          type='doubt'
          title={confirmationMessage}
          loading={false}
        />}
      </ThemeProvider>
    )
}

export default DraftOfferList