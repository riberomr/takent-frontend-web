import { theme } from "../../../../../../constants/generalConstants/theme"
import { red, yellow, green } from '@material-ui/core/colors'

export const styles = {
  title: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(4)
  },
  buttonContainer: {
    padding: theme.spacing(4, 0, 2, 0),
    display: 'flex',
    justifyContent: 'center',
    '& button': {
      width: '150px',
      height: '40px',
      margin: theme.spacing(2)
    }
  },
  addressContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > *': {
      marginRight: theme.spacing(2)
    },
    '& > button': {
      marginTop: theme.spacing(2)
    }
  },
  chipColors:{
    backgroundColor: ({availableOffers}) => availableOffers && availableOffers >=3? green[800] : availableOffers && availableOffers > 0? yellow[800] : red[800]
  },
  chipColorsNull:{
    backgroundColor: 'grey'
  }
}