import { Button, Chip, Container, Grid, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, ThemeProvider, Typography, withStyles } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { styles } from './styles'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { theme } from '../../../../../../constants/generalConstants/theme';
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect';
import { apiTakent } from '../../../../../../services/API-TAKENT';
import PaymentIcon from '@material-ui/icons/Payment'
import CircularProgress from '@material-ui/core/CircularProgress'
import queryString from 'query-string'
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(styles)

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell)

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow)

const BuyOfferForm = props => {

  const [ availableOffers, setAvailableOffers ] = useState(null)
  const classes = useStyles({availableOffers})

  const { offerPacks, location } = props

  const [ comboValues, setComboValues ] = useState([])
  const [ formValues, setFormValues ] = useState({concept_id: "OFFER_SINGLE", quantity: 1})
  const [ tableContent, setTableContent ] = useState()
  const [ preferenceId, setPreferenceId ] = useState(null)

  const [ loading, setLoading ] = useState(false)

  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    const payment = queryString.parse(location.search)
    if(payment.preference_id){
      if(payment.payment_status === "approved"){
        enqueueSnackbar('Pack comprado con éxito! En instantes se actualizarán las publicaciones disponibles', { variant: 'success' })
      }
      else if(payment.payment_status === "pending"){
        enqueueSnackbar('Pago pendiente, en cuanto se acredite el pago se actualizarán las publicaciones disponibles', { variant: 'warning' })
      }
      else{
        enqueueSnackbar('Error al procesar el pago', { variant: 'error' })
      }
    }
  }, [enqueueSnackbar, location.search])

  useEffect(() => {
    if(offerPacks.length > 0) {
      setComboValues(offerPacks.map(offerPack => {
        return {code: offerPack._id, name: offerPack.common_name}
      }))
    }
  }, [offerPacks])

  useEffect(() => {
    if(offerPacks.length === 0)
      return
    const offerPackDetails = offerPacks.find(element => {
      return element._id === formValues.concept_id
    })
    const displayContent = {
      concept_id: offerPackDetails.common_name,
      unit_price: offerPackDetails.item_template.unit_price,
      quantity: formValues.quantity,
      price: formValues.quantity * offerPackDetails.item_template.unit_price
    }
    setTableContent(displayContent)
  }, [offerPacks, formValues])

  useEffect(() => {
    setFormValues(prevState => {
      return {...prevState, quantity: 1}
    })
  }, [formValues.concept_id])

  useEffect(() => {
    apiTakent.getEnterprise()
    .then(response => {
      setAvailableOffers(response.data.available_offers)
    })
  }, [])

  const handleChange = (event, input) => {
    if(!event.target) {
      return
    }
    setFormValues(prevState => {
      return {...prevState, [input]: event.target.value}
    })
    setPreferenceId(null)
    event.persist()
  } 

  useEffect(() => {
    if(!preferenceId){
      const beforeButton = document.getElementsByClassName('mercadopago-button')
      if(beforeButton.length > 0)
        beforeButton[0].remove()
      return
    }
    const scriptElemen = document.getElementById('mp-script')
    if(scriptElemen)
      scriptElemen.remove()
    const beforeButton = document.getElementsByClassName('mercadopago-button')
    if(beforeButton.length > 0)
      beforeButton[0].remove()
    const script = document.createElement('script');
    script.src = 'https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js';
    script.async = false;
    script.setAttribute('id', 'mp-script')
    script.setAttribute('data-preference-id', preferenceId);
    document.getElementById('mercadoForm').appendChild(script);
  }, [preferenceId])

  const handleConfirm = () => {
    const beforeButton = document.getElementsByClassName('mercadopago-button')
    if(beforeButton.length > 0)
      beforeButton[0].remove()
    setLoading(true)
    apiTakent.postGenerateMPPayment(formValues)
    .then(response => {
      setPreferenceId(response.data.response.id)
      setLoading(false)
    })
  }
  
  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            <ShoppingCartIcon color="primary" style={{paddingRight: '5px'}}/>
            <Typography variant="h5">Comprar publicaciones</Typography>
          </div>
          {availableOffers === null && <Chip label={"Cargando... "} classes={{colorPrimary: classes.chipColorsNull}} color="primary"/> }
          {availableOffers !== null && <Chip label={"Publicaciones disponibles: " + (availableOffers !== null? availableOffers : "-")} classes={{colorPrimary: classes.chipColors}} color="primary"/>}
        </div>
        <form>
          <Grid container spacing={5}>
            <Grid item xs={12} md={12}>
              <Select
                label="Seleccioná un pack"
                variant={"standard"}
                fullWidth
                options={comboValues}
                value={formValues.concept_id}
                onChange={e => handleChange(e, 'concept_id')}
              />
            </Grid>
            {/* <Grid item xs={12} md={4} className={classes.gridItem}>
              <TextField
                type='number'
                label="Cantidad"
                onlyNumbers
                variant={"standard"}
                fullWidth
                InputProps={{inputProps: {min: 1, disabled: formValues.concept_id !== "SINGLE"}}}
                InputLabelProps={{shrink: true}}
                value={formValues.quantity}
                onChange={e => handleChange(e, 'quantity')}
                onKeyDown={e => e.preventDefault()}
              />
            </Grid> */}
          </Grid>
        </form>
        {tableContent && <TableContainer component={Paper} style={{marginTop: theme.spacing(4)}}>
            <Table size="small">
              <caption>
                <div style={{display: "flex", alignItems: "center", justifyContent: "space-between"}}>
                  <Typography variant="h6" align="right">Total a pagar</Typography>
                  <Typography variant="h6" align="right">${tableContent.price}</Typography>
                </div>
              </caption>
              <TableHead>
                <TableRow>
                  <StyledTableCell align="center">Resumen</StyledTableCell>
                  {/* <StyledTableCell align="center">Precio unitario</StyledTableCell>
                  <StyledTableCell align="center">Cantidad</StyledTableCell> */}
                  {/* <StyledTableCell align="center">Precio total</StyledTableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                <StyledTableRow>
                  <StyledTableCell align="left">{tableContent.concept_id}</StyledTableCell>
                  {/* <StyledTableCell align="center">${tableContent.unit_price}</StyledTableCell>
                  <StyledTableCell align="center">{tableContent.quantity}</StyledTableCell> */}
                  {/* <StyledTableCell align="center">${tableContent.price}</StyledTableCell> */}
                </StyledTableRow>
              </TableBody>
            </Table>
          </TableContainer>}
          <div style={{marginTop: theme.spacing(4), display: "flex", justifyContent: 'center'}}>
            <Button
              variant="contained"
              color="primary"
              startIcon={<PaymentIcon/>}
              onClick={handleConfirm}
            >
              Confirmar compra
            </Button>
          </div>
          <form method="GET" id="mercadoForm" style={{marginTop: theme.spacing(4), display: "flex", justifyContent: 'center'}}>
            {loading && <CircularProgress/>}
          </form>
      </Container>
    </ ThemeProvider>
  )
}

export default BuyOfferForm