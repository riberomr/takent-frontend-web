import { theme } from "../../../../../../../constants/generalConstants/theme"

export const styles = {
    buttonContainer:{
        backgroundColor: 'white',
        paddingTop: '30px',
        paddingBottom: '30px',
        position: 'sticky',
        bottom: '0px',
        display: 'flex',
        justifyContent: 'space-around',
        alignSelf: 'flex-end',
        '& button': {
          width: '150px',
          height: '40px'
        }
    },
    buttons: {
        width: '150px',
        height: '40px'
    },
    titleLabel:{
        fontFamily: 'Roboto',
        fontSize: '16px',
        fontWeight: 'bold',
        paddingBottom: '10px',
        display: 'flex'
    },
    // gridItem: {
    //     display: 'flex',
    //     flexGrow: 1,
    // },
    main: {
        flexGrow: 1,
        padding: theme.spacing(6),
        paddingBottom: "0",
        display: 'grid',
        alignItems: 'flex-start'
    },
    subtitle: {
        fontFamily: 'Roboto',
        fontSize: '16px',
        margin: '2px 2px 15px 2px',
        color: 'grey'
    }
}