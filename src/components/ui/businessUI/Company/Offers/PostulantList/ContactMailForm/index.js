import React, { useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Grid, Tooltip } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import WhatsAppIcon from '@material-ui/icons/WhatsApp'
import MailIcon from '@material-ui/icons/Mail'

const useStyles = makeStyles(styles)

const ContactMailForm = (props) => {
    const { data, sendMail, sendWhatsapp } = props
    const variant = 'standard'
    const [comment, setComment] = useState('')
    const classes = useStyles()

    const email = () => {
      sendMail(data.offerId, data.postulationId, comment)
    }

    const whatsapp = () => {
      sendWhatsapp(data.offerId, data.postulationId, data.phone, comment, data.state)
    }

    return(
        <div className={classes.main}>
            <Grid container spacing={5}>
                <Grid item xs={12} className={classes.gridItem}>
                    <span className={classes.titleLabel}>{'Contactar postulante'}</span>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <span className={classes.subtitle}>Estás por contactar a {data.surname} { data.name} ({data._id}). Podés agregar opcionalmente un comentario que se adjuntará al mensaje a enviar.</span>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                    <TextField
                        multiline
                        rows={4}
                        id='comment'
                        type='text'
                        name='comment'
                        label="Comentario"
                        inputProps={{maxLength: 300}}
                        variant={variant}
                        fullWidth
                        InputLabelProps={{shrink: true}}
                        onChange={(ev)=>{setComment(ev.target.value)}}
                        value={comment}
                        // error={errors.description && touched.description}
                        // helperText={(errors.description && touched.description)? errors.description : ""}
                    />
                </Grid>
            </Grid>
            <div className={classes.buttonContainer}>
                <Tooltip title="Contactar postulante por WhatsApp">
                  <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    startIcon={<WhatsAppIcon/>}
                    style={{color: "black"}}
                    onClick={whatsapp}
                  >
                    WhatsApp
                  </Button>
                </Tooltip>
                <Tooltip title="Contactar postulante por Email">
                  <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    startIcon={<MailIcon/>}
                    style={{color: "black"}}
                    onClick={email}
                  >
                    Email
                  </Button>
                </Tooltip>
            </div>
        </div>)
}

export default ContactMailForm