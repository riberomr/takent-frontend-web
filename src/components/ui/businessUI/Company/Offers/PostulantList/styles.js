import { theme } from "../../../../../../constants/generalConstants/theme";

export const styles = {
  filtersContainer:{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    border: '1px solid #ccc',
    borderRadius: '4px',
    padding: '.35em',
    boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
  },
  itemFilterContainer:{
    width: '40%'
  },
  title: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: theme.spacing(4)
  },
  marginBottom: {
    marginBottom: theme.spacing(4)
  },
  filterButton:{
    height: '44px',
    width: '65px',
    borderRadius: '3px'
  },
}
