import { theme } from '../../../../../../../constants/generalConstants/theme'
export const styles = {  
    dialogPaper: {
       minWidth: '600px',
       border: 'solid 2px '+theme.palette.primary.main,
    },
    dialogTitleStyles: {
      margin: '0px 23px',
      display: 'flex', 
      justifyContent: 'space-between',
    },
    buttonStyles:{
      height: '44px',
      borderRadius: '3px',
      marginRight: '14px'
    },
    ageContainer:{
      display: 'flex',
      marginTop: '8px',
      marginBottom: '10px',
      justifyContent: "space-between"
    },
    containerButtons:{
      display: 'flex',
      justifyContent: 'space-evenly'
    },
    minAge:{
      width: "30%"
    }
  }