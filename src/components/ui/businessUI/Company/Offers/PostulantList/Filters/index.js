import React, { useState } from 'react'
import { Dialog, DialogTitle, DialogActions, DialogContent, FormControlLabel, Switch} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { styles } from './styles'
import Button from '../../../../../generalUI/Buttons/PrimaryButton'
import Select from '../../../../../generalUI/Selects/CustomSelect/CustomSelect'
import TextField from '../../../../../generalUI/Inputs/CustomTextField'

const useStyles = makeStyles(styles)

const Filters = (props) => {

    const variant = 'standard'
    const { open, handleClose, title , setSelectedProvince, languages, provinces, cities, selectedProvince, handleChangeFilters } = props
    const [ selectedCities, setSelectedCities ] = useState([])
    const [ selectedLanguages, setSelectedLanguages ] = useState([])
    const [ minAge, setMinAge ] = useState('')
    const [ maxAge, setMaxAge ] = useState('')
    const [errorMinAge, setErrorMinAge] = useState(false)
    const [errorMaxAge, setErrorMaxAge] = useState(false)
    const [ filterCVRequired, setFilterCVRequired ] = useState(false)

    const classes = useStyles()

    const handleSumit = () => {
      if(minAge !== '' && minAge<18) return setErrorMinAge('La edad mínima debe ser mayor o igual 18')
      if(minAge !== '' && minAge>100) return setErrorMinAge('La edad mínima no debe ser superior a 100')
      if(maxAge !== '' && maxAge>100) return setErrorMaxAge('La edad máxima no debe ser superior a 100')
      if(minAge === '' && maxAge !== '' && maxAge<18) return setErrorMaxAge('La edad máxima debe ser mayor o igual a 18')
      if(minAge !== '' && maxAge !== '' && (maxAge-minAge)<0) return setErrorMaxAge('La edad máxima debe ser mayor o igual a la edad mínima')

      let filters = {}
      if(selectedLanguages.length>0) filters = {...filters, 'language': selectedLanguages }
      if(selectedProvince !== '') filters = {...filters, 'province': selectedProvince}
      if(selectedCities.length>0) filters = {...filters, 'city': selectedCities}
      if(minAge && minAge !== '') filters = {...filters, 'minAge': minAge}
      if(maxAge && maxAge !== '') filters = {...filters, 'maxAge': maxAge}
      if(filterCVRequired) filters = {...filters, 'cvRequired': true}
      handleChangeFilters(filters)
      handleClose()
    }

    const handleSelectProvince = (id) => {
      if(id === '') {
        setSelectedCities([])
        setSelectedProvince(id)
      }
      setSelectedProvince(id)
    }

    const handleCleanFilters = () => {
      setSelectedLanguages([])
      setMinAge('')
      setMaxAge('')
      setSelectedCities([])
      setSelectedProvince(null)
    }
    return (<Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        classes={{paper: classes.dialogPaper }}
        BackdropProps={{invisible: true}}
      >
        <DialogTitle>{title}</DialogTitle>
      
        <DialogContent>
            <Select
                id="language"
                name="language"
                label="Idioma"
                variant={variant}
                fullWidth
                multiple
                options={languages}
                value={selectedLanguages}
                onChange={(event) => setSelectedLanguages(event.target.value)}
            />
            <Select
                id="province"
                name="province"
                label="Provincia"
                variant={variant}
                fullWidth
                options={provinces}
                value={selectedProvince}
                onChange={(event) => handleSelectProvince(event.target.value)}
            >
            </Select>
            <Select
                id="city"
                name="city"
                label="Localidad"
                variant={variant}
                disabled={!selectedProvince || selectedProvince === ''}
                fullWidth
                multiple
                options={cities}
                value={selectedCities}
                onChange={(event) => setSelectedCities(event.target.value)}
            />
            <div className={classes.ageContainer}>
            <TextField
                  className={classes.minAge}
                  id="minAge"
                  type="number"
                  name="minAge"
                  label="Edad mínima"
                  variant="standard"
                  inputProps={{ min: "18"}}
                  InputLabelProps={{shrink: true}}
                  onChange={ev => setMinAge(ev.target.value)}
                  value={minAge}
                  onFocus={()=>setErrorMinAge(false)}
                  error={errorMinAge}
                  helperText={errorMinAge}
                  onlyNumbers
              />
              <TextField
                  className={classes.minAge}
                  id="maxAge"
                  type="number"
                  name="maxAge"
                  label="Edad máxima"
                  variant="standard"
                  inputProps={{ min: "18"}}
                  InputLabelProps={{shrink: true}}
                  onChange={ev => setMaxAge(ev.target.value)}
                  value={maxAge}
                  onFocus={()=>setErrorMaxAge(false)}
                  error={errorMaxAge}
                  helperText={errorMaxAge}
                  onlyNumbers
              />
              <FormControlLabel
                control={
                  <Switch
                    checked={filterCVRequired}
                    onChange={event => setFilterCVRequired(event.target.checked)}
                    color="primary"
                  />
                }
                label="CV cargado"
              />
            </div>
        </DialogContent>
        <DialogActions className={classes.containerButtons}>
          <Button 
              className={classes.buttonStyles} 
              onClick={handleCleanFilters}
              variant='contained' 
              color='primary'
              >
                Borrar
            </Button>
          <Button 
            className={classes.buttonStyles} 
            onClick={handleSumit}
            variant='contained' 
            color='primary'
          >
            Filtrar
          </Button>
        </DialogActions>   
      </Dialog>)
}

export default Filters