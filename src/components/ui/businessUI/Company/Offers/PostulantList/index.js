import { Button, Container, IconButton, makeStyles, ThemeProvider, Tooltip, Typography } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import ContactPhoneIcon from '@material-ui/icons/ContactPhone'
import CheckIcon from '@material-ui/icons/Check'
import clsx from 'clsx'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import Filters from './Filters'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import AlertAction from '../../../../generalUI/AlertAction'
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled'
import DataDrawer from '../../../../generalUI/DataDrawer'
import PersonIcon from '@material-ui/icons/Person'

import FilterListIcon from '@material-ui/icons/FilterList'
import TextField from '../../../../generalUI/Inputs/CustomTextField'

const useStyles = makeStyles(styles)

const ActionButtons = props => {

  const { actionButtonsProps, rowData, className } = props
  const { firstTooltip, secondTooltip, thirdTooltip, fourthTooltip, firstFunction, secondFunction, thirdFunction, fourthFunction } = actionButtonsProps

  const classes = makeStyles({
    container: {
      display: 'flex',
      justifyContent: 'space-evenly',
      [theme.breakpoints.down('md')]: {
        justifyContent: 'flex-end',
        '& :not(:first-child)' : {
          marginLeft: '20px'
        }
      }
    },
    buttons:{
      boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.14), 0px 4px 5px rgba(0, 0, 0, 0.12), 0px 1px 10px rgba(0, 0, 0, 0.2)',
      minHeight: '40px',
      minWidth: '40px',
      borderRadius: '100%',
      backgroundColor: 'white',
      color: 'orange',
      '&&:hover ' : {
        backgroundColor: 'orange',
        color: 'white'
      },
    }
  })()
  
  return (
    <div id='action-buttons' className={clsx(classes.container, className)}>
      <Tooltip title={firstTooltip}>
        <Button
          className={classes.buttons}
          onClick={() => firstFunction(rowData)}
          disabled={["Descartado"].includes(rowData.state)}
        >
         <PersonIcon/>
        </Button>
      </Tooltip>
      <Tooltip title={secondTooltip}>
        <Button
          className={classes.buttons}
           onClick={() => { secondFunction(rowData) }}
           disabled={["Preseleccionado", "Descartado", "Contactado"].includes(rowData.state)} 
          >
          <CheckIcon />
        </Button>
      </Tooltip>
      <Tooltip title={thirdTooltip}>
        <Button
          className={classes.buttons}
          onClick={() => thirdFunction(rowData)}
          >
            <ContactPhoneIcon /> 
        </Button>
      </Tooltip>
      <Tooltip title={fourthTooltip}>
        <Button
          className={classes.buttons}
          onClick={() => fourthFunction(rowData)}
          disabled={["Descartado"].includes(rowData.state)}
          >
          <PersonAddDisabledIcon/>
        </Button>
      </Tooltip>
    </div>
  )
}

const PostulantList = props => {

  const { postulants, setViewPostulants, stateFilter, setStateFilter, contactPostulant, preselectPostulant, discardPostulant, loading } = props
  const [ openFilters, setOpenFilters ] = useState(false)
  const [ languages, setLanguages ] = useState([])
  const [ provinces, setProvinces ] = useState([])
  const [ selectedProvince, setSelectedProvince ] = useState('')
  const [ cities, setCities ] = useState([])
  const [ advancedFilters, setAdvancedFilters] = useState(null)
  const [ postulantsFiltered, setPostulantsFiltered] = useState([])

  const classes = useStyles()
  const [ drawerData, setDrawerData ] = useState({})
  const [ isDrawerOpen, setIsDrawerOpen ] = useState(false)
  const [ discardConfirmationOpen, setDiscardConfirmationOpen ] = useState(false)
  const [ handleDiscard, setHandleDiscard ] = useState(null)

  const [ nameFilter, setNameFilter ] = useState("")

  const handleViewOffer = (rowData) => {
    if(!rowData){
      return
    }
    let displayValues = {
      title: `${rowData.name} ${rowData.surname}`,
      content: {
          "Edad":rowData.age,
          "E-mail de contacto": rowData._id,
          "Teléfono": rowData.phone,
      },
      location: rowData.address,
      education: rowData.education,
      experience: rowData.experience,
      skill: rowData.skill,
      language: rowData.language,
      profilePhoto: true
    }
    setDrawerData(displayValues)
    setIsDrawerOpen(true)
    apiTakent.getProfilePhotoCommonUser(rowData._id)
    .then(response => {
      let flag = true
      if(response.data.profile_photo !== null) flag = response.data.profile_photo
      setDrawerData(prevState => {
        return {...prevState, profilePhoto: flag}
      })
    })
  }

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmDiscard = (rowData) => {
    setDiscardConfirmationOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        discardPostulant(rowData.offerId, rowData.postulationId)
        setDiscardConfirmationOpen(false)
      }
      if(type === 'cancel') {
        setDiscardConfirmationOpen(false)
      }
    })
  }

  const chipColors = {
    GREEN: '#E8F5E9',
    B_GREEN: '#4CAF50',
    RED: '#FEEBEE',
    B_RED: '#F2453D',
    GREY: '#E8E8E8',
    B_GREY: '#808080',
    BLUE: '#E3F2FD',
    B_BLUE: '#2C98F0'
  }

  const CHIP_STATE_POSTULANT_LIST = {
    type: 'chip', 
    values: { 
      'Pendiente' : { ligth : chipColors.GREY, dark : chipColors.B_GREY},
      'Preseleccionado' : { ligth : chipColors.GREEN, dark : chipColors.B_GREEN},
      'Contactado' : { ligth : chipColors.BLUE, dark : chipColors.B_BLUE},
      'Descartado' : { ligth : chipColors.RED, dark : chipColors.B_RED}
    }
  }

  const header = [
    createHeader('name', 'center', 'center', false, 'Nombre', false),
    createHeader('surname', 'center', 'center', false, 'Apellido', false),
    createHeader('age', 'center', 'center', false, 'Edad', false),
    createHeader('cv', 'center', 'center', false, 'CV', false),
    createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_POSTULANT_LIST),
  ]

  const actionButtonsProps = {
    firstTooltip: 'Ver postulante',
    secondTooltip: 'Preseleccionar postulante',
    thirdTooltip: 'Contactar postulante',
    fourthTooltip: 'Descartar postulante',
    firstFunction : (rowData) => handleViewOffer(rowData),
    secondFunction: (rowData) => preselectPostulant(rowData.offerId, rowData.postulationId),
    thirdFunction: (rowData) => contactPostulant(rowData),
    fourthFunction: (rowData) => confirmDiscard(rowData)
  }

  const handleOpenFilter = () => {
    setOpenFilters(true)
  }

  const handleCloseFilter = () => {
    setOpenFilters(false)
  }

  const handleChangeFilters = (object) => {
    setAdvancedFilters(object)
  }

  useEffect(() => {
    apiTakent.getLanguages()
    .then((response) => {
      setLanguages(response.data.map(item => {
        return item.name
      }))
    })
  }, [])

  useEffect(() => {
    apiTakent.getProvinces()
      .then(response => setProvinces(response.data))
  }, [])

  useEffect(() => {
    if(selectedProvince) {
      apiTakent.getCities(selectedProvince)
        .then(response => setCities(response.data))
    }
    else setCities([])
  }, [selectedProvince])

  const searchLanguage = (arrayFilter, arrayPostulant) => {
    let flag = false
    if(arrayPostulant) {
      arrayPostulant.forEach(item => {
        if(arrayFilter.includes(item)) flag = true 
      })
    }
    return flag
  }

  useEffect(()=> {
    let filtered = postulants
    if(stateFilter.length !== 0) filtered = postulants.filter(postulant => stateFilter.includes(postulant.state))
    if(nameFilter !== "") filtered = filtered.filter(postulant => (
      `${postulant.name} ${postulant.surname}`.toLowerCase().includes(nameFilter.toLowerCase())
    ))
    if(advancedFilters){
      if(advancedFilters.province) filtered = filtered.filter(postulant => postulant.address.province.code === advancedFilters.province)
      if(advancedFilters.city) filtered = filtered.filter(postulant => advancedFilters.city.includes(postulant.address.city.code))
      if(advancedFilters.language) filtered = filtered.filter(postulant => searchLanguage(advancedFilters.language, postulant.language.map(item => item.language)))
      if(advancedFilters.minAge && advancedFilters.maxAge) filtered = filtered.filter(postulant => (postulant.age >= advancedFilters.minAge) && (postulant.age <= advancedFilters.maxAge))
      if(advancedFilters.minAge && !advancedFilters.maxAge) filtered = filtered.filter(postulant => postulant.age >= advancedFilters.minAge)
      if(advancedFilters.maxAge && !advancedFilters.minAge) filtered = filtered.filter(postulant => postulant.age <= advancedFilters.maxAge)
      if(advancedFilters.cvRequired) filtered = filtered.filter(postulant => !postulant.cv?.props?.disabled)
    }
    setPostulantsFiltered(filtered)
  },[stateFilter, advancedFilters, postulants, nameFilter])
  
  return (
    <ThemeProvider theme={theme}>
      <DataDrawer
        anchor="right"
        isOpen={isDrawerOpen}
        setIsOpen={setIsDrawerOpen}
        variant="temporary"
        data={drawerData}
      />
      <Container classes={{root: classes.marginBottom}} maxWidth="md">
        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start', marginBottom : theme.spacing(4)}}>
          <Tooltip title="Volver a ofertas">
            <IconButton color="primary" component="span" onClick={() => setViewPostulants(false)}>
              <ArrowBackIcon/>
            </IconButton>
          </Tooltip>
          <Typography variant="h5">Postulantes</Typography>
        </div>
        <div className={classes.filtersContainer}>
          <div className={classes.itemFilterContainer}>
            <Select
              id="status"
              name="status"
              label="Estado"
              variant='standard'
              options={['Pendiente', 'Preseleccionado', 'Contactado', 'Descartado']}
              value={stateFilter}
              multiple
              onChange={event => setStateFilter(event.target.value)}
            />
          </div>
          <div className={classes.itemFilterContainer}>
            <TextField
              type='text'
              label="Nombre"
              fullWidth
              variant='standard'
              InputLabelProps={{shrink: true}}
              value={nameFilter}
              onChange={(ev)=> setNameFilter(ev.target.value)}
            />
          </div>
          <Tooltip title='Filtrar postulantes'>
                <Button
                  className={classes.filterButton}
                  variant="contained"
                  color="primary"
                  onClick={handleOpenFilter}
                  >
                  <FilterListIcon/>
                </Button>
              </Tooltip>
        </div>
        <ResponsiveTable
          noDataMessage={!postulants || postulants.length === 0? 'Esta oferta no tiene postulantes.' : 'No se encontraron postulantes.'}
          header={header}
          rows={postulantsFiltered}
          actions={actionButtonsProps ? <ActionButtons
          actionButtonsProps={actionButtonsProps}
          /> : null}
          loading={loading}
        />
      </Container>
      <Filters 
        open={openFilters} 
        handleClose={handleCloseFilter} 
        title={'Filtrar postulantes'}
        selectedProvince={selectedProvince}
        setSelectedProvince={setSelectedProvince}
        languages={languages}
        provinces={provinces}
        cities={cities}
        handleChangeFilters={handleChangeFilters}
      />
      {discardConfirmationOpen && <AlertAction
          isOpen={discardConfirmationOpen}
          setIsOpen={setDiscardConfirmationOpen}
          actions={actions}
          type='doubt'
          title={"¿Descartar postulante?"}
          loading={false}
        />}
    </ThemeProvider>
  )
}

export default PostulantList