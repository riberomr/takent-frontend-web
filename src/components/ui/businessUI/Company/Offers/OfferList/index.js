import React, { useEffect, useState } from 'react'
import { makeStyles, Container, ThemeProvider, Typography } from '@material-ui/core'
import { theme } from '../../../../../../constants/generalConstants/theme'
import ResponsiveTable from '../../../../generalUI/ResponsiveTable'
import ActionButtons from '../../../../generalUI/ActionButtons'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import ViewListIcon from '@material-ui/icons/ViewList';
import { format } from 'date-fns'
import { createHeader } from '../../../../../../helpers/generalHelpers/table'
import { styles } from './styles'
import DataDrawer from '../../../../generalUI/DataDrawer'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import AlertAction from '../../../../generalUI/AlertAction'
import { useHistory } from 'react-router-dom'
import { KeyboardDatePicker } from '@material-ui/pickers'

const useStyles = makeStyles(styles)

const isRemote = (flag) => {
  return flag? 'Si' : 'No'
}

const OfferList = (props) => {
  const { 
      offerList,
      stateFilter,
      setStateFilter,
      setFirstDateFilter,
      setLastDateFilter,
      setViewPostulants,
      setIdSelectedOffer,
      handleChageStateOffer,
      loading
  } = props
  
  const classes = useStyles(styles)

  const [ isDrawerOpen, setIsDrawerOpen ] = useState(false)
  const [ drawerData, setDrawerData ] = useState({})

  const [ confirmSelectionOpen, setConfirmSelectionOpen ] = useState(false)
  const [ handleDiscard, setHandleDiscard ] = useState(null)
  const [ confirmationMessage, setConfirmationMessage ] = useState(null)
  const [ confirmationMessageSubtitle, setConfirmationMessageSubtitle ] = useState(null)

  const [ firstDate, setFirstDate ] = useState(null)
  const [ lastDate, setLastDate ] = useState(null)

  const history = useHistory()

  const actions = [
    {
        name: 'Cancelar',
        type: 'cancel',
        handleSubmit: handleDiscard
    },
    {
        name: 'Aceptar',
        type: 'accept',
        handleSubmit: handleDiscard
    }  
  ]

  const confirmSelection = (_id, state, isProgrammed) => {
    setConfirmationMessage(isProgrammed? '¿Publicar ahora?' : state === 'in_selection'? '¿Pasar oferta a selección?' : state === 'deleted'? '¿Eliminar oferta?' : state === 'active'? '¿Activar oferta?' : '¿Finalizar oferta?');
    setConfirmationMessageSubtitle(state === 'deleted'? 'Al eliminar una oferta programada, se pierde la publicación' : null)
    setConfirmSelectionOpen(true)
    setHandleDiscard(() => (type) => {
      if(type === 'accept') {
        handleChageStateOffer(_id, state, isProgrammed)
        setConfirmSelectionOpen(false)
      }
      if(type === 'cancel') {
        setConfirmSelectionOpen(false)
      }
    })
  }

    const handleViewOffer = (rowData) => {
      if(!rowData){
        return
      }
      let date = new Date(rowData.states_history[rowData.states_history.length - 1].date);
      let displayValues = {
        title: rowData.title,
        content: {
          "Estado": rowData.state,
          "Autor": rowData.publication_user,
          "Última modificación": `${format(date, 'dd/MM/yyyy')} a las ${format(date, 'HH:mm')} hs, por ${rowData.states_history[rowData.states_history.length - 1].user === "system"? "sistema" : rowData.states_history[rowData.states_history.length - 1].user}`,
          "Second Row": [
              { "Fecha de publicación": rowData.publication_date },
              { "Días restantes": rowData.remaining_days }
            ],
          "Postulantes": rowData.postulants_quantity,
          "Sixth Row": [
            { "Puesto": rowData.position},
            { "Trabajo remoto": isRemote(rowData.remote)}
          ],
          "Rubro": rowData.sector,
          "Tipo de contrato": rowData.contract_type,
          "Experiencia": rowData.experience,
          "Eighth Row": [ 
              { "Idioma": rowData.language.language },
              { "Certificado": rowData.language.certificate? 'Si' : 'No'}
            ],
          "Descripción": rowData.description
        },
        location: rowData.address,
      }
      setDrawerData(displayValues)
      setIsDrawerOpen(true)
      apiTakent.getOfferImages(rowData._id)
      .then(response => {
        setDrawerData(prevState => {
          return {...prevState, images: response.data}
        })
      })
    }

    const handleViewPostulants = rowData => {
      setIdSelectedOffer(rowData._id)
      setViewPostulants(true)
    }

    const chipColors = {
        GREEN: '#E8F5E9',
        B_GREEN: '#4CAF50',
        RED: '#FEEBEE',
        B_RED: '#F2453D',
        YELLOW: '#FFF8E1',
        B_YELLOW: '#FFC107',
        BLUE: '#E3F2FD',
        B_BLUE: '#2C98F0',
        ORANGE: '#ffe6c7',
        B_ORANGE: '#ff8f05',
        VIOLET: '#EED9FF',
        B_VIOLET: '#9865C4'
      }

    const CHIP_STATE_OFFER_LIST = { type: 'chip', values: 
        { 
        'Activa' : { ligth : chipColors.GREEN, dark : chipColors.B_GREEN},
        'Finalizada' : { ligth : chipColors.VIOLET, dark : chipColors.B_VIOLET},
        'En selección' : { ligth : chipColors.BLUE, dark : chipColors.B_BLUE},
        'Programada': { ligth : chipColors.ORANGE, dark : chipColors.B_ORANGE},
        'Eliminada': { ligth : chipColors.RED, dark : chipColors.B_RED}
        }
    }

    const header = [
        createHeader('title', 'center', 'left', false, 'Título', false),
        createHeader('postulants_quantity', 'center', 'center', false, 'Postulantes', false),
        createHeader('publication_date', 'center', 'center', false, 'Fecha de publicación', false),
        createHeader('remaining_days', 'center', 'center', false, 'Días restantes', false),
        createHeader('state', 'center', 'center', false, 'Estado', false, CHIP_STATE_OFFER_LIST)
    ]

    const actionButtonsProps = {
        firstTooltip: 'Ver postulantes',
        secondTooltip: 'Ver oferta',
        thirdTooltip: 'Pasar a selección',
        fourthTooltip: 'Finalizar oferta',
        fifthTooltip: 'Activar oferta',
        fifthTooltiBiz: 'Publicar ahora',
        sixthTooltip: 'Republicar oferta',
        seventhTooltip: 'Editar oferta',
        eighthTooltip: 'Eliminar oferta',
        firstFunction: rowData => handleViewPostulants(rowData),
        secondFunction: rowData => handleViewOffer(rowData),
        thirdFunction: rowData => confirmSelection(rowData._id, 'in_selection'),
        fourthFunction: rowData => confirmSelection(rowData._id, 'finished'),
        fifthFunction: rowData => confirmSelection(rowData._id, 'active', rowData.state === 'Programada'),
        sixthFunction: rowData => history.push("/app/company/offer/new", rowData),
        seventhFunction: rowData => history.push("/app/company/offer/new", {...rowData, idProgrammed: rowData._id}),
        eighthFunction: rowData => confirmSelection(rowData._id, 'deleted')
      }

    const handleChangeState = (ev) => {
        setStateFilter(ev.target.value)
    }

    useEffect(() => {
      if(!isNaN(firstDate))
        setFirstDateFilter(
          firstDate? format(firstDate, "dd/MM/yyyy") : null
        )
    }, [firstDate, setFirstDateFilter])

    useEffect(() => {
      if(!isNaN(lastDate))
        setLastDateFilter(
          lastDate? format(lastDate, "dd/MM/yyyy") : null
        )
    }, [lastDate, setLastDateFilter])
    
    return(
        <ThemeProvider theme={theme}>
              <Container classes={{root: classes.marginBottom}} maxWidth="md">
                <div className={classes.title}>
                    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <ViewListIcon color="primary" style={{paddingRight: '5px'}}/>
                        <Typography variant="h5">Consultar ofertas</Typography>
                    </div>
                </div>
                <div className={classes.filtersContainer}>
                    <div className={classes.itemFilterContainer}>
                      <KeyboardDatePicker
                        label="Inicio periodo"
                        clearable
                        format="dd/MM/yyyy"
                        placeholder="dd/mm/yyyy"
                        value={firstDate}
                        onChange={date => setFirstDate(date)}
                        clearLabel="Borrar"
                        cancelLabel="Cancelar"
                        autoOk={true}
                        invalidDateMessage="Fecha no válida"
                        InputLabelProps={{shrink: true}}
                      />
                    </div>
                    <div className={classes.itemFilterContainer}>
                      <KeyboardDatePicker
                        label="Fin periodo"
                        clearable
                        format="dd/MM/yyyy"
                        placeholder="dd/mm/yyyy"
                        value={lastDate}
                        onChange={date => setLastDate(date)}
                        clearLabel="Borrar"
                        cancelLabel="Cancelar"
                        autoOk={true}
                        invalidDateMessage="Fecha no válida"
                        InputLabelProps={{shrink: true}}
                      />
                    </div>
                    <div className={classes.itemFilterContainer}>
                        <Select
                            id="status"
                            name="status"
                            label="Estado"
                            // fullWidth
                            variant='standard'
                            options={['Seleccione', 'Activa', 'En selección', 'Finalizada', 'Programada', 'Eliminada']}
                            value={stateFilter}
                            onChange={(ev) => handleChangeState(ev)}
                            // error={errors.enterprise_type && touched.enterprise_type}
                            // helperText={(errors.enterprise_type && touched.enterprise_type)? errors.enterprise_type : ""}
                        />
                    </div>
                </div>
                <ResponsiveTable
                    title={'Titulin'}
                    noDataMessage={'No se encontraron ofertas.'}
                    header={header}
                    rows={offerList}
                    actions={actionButtonsProps ? <ActionButtons
                    actionButtonsProps={actionButtonsProps}
                    /> : null}
                    loading={loading}
                />
                <DataDrawer
                  anchor="right"
                  isOpen={isDrawerOpen}
                  setIsOpen={setIsDrawerOpen}
                  variant="temporary"
                  data={drawerData}
                />
        </Container>
        {confirmSelectionOpen && <AlertAction
          isOpen={confirmSelectionOpen}
          setIsOpen={setConfirmSelectionOpen}
          actions={actions}
          type='doubt'
          title={confirmationMessage}
          subtitle={confirmationMessageSubtitle}
          loading={false}
        />}
      </ThemeProvider>
    )
}

export default OfferList