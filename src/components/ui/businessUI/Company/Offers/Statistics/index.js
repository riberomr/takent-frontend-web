import { Button, CircularProgress, Container, Grid, makeStyles, Paper, ThemeProvider, Typography } from '@material-ui/core'
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import EqualizerIcon from '@material-ui/icons/Equalizer'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"
import { DatePicker } from '@material-ui/pickers'
import { differenceInMonths, format, lastDayOfMonth } from 'date-fns'
import addMonths from 'date-fns/addMonths'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles(styles)
am4core.useTheme(am4themes_animated)
const id = "divchart"

const CompanyStatistics = props => {

  const classes = useStyles()

  const { statsComboInfo, currentStat, setCurrentStat, getStat, statInfo, graphStatsInfo, loading, dateFilter, setDateFilter } = props

  const [ dateFrom, setDateFrom ] = useState(null)
  const [ dateTo, setDateTo ] = useState(null)

  const { enqueueSnackbar } = useSnackbar()

  const chart = useRef(null)

  const handleClickButtonFilter = () => {
    let result = {}
    const currentDate = new Date()
    currentDate.setHours(0, 0, 0)
    if(dateFrom && !dateTo) {
      result.date_to = addMonths(dateFrom, 11)
      if(result.date_to > currentDate){
        result.date_to = currentDate
      }
      else {
        result.date_to = lastDayOfMonth(result.date_to)
      }
      result.date_to = format(result.date_to, "dd/MM/yyyy")
    }
    else if(dateFrom && dateTo && differenceInMonths(dateTo, dateFrom) > 11) {
      return enqueueSnackbar("El periodo entre las dos fechas no puede ser superior a 12 meses", { variant: "error" })
    }
    result = {
      date_from: dateFrom? format(dateFrom, "01/MM/yyyy") : undefined,
      date_to: dateTo? format(lastDayOfMonth(dateTo) > currentDate? currentDate : lastDayOfMonth(dateTo), "dd/MM/yyyy") : undefined,
      ...result
    }
    Object.entries(result).forEach(([key, value]) => {
      if(!value) {
        delete result[key]
      }
    })
    setDateFilter(result)
  }

  const createSimpleColumnChart = (categoryX, valueY, labelY, xAxisTitle, yAxisTitle, overflowY=false) => {
    let x = am4core.create(id, am4charts.XYChart)
    x.data = statInfo

    let categoryAxis = x.xAxes.push(new am4charts.CategoryAxis())
    categoryAxis.dataFields.category = categoryX
    categoryAxis.renderer.grid.template.location = 0
    categoryAxis.renderer.minGridDistance = 30
    categoryAxis.title.text = xAxisTitle
    categoryAxis.title.height = 25
    categoryAxis.title.dy = 25
    categoryAxis.title.fontWeight = 800

    let label = categoryAxis.renderer.labels.template
    label.truncate = true
    label.maxWidth = 120
    label.tooltipText = "{category}"

    categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
      if (overflowY && target.dataItem && target.dataItem.index % 2) {
        return dy + 25
      }
      return dy
    })

    let valueAxis = x.yAxes.push(new am4charts.ValueAxis())
    valueAxis.min = 0
    const maxValue = Math.max(...statInfo.map(stat => stat.quantity))
    valueAxis.max = maxValue > 10? maxValue : 10
    valueAxis.title.text = yAxisTitle
    valueAxis.title.fontWeight = 800

    let series = x.series.push(new am4charts.ColumnSeries())
    series.dataFields.valueY = valueY
    series.dataFields.categoryX = categoryX
    series.name = labelY
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]"
    series.columns.template.strokeOpacity = 1

    series.columns.template.adapter.add("fill", (fill, target) => {
      return x.colors.getIndex(target.dataItem.index)
    })

    series.columns.template.adapter.add("stroke", (stroke, target) => {
      return x.colors.getIndex(target.dataItem.index)
    })

    return x
  }

  const createPieChart = (category, value, colorsMap) => {
    let x = am4core.create(id, am4charts.PieChart)

    x.data = statInfo

    let pieSeries = x.series.push(new am4charts.PieSeries())
    pieSeries.dataFields.value = value
    pieSeries.dataFields.category = category
    pieSeries.slices.template.stroke = am4core.color("#fff")
    pieSeries.slices.template.strokeOpacity = 1

    if(colorsMap)
      pieSeries.slices.template.adapter.add("fill", (fill, target) => {
        return colorsMap[target.dataItem.properties.category]
      })
      pieSeries.slices.template.adapter.add("stroke", (stroke, target) => {
        return colorsMap[target.dataItem.properties.category]
      })

    pieSeries.hiddenState.properties.opacity = 1
    pieSeries.hiddenState.properties.endAngle = -90
    pieSeries.hiddenState.properties.startAngle = -90

    x.hiddenState.properties.radius = am4core.percent(0)
    
    let totalOffers = 0
    x.data.forEach(element => totalOffers += element[value])
    let label = x.createChild(am4core.Label)
    label.text = `Total de ofertas: ${totalOffers}`
    label.fontSize = 16
    label.align = "right"

    return x
  }

  const createClusteredColumnChart = (category, series, colorsMap, xAxisTitle, yAxisTitle) => {
    let x = am4core.create(id, am4charts.XYChart)
    x.colors.step = 2;

    x.legend = new am4charts.Legend()
    x.legend.position = 'top'
    x.legend.paddingBottom = 20
    x.legend.labels.template.maxWidth = 95

    let xAxis = x.xAxes.push(new am4charts.CategoryAxis())
    xAxis.dataFields.category = category
    xAxis.renderer.cellStartLocation = 0.1
    xAxis.renderer.cellEndLocation = 0.9
    xAxis.renderer.grid.template.location = 0
    xAxis.renderer.minGridDistance = 30
    xAxis.title.text = xAxisTitle
    xAxis.title.fontWeight = 800

    let allValues = []
    statInfo.forEach(stat => {
      let {month, ...properties} = stat
      allValues.push(...Object.values(properties))
    })
    const maxValue = Math.max(...allValues)

    let yAxis = x.yAxes.push(new am4charts.ValueAxis())
    yAxis.min = 0
    yAxis.max = maxValue > 10? maxValue : 10
    yAxis.title.text = yAxisTitle
    yAxis.title.fontWeight = 800

    function createSeries(value, name) {
      let series = x.series.push(new am4charts.ColumnSeries())
      series.dataFields.valueY = value
      series.dataFields.categoryX = category
      series.name = name
      if(colorsMap?.[value])
        series.fill = colorsMap[value]
        series.stroke = colorsMap[value]
      series.columns.template.strokeOpacity = 1
  
      series.events.on("hidden", arrangeColumns);
      series.events.on("shown", arrangeColumns);

      series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]"
  
      // let bullet = series.bullets.push(new am4charts.LabelBullet())
      // bullet.interactionsEnabled = false
      // bullet.dy = 13;
      // bullet.label.text = '{valueY}'
      // bullet.label.fill = am4core.color('#ffffff')
  
      return series
    }

    x.data = statInfo

    Object.entries(series).forEach(([value, name]) => {
      createSeries(value, name)
    })

    function arrangeColumns() {
      let series = x.series.getIndex(0);
  
      let w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
      if (series.dataItems.length > 1) {
          let x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
          let x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
          let delta = ((x1 - x0) / x.series.length) * w;
          if (am4core.isNumber(delta)) {
              let middle = x.series.length / 2;
  
              let newIndex = 0;
              x.series.each(function(series) {
                  if (!series.isHidden && !series.isHiding) {
                      series.dummyData = newIndex;
                      newIndex++;
                  }
                  else {
                      series.dummyData = x.series.indexOf(series);
                  }
              })
              let visibleCount = newIndex;
              let newMiddle = visibleCount / 2;
  
              x.series.each(function(series) {
                  let trueIndex = x.series.indexOf(series);
                  let newIndex = series.dummyData;
  
                  let dx = (newIndex - trueIndex + middle - newMiddle) * delta
  
                  series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                  series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
              })
            }
      }
    }
    return x
  }

  useEffect(() => {
    getStat(currentStat)
  // eslint-disable-next-line
  }, [currentStat, dateFilter])

  useLayoutEffect(() => {
    if(loading || !graphStatsInfo || !statInfo)
      return

    let x
    switch(graphStatsInfo.graphType){
      case "XYChart":
        x = createSimpleColumnChart(graphStatsInfo.x, graphStatsInfo.y, graphStatsInfo.labelY, graphStatsInfo.xAxisTitle, graphStatsInfo.yAxisTitle, graphStatsInfo.overflowY ?? false)
        break
      case "PieChart":
        x = createPieChart(graphStatsInfo.category, graphStatsInfo.value, graphStatsInfo.colors_map)
        break
      case "ClusteredColumnChart":
        x = createClusteredColumnChart(graphStatsInfo.category, graphStatsInfo.series, graphStatsInfo.colors_map, graphStatsInfo.xAxisTitle, graphStatsInfo.yAxisTitle)
        break
      default:
        x = null
    }

    chart.current = x
    chart.current.exporting.menu = new am4core.ExportMenu()
    chart.current.exporting.filePrefix = `${currentStat}_${format(new Date(), "ddMMyyHHmm")}`

    return () => {
      if(x)
        x.dispose()
    }
    // eslint-disable-next-line
  }, [loading])

  return (
    <ThemeProvider theme={theme}>
      <Container classes={{root: classes.marginBottom}} maxWidth="md">
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
              <EqualizerIcon color="primary" style={{paddingRight: '5px'}}/>
              <Typography variant="h5">Estadísticas</Typography>
          </div>
        </div>
        <Grid container spacing={5}>
          <Grid item xs={12}>
            <Select
              label="Estadística"
              variant={"standard"}
              options={statsComboInfo}
              value={currentStat}
              onChange={e => setCurrentStat(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} style={{display: "flex", alignItems: "center", justifyContent: "center"}}>
            <DatePicker
              style={{marginRight: theme.spacing(5)}}
              views={["year", "month"]}
              clearable
              clearLabel="Borrar"
              cancelLabel="Cancelar"
              label="Mes desde"
              value={dateFrom || null}
              disableFuture={true}
              autoOk={true}
              maxDate={dateTo || undefined}
              emptyLabel=" "
              onChange={setDateFrom}
            />
            <DatePicker
              style={{marginRight: theme.spacing(5)}}
              views={["year", "month"]}
              clearable
              clearLabel="Borrar"
              cancelLabel="Cancelar"
              label="Mes hasta"
              value={dateTo || null}
              disableFuture={true}
              autoOk={true}
              minDate={dateFrom || undefined}
              emptyLabel=" "
              onChange={setDateTo}
            />
            <Button
              variant="contained"
              color="primary"
              style={{color: "black"}}
              onClick={handleClickButtonFilter}
            >
              Filtrar
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper} elevation={5}>
              {loading && <CircularProgress />}
              {!loading && <Typography variant="h5" gutterBottom>{statsComboInfo.find(item => item.code === currentStat).name}</Typography>}
              <div id={id} style={{ width: "100%", height: "100%", display: loading? "none":"block"}}></div>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </ThemeProvider>
  )

}

export default CompanyStatistics
