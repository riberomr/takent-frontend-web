import { Chip, Container, Fab, FormControlLabel, Grid, makeStyles, Switch, ThemeProvider, Tooltip, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { styles } from './styles'
import PostAddIcon from '@material-ui/icons/PostAdd'
import { Formik } from 'formik'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import CompanyNewUserForm from '../../../../../rules/validationSchema/companyNewOfferForm'
import AlertAction from '../../../../generalUI/AlertAction'
import ImagesUpload from '../../../../generalUI/ImagesUpload'
import AddressDialog from '../../../../generalUI/AddressDialog'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import BusinessIcon from '@material-ui/icons/Business'
import _ from 'underscore'
import { useSnackbar } from 'notistack'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import { useHistory } from 'react-router-dom'
import { addHours, differenceInCalendarDays, format } from 'date-fns'
import LoadingButton from '../../../../generalUI/LoadingButton'

const dateFromPicker = (date) => {
  if(!date || date === '') return
  return addHours(new Date(date), 3)
}
const useStyles = makeStyles(styles)

const NewOfferForm = (props) => {

  const [ availableOffers, setAvailableOffers ] = useState(null)
  const classes = useStyles({availableOffers})
  const fieldVariant = "standard"

  const { defaultSector,
      enterpriseAddress,
      sectors, experiences, contractTypes, languages, provinces, cities, setSelectedProvince, submitOffer, location, loading } = props

  const [ isCancelOpen, setIsCancelOpen ] = useState(false)
  const [ isConfirmationOpen, setIsConfirmationOpen ] = useState(false)
  const [ isConfirmationDraftOpen, setIsConfirmationDraftOpen ] = useState(false)
  const [ isAddressDialogOpen, setIsAddressDialogOpen ] = useState(false)
  const [ isProgrammationOpen, setIsProgramationOpen ] = useState(false)
  const [ uploadedImages, setUploadedImages ] = useState([])
  const [ addressEmpty, setAddressEmpty ] = useState(false)

  const [ programmedDate, setProgrammedDate ] = useState('')
  const [ errorProgrammed, setErrorProgrammed ] = useState(false) 
  const history = useHistory()

  const [ address, setAddress ] = useState(() => {
    let initialAddress = {}
    if(location.state?.address?.province)
    {
      initialAddress = {
        ...location.state.address,
        province: location.state.address.province.code,
        city: location.state.address.city.code,
      }
    }
    return initialAddress
  })

  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    if(location.state?._id) {
      apiTakent.getOfferImages(location.state._id)
      .then(response => {
        setUploadedImages(response.data)
      })
    }
    else {
      setUploadedImages([])
    }
    if(location.state?.publication_date_not_formated && location.state?.state === 'Programada')
     setProgrammedDate(format(new Date(location.state.publication_date_not_formated),'yyyy-MM-dd'))
  }, [location.state])

  useEffect(() => {
    apiTakent.getEnterprise()
    .then(response => {
      setAvailableOffers(response.data.available_offers)
    })
  }, [isConfirmationOpen, isProgrammationOpen])

  useEffect(() => {
    if(address.province && addressEmpty){
      setAddressEmpty(false)
    }
    setSelectedProvince(address.province)
  }, [address, addressEmpty, setSelectedProvince])

  const initialValues = {
    title: location.state?.title ?? "",
    position: location.state?.position ?? "",
    sector: location.state?.sector ?? defaultSector,
    experience: location.state?.experience ?? experiences[0],
    contract_type: location.state?.contract_type ?? contractTypes[1],
    language: location.state?.language?.language ?? "ESPAÑOL",
    certificate: location.state?.language?.certificate ?? false,
    description: location.state?.description ?? "",
    idDraft: location.state?.idDraft?? null,
    idProgrammed: location.state?.idProgrammed?? null,
    remote: location.state?.remote? "Si" : "No",
    cv_required: location.state?.cv_required ?? false
  }
  
  const handleSubmit = (values) => {
    if(!address.province) {
      setAddressEmpty(true)
      return
    }
    if(values.idProgrammed) setIsProgramationOpen(true)
    else setIsConfirmationOpen(true)
  }

  const submit = (values, type) => {
    if(!address.province && type!== 'draft') {
      setAddressEmpty(true)
      return
    }
    if((availableOffers === 0 && type === 'accept' && !values.idProgrammed)) {
      // No limpiar form cuando no se tiene ofertas disponibles
      // history.push('/app/company/offer/new', null)
      // setAddress({})
      // setUploadedImages([])
      enqueueSnackbar('No tenés publicaciones disponibles, accedé a la sección "Comprar publicaciones" para obtener más', { variant: 'error' })
      return false
    }
    const { language, certificate, ...other} = values
    let submitJson = {
      ...other,
      language: {
        language: language,
        certificate: certificate
      },
      address: address? _.omit(address, "province") : null,
      images: uploadedImages? uploadedImages : null
    }

    if(values.remote === 'No') submitJson = {...submitJson, remote: false}
    if(values.remote === 'Si') submitJson = {...submitJson, remote: true}
    
    if(type === 'draft' && !location?.state?.idDraft) submitJson = {...submitJson, state: 'draft'}
    if(type === 'draft' && location?.state?.idDraft) submitJson = {...submitJson, idDraft: location.state.idDraft}
    if(type === 'accept' && submitJson.idDraft && submitJson.state === 'draft') submitJson = {...submitJson, state: 'active', publication_date: new Date()}
    submitOffer(submitJson, type)
    history.push('/app/company/offer/new', null)
    setUploadedImages([])
    setAddress({})
  }

  const getCancelAlertActions = (resetForm, values) => {
  
    const handleSubmit = (type) => {
      if(type === 'draft') {
        submit(values, type)
      }
      if(["accept", "draft"].includes(type)) {
        history.push('/app/company/offer/new', null)
        setAddress({})
        resetForm()
        setAddressEmpty(false)
      }
      setIsCancelOpen(false)
    }

    return [
      {
        name: 'No',
        type: 'cancel',
        handleSubmit: handleSubmit
      },
      {
          name: 'Si',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]
  }

  const getConfirmationAlertActions = (resetForm, values) => {

    const handleSubmit = (type) => {
      if(type === 'programmed') {
        setIsProgramationOpen(true)
      }
      if(type === "accept") {
        if(submit(values, type) !== false)
          resetForm()
      }
      setIsConfirmationOpen(false)
      window.scrollTo(0, 0)
    }

    return [
      {
        name: 'Programar',
        type: 'programmed',
        handleSubmit: handleSubmit
      }, 
      {
          name: 'No',
          type: 'cancel',
          handleSubmit: handleSubmit
      },  
      {
          name: 'Si',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]
  }

  const getDraftConfirmationActions = (resetForm, values) => {
    
    const handleSubmit = (type) => {
      if(type === "accept") {
        if(values.title === "") {
          setIsConfirmationDraftOpen(false)
          return enqueueSnackbar('Para guardar una oferta en borrador, debe tener un título', { variant: 'error' })
        }
        submit(values, "draft")
        resetForm()
        setAddress({})
        setAddressEmpty(false)
      }
      setIsConfirmationDraftOpen(false)
    }

    return [
      {
          name: 'No',
          type: 'cancel',
          handleSubmit: handleSubmit
      },  
      {
          name: 'Si',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]

  }

  const getConfirmationProgrammationActions = (resetForm, values) => {

    const handleSubmit = (type) => {
      if(type === 'cancel') {
        setIsProgramationOpen(false)
        initialValues.idProgrammed? setProgrammedDate(format(new Date(location.state.publication_date_not_formated),'yyyy-MM-dd')) : setProgrammedDate('')
        window.scrollTo(0, 0)
        return
      }
      if(type === "accept" && !initialValues.idProgrammed) {
        if(!programmedDate || programmedDate === '' || !dateFromPicker(programmedDate)){
          setErrorProgrammed('Tenés que seleccionar una fecha para la programación')
          return
        }
        if(differenceInCalendarDays(dateFromPicker(programmedDate), new Date())<1){
          setErrorProgrammed('Podés programar publicaciones sólo a fechas futuras')
          return
        }
        if(differenceInCalendarDays(dateFromPicker(programmedDate), new Date())>30){
          setErrorProgrammed('Podés programar publicaciones a 30 días cómo máximo')
          return
        }
      } else {
        if(!programmedDate || programmedDate === '' || !dateFromPicker(programmedDate)){
          setErrorProgrammed('Tenés que seleccionar una fecha para la programación')
          return
        }
        if(differenceInCalendarDays(dateFromPicker(programmedDate), new Date())<1){
          setErrorProgrammed('Podés programar publicaciones sólo a fechas futuras')
          return
        }
        if(differenceInCalendarDays(dateFromPicker(programmedDate), new Date(location.state.publication_date_not_formated))>0){
          setErrorProgrammed('No puedes extender la programación de una oferta')
          return
        }
      }
      if(type === 'accept') { 
        let body = {...values, publication_date: dateFromPicker(programmedDate)}
        if(!initialValues.idProgrammed) body = {...body, state:'programmed'}
        submit(body, type)
        resetForm()
      }
      setIsProgramationOpen(false)
      initialValues.idProgrammed? setProgrammedDate(format(new Date(location.state.publication_date_not_formated),'yyyy-MM-dd')) : setProgrammedDate('')
      window.scrollTo(0, 0)
    }

    return [
      {
          name: 'Cancelar',
          type: 'cancel',
          handleSubmit: handleSubmit
      },  
      {
          name: 'Aceptar',
          type: 'accept',
          handleSubmit: handleSubmit
      }
    ]
  }

  const handleClickSetEnterpriseAddress = () => {
    if(enterpriseAddress.province) {
      setAddress(enterpriseAddress)
      setSelectedProvince(enterpriseAddress.province)
    }
    else {
      enqueueSnackbar('La empresa no tiene un domicilio cargado', { variant: 'error' })
    }
  }

  const handleChangeProgreammedDate = (ev) => {
    if(ev.target.value && ev.target.value !== '') setErrorProgrammed(false)
    setProgrammedDate(ev.target.value)
  }

  const handleCloseProgrammation = (boolean) => {
    setIsProgramationOpen(boolean)
    initialValues.idProgrammed? setProgrammedDate(format(new Date(location.state.publication_date_not_formated),'yyyy-MM-dd')) : setProgrammedDate('')
  }
  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="md">
        <div className={classes.title}>
          <div style={{display: 'flex', alignItems: 'center', justifyContent: 'flex-start'}}>
            <PostAddIcon color="primary" style={{paddingRight: '5px'}}/>
            <Typography variant="h5">{initialValues.idDraft? 'Editar borrador' : initialValues.idProgrammed? 'Editar oferta programada' : 'Publicar oferta'}</Typography>
          </div>
          {availableOffers === null && <Chip label={"Cargando... "} classes={{colorPrimary: classes.chipColorsNull}} color="primary"/> }
          {availableOffers !== null && <Chip label={"Publicaciones disponibles: " + (availableOffers !== null? availableOffers : "-")} classes={{colorPrimary: classes.chipColors}} color="primary"/>}
        </div>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={CompanyNewUserForm}
          enableReinitialize={true}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            errors,
            touched,
            setFieldValue,
            resetForm
          }) => (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={5}>
                  <Grid item xs={12}>
                    <TextField
                      id='title'
                      type='text'
                      name='title'
                      label="Título de la oferta"
                      variant={fieldVariant}
                      fullWidth
                      InputLabelProps={{shrink: true}}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.title}
                      error={errors.title && touched.title}
                      helperText={touched.title && errors.title? errors.title : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      id='position'
                      type='text'
                      name='position'
                      label="Puesto"
                      variant={fieldVariant}
                      fullWidth
                      InputLabelProps={{shrink: true}}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.position}
                      error={errors.position && touched.position}
                      helperText={touched.position && errors.position? errors.position : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <Select
                      id="remote"
                      name="remote"
                      label="Trabajo remoto"
                      variant={fieldVariant}
                      fullWidth
                      options={['Si', 'No']}
                      value={values.remote}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.remote && touched.remote}
                      helperText={(errors.remote && touched.remote)? errors.remote : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <FormControlLabel
                      control={
                        <Switch
                          id="cv_required"
                          label="cv_required"
                          checked={values.cv_required}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          color="primary"
                        />
                      }
                      label="CV requerido"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Select
                      id="sector"
                      name="sector"
                      label="Rubro"
                      variant={fieldVariant}
                      fullWidth
                      options={sectors}
                      value={values.sector}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.sector && touched.sector}
                      helperText={(errors.sector && touched.sector)? errors.sector : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Select
                      id="experience"
                      name="experience"
                      label="Experiencia"
                      variant={fieldVariant}
                      fullWidth
                      options={experiences}
                      value={values.experience}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.experience && touched.experience}
                      helperText={(errors.experience && touched.experience)? errors.experience : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Select
                      id="contract_type"
                      name="contract_type"
                      label="Tipo de contrato"
                      variant={fieldVariant}
                      fullWidth
                      options={contractTypes}
                      value={values.contract_type}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.contract_type && touched.contract_type}
                      helperText={(errors.contract_type && touched.contract_type)? errors.contract_type : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <Select
                      id="language"
                      name="language"
                      label="Idioma"
                      variant={fieldVariant}
                      fullWidth
                      options={languages}
                      value={values.language}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errors.language && touched.language}
                      helperText={(errors.language && touched.language)? errors.language : ""}
                    />
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <FormControlLabel
                      control={
                        <Switch
                          id="certificate"
                          label="certificate"
                          checked={values.certificate}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          color="primary"
                        />
                      }
                      label="Certificado"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      id='description'
                      type='text'
                      name='description'
                      label="Descripción"
                      variant={fieldVariant}
                      multiline
                      rows={4}
                      fullWidth
                      InputLabelProps={{shrink: true}}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.description}
                      error={touched.description && errors.description}
                      helperText={touched.description && errors.description? errors.description : ""}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Ubicación</Typography>
                    <div className={classes.addressContainer}>
                      {address.province? (
                        <div>
                          <Typography variant="subtitle2">
                            {provinces.find(element => element.code === address.province)?.name}
                            {address.postal_code && ` (${address.postal_code})`},
                            {" " + cities.find(element => element.code === address.city)?.name}
                          </Typography>
                          <Typography variant="body2">
                            {address.district && `${address.district}, `}
                            {`${address.street} ${address.number? address.number : ''}`}
                          </Typography>
                          {address.tower && <Typography variant="body2">
                            Torre {address.tower}, Piso {address.floor}, Departamento {address.apartment}
                          </Typography>}
                        </div>): (
                          <Typography variant="subtitle2" color="textSecondary">No se añadió ninguna ubicación</Typography>
                        )}
                      <div style={{marginTop: theme.spacing(2)}}>
                        <Tooltip title="Utilizar domicilio de la empresa">
                          <Fab size="small" color="primary" style={{marginRight: theme.spacing(2)}} onClick={() => handleClickSetEnterpriseAddress()}>
                            <BusinessIcon />
                          </Fab>
                        </Tooltip>
                        <Tooltip title="Añadir nueva ubicación">
                          <Fab size="small" color="primary" style={{marginRight: theme.spacing(2)}} onClick={() => setIsAddressDialogOpen(true)}>
                            <EditIcon />
                          </Fab>
                        </Tooltip>
                        <Tooltip title="Eliminar ubicación">
                          <Fab size="small" color="primary" onClick={() => setAddress({})}>
                            <DeleteIcon />
                          </Fab>
                        </Tooltip>
                      </div>
                      {addressEmpty && <Typography variant="body2" className={classes.error}>
                        Debés seleccionar una ubicación
                      </Typography>}
                    </div>
                    <AddressDialog
                      isOpen={isAddressDialogOpen}
                      setIsOpen={setIsAddressDialogOpen}
                      address={address}
                      setAddress={setAddress}
                      provinces={provinces}
                      cities={cities}
                      setSelectedProvince={setSelectedProvince}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Imágenes</Typography>
                    <Typography variant="body2" color="textSecondary" style={{marginBottom: theme.spacing(3)}}>
                      Se pueden cargar hasta 6 imágenes (Tamaño máximo: 5 MB)
                    </Typography>
                    <ImagesUpload
                      images={uploadedImages}
                      setImages={setUploadedImages}
                      height={"300px"}
                      carouselLength={2}
                      imagesLimit={6}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant="body2"><strong>Importante</strong>: Los datos de la oferta no se podrán modificar una vez publicada</Typography>
                    <div className={classes.buttonContainer}>
                      <LoadingButton
                          onClick={() => setIsCancelOpen(true)}
                          size="large"
                          >
                          Cancelar
                      </LoadingButton>
                      <LoadingButton
                        onClick={() => setIsConfirmationDraftOpen(true)}
                        size="large"
                      >
                        Guardar borrador
                      </LoadingButton>
                      <LoadingButton
                          type="submit"
                          loading={loading}
                          size="large"
                          >
                          {initialValues.idProgrammed? 'Guardar' : 'Publicar'}
                      </LoadingButton>
                    </div>
                  </Grid>
                </Grid>
                {isCancelOpen && <AlertAction
                  isOpen={isCancelOpen}
                  setIsOpen={setIsCancelOpen}
                  actions={getCancelAlertActions(resetForm, values)}
                  type='doubt'
                  title={"¿Estás seguro de cancelar?"}
                  loading={false}
                  // moreThanTwoActions={true}
                />}
                {isConfirmationOpen && <AlertAction
                  isOpen={isConfirmationOpen}
                  setIsOpen={setIsConfirmationOpen}
                  actions={getConfirmationAlertActions(resetForm, values)}
                  type='doubt'
                  title={"¿Deseas publicar la oferta?"}
                  loading={false}
                />}
                {isConfirmationDraftOpen && <AlertAction
                  isOpen={isConfirmationDraftOpen}
                  setIsOpen={setIsConfirmationDraftOpen}
                  actions={getDraftConfirmationActions(resetForm, values)}
                  type='doubt'
                  title={"¿Guardar oferta en borrador?"}
                  loading={false}
                />

                }
                {isProgrammationOpen && <AlertAction
                  isOpen={isProgrammationOpen}
                  setIsOpen={handleCloseProgrammation}
                  actions={getConfirmationProgrammationActions(resetForm, values)}
                  type='doubt'
                  title={initialValues.idProgrammed? "¿Cambiar fecha de publicación?" : "¿Programar oferta?"}
                  loading={false}
                  >
                    <TextField
                      className={classes.textFieldProgrammedDate}
                      id='programmedDate'
                      type='date'
                      name='programmedDate'
                      label="Fecha publicación"
                      variant={fieldVariant}
                      fullWidth
                      InputLabelProps={{shrink: true}}
                      onChange={(ev)=> handleChangeProgreammedDate(ev)}
                      onBlur={handleBlur}
                      value={programmedDate}
                      error={errorProgrammed}
                      helperText={errorProgrammed? errorProgrammed : ""}
                    />
                   
                  </AlertAction>}
            </form>
          )}
        </Formik>
      </Container>
    </ThemeProvider>
  )
}

export default NewOfferForm
