import React from 'react'
import ProfileForm from './ProfileForm'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(styles)

const Profile = (props) => {

    const classes = useStyles()

    return (
        <div className={classes.container}>
            {props.enterprise && <ProfileForm {...props}/>}
        </div>
        )
}

export default Profile