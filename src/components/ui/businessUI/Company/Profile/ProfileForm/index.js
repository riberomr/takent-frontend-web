import React, { useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import { Grid, Tooltip, Typography } from '@material-ui/core'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
import AvatarProfile from '../../../../generalUI/AvatarProfile'
import { editCompanyData } from '../../../../../rules/validationSchema/editCompanyData'
import ImagesUpload from '../../../../generalUI/ImagesUpload'
import { theme } from '../../../../../../constants/generalConstants/theme'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import GoogleMap from '../../../../generalUI/GoogleMaps/GoogleMap'
import { placesHelper } from '../../../../../../helpers/customHelpers/places'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

const useStyles = makeStyles(styles)

const SignInForm = (props) => {

    const { admin, enterprise, submitChanges, provinces, isSubmitted, cities, setIdProvince, sectors, enterpriseTypes, setPhoto, setImages, photo, images, disabledStatus, setDisabledStatus } = props

    const [disabledAddress, setDisabledAddress] = useState(false)
    const [location, setLocation] = useState(enterprise.address.location? enterprise.address.location : null)
    const [changePhoto, setChangePhoto] = useState(false)
    const [changeImages, setChangeImages] = useState(false)
    const classes = useStyles({isSubmitted})
    const { tower, apartment, street, floor, district, city, number, province, postal_code } = enterprise.address? enterprise.address : { tower: '', apartment: '', street: '', floor: '', district: '', city: { code:'', name:''}, number: '', province: { code:'', name:''}, postal_code: '' }
    
    const initialValuesUser = {
        tower: tower,
        apartment: apartment,
        street: street,
        floor: floor,
        district: district,
        number: number,
        city: city.code,
        province: province.code,
        business_name: enterprise.business_name? enterprise.business_name : '',
        fantasy_name: enterprise.fantasy_name? enterprise.fantasy_name : '',
        sector: enterprise.sector? enterprise.sector : '',
        enterprise_type: enterprise.enterprise_type? enterprise.enterprise_type : '',
        description: enterprise.description? enterprise.description : '',
        contact_email: enterprise.contact_email? enterprise.contact_email : '',
        enterprise_phone: enterprise.contact_phone? enterprise.contact_phone : '',
        web_page: enterprise.web_page? enterprise.web_page : '',
        postal_code: postal_code
    }

    const handleProvinceChange = (value, setFieldValue) => {
        setFieldValue('city', '' )
        setIdProvince(value)
        setFieldValue('province', value )
    }

    const handleCityChange = (value, setFieldValue) => {
        setFieldValue('city', value )
    }

    const handleSectorChange = (value, setFieldValue) => {
        setFieldValue('sector', value )
    }
    const handleETChange = (value, setFieldValue) => {
        setFieldValue('enterprise_type', value )
    }

    const handleStreetChange = (value, setFieldValue) => {
        setDisabledAddress(value? false : true)
        if (!value) {
            setFieldValue('tower', '')
            setFieldValue('floor', '')
            setFieldValue('apartment', '')
        }
        setFieldValue('street', value)
    }


    const handleCancel = (setValues) => {
        setValues(initialValuesUser)
        setDisabledStatus(true)
    }

    const handleSubmit = (values) => {
        //how backend receives the body
        let body = {
            'business_name': values.business_name,
            'fantasy_name': values.fantasy_name,
            'sector': values.sector,
            'enterprise_type': values.enterprise_type,
            'address': {
                'district': values.district !==''? values.district : null,
                'street': values.street !==''? values.street : null,
                'number': values.number !==''? values.number : null,
                'apartment': values.apartment !==''? values.apartment : null,
                'floor': values.floor !==''? values.floor : null,
                'postal_code': values.postal_code !==''? values.postal_code : null,
                'city': values.city !==''? values.city : null,
                'tower': values.tower !==''? values.tower : null,
                'location': location? location : null,
            },
            'description': values.description !==''? values.description : null,
            'contact_email': values.contact_email !==''? values.contact_email : null,
            'contact_phone': values.enterprise_phone !== ''? values.enterprise_phone : null,
            'web_page': values.web_page !== ''? values.web_page : null,
        }
    
        let newPhoto = null
        if(changePhoto) {
            if(photo) newPhoto = {'profile_photo': photo.profile_photo}
            else apiTakent.deleteEnterpriseProfilePhoto()
            }
        let newImages = null 
        if(changeImages) newImages = {'images': images && images.length > 0? images : []}
        
        submitChanges(body, newPhoto, newImages)
    }

    const variant = 'standard'
    

    const handlePhoto = (data) => {
        if(!changePhoto) setChangePhoto(true)
        setPhoto(data)
    }

    const handleImages = (data) => {
        if(!changeImages) setChangeImages(true)
        setImages(data)
    }

    const setAddressInfo = (addressInfo, setFieldValue) => {
      const province = placesHelper.parseProvince(addressInfo.province, provinces)
      if(province) {
        setFieldValue("province", province)
        setIdProvince(province)
      }
      placesHelper.parseCity(addressInfo.city, province)
      .then(city => {
        setFieldValue("city", city)
      })
      setFieldValue("district", addressInfo.district)
      setFieldValue("street", addressInfo.street)
      setFieldValue("number", addressInfo.number)
      setFieldValue("postal_code", placesHelper.parsePostalCode(addressInfo.postal_code))
    }

    return(<>
         {/* <ThemeProvider theme={theme}>
             <HeaderLayout/> */}
            <div className={classes.mainContainer}> 
                <Formik
                    initialValues={initialValuesUser}
                    onSubmit={values => handleSubmit(values)}
                    validationSchema={editCompanyData}
                    >
                    {({
                        values,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        errors,
                        touched,
                        setFieldValue,
                        setValues
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <div className={classes.root}>
                            <Grid container spacing={3}>
                                <Grid item xs={9} style={{display: "flex", alignItems: "center", justifyContent: "flex-start"}}>
                                  <Tooltip
                                    title={
                                      (
                                        <Typography variant="body2">
                                          {enterprise.verified? "Empresa verificada" : "Empresa no verificada, para comenzar el proceso de verificación hacé click en la sección \"Mi cuenta\""}
                                        </Typography>
                                      )
                                    }
                                  >
                                    <CheckCircleIcon
                                      fontSize="large"
                                      style={{
                                        marginRight: theme.spacing(1),
                                        color: enterprise.verified? "#1DA1F2" : "gray"
                                      }}
                                    />
                                  </Tooltip>
                                  <span className={classes.mainTitle}>Datos de la empresa</span>
                                </Grid>
                                {admin && 
                                <Grid item xs={3}>
                                    <FormButton
                                        onClick={()=>{setDisabledStatus(!disabledStatus)}}
                                        variant="contained"
                                        color="primary"
                                        disabled={!disabledStatus}
                                        className={classes.buttonWidth}
                                        >
                                        Editar
                                    </FormButton>
                                </Grid>}
                            </Grid>
                            </div>
                            <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    <Grid item md={6} className={classes.gridItem}>
                                        <TextField
                                            className={classes.textField}
                                            id='business_name'
                                            type='text'
                                            name='business_name'
                                            label="Razón social"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.business_name}
                                            error={errors.business_name && touched.business_name}
                                            helperText={(errors.business_name && touched.business_name)? errors.business_name : ""}
                                        />
                                          <TextField
                                            className={classes.textField}
                                            id='fantasy_name'
                                            type='text'
                                            name='fantasy_name'
                                            label="Nombre de fantasía"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.fantasy_name}
                                            error={errors.fantasy_name && touched.fantasy_name}
                                            helperText={(errors.fantasy_name && touched.fantasy_name)? errors.fantasy_name : ""}
                                        />
                                          <Select
                                            className={classes.textField}
                                            id="sector"
                                            name="sector"
                                            label="Sector"
                                            readOnly={disabledStatus}
                                            variant={variant}
                                            fullWidth
                                            options={sectors}
                                            value={values.sector}
                                            onChange={(event) => handleSectorChange(event.target.value, setFieldValue)}
                                            error={errors.sector && touched.sector}
                                            helperText={(errors.sector && touched.sector)? errors.sector : ""}
                                        />
                                        <Select
                                            className={classes.textField}
                                            id="enterprise_type"
                                            name="enterprise_type"
                                            label="Tipo de empresa"
                                            readOnly={disabledStatus}
                                            variant={variant}
                                            fullWidth
                                            options={enterpriseTypes}
                                            value={values.enterprise_type}
                                            onChange={(event) => handleETChange(event.target.value, setFieldValue)}
                                            error={errors.enterprise_type && touched.enterprise_type}
                                            helperText={(errors.enterprise_type && touched.enterprise_type)? errors.enterprise_type : ""}
                                        />
                                        <TextField
                                            id='description'
                                            type='text'
                                            name='description'
                                            label="Descripción"
                                            multiline
                                            rows={4}
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.description}
                                            error={errors.description && touched.description}
                                            helperText={(errors.description && touched.description)? errors.description : ""}
                                        />
                                    </Grid>
                                    <Grid item md={6} className={classes.gridItemCentered}>
                                        <AvatarProfile 
                                            disabledStatus={disabledStatus} 
                                            setProfilePhoto={handlePhoto}
                                            profilePhoto={photo}
                                            needConfirmation
                                            />
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid container spacing={5}>
                                <div className={classes.horizontalLine}></div>
                            </Grid>
                            <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    <Grid md={12} className={classes.gridItem}>
                                        <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Contacto</Typography>
                                    </Grid>
                                    <Grid item md={4} className={classes.gridItem}>
                                        <TextField
                                            className={classes.textField}
                                            id='contact_email'
                                            type='text'
                                            name='contact_email'
                                            label="Mail de contacto"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            value={values.contact_email}
                                            error={touched.contact_email && errors.contact_email ? true : false}
                                            helperText={(errors.contact_email && touched.contact_email)? errors.contact_email : ""}
                                        />
                                    </Grid>
                                    <Grid item md={4} className={classes.gridItem}>
                                         <TextField
                                            className={classes.textField}
                                            id='web_page'
                                            type='text'
                                            name='web_page'
                                            label="Página web"
                                            variant={variant}
                                            InputProps={{readOnly: disabledStatus}}
                                            fullWidth
                                            InputLabelProps={{shrink: true}}
                                            onChange={handleChange}
                                            value={values.web_page}
                                            error={touched.web_page && errors.web_page ? true : false}
                                            helperText={(errors.web_page && touched.web_page)? errors.web_page : ""}
                                        />
                                    </Grid>
                                    <Grid item md={4} className={classes.gridItem}>
                                        <TextField
                                                className={classes.textField}
                                                id='enterprise_phone'
                                                name='enterprise_phone'
                                                label="Teléfono"
                                                variant={variant}
                                                InputProps={{readOnly: disabledStatus}}
                                                fullWidth
                                                InputLabelProps={{shrink: true}}
                                                onChange={handleChange}
                                                value={values.enterprise_phone}
                                                error={touched.enterprise_phone && errors.enterprise_phone ? true : false}
                                                helperText={(errors.enterprise_phone && touched.enterprise_phone)? errors.enterprise_phone : ""}
                                            />
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid container spacing={5}>
                                <div className={classes.horizontalLine}></div>
                            </Grid>
                            <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    <Grid md={12} className={classes.gridItem}>
                                            <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Dirección</Typography>
                                    </Grid>
                                    <Grid item md={6}className={classes.gridItem}>
                                            <Select
                                                className={classes.textField}
                                                id="province"
                                                name="province"
                                                label="Provincia"
                                                readOnly={disabledStatus}
                                                variant={variant}
                                                fullWidth
                                                options={provinces}
                                                value={values.province}
                                                onChange={(event) => handleProvinceChange(event.target.value, setFieldValue)}
                                                error={errors.province && touched.province}
                                                helperText={(errors.province && touched.province)? errors.province : ""}
                                            />
                                            <Select
                                                className={classes.textField}
                                                id="city"
                                                name="city"
                                                label="Localidad"
                                                readOnly={values.province === '' || disabledStatus}
                                                variant={variant}
                                                fullWidth
                                                options={cities}
                                                value={values.city}
                                                onChange={(event) => handleCityChange(event.target.value, setFieldValue)}
                                                error={errors.city && touched.city}
                                                helperText={(errors.city && touched.city)? errors.city : ""}
                                            />
                                            <div className={classes.multipleRow}>
                                                <TextField
                                                    className={classes.textFieldMarginRight}
                                                    id='district'
                                                    type='text'
                                                    name='district'
                                                    label="Barrio"
                                                    variant={variant}
                                                    InputProps={{readOnly:values.city === '' || disabledStatus}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.district}
                                                    error={errors.district && touched.district}
                                                    helperText={(errors.district && touched.district)? errors.district : ""}
                                                    />
                                                <TextField
                                                    className={classes.textFieldMarginRight}
                                                    id='street'
                                                    type='text'
                                                    name='street'
                                                    label="Calle"
                                                    variant={variant}
                                                    InputProps={{readOnly:values.city === '' || disabledStatus}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={(event) => handleStreetChange(event.target.value, setFieldValue)}
                                                    onBlur={handleBlur}
                                                    value={values.street}
                                                    error={errors.street && touched.street}
                                                    helperText={(errors.street && touched.street)? errors.street : ""}
                                                />
                                                <TextField
                                                    id='number'
                                                    type='text'
                                                    name='number'
                                                    label="Número"
                                                    onlyNumbers
                                                    variant={variant}
                                                    InputProps={{readOnly: disabledStatus}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.number}
                                                    error={errors.number && touched.number}
                                                    helperText={(errors.number && touched.number)? errors.number : ""}
                                                />
                                            </div>
                                            <div className={classes.multipleRow}>
                                                <TextField
                                                    className={classes.textFieldMarginRight}
                                                    id='postal_code'
                                                    type='text'
                                                    name='postal_code'
                                                    label="CP"
                                                    onlyNumbers
                                                    variant={variant}
                                                    InputProps={{readOnly: (values.city === '' || disabledStatus)}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.postal_code}
                                                    error={errors.postal_code && touched.postal_code}
                                                    helperText={(errors.postal_code && touched.postal_code)? errors.postal_code : ""}
                                                />
                                                <TextField
                                                    className={classes.textFieldMarginRight}
                                                    id='tower'
                                                    type='text'
                                                    name='tower'
                                                    label="Torre"
                                                    variant={variant}
                                                    InputProps={{readOnly: (values.street === '' || disabledStatus)}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.tower}
                                                    error={errors.tower && touched.tower}
                                                    helperText={(errors.tower && touched.tower)? errors.tower : ""}
                                                />
                                                <TextField
                                                    className={classes.textFieldMarginRight}
                                                    id='floor'
                                                    type='text'
                                                    name='floor'
                                                    label="Piso"
                                                    onlyNumbers
                                                    variant={variant}
                                                    InputProps={{readOnly: (disabledStatus || disabledAddress || values.street === '')}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.floor}
                                                    error={errors.floor && touched.floor}
                                                    helperText={(errors.floor && touched.floor)? errors.floor : ""}
                                                />
                                                <TextField
                                                    id='apartment'
                                                    type='text'
                                                    name='apartment'
                                                    label="Departamento"
                                                    variant={variant}
                                                    InputProps={{readOnly: (disabledStatus || (disabledAddress || values.street === ''))}}
                                                    fullWidth
                                                    InputLabelProps={{shrink: true}}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    value={values.apartment}
                                                    error={errors.apartment && touched.apartment}
                                                    helperText={(errors.apartment && touched.apartment)? errors.apartment : ""}
                                                />
                                            </div>
                                          </Grid>
                                          <Grid item md={6} xs={12} sm={12}>
                                            <GoogleMap
                                              height="350px"
                                              initialLatLng={location}
                                              interactive
                                              geocoding
                                              setExtLatLng={setLocation}
                                              setExtAddressInfo={addressInfo => setAddressInfo(addressInfo, setFieldValue)}
                                              disabled={disabledStatus}
                                            />
                                        </Grid>
                                    </Grid>
                                </div>
                                <Grid container spacing={5}>
                                    <div className={classes.horizontalLine}></div>
                                </Grid>
                                <div className={classes.formRoot}>
                                    <Grid container spacing={5}>
                                        <Grid xs={12}>
                                            <Typography variant="h6" style={{marginBottom: theme.spacing(3)}}>Imágenes</Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography variant="body2" color="textSecondary" style={{marginBottom: theme.spacing(3)}}>
                                                Se pueden cargar hasta 6 imágenes (Tamaño máximo: 5 MB)
                                            </Typography>
                                            <ImagesUpload
                                                images={images}
                                                setImages={handleImages}
                                                height={"300px"}
                                                carouselLength={2}
                                                imagesLimit={6}
                                                disabled={disabledStatus}
                                            />
                                        </Grid>
                                    </Grid>
                                </div> 
                            {admin && <div className={classes.formRoot}>
                                <Grid container spacing={5}>
                                    <Grid item md={12} className={classes.buttonContainer}>
                                        <FormButton
                                            disabled={disabledStatus}
                                            variant="contained"
                                            color="primary"
                                            onClick={()=> {
                                                handleCancel(setValues)}}
                                        >
                                            Cancelar
                                        </FormButton>
                                        <FormButton
                                            disabled={disabledStatus}
                                            type="submit"
                                            variant="contained"
                                            color="primary"
                                        >
                                            Guardar
                                        </FormButton>
                                    </Grid>
                                </Grid>
                            </div>}
                        </form>
                    )}
                </Formik>
            </div>
            {/* <Footer/>
        </ThemeProvider> */}
        </>
        )
}

export default SignInForm