import React, { useContext, useEffect } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import {
  Badge,
  Avatar,
  Popover,
  ButtonGroup,
  Button,

} from '@material-ui/core'
import { useSnackbar } from 'notistack'
//import { authService } from '../../../../../services/auth.service.js'
// import EditIcon from '@material-ui/icons/Edit'
import { Store } from '../../../../../../../Store'
import { apiTakent } from '../../../../../../../services/API-TAKENT'

const ALLOWED_TYPES = ['jpeg', 'png']

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    '& > *': {
      margin: theme.spacing(1)
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
  textField: {
    margin: '0.5rem',
  },
  grid: {
    margin: '0.5rem',
  },
  large: {
    marginTop: '7.5rem',
    width: theme.spacing(20),
    height: theme.spacing(20),
    border: '1px solid rgba(0,0,0, 0.15)',

  },
  inputFileStyle: {
    display: 'none',
  }
}))

const SmallAvatar = withStyles(() => ({
  root: {
    width: 35,
    height: 35,
    marginLeft: '1rem',
    marginTop: '1rem',
    backgroundColor: '#777',
  }
}))(Avatar)


const AvatarProfile = () => {
  const classes = useStyles()
  const context = useContext(Store)
  const [anchorEl, setAnchorEl] = React.useState(null)
  const { enqueueSnackbar } = useSnackbar()
  // const [photo, setPhoto] = useState(null)

  useEffect(()=>{
    // const reader = new FileReader()
    if(!context.user.profile_photo)
      apiTakent.getProfilePhotoCommonUser()
        .then(res => {
          context.handleSetUser({...context.user, profile_photo: res.data.profile_photo})
        }) 
  },[context])

  const convertImage = event => {
    // setError(false)
    if (!event.target.files.length) return
    let file = event.target.files[0]
    let [type, extension] = file.type.split('/')
    if (!ALLOWED_TYPES.includes(extension) || type !== 'image' || !file.type) {
      // setError(true)
      setAnchorEl(null)
      return
    }

    let reader = new FileReader()
    reader.onloadend = function () {
      let values = { profile_photo: reader.result }
      context.handleSetUser({...context.user, profile_photo: reader.result})
      apiTakent.postProfilePhotoCommonUser(values)
        .then(() => {
         // let newValues = { ...context.user, picture: values.picture }
          enqueueSnackbar('Foto de perfil cargada con éxito', { variant: 'success' })
          
        })
        .catch(() => enqueueSnackbar('Error al cargar foto de perfil', { variant: 'error' }))
    }
    reader.readAsDataURL(file)
  }

  const handleDeleteImage = () => {
    let values = { profile_photo: '' }
    apiTakent.postProfilePhotoCommonUser(values)
      .then(() => {
        enqueueSnackbar('Foto de perfil eliminada con éxito', { variant: 'success' })
        let newValues = { ...context.user, picture: '' }
        context.handleSetUser(newValues)
      })
      .catch(() => enqueueSnackbar('Error al eliminar foto de perfil', { variant: 'error' }))
  }

  const handleClick = event => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined

  return (
    <Badge
      overlap="circle"
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right'
      }}
      badgeContent={
        <SmallAvatar alt="Remy Sharp">
          <Button
            aria-describedby={id}
            variant="contained"
            color="primary"
            onClick={handleClick}
          >
            {/* <EditIcon /> */}
          </Button>
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <ButtonGroup
              orientation="vertical"
              color="primary"
              aria-label="vertical outlined primary button group"
            >
              <Button
                onClick={() => {
                  document.getElementById('inputFile').click()
                }}
              >Subir</Button>
              <Button
                onClick={() => {
                  handleDeleteImage()
                }}
              >Eliminar</Button>
            </ButtonGroup>
          </Popover>
        </SmallAvatar>
      }
    >
      {context.user.profile_photo && <Avatar
        src={
          context.user.profile_photo !== null
            ? context.user.profile_photo
            : ''
        }
        className={classes.large}
      />}
      <input
        type="file"
        id="inputFile"
        accept="image/jpeg,image/png"
        className={classes.inputFileStyle}
        onChange={convertImage}
      />
    </Badge>

  )
}

export default AvatarProfile