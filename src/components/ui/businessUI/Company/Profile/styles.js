import { theme } from "../../../../../constants/generalConstants/theme"

export const styles = {
    title: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: theme.spacing(4)
      }
}