export const styles = {
    content:{
        margin: '0px 50px 0px 50px',
        width: '80%'
    },
    titleText:{
        fontFamily: 'Roboto',
        fontSize: '18px',
        fontWeight: 'bold'
    },
    mainContent:{
        display: 'grid',
        paddingTop: '35px'
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    iconSpanContainer:{
        display: 'flex',
        alignItems: 'flex-end',
        marginBottom: '8px',
        cursor: 'pointer'
    },
    spanSubtitle:{
        fontSize: '16px',
    },
    icon:{
        marginRight: '10px',
        color: 'orange'
    }
}