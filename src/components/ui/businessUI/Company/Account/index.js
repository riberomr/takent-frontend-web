import React, { useContext, useState } from 'react'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Store } from '../../../../../Store'
import ChangePassForm from './ChangePassForm'
import LockIcon from '@material-ui/icons/Lock'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import AlertAction from '../../../generalUI/AlertAction'
import DeleteAccountDialog from './DeleteAccountDialog'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

const useStyles = makeStyles(styles)

const Account = (props) => {
    const { handleFormSubmit, isVerified } = props
    const context = useContext(Store)
    const classes = useStyles()
    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [values, setValues] = useState(null)

    const [ isDeleteDialogOpen, setIsDeleteDialogOpen ] = useState(false)


    const closeLeftDialog = () => {
        context.setLeftDialog(null)
    }

    //this function open the alert and receive and set the values from the form
    const openAlert = (value) => {
        setValues(value)
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {
        if(type === 'accept') {
            handleFormSubmit(values)
            setIsAlertOpen(false)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    } 

    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const changePassword = () => {
        context.setLeftDialog(
            <ChangePassForm 
                handleFormSubmit={openAlert}
                closeLeftDialog={closeLeftDialog}
            />
        )
    }

    return (
        <div className={classes.content}>
            <div className={classes.titleContainer}>
                <span className={classes.titleText}>Mi cuenta</span>
            </div>
            <div className={classes.mainContent}>
                <div className={classes.iconSpanContainer} onClick={()=> changePassword()}>
                    <LockIcon className={classes.icon}/><span className={classes.spanSubtitle} >Cambiar contraseña</span>
                </div>
                {context?.user?.permission === "admin" && isVerified === false && <div className={classes.iconSpanContainer} >
                    <CheckCircleIcon className={classes.icon}/>
                    <a 
                      className={classes.spanSubtitle} 
                      href="mailto:takentuser@gmail.com?subject=Verificación de cuenta empresa&body=Completá los siguientes datos para poder verificar tu cuenta:%0D
                      - CUIT de la empresa%0D
                      - Razón social%0D
                      - Documento del dueño%0D"
                      style={{textDecoration: "inherit", color: "inherit"}}
                    >
                      Verificar empresa
                    </a>
                </div>}
                {context?.user?.permission === "admin" && <div className={classes.iconSpanContainer} onClick={() => setIsDeleteDialogOpen(true)}>
                    <DeleteForeverIcon className={classes.icon}/><span className={classes.spanSubtitle}>Eliminar cuenta</span>
                </div>}
            </div>
            <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title='¿Desea confirmar el cambio de contraseña?'
                loading={false}
            />
            <DeleteAccountDialog
                open={isDeleteDialogOpen}
                setOpen={setIsDeleteDialogOpen}
            />
        </div>
    )
}

export default Account