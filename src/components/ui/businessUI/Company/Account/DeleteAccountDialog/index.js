import React, { useState, useContext } from 'react'
import { Dialog, DialogTitle, makeStyles, Typography, DialogContent, DialogContentText, TextField, IconButton, InputAdornment, Button, DialogActions } from '@material-ui/core'
import { styles } from './styles'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import { authService } from '../../../../../../services/auth.service'
import { Store } from '../../../../../../Store'
import { apiTakent } from '../../../../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'

const useStyles = makeStyles(styles)

const DeleteAccountDialog = (props) => {

    const { open, setOpen, ...other } = props
    const classes = useStyles()
    const srcIcon = "/images/AlertIcons/warning.png"
    const [ password, setPassword ] = useState("")
    const [ showPassword, setShowPassword ] = useState(false)
    const [ passwordIncorrect, setPasswordIncorrect ] = useState(false)
    const user = JSON.parse(localStorage.getItem('takent_user'))
    const { handleSetUser } = useContext(Store)
    const { enqueueSnackbar } = useSnackbar()

    const handlePasswordChange = (event) => {
        setPassword(event.target.value)
        setPasswordIncorrect(false)
    }

    const deleteAccount = () => {
        authService.checkPass(user._id, password)
        .then(() => {
          if(user.permission === 'admin') {
            apiTakent.deleteEnterpriseUser()
            .then(() => {
                enqueueSnackbar('Cuenta eliminada con éxito', { variant: 'success' })
                authService.logout()
                handleSetUser(null)
            })
            .catch(() => {
                enqueueSnackbar('Error al intentar eliminar la cuenta', { variant: 'error' })
            })
          }
          else {
            apiTakent.deleteMemberUser()
            .then(() => {
                enqueueSnackbar('Cuenta eliminada con éxito', { variant: 'success' })
                authService.logout()
                handleSetUser(null)
            })
            .catch(() => {
                enqueueSnackbar('Error al intentar eliminar la cuenta', { variant: 'error' })
            })
          }
        })
        .catch(() => {
            setPasswordIncorrect(true)
        })
    }

    return (
        <Dialog
            maxWidth="sm"
            fullWidth
            open={open}
            onClose={() => setOpen(false)}
            className={classes.dialog}
            {...other}
        >
                <div className={classes.iconContainer}>
                    <img src={srcIcon} className={classes.icon} alt=''></img>
                </div>
                <DialogTitle disableTypography>
                    <Typography variant="h5" align="center">
                        Eliminación de cuenta
                    </Typography>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText align="left">
                        Ingresá tu contraseña actual para confirmar la eliminación.
                    </DialogContentText>
                    <TextField
                        type={showPassword ? 'text' : 'password'}
                        label="Contraseña"
                        variant="standard"
                        fullWidth
                        margin="dense"
                        InputLabelProps={{shrink: true}}
                        value={password}
                        error={passwordIncorrect}
                        helperText={passwordIncorrect? "La contraseña ingresada no es correcta" : ""}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        onClick={() => setShowPassword(!showPassword)}
                                    >
                                        {showPassword? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        onChange={handlePasswordChange}
                    />
                </DialogContent>
                <DialogActions className={classes.dialogActions}>
                    <Button color="primary" variant="contained" onClick={() => setOpen(false)} className={classes.button}>
                        Cancelar
                    </Button>
                    <Button color="primary" variant="contained" onClick={deleteAccount} className={classes.button}>
                        Aceptar
                    </Button>
                </DialogActions>
        </Dialog>
    )
}

export default DeleteAccountDialog