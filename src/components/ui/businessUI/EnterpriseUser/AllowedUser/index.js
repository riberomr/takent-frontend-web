import React, { useState } from 'react'
import { Formik } from 'formik'
import FormButton from '../../../generalUI/Buttons/PrimaryButton'
import TextField from '../../../generalUI/Inputs/CustomTextField'
import TextFieldEmail from '@material-ui/core/TextField'
import { format, addHours } from 'date-fns'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import newAllowedUser from '../../../../rules/validationSchema/newAllowedUser'
import { Grid } from '@material-ui/core'
import { apiTakent } from '../../../../../services/API-TAKENT'

const useStyles = makeStyles((theme) => (styles))

const ProfileForm = (props) => {
    const { handleFormSubmit, loading, setIsOpen } = props
    const classes = useStyles()
    const [userExists, setUserExists] = useState(false)

    let initialValuesEnterpriseUser
    const variant = 'standard'

    const validateUserExists = async (user) => {
        let exists = false
        await apiTakent.getUserExists(user).then(res => {
            exists = res.data.exists
        });
        return exists
    }

    const handleAccountBlur = async (event, handleBlur, hasError) => {
        handleBlur(event)
        if(!hasError) {
            const exists = await validateUserExists(event.target.value)
            setUserExists(exists)
        }
        else {
            setUserExists(false)
        }
    }

    initialValuesEnterpriseUser = {
        account: '',
        password: '',
        name: '',
        surname: '',
        birth_date: '',
        phone: '',
        position: '',
    }




    const handleSubmitForm = (values) => {
        let valuesToSend = {
            account: values.account,
            password: values.password,
            name: values.name,
            surname: values.surname,
            birth_date: format(
                addHours(new Date(`${values.birth_date}`), 4),
                'yyyy-MM-dd'
            ),
            position: values.position
        }
        valuesToSend = {
            ...valuesToSend,
            phone: values.phone !== '' ? values.phone : null
        }

        handleFormSubmit(valuesToSend)
    }

    return (<>

        <Formik initialValues={initialValuesEnterpriseUser}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={newAllowedUser}>
            {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                errors,
                touched,
                setFieldValue
            }) => (
                <form onSubmit={handleSubmit}>
                    <div className={classes.formRoot}>
                        <Grid container spacing={5}>

                            <Grid item xs={12} className={classes.gridItem}>
                                <TextFieldEmail
                                    id='account'
                                    type='email'
                                    name='account'
                                    label="Correo electrónico"
                                    variant={variant}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={(event) => handleAccountBlur(event, handleBlur, errors.account)}
                                    value={values.account}
                                    error={touched.account && (errors.account || userExists)? true : false}
                                    helperText={((errors.account || userExists) && touched.account)? errors.account || "El correo electrónico ingresado ya existe": ""}
                                />
                            </Grid>
                           <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='password'
                                    type='password'
                                    name='password'
                                    label="Contraseña"
                                    variant={variant}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    error={errors.password && touched.password}
                                    helperText={(errors.password && touched.password) ? errors.password : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='name'
                                    type='text'
                                    name='name'
                                    label="Nombre"
                                    variant={variant}
                                    fullWidth
                                    onlyLetters
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.name}
                                    error={errors.name && touched.name}
                                    helperText={(errors.name && touched.name) ? errors.name : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='surname'
                                    type='text'
                                    name='surname'
                                    label="Apellido"
                                    variant={variant}
                                    fullWidth
                                    onlyLetters
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.surname}
                                    error={errors.surname && touched.surname}
                                    helperText={(errors.surname && touched.surname) ? errors.surname : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='position'
                                    type='text'
                                    name='position'
                                    label="Puesto"
                                    variant={variant}
                                    fullWidth
                                    onlyLetters
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.position}
                                    error={errors.position && touched.position}
                                    helperText={(errors.position && touched.position) ? errors.position : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='phone'
                                    type='text'
                                    name='phone'
                                    label="Teléfono"
                                    variant={variant}
                                    fullWidth
                                    onlyNumbers
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.phone}
                                    error={errors.phone && touched.phone}
                                    helperText={(errors.phone && touched.phone) ? errors.phone : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.gridItem}>
                                <TextField
                                    id='birth_date'
                                    type='date'
                                    name='birth_date'
                                    label="Fecha de nacimiento"
                                    variant={variant}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.birth_date}
                                    error={errors.birth_date && touched.birth_date}
                                    helperText={(errors.birth_date && touched.birth_date) ? errors.birth_date : ""}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.buttonContainer}>
                                <FormButton
                                    onClick={() => setIsOpen(false)}
                                    variant="contained"
                                    color="primary"
                                    className={classes.buttonWidth}
                                    disabled={loading}
                                >
                                    Cancelar
                                </FormButton>
                                <FormButton
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    className={classes.buttonWidth}
                                    disabled={loading}
                                >
                                    Guardar
                                </FormButton>
                            </Grid>
                        </Grid>
                    </div>
                </form>
            )}
        </Formik>
    </>
    )
}

export default ProfileForm