import { theme } from "../../../../../../constants/generalConstants/theme";

export const styles = {
    root: {
        flexGrow: 1,
        padding: theme.spacing(10),
        paddingTop: theme.spacing(2)
    },
    formRoot: {
        flexGrow: 1,
        paddingLeft: theme.spacing(20),
        paddingRight: theme.spacing(20),
        paddingBottom: theme.spacing(6)
    },
    gridItem: {
        display: 'flex',
        flexGrow: 1,
    },
    gridItemGrid:{
        display: 'grid',
        flexGrow: 1,
    },
    gridItemCentered: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainTitle: {
        fontFamily: 'Roboto',
        fontSize: '20px',
        fontWeight: 'bold'
    },
    textField: {
        display: 'flex',
        justifyContent: 'center'
    },
    inlineElement: {
        display: 'flex',
        alignItems: 'center',
        width: 'auto',
        fontFamily: 'Roboto',
        fontSize: '18px'
    },
    spanNamePdf:{
        display: 'flex',
        alignItems: 'center',
        width: 'auto',
        fontFamily: 'Roboto',
        fontSize: '14px'
    },
    buttonContainer:{
        display: 'flex',
        justifyContent: 'space-evenly'
    },
    buttonWidth:{
        width: '150px',
        height: '40px'
    }
}