import React from 'react'
import { Formik } from 'formik'
import FormButton from '../../../../generalUI/Buttons/PrimaryButton'
import TextField from '../../../../generalUI/Inputs/CustomTextField'
import { format, addHours } from 'date-fns'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import enterpriseUserProfile from '../../../../../rules/validationSchema/enterpriseUserProfile'
import { Grid } from '@material-ui/core'
// import Select from '../../../../generalUI/Selects/CustomSelect/CustomSelect'
// import { BorderColor } from '@material-ui/icons'

const useStyles = makeStyles((theme) => (styles))

const ProfileForm = (props) => {
    const { userData, handleFormSubmit,
        //  loading, setLoading, 
        disabledStatus, setDisabledStatus } = props
    const classes = useStyles();

    let initialValuesEnterpriseUser
    const variant = 'standard'

    if (userData) {
        initialValuesEnterpriseUser = {
            name: userData.name,
            surname: userData.surname,
            description: userData.description,
            birth_date: format(
                addHours(new Date(`${userData.birth_date}`), 4),
                'yyyy-MM-dd'
            ),
            position: userData.position
        }
        if (userData.phone) initialValuesEnterpriseUser = { ...initialValuesEnterpriseUser, phone: userData.phone }
    } else {
        initialValuesEnterpriseUser = {
            name: '',
            surname: '',
            description: '',
            birth_date: '',
            phone: '',
            position: '',
        }
    }

    const handleSubmitForm = (values) => {
        let valuesToSend = {
            name: values.name,
            surname: values.surname,
            description: values.description,
            birth_date: format(
                addHours(new Date(`${values.birth_date}`), 4),
                'yyyy-MM-dd'
            ),
            position: values.position
        }
        valuesToSend = {
            ...valuesToSend,
            phone: values.phone !== '' ? values.phone : null
        }
        handleFormSubmit(valuesToSend)
    }

    const handleCancel = (values, setFieldValue) => {
        setDisabledStatus(!disabledStatus)
        Object.keys(values).map(key => {
            return Object.keys(initialValuesEnterpriseUser).map(key2 => {
                if (key === key2) return setFieldValue(`${key}`, initialValuesEnterpriseUser[key2])
                return null
            })
        })
    }

    return (<>
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={9}>
                    <span className={classes.mainTitle}>Mis datos personales</span>
                </Grid>
                <Grid item xs={3}>
                    <FormButton
                        onClick={() => { setDisabledStatus(!disabledStatus) }}
                        variant="contained"
                        color="primary"
                        disabled={!disabledStatus}
                        className={classes.buttonWidth}
                    >
                        Editar
                    </FormButton>
                </Grid>
            </Grid>
        </div>
        {userData && <Formik initialValues={initialValuesEnterpriseUser}
            onSubmit={values => handleSubmitForm(values)}
            validationSchema={enterpriseUserProfile}>
            {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                errors,
                touched,
                setFieldValue
            }) => (
                <form onSubmit={handleSubmit}>
                    <div className={classes.formRoot}>
                        <Grid container spacing={5}>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='name'
                                    type='text'
                                    name='name'
                                    label="Nombre"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    onlyLetters
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.name}
                                    error={errors.name && touched.name}
                                    helperText={(errors.name && touched.name) ? errors.name : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='surname'
                                    type='text'
                                    name='surname'
                                    label="Apellido"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    onlyLetters
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.surname}
                                    error={errors.surname && touched.surname}
                                    helperText={(errors.surname && touched.surname) ? errors.surname : ""}
                                />
                            </Grid>
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='position'
                                    type='text'
                                    name='position'
                                    label="Puesto"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    lettersAndNumbers
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.position}
                                    error={errors.position && touched.position}
                                    helperText={(errors.position && touched.position) ? errors.position : ""}
                                />
                            </Grid>
                            
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='birth_date'
                                    type='date'
                                    name='birth_date'
                                    label="Fecha de nacimiento"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus }}
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.birth_date}
                                    error={errors.birth_date && touched.birth_date}
                                    helperText={(errors.birth_date && touched.birth_date) ? errors.birth_date : ""}
                                />
                            </Grid>
                            
                            <Grid item xs={6} className={classes.gridItem}>
                                <TextField
                                    id='phone'
                                    type='text'
                                    name='phone'
                                    label="Teléfono"
                                    variant={variant}
                                    InputProps={{ readOnly: disabledStatus, inputProps: { min: 0 } }}
                                    fullWidth
                                    onlyNumbers={true}
                                    InputLabelProps={{ shrink: true }}
                                    onChange={handleChange}                                   
                                    onBlur={handleBlur}
                                    value={values.phone}
                                    error={errors.phone && touched.phone}
                                    helperText={(errors.phone && touched.phone) ? errors.phone : ""}
                                />
                            </Grid>
                            {!disabledStatus && <Grid item xs={12} className={classes.buttonContainer}>
                                <FormButton
                                    onClick={() => handleCancel(values, setFieldValue)}
                                    variant="contained"
                                    color="primary"
                                    disabled={disabledStatus}
                                    className={classes.buttonWidth}
                                >
                                    Cancelar
                                </FormButton>
                                <FormButton
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={disabledStatus}
                                    className={classes.buttonWidth}
                                >
                                    Guardar
                                </FormButton>
                            </Grid>}
                        </Grid>
                    </div>
                </form>
            )}
        </Formik>}
    </>
    )
}

export default ProfileForm