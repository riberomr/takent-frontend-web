import React, { useState } from 'react'
import ProfileForm from './ProfileForm'
import AlertAction from '../../../generalUI/AlertAction'

const Profile = (props) => {

    const { userData, handleFormSubmit, loading, setLoading } = props

    const [isAlertOpen, setIsAlertOpen] = useState(false)
    const [values, setValues] = useState(null)
    const [disabledStatus, setDisabledStatus] = useState(true)

    //this function open the alert and receive and set the values from the form
    const openAlert = (value) => {
        setValues(value)
        setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {
        if(type === 'accept') {
            handleFormSubmit(values)
            setIsAlertOpen(false)
            setDisabledStatus(!disabledStatus)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    } 

    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    return (
        <div>
            <ProfileForm 
                userData={userData}
                handleFormSubmit={openAlert}
                loading={loading}
                setLoading={setLoading}
                disabledStatus={disabledStatus}
                setDisabledStatus={setDisabledStatus}
            />
            <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title='¿Desea confirmar los cambios?'
                loading={false}
            />
        </div>
    )
}

export default Profile