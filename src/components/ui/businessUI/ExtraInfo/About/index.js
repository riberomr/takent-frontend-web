import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { styles } from './styles'
import { Card, Typography, CardContent, CardMedia } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import CheckIcon from '@material-ui/icons/Check'

const useStyles = makeStyles(styles)

const About = (props) => {

  const isXSSize = useMediaQuery('(max-width:600px)')
  const classes = useStyles({isXSSize})
  
  return (
    <div>
      <Card className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="h4" className={classes.title}>¿Qué es Takent?</Typography>
            <Typography variant="body1">
              <CheckIcon color="primary"/>Es una herramienta que permite la búsqueda de empleos y de servicios.
              <br />
              <CheckIcon color="primary"/>Las empresas podrán publicar sus ofertas laborales y los usuarios aplicar a ellas.
              <br />
              <CheckIcon color="primary"/>Cada usuario podrá buscar un determinado servicio que solicite, como también realizar una publicación de uno que pueda ofrecer.
            </Typography>
          </CardContent>
        </div>
        <CardMedia
          className={classes.image}
          image="/images/AboutImages/takent.jpg"
        />
      </Card>
      <Card className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="h4" className={classes.title}>Empleos</Typography>
            <Typography variant="body1">
              <CheckIcon color="primary"/>Una vez encontrada la oferta de trabajo, se podrá aplicar a ella de manera inmediata.
              <br />
              <CheckIcon color="primary"/>La empresa se encargará de seleccionar o dar "Take" a aquellos postulantes que cumplan con las condiciones requeridas.
              <br />
              <CheckIcon color="primary"/>Posteriormente, se comunicará con los candidatos elegidos para coordinar el siguiente paso de selección.
            </Typography>
          </CardContent>
        </div>
        <CardMedia
          className={classes.image}
          image="/images/AboutImages/employ.jpg"
        />
      </Card>
      <Card className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography variant="h4" className={classes.title}>Servicios</Typography>
            <Typography variant="body1">
              <CheckIcon color="primary"/>Se puede buscar distintos tipos de servicios, aplicando filtros de búsqueda para mayor exactitud.
              <br />
              <CheckIcon color="primary"/>Luego de elegir un servicio determinado, se enviará una notificación o "Take" al oferente del servicio para informarle sobre la petición.
              <br />
              <CheckIcon color="primary"/>Una vez finalizado, se podrá realizar una valoración sobre la calidad del servicio recibido.
            </Typography>
          </CardContent>
        </div>
        <CardMedia className={classes.image}
          image="/images/AboutImages/services.jpg"
        />
      </Card>
    </div>
    )
}

export default About