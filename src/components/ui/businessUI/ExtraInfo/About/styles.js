import { theme } from "../../../../../constants/generalConstants/theme";

export const styles = {
    title: {
        marginBottom: 14,
        textAlign: "center"
    },
    root: {
        display: "flex",
        marginBottom: 30,
        borderRadius: '8px',
        border: '2px solid ' + theme.palette.primary.main,
        boxShadow: '7px 7px 7px rgba(0, 0, 0, 0.2)'
    },
    content: {
        flex: "1 0 auto"
    },
    details: {
        display: "flex",
        flexDirection: "column",
        width: '70%'
    },
    image: {
        flex: "1 0 auto"
    }
}