import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { theme } from '../../../../../constants/generalConstants/theme';
import PrimaryButton from '../../../../../components/ui/generalUI/Buttons/PrimaryButton/index.js';

const Support = (props) => {

    const handleClick = () => {
        window.open('mailto:takentuser@gmail.com?subject=Deseo soporte con la aplicación');
    }

    return (
        <ThemeProvider theme={theme}>
            <Typography variant="h4">
                Contáctate con nosotros
            </Typography>
            <br />
            <Typography variant="h6">
                Si tuviste algún inconveniente, envianos un correo para poder ayudarte.
                <br />
                Recordá que en este espacio podés pedirnos mejoras que consideres para la aplicación,
                <br />
                como por ejemplo agregar un nuevo rubro o categoría!
                <br />
                También nos vas a encontrar en nuestras redes sociales de <a style={{textDecoration: 'none', color: '#F29222'}} href="https://www.instagram.com/takentapp/" target="_blank" rel="noreferrer"><b>Instagram</b></a> y <a style={{textDecoration: 'none', color: '#F29222'}} href="https://www.facebook.com/Takent-107583084366371" target="_blank" rel="noreferrer"><b>Facebook</b></a>.
                <br />
                <br />
                <PrimaryButton variant="contained" color="primary" onClick={handleClick}>Enviar correo</PrimaryButton>
            </Typography>
        </ThemeProvider>
    );
}

export default Support;