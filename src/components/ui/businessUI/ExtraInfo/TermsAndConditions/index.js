import React, { useEffect } from 'react'
import { Typography } from '@material-ui/core'
import { Grid } from '@material-ui/core'

const TermsAndConditions = (props) => {

    useEffect(() => {
        window.scroll(0, 0);
    }, []);
    
    return (
        <div>
            <Grid container spacing={3}>
                <Grid item xs={2}>

                </Grid>
                <Grid item xs={8}>
                    <Typography variant="h5" align="left" gutterBottom>POLÍTICA DE PRIVACIDAD PARA POSTULANTES O USUARIOS PARTICULARES PRESTADORES O SOLICITANTES DE SERVICIOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        La presente Política de Privacidad explica quién es el responsable de los datos de carácter personal de aquellos usuarios personas físicas (en adelante, “Usuario particular”) que buscan empleo o servicios o publican servicios y acceden y/o utilizan los servicios del portal web de empleo y/o aplicación móvil Takent y cómo se recopilan, comparten y tratan los referidos datos de carácter personal (en adelante, los “Datos personales”), así como los derechos que tienen los Usuarios particulares al respecto y cómo pueden ejercitarlos. Para Takent es fundamental mantener la privacidad y seguridad de los Datos personales.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>1. RESPONSABLE</Typography>

                    <Typography variant="subtitle1" align="left" gutterBottom>
                        ¿Quién es el Responsable del tratamiento de sus datos?
                <ul>
                            <li>Nombre: Takent.</li>
                            <li>Domicilio: Julio A. Roca 477 - Laguna Larga - Córdoba.</li>
                            <li>Contacto: contact@takent.tk</li>
                        </ul>
                    </Typography>

                    <Typography variant="h6" align="left" gutterBottom>2. DATOS QUE RECOPILAMOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        A fin de crear una cuenta en Takent como Usuario particular o utilizar otros de los servicios de nuestro sistema, como la recepción de alertas, puede ser necesario que el Usuario particular proporcione los siguientes datos: nombre y apellidos, dirección de correo electrónico, domicilio, así como, en su caso, una contraseña.
                        También, podrá incluir en su perfil sus datos académicos, experiencia laboral, idiomas y aptitudes. Además de una descripción y una fotografía. También podrá subir su C.V. a la aplicación en formato Pdf.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>3. FINALIDADES Y LEGITIMACIÓN</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        El objetivo de Takent mediante los servicios ofrecidos en su portal web y en su aplicación móvil es el facilitar el contacto entre Postulantes o Demandantes de empleo y Empleadores o Empresas reclutadoras que ofrecen puestos de trabajo (en adelante, <strong>“Usuarios Profesionales”</strong> u <strong>“Usuarios Empresa”</strong>), ofreciendo herramientas y servicios que apoyan los procesos de búsqueda de empleo. Por otro lado, Takent busca facilitar el contacto entre Prestadores que ofrecen sus servicios y Solicitantes o Contratantes que desean satisfacer sus necesidades.
            </Typography>

                    <Typography variant="subtitle1" align="left" paragraph>
                        En concreto, los Datos Personales que usted facilita al registrarse y/o utilizar nuestro portal web y/o aplicación móvil serán destinados para las siguientes finalidades:
            </Typography>

                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul>
                            <li>
                                3.1. Realizar una correcta gestión de nuestros servicios consistentes en:
                        <ul>
                                    <li><strong>Creación de una cuenta</strong> en el sistema Takent.</li>
                                    <li><strong>Posibilitar el contacto con los Usuarios profesionales</strong> (Usuarios empresa) mediante la aplicación a avisos u ofertas de empleo publicados por los mismos en el sistema. En este sentido, los ofertantes de empleo podrán acceder a sus Datos Personales y a su C.V. Cabe aclarar que los usuarios pueden comunicarse a través del servicio de mensajería Whatsapp o vía Correo electrónico.</li>
                                    <li><strong>Posibilitar el contacto con los Usuarios particulares</strong> (sólo aquellos que actúen como Contratantes o Solicitantes de servicios que haya publicado) a través de la solicitud de servicios publicados por usted en la aplicación. En este sentido, se aclara que los usuarios se comunican a través del servicio de mensajería Whatsapp.</li>
                                    <li><strong>Recepción automática y gratuita de avisos por vía electrónica</strong>. En caso de activación de cuenta, recuperación de contraseña y/o ante la postulación a ofertas de empleo, para tomar conocimiento del estado de la misma.</li>
                                    <li><strong>Gestión de nuestros servicios y tareas comerciales diarias</strong>, incluyendo recordatorios, avisos técnicos, actualizaciones, alertas de seguridad, y mensajes de soporte, entre otros. En el caso concreto de que utilice nuestro correo de contacto de atención al usuario, sus datos serán utilizados para atender las consultas o reclamaciones que nos plantee a través de ese medio.</li>
                                </ul>
                            </li>
                            <li>3.2. Gestionar la suscripción y moderar, de forma anónima, los comentarios proporcionados en el sistema.</li>
                            <li>3.3. Utilizar sus datos para generar y compartir información agregada, que no le identifica, para fines analíticos y estadísticos, que nos da un mejor conocimiento de los usuarios de nuestra aplicación en su conjunto y nos ayuda a ofrecerles un mejor servicio.</li>
                            <li>3.4. Prevención de abusos y fraudes en el uso de nuestros servicios (por ejemplo, actividades fraudulentas, ataques de denegación de servicios, envío de spam, entre otros).</li>
                            <li>3.5. Transferencia de datos a organismos y autoridades públicas, siempre y cuando sean requeridos de conformidad con las disposiciones legales y reglamentarias.</li>

                        </ul>
                    </Typography>

                    <Typography variant="subtitle1" align="left" paragraph>
                        La base legal para el tratamiento de sus Datos Personales es la ejecución de un contrato con Takent en relación con las finalidades indicadas en el apartado 3.1 y 3.2 para realizar una correcta gestión y prestación de nuestros servicios.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Asimismo, el tratamiento de sus Datos Personales para la finalidad indicada en el apartado 3.3. y 3.4 anterior radica en el interés legítimo de Takent como el responsable del tratamiento.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Adicionalmente, el tratamiento de sus Datos Personales para la finalidad indicada en el apartado 3.5 es necesario para el cumplimiento de obligaciones legales aplicables a Takent.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        La base legal correspondiente a las finalidades indicadas en los anteriores apartados, a excepción del 3.5, radica en el consentimiento del usuario.
            </Typography>


                    <Typography variant="h6" align="left" gutterBottom>4. PLAZO DE CONSERVACIÓN DE LOS DATOS</Typography>

                    <Typography variant="subtitle1" align="left" paragraph>
                        Los Datos Personales que nos facilite serán conservados en nuestro sistema, aún cuando su cuenta no se encuentre activa o ni utilice los servicios de Takent. Esta disposición radica en la necesidad de facilitar al Usuario la recuperación de una cuenta en desuso. Cabe mencionar que es posible solicitar la supresión completa de los datos de la cuenta mediante el correo de contacto.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>5. DESTINATARIOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Los Datos Personales que nos proporcione serán comunicados a la empresa ofertante o reclutadora de empleo (Usuarios profesionales) cuando usted haya aplicado a un aviso para posibilitar el contacto con esta última. O cuando, siendo Usuario particular, publique sus servicios, o se postule a servicios publicados por otros usuarios .
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>Sus Datos Personales también podrán ser comunicados en los casos que exista una obligación legal para ello.</Typography>
                    <Typography variant="h6" align="left" gutterBottom>6.  DERECHOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>Como titular de los Datos Personales que tratamos de conformidad con lo especificado en la presente Política, tiene los siguientes derechos:</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul>
                            <li><strong>Acceder o recopilar sus Datos:</strong> puede solicitarnos una copia de sus Datos Personales.</li>
                            <li><strong>Cambiar o rectificar Datos:</strong> puede editar parte de sus Datos Personales a través de su área privada (perfil de usuario). Asimismo, puede solicitar la modificación, actualización o corrección de sus Datos, si éstos no son precisos, sólo si no está disponible su edición desde su área privada.</li>
                            <li><strong>Suprimir sus Datos:</strong> puede solicitar la eliminación de sus Datos Personales.</li>
                        </ul>
                    </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Debe tener en cuenta que los referidos derechos están sujetos a determinadas limitaciones, tal y como establece la legislación aplicable. Las solicitudes individuales correspondientes al ejercicio de los anteriores derechos terminarán de tratarse dentro del plazo asignado por las regulaciones legales pertinentes, que comienza a partir del momento en que Takent confirme la solicitud. Debe tener en cuenta que Takent podrá cobrar un cargo por la gestión de solicitudes posteriores que provengan de la misma persona, siempre que la legislación aplicable lo permita.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si desea hacer uso de cualquiera de sus derechos puede dirigirse a nosotros utilizando la información de contacto que aparece al inicio de la presente política y en el apartado 10; gestionaremos su solicitud conforme a la legislación aplicable.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si tras contactarnos no se resuelve su consulta o queja, puede también contactar con la autoridad de protección de datos y demás organismos públicos competentes para cualquier reclamación derivada del tratamiento de sus Datos Personales.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        En relación a los Datos Personales facilitados a las empresas ofertantes de empleo o reclutadoras (Usuarios Profesionales) con los que se haya contactado, puede ejercer sus derechos frente a los mismos.
            </Typography>
                    <Typography variant="subtitle1" algin="left" paragraph>
                        Takent no se responsabiliza por acciones discriminatorias, sexistas, xenófobas, o de cualquier índole lastimosa, que llevasen a cabo los Usuarios profesionales al momento de la selección de candidatos. Sólo actuará en defensa de sus usuarios si dichas acciones estuvieran reflejadas en los datos que las mismas registran y/o avisos u ofertas que publican, cuando las mismas fueran denunciadas por parte de los Usuarios particulares afectados, significando ello la revisión y posible posterior suspensión de los avisos y/o la empresa.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent no se responsabiliza por acciones discriminatorias, sexistas, xenófobas, o de cualquier índole lastimosa, que llevasen a cabo los Usuarios particulares al momento del contacto por intermedio de un servicio publicado en la plataforma. Sólo actuará en defensa de los damnificados cuando dichas acciones estuvieran reflejadas en los datos que los mismos registran y/o servicios publicados, cuando las mismas fueran denunciadas por parte de los Usuarios particulares afectados, significando ello la revisión y posible posterior suspensión de los servicios y/o usuario.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent no se responsabiliza por acciones ilegales y/o delictivas llevadas a cabo por los Usuarios al momento de la prestación de un servicio o la contratación (o entrevista previa) de un postulante,cualquiera sea el actor damnificado. Es responsabilidad del Usuario velar por su seguridad e integridad, analizando con criterio la información vertida por los Usuarios de la aplicación. Sólo como consecuencia de algún hecho, y ante denuncia comprobable, Takent procederá a la eliminación de los datos del agresor y su prohibición de acceder nuevamente a sus servicios.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>7. SEGURIDAD</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent se preocupa por garantizar la seguridad y confidencialidad de los Datos Personales. Por eso, se han adoptado medidas de seguridad y medios técnicos adecuados para evitar su pérdida, mal uso o su acceso sin autorización de los usuarios. También Takent dispone de procedimientos de actuación ante cualquier sospecha de violación de la seguridad de los Datos Personales que tratamos. Taknet realizará la pertinente notificación al usuario y a la autoridad aplicable en caso de una violación de la seguridad de los Datos Personales, cuando así lo exija la ley. Una vez Takent haya recibido los Datos Personales, Takent utilizará procedimientos estrictos y medidas de seguridad para evitar el acceso no autorizado.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Sin perjuicio de lo anterior, cabe mencionar que lamentablemente, la transmisión de información a través de Internet no es completamente segura. Aunque Takent ha adoptado medidas de seguridad y medios técnicos para proteger los Datos Personales, no puede garantizar la seguridad de los datos transmitidos a través de Internet, por lo tanto, cualquier transmisión queda bajo su propio riesgo.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>8. EDAD MÍNIMA</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Para acceder o utilizar nuestro sistema debe tener 18 años de edad o más. Si tenemos conocimiento de que un menor de edad nos ha proporcionado información, eliminaremos dicha información y suprimiremos la cuenta de referido menor.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>9. ACTUALIZACIONES</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Podemos actualizar esta Política de Privacidad mediante la publicación de una versión actualizada en el Portal Web. Si realizamos alguna modificación que consideremos que pudiera afectar a sus derechos se lo notificaremos por email o mediante un aviso en nuestros sitios web.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Le recomendamos que revise de forma periódica la Política de Privacidad para estar actualizado de todas las novedades. Asimismo, declara que el uso continuado de nuestros servicios, tras publicar o, en su caso, enviar un aviso acerca de nuestros cambios en esta Política de privacidad implica que el tratamiento de sus Datos Personales tendrá lugar conforme a la Política de privacidad actualizada.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si no está de acuerdo con cualquiera de los cambios, goza del pleno derecho a cerrar su cuenta de usuario y solicitar la supresión de los mismos.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>10. INFORMACIÓN DE CONTACTO</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si desea hacer uso de cualquiera de sus derechos y si tiene alguna pregunta o queja sobre esta Política de Privacidad puede contactar con nosotros en la dirección y/o formulario de contacto indicados en el <strong>apartado 1</strong>.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Debe facilitarnos la mayor información posible sobre su solicitud: nombre y apellidos, dirección de correo electrónico que utiliza para nuestro sistema y los motivos de su solicitud, especificando, en su caso, el derecho que desea ejercer. Asimismo, será necesario acompañar la documentación necesaria para sustentar la solicitud y dar trámite a la misma.
            </Typography>

                    <Typography variant="h5" align="left" gutterBottom>POLÍTICA DE PRIVACIDAD PARA EMPRESAS O USUARIOS PROFESIONALES</Typography>

                    <Typography variant="subtitle1" align="left" paragraph>
                        La presente política de privacidad explica quién es el responsable de los datos de carácter personal de los representantes, empleados y/o usuarios de las empresas o entidades Clientes (también denominados, <strong>“Usuarios profesionales”</strong>) que actúan en representación y/o utilizan los servicios, productos y/o herramientas online ofrecidas por Takent (en adelante los <strong>“Datos personales”</strong>) y cómo se recopilan, comparten y tratan los referidos Datos Personales, así como los derechos que tienen los representantes, empleados y/o usuarios de los Clientes (en adelante, los <strong>“Usuarios empresa”</strong>) al respecto y cómo pueden ejercitarlos. Para Takent es fundamental mantener la privacidad y seguridad de los Datos Personales de los Usuarios profesionales.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>1. RESPONSABLE</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        El Responsable del tratamiento de los datos de los Usuarios empresa recabados por medio de este sitio web-herramienta online, es Takent, cuyos datos son los siguientes:
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul>
                            <li>Nombre: Takent.</li>
                            <li>Domicilio: Julio A. Roca 477 - Laguna Larga - Córdoba.</li>
                            <li>Contacto: contacto@takent.tk</li>
                        </ul>
                    </Typography>

                    <Typography variant="h6" align="left" gutterBottom>2. DATOS QUE SE RECOPILAN</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent recopila Datos Personales de los Usuarios empresa para poder proporcionar a sus Clientes o Usuarios profesionales los servicios contratados de la mejor manera posible. En concreto, Takent puede recopilar lo siguiente:
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul>
                            <li>
                                Información personal de los Usuarios empresa, proporcionada en el momento de suscribirse o registrarse, para utilizar los servicios de Takent, crear las distintas cuentas de usuario, publicar material o solicitar información, entre otros. También es posible que se solicite determinada información al Cliente cuando Takent preste asesoramiento en el uso de los servicios. Dicha información personal podrá ser: nombre completo, cargo, dirección de correo electrónico profesional, número de teléfono profesional y, en su caso, contraseña, debiendo indicar el nombre de la Empresa Cliente.
                    </li>
                            <li>
                                Detalles de las acciones que los Usuarios empresa llevan a cabo a través de nuestros sitios web y/o herramienta online y de la prestación de nuestros servicios, en caso de que ello resulte de aplicación.
                    </li>
                        </ul>
                    </Typography>
                    <Typography variant="h6" align="left" gutterBottom>3. FINALIDADES Y LEGITIMACIÓN</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent trata los Datos Personales con un objetivo específico y sólo se tratan aquellos Datos que son pertinentes para cumplir ese objetivo y según lo especificado en la presente Política de Privacidad. En este sentido, los Datos Personales de los Usuarios empresa podrán ser tratados para las siguientes finalidades, según sea de aplicación:
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul>
                            <li>
                                3.1. Realizar una correcta gestión de nuestros servicios consistentes en:
                        <ul>
                                    <li><strong>Creación de una cuenta</strong> en el sistema Takent.</li>
                                    <li><strong>Visualización de datos</strong> (de parte de los Usuarios particulares) mediante los avisos u ofertas de empleo que haya publicado en el sistema. En este sentido, los postulantes podrán acceder a sus Datos Personales y a su C.V.</li>
                                    <li><strong>Recepción automática y gratuita de avisos por vía electrónica</strong>. En caso de activación de cuenta, recuperación de contraseña, entre otros.</li>
                                    <li><strong>Gestión de nuestros servicios y tareas comerciales diarias</strong>, incluyendo recordatorios, avisos técnicos, actualizaciones, alertas de seguridad, y mensajes de soporte, entre otros. En el caso concreto de que utilice nuestro correo de contacto de atención al usuario, sus datos serán utilizados para atender las consultas o reclamaciones que nos plantee a través de ese medio.</li>
                                </ul>
                            </li>
                            <li>3.2. Gestionar la suscripción y moderar, de forma anónima, los comentarios proporcionados en el sistema.</li>
                            <li>3.3. Utilizar sus datos para generar y compartir información agregada, que no le identifica, para fines analíticos y estadísticos, que nos da un mejor conocimiento de los usuarios de nuestra aplicación en su conjunto y nos ayuda a ofrecerles un mejor servicio.</li>
                            <li>3.4. Prevención de abusos y fraudes en el uso de nuestros servicios (por ejemplo, actividades fraudulentas, ataques de denegación de servicios, envío de spam, entre otros).</li>
                            <li>3.5. Transferencia de datos a organismos y autoridades públicas, siempre y cuando sean requeridos de conformidad con las disposiciones legales y reglamentarias.</li>
                        </ul>
                    </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        La base legal para el tratamiento de sus Datos Personales es la ejecución de un contrato con Takent en relación con las finalidades indicadas en el apartado 3.1 y 3.2 para realizar una correcta gestión y prestación de nuestros servicios.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Asimismo, el tratamiento de sus Datos Personales para la finalidad indicada en el apartado 3.3. y 3.4 anterior radica en el interés legítimo de Takent como el responsable del tratamiento.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Adicionalmente, el tratamiento de sus Datos Personales para la finalidad indicada en el apartado 3.5 es necesario para el cumplimiento de obligaciones legales aplicables a Takent.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        La base legal correspondiente a las finalidades indicadas en los anteriores apartados, a excepción del 3.5, radica en el consentimiento del usuario.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>4. PLAZO DE CONSERVACIÓN DE LOS DATOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Conservaremos los Datos Personales durante el tiempo que sea necesario para que Takent proporcione los servicios, hasta el momento en que el cliente solicite la eliminación de su cuenta o de los Datos Personales; así como durante el tiempo necesario para cumplir las obligaciones legales. Cabe destacar que, en caso de eliminación de cuenta por parte del usuario, los datos permanecen inactivos en el sistema, de manera que sólo pueden ser consultados por los administradores del mismo, hasta tanto el Cliente solicite la eliminación física de los mismos mediante los medios de contacto especificados o desee recuperar su cuenta.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>5. DESTINATARIOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Los Datos Personales que nos proporcione serán comunicados a los postulantes (Usuarios particulares) cuando usted haya publicado un aviso u oferta, para posibilitar el contacto del particular con su empresa.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Sus Datos Personales también podrán ser comunicados en los casos que exista una obligación legal para ello.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>6. DERECHOS</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        En relación con los Datos Personales que Takent trata para la prestación de sus servicios, el Usuario profesional/los Usuarios empresa tienen los siguientes derechos:
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        <ul style={{ listStyleType: "square" }}>
                            <li>Acceder o recopilar sus datos: es posible solicitar una copia de los Datos Personales.</li>
                            <li>Rectificar los datos: es posible solicitar a través del área privada la rectificación de los Datos Personales facilitados.</li>
                            <li>Rechazar, limitar, oponerse o restringir el uso de datos: es posible solicitar que Takent deje de usar la totalidad o parte de los Datos Personales, salvo que dicho acceso deba efectuarse por motivos legítimos imperiosos o debido al ejercicio o defensa de posibles reclamaciones.</li>
                            <li>Suprimir sus datos: es posible solicitar borrar o eliminar todos o parte de los Datos Personales.</li>
                        </ul>
                    </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Debe tenerse en cuenta que los referidos derechos están sujetos a determinadas limitaciones, tal y como establece la legislación aplicable. Las solicitudes individuales correspondientes al ejercicio de los anteriores derechos terminarán de tratarse dentro del plazo asignado por las regulaciones legales pertinentes, que comienza a partir del momento en que Takent confirme su solicitud. Debes tener en cuenta que Takent podrá cobrar un cargo por solicitudes posteriores que provengan de la misma persona, siempre que la legislación aplicable lo permita.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Para hacer uso de los indicados derechos el Usuario profesional y/o los Usuarios empresa pueden dirigirse a Takent utilizando la información de contacto que aparece en el <strong>apartado 1</strong> de esta política. Takent gestionará la solicitud conforme a la legislación aplicable.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si tras el contacto con Takent no se resuelve la consulta o queja, también es posible contactar con la autoridad de protección de datos y demás organismos públicos competentes para cualquier reclamación derivada del tratamiento de los Datos Personales.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>7. SEGURIDAD</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Takent se preocupa por garantizar la seguridad y confidencialidad de los Datos Personales. Por eso, se han adoptado medidas de seguridad y medios técnicos adecuados para evitar su pérdida, mal uso o su acceso sin autorización de los usuarios. También Takent dispone de procedimientos de actuación ante cualquier sospecha de violación de la seguridad de los Datos Personales que tratamos. Taknet realizará la pertinente notificación al usuario y a la autoridad aplicable en caso de una violación de la seguridad de los Datos Personales, cuando así lo exija la ley. Una vez Takent haya recibido los Datos Personales, Takent utilizará procedimientos estrictos y medidas de seguridad para evitar el acceso no autorizado.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Sin perjuicio de lo anterior, cabe mencionar que lamentablemente, la transmisión de información a través de Internet no es completamente segura. Aunque Takent ha adoptado medidas de seguridad y medios técnicos para proteger los Datos Personales, no puede garantizar la seguridad de los datos transmitidos a través de Internet, por lo tanto, cualquier transmisión queda bajo su propio riesgo.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>8. EDAD MÍNIMA</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Para acceder o utilizar nuestro sistema debe tener 18 años de edad o más. Si tenemos conocimiento de que un menor de edad nos ha proporcionado información, eliminaremos dicha información y suprimiremos la cuenta de referido menor.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>9. ACTUALIZACIONES</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Podemos actualizar esta Política de Privacidad mediante la publicación de una versión actualizada en el Portal Web. Si realizamos alguna modificación que consideremos que pudiera afectar a sus derechos se lo notificaremos por email o mediante un aviso en nuestros sitios web.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Le recomendamos que revise de forma periódica la Política de Privacidad para estar actualizado de todas las novedades. Asimismo, declara que el uso continuado de nuestros servicios, tras publicar o, en su caso, enviar un aviso acerca de nuestros cambios en esta Política de privacidad implica que el tratamiento de sus Datos Personales tendrá lugar conforme a la Política de privacidad actualizada.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si no está de acuerdo con cualquiera de los cambios, goza del pleno derecho a cerrar su cuenta de usuario y solicitar la supresión de los mismos.
            </Typography>

                    <Typography variant="h6" align="left" gutterBottom>10. INFORMACIÓN DE CONTACTO</Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Si desea hacer uso de cualquiera de sus derechos y si tiene alguna pregunta o queja sobre esta Política de Privacidad puede contactar con nosotros en la dirección y/o formulario de contacto indicados en el <strong>apartado 1</strong>.
            </Typography>
                    <Typography variant="subtitle1" align="left" paragraph>
                        Debe facilitarnos la mayor información posible sobre su solicitud: nombre y apellidos, dirección de correo electrónico que utiliza para nuestro sistema y los motivos de su solicitud, especificando, en su caso, el derecho que desea ejercer. Asimismo, será necesario acompañar la documentación necesaria para sustentar la solicitud y dar trámite a la misma.
            </Typography>

                    <Typography variant="subtitle2" align="right" paragraph>Última actualización: 19 de diciembre de 2020.</Typography>
                </Grid>
                <Grid item xs={2}>

                </Grid>
            </Grid>

        </div>
    )
}

export default TermsAndConditions;