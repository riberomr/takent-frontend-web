import { Container, Grid, TextField, ThemeProvider, Typography } from '@material-ui/core'
import React from 'react'
import { theme } from '../../../../../constants/generalConstants/theme'
import * as yup from 'yup'
import { useFormik } from 'formik'
import LoadingButton from '../../../generalUI/LoadingButton'

const validationSchema = yup.object({
  email: yup
    .string("Ingresá tu email")
    .email("Email no válido")
    .required("Email requerido"),
  text: yup
    .string("Ingresá una opinión")
    .max(300, "El texto puede tener hasta 300 caracteres")
    .required("Este campo es obligatorio")
})

const Opinions = ({
  postMethod,
  loading
}) => {

  const formik = useFormik({
    initialValues: {
      email: "",
      text: ""
    },
    validationSchema: validationSchema,
    onSubmit: async values => {
      await postMethod(values)
      formik.resetForm()
    }
  })

  return (
    <ThemeProvider theme={theme}>
      <Typography variant="h4" align="center" style={{marginBottom: theme.spacing(5)}}>
        Dejanos tu opinión para seguir mejorando!
      </Typography>
      <Container maxWidth="sm">
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={5}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="email"
                name="email"
                label="Email"
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="text"
                name="text"
                label="Opinión"
                multiline
                rows={5}
                value={formik.values.text}
                onChange={formik.handleChange}
                error={formik.touched.text && Boolean(formik.errors.text)}
                helperText={formik.touched.text && formik.errors.text}
                inputProps={{
                  maxLength: 300
                }}
              />
            </Grid>
            <Grid item xs={12} style={{display: "flex", justifyContent: "center"}}>
              <LoadingButton
                type="submit"
                loading={loading}
                size="large"
              >
                Enviar
              </LoadingButton>
            </Grid>
          </Grid>
        </form>
      </Container>
    </ThemeProvider>
  )

}

export default Opinions
