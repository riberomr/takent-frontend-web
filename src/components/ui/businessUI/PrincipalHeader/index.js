import React, { useEffect, useRef, useState, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { styles } from './styles'
import { makeStyles } from '@material-ui/core/styles'
import { Avatar, AppBar, Toolbar, Typography, IconButton, Badge, Tooltip } from '@material-ui/core'
import { theme } from '../../../../constants/generalConstants/theme'
import NotificationsIcon from '@material-ui/icons/Notifications'
import MenuIcon from '@material-ui/icons/Menu'
import NotificationsList from '../NotificationsList'
import { apiTakent } from '../../../../services/API-TAKENT'
import { format } from 'date-fns'
import { Store } from '../../../../Store'
import { notificationsHelper } from '../../../../helpers/customHelpers/notifications'

const useStyles = makeStyles(styles)

const PrincipalHeader = (props) => {

    const {userName, profilePhoto, navigatorOpen, setNavigatorOpen} = props

    const classes = useStyles()
    const history = useHistory();
    const context = useContext(Store);

    const anchorRef = useRef(null)
    const [ notifications, setNotifications ] = useState([])
    const [ notificationMenuOpen, setNotificationMenuOpen ] = useState(false)
    const [ notificationsNumber, setNotificationsNumber ] = useState(undefined)

    const getNotifications = () => {
      return apiTakent.getNotifications()
      .then(response => {
        setNotifications(() => response.data.map(notification => {
          return {
            ...notification,
            date: format(new Date(notification.date), "HH:mm 'hs' dd/MM") // "dd/MM/yyyy HH:mm"
          }
        }))    
      })
      .catch(() => {

      })
    }

    const onClickNotification = (notificationType, entity = null, info = null) => {
      const link = notificationsHelper.getNotificationLink(notificationType, info)
      history.push(link, entity)
      setNotificationMenuOpen(false)
    }

    const handleClickNotificationButton = () => {
      getNotifications()
      if(!notificationMenuOpen)
        apiTakent.seeNotifications()
      setNotificationMenuOpen(prevState => !prevState)
    }

    const handleClickAvatar = () => {
      if (context.user.permission) history.push('/app/company/profile');
      else history.push('/app/user/data');
    }

    useEffect(() => {
      const interval = setInterval(() => {
        getNotifications()
      }, 10000)

      return () => clearInterval(interval)
    // eslint-disable-next-line
    }, [])

    useEffect(() => {
      setNotificationsNumber(notifications? notifications.filter(obj => !obj.viewed).length : undefined)
    }, [notifications])

    return (
      <div className={classes.grow}>
        <AppBar position="fixed" style={{background: "#DCDCDC"}} className={classes.appBar} ref={anchorRef}>
          <Toolbar>
            <Tooltip title={(navigatorOpen? "Cerrar" : "Abrir") + " menú"}>
              <IconButton
                onClick={() => setNavigatorOpen(!navigatorOpen)}
                edge="start"
                style={{marginRight: theme.spacing(2)}}
              >
                <MenuIcon />
              </IconButton>
            </Tooltip>
            <img
              alt="menu-logo"
              src="/images/logo-maletin-solo-takent.png"
              className={classes.menuLogo}
            />
            <Typography className={classes.title} variant="h6" noWrap color="textPrimary">
              TAKENT
            </Typography>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <Typography variant="h6" noWrap style={{marginRight: theme.spacing(2)}} color="textPrimary">
                ¡Hola {userName}!
              </Typography>
              <IconButton onClick={handleClickAvatar}>
                <Avatar src={profilePhoto ?? null} style={{marginRight: theme.spacing(2)}}/>
              </IconButton>
              
              <Tooltip title="Notificaciones">
                <IconButton 
                  color="inherit"
                  edge="end"
                  onClick={handleClickNotificationButton}
                >
                  <Badge color="error" badgeContent={notificationsNumber}>
                    <NotificationsIcon style={{color: '#F29222'}}/>
                  </Badge>
                </IconButton>
              </Tooltip>
              <NotificationsList
                open={notificationMenuOpen}
                setOpen={setNotificationMenuOpen}
                anchorEl={anchorRef}
                notifications={notifications}
                onClickNotification={onClickNotification}
              />
            </div>
          </Toolbar>
        </AppBar>
      </div>
      )

}
export default PrincipalHeader