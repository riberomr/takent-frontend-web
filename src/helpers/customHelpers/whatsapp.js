const getWhatsAppLink = (phone, text="") => {
  phone = formatPhoneNumber(phone)
  return `https://wa.me/${phone}?text=${text}`
}

const formatPhoneNumber = phone => {
  phone = phone.toString()
  if(phone.startsWith("549"))
    phone = phone.slice(3)
  else if(phone.startsWith("54"))
    phone = phone.slice(2)
  if(phone.startsWith("15")){
    phone = phone.slice(2)
    phone = `351${phone}`
  }
  phone = `+549${phone}`
  return phone
}

export default getWhatsAppLink
