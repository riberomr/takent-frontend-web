import { differenceInCalendarDays } from 'date-fns'

export const publishedDaysOffer = (date) => {
    const days = differenceInCalendarDays(new Date(), new Date(date))
    if(days === 0) return 'Publicada hoy'
    if(days === 1) return 'Publicada ayer'
    if(days <= 15) return 'Publicada hace '+days+' días'

    return 'Publicada hace más de 15 días'
}

export const publishedDaysService = (date) => {
    const days = differenceInCalendarDays(new Date(), new Date(date))
    if(days === 0) return 'Publicado hoy'
    if(days === 1) return 'Publicado ayer'
    if(days <= 15) return 'Publicado hace '+days+' días'

    return 'Publicado hace más de 15 días'
}