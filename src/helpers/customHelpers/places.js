import { apiTakent } from "../../services/API-TAKENT"

const parseProvince = (province, provinces) => {
  return provinces.find(element => element.name === province)?.code
}

const parseCity = (city, province) => {
  if(!city || !province)
    return ""
  return apiTakent.getCities(province)
  .then(response => {
    const cities = response.data
    const formattedCity = city.toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    return cities.find(element => element.name === formattedCity)?.code || 14014010000
  })
}

const parsePostalCode = postalCode => {
  return postalCode?.substring(1) ?? ""
}

const formatAddress = address => {
  return `${address.province.name} - ${address.city.name} - ${address.district}`
}

export const placesHelper = {
  parseProvince,
  parseCity,
  parsePostalCode,
  formatAddress
}
