const NOTIFICATION_TYPES_LINKS = {
  "requested_service": "/app/user/service/mine",
  "request_assessment": "/app/user/service/requests",
  "suggest_preferred_service": "/app/user/service/buy",
  "few_available_publications": "/app/company/offer/buy",
  "state_postulation_changed": "/app/user/offer/applied",
  "suggest_info_completion": "/app/user/data",
  "offers_nearing_to_finish": "/app/company/offer/list",
  "suggest_register_new_member": "/app/company/members/list",
  "suggest_publish_new_offer": "/app/company/offer/new",
  "near_offers": "/app/user/offer/list",
  "near_services": "/app/user/service/list"
}

const getNotificationLink = (notificationType, queryParams = null) => {
  let formattedParams = null
  if(queryParams?.params) {
    formattedParams = "?" + Object.entries(queryParams.params).map(([key, value]) => `${key}=${value}`).join("&")
  }
  return NOTIFICATION_TYPES_LINKS[notificationType] + (formattedParams ?? "")
}

export const notificationsHelper = {
  getNotificationLink
}
