import React, { useState } from 'react'
import { Button } from '@material-ui/core'
import { apiTakent } from '../../../services/API-TAKENT'
import { useSnackbar } from 'notistack'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import DeleteIcon from '@material-ui/icons/Delete'
import AlertAction from '../../../components/ui/generalUI/AlertAction'

export const DownloadPDF = (props) => {

    const {pdf, loading, classes, setLoading, disabledStatus } = props
    const { enqueueSnackbar } = useSnackbar()
    const DISABLED = (!(pdf && pdf.base64) || loading || disabledStatus)
    const [isAlertOpen, setIsAlertOpen] = useState(false)

    //this function open the alert and receive and set the an id if from the form
    const openAlert = () => { 
        setIsAlertOpen(true)
    }

    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {

        if(type === 'accept') {
            deletePDF()
            setIsAlertOpen(false)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
        }
    }


    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]

    const deletePDF = () => {
        setLoading(true)
        apiTakent.deleteCurriculum()
            .then(()=> {
                setLoading(true)
                enqueueSnackbar('Curriculum eliminado con éxito', { variant: 'success' })
            })
            .catch(()=> enqueueSnackbar('Error al eliminar curriculum', { variant: 'error' }))
            .finally(()=> setLoading(false))
    }

    const downloadPDF = () => {

        const linkSource = 'data:application/pdf;base64, '+pdf.base64
        const downloadLink = document.createElement("a")
        const fileName = pdf.name
    
        downloadLink.href = linkSource
        downloadLink.download = fileName
        downloadLink.click()
    }
    return(<div className={classes.gridItem}>
        <span className={classes.spanNamePdf}>{pdf.name? pdf.name : 'No tienes un curriculum cargado'}</span>
        <Button title='Descargar CV' disabled={!(pdf && pdf.base64)} onClick={downloadPDF}><CloudDownloadIcon/></Button>
        <Button title='Eliminar CV' disabled={DISABLED} onClick={openAlert}><DeleteIcon/></Button>
        {isAlertOpen && <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title={'¿Estás seguro de eliminar el curriculum?'}
                loading={false}
            />}
    </div>)
}