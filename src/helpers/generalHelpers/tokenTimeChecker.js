import jwtdecode from 'jwt-decode'

export const tokenTimeChecker = () => {

    const token = localStorage.getItem('takent_token')
        if (token) {
          const decodedToken = jwtdecode(token)
          const now = new Date()
          if ((decodedToken.exp * 1000) < now.getTime()) return true
          else return false
        }
}