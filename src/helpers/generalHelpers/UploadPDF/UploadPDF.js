import React,{ useEffect, useState }  from 'react' 
import { apiTakent } from '../../../services/API-TAKENT'
import FolderOpenIcon from '@material-ui/icons/FolderOpen'
import { Button } from '@material-ui/core'
import AlertAction from '../../../components/ui/generalUI/AlertAction'
import { useSnackbar } from 'notistack'
import LoadingButton from '../../../components/ui/generalUI/LoadingButton'

const ALLOWED_TYPES = ['pdf']

export const UploadPDF = (props) => { 
  
    const { uploadCurriculum, setUploadCurriculum, loading, setLoading, disabledStatus, cancelChanged}= props
    const [selectedFile, setSelectedFile] = useState(null)
    const [base64, setBase64] = useState('')
    const { enqueueSnackbar } = useSnackbar()

    const [isAlertOpen, setIsAlertOpen] = useState(false)

    useEffect(() => {
      setSelectedFile(null)
    }, [cancelChanged])

    const handleFileSelect = (evt) => {
      var files = evt.target.files
      var file = files[0]

      let [type, extension] = file.type.split('/')

      if (!ALLOWED_TYPES.includes(extension) || type !== 'application' || !file.type || !file) {
        return enqueueSnackbar('Error al cargar curriculum, debe ser formato PDF.', { variant: 'error' })
      }

      if(((file.size/1024)/1024)>5) return enqueueSnackbar('Error al cargar curriculum, el tamaño excede los 5 Mb.', { variant: 'error' })
      if (files && file) {
          var reader = new FileReader()
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result
              setBase64(btoa(binaryString))
          }
  
          reader.readAsBinaryString(file)
          setSelectedFile(file)
      }
    }

    const onFileUpload = () => { 
      
      const values = {
        name: selectedFile.name,
        base64: base64
      }
      setLoading(true)
      apiTakent.postCurriculum(values)
        .then(()=> {
          enqueueSnackbar('Curriculum cargado con éxito', { variant: 'success' })
          setSelectedFile(null)
          setUploadCurriculum(!uploadCurriculum)
        })
        .catch(() => enqueueSnackbar('Error al intentar cargar curriculum', { variant: 'error' }))
        .finally(()=>{setLoading(false)}) 
    } 
    
    //this function open the alert 
    const openAlert = () => {
      setIsAlertOpen(true)
    }
    
    //this function handle the action of the actionButtons by type
    const handleSubmitAction = (type) => {
        if(type === 'accept') {
            onFileUpload()
            setIsAlertOpen(false)
        }
        if(type === 'cancel') {
            setIsAlertOpen(false)
            setSelectedFile(false)
        }
    } 

    //these are the actionButtons, an array of objects with properties
    const actions = [
        {
            name: 'Cancelar',
            type: 'cancel',
            handleSubmit: handleSubmitAction
        },
        {
            name: 'Aceptar',
            type: 'accept',
            handleSubmit: handleSubmitAction
        }  
    ]
    
    return ( 
    <div> 
        <div>
            <input style={{display: 'none'}} id='inputPDF' type="file" onChange={handleFileSelect} />
            <Button disabled={disabledStatus} onClick={() => document.getElementById('inputPDF').click()} title={'Click aquí para adjuntar tu CV en formato pdf'}>
              <FolderOpenIcon/>
            </Button> 
            <LoadingButton
              loading={loading}
              color="inherit"
              variant="text"
              onClick={openAlert}
              disabled={!selectedFile || disabledStatus}
              style={{color: null}}
            >
              Subir
            </LoadingButton>
        </div> 
        <AlertAction
                isOpen={isAlertOpen}
                setIsOpen={setIsAlertOpen}
                actions={actions}
                type='doubt'
                title='¿Desea confirmar la carga de un nuevo curriculum?'
                loading={false}
            />
    </div> 
    ) 
  } 