export const shortText = (number, string, flagShortable = true) => {
    if(!string) return

    if(string.length<=number || !flagShortable ) return string

    return string.slice(0,number-3)+'...'
}