export const createHeader = (id, alignHeader, alignRow, disablePadding, label, sortable, opts = { type: '' }, maxWidth = null) => {
  return { id, alignHeader, alignRow, disablePadding, label, sortable, opts, maxWidth }
}