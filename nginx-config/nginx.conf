user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {

  ##
	# Basic Settings
	##

	# Post body size
	client_max_body_size 15M;
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

  include /etc/nginx/mime.types;
	default_type application/octet-stream;

  ##
	# SSL Settings
	##

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

  ##
	# Logging Settings
	##

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

  ##
	# Gzip Settings
	##

	gzip on;

	# gzip_vary on;
	# gzip_proxied any;
	# gzip_comp_level 6;
	# gzip_buffers 16 8k;
	# gzip_http_version 1.1;
	# gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

  map $http_upgrade $connection_upgrade {
      default upgrade;
      ''      close;
    }
  
  # redirect www -> non-www 

  server {
    server_name www.takent.tk;
    return 301 $scheme://takent.tk$request_uri;
  }
  
  # redirect http -> https

  server {
    listen 80;
    server_name takent.tk;
    return 301 https://takent.tk$request_uri;
  }

  # read the ssl certificate / key when listening on https
  
  server {
    listen 443 ssl;
    server_name takent.tk www.takent.tk;
    root /usr/share/nginx/html;
    index index.html index.htm;
    
    ssl_certificate /etc/nginx/certs/fullchain.pem;
    ssl_certificate_key /etc/nginx/certs/privkey.pem;
    include /etc/nginx/certs/options-ssl-nginx.conf;
    ssl_dhparam /etc/nginx/certs/ssl-dhparams.pem;

    access_log /var/log/nginx/data-access.log combined;

    # serve the static files 
    
    location / {
      try_files $uri /index.html =404;
    }

    # fetch data from proxied server when /api is called
    
    location /api {
      proxy_pass http://backend:5000;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $host;
      proxy_redirect off;
    }

  }

}
